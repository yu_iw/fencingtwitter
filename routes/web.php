<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * ADMIN
 *
 */
Route::group(['prefix' => 'x_wedaelc', 'middleware' => ['restrict_ip' ]], function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

//    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//    Route::post('register', 'Auth\RegisterController@register');
//    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
//    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});

Route::group(['prefix' => 'x_wedaelc', 'middleware' => ['auth:admin','restrict_ip' ]], function () {
    Route::get('index', 'AdminController@index');
    Route::get('entryList', 'AdminController@entryList');
    Route::get('entryListTw', 'AdminController@entryListTw');
    Route::get('download', 'AdminController@download');
    Route::get('excel', 'AdminController@excel');
    Route::get('excel_twitter', 'AdminController@excelTwitter');
    Route::get('excel_winners', 'AdminController@excelWinners');
    Route::get('excel_notinput', 'AdminController@excelNotInput');
    Route::post('updateEntry', 'AdminController@updateEntry');
    Route::post('checkDm', 'AdminController@checkDm');
    Route::get('checkDm', 'AdminController@checkDm');

    Route::get('copyDm', 'AdminController@copyDm');
});
/**
 * ENTRY
 *
 */
Route::get('/', 'HomeController@index');
Route::get('error', 'HomeController@error');
Route::get('error_term', 'HomeController@error_term');
Route::get('index', 'HomeController@index');
Route::get('entry', 'HomeController@entry');
Route::post('conf', 'HomeController@conf');
Route::post('store', 'HomeController@store');
Route::get('comp', 'HomeController@comp');

/*　ENTRY　*/
Route::get('error', 'HomeController@error');
/*　 WINNER ENTRY　*/
Route::get('input_entry', 'HomeController@winnerEntry');
Route::post('input_entry', 'HomeController@winnerEntry');
Route::get('input_conf', 'HomeController@winnerConf');
Route::post('input_conf', 'HomeController@winnerConf');
Route::post('input_store', 'HomeController@winnerStore');
Route::get('input_comp', 'HomeController@winnerComp');
/**
 * Twitter
 *
 */
Route::get('/tweet', 'SnsController@tweetList');
Route::get('/hashtag', 'SnsController@searchHashtag');
Route::get('/follower', 'SnsController@checkFollower');
Route::get('/month', 'SnsController@searchMonth');
Route::get('/api', 'SnsController@getApi');
