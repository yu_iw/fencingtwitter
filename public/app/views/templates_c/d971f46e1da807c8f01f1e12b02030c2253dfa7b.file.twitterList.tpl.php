<?php /* Smarty version Smarty-3.1.13, created on 2019-06-10 18:09:06
         compiled from "/home/fencing-twitter.grpht.co.jp/public_html/app/views/templates/xx_nimda/twitterList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16257248975cfdfbd431f463-67423340%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd971f46e1da807c8f01f1e12b02030c2253dfa7b' => 
    array (
      0 => '/home/fencing-twitter.grpht.co.jp/public_html/app/views/templates/xx_nimda/twitterList.tpl',
      1 => 1560157745,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16257248975cfdfbd431f463-67423340',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5cfdfbd44fa878_75237952',
  'variables' => 
  array (
    'viewSum' => 0,
    'entrySum' => 0,
    'v' => 0,
    'entry' => 0,
    '_' => 0,
    'n' => 0,
    'p' => 0,
    'table' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5cfdfbd44fa878_75237952')) {function content_5cfdfbd44fa878_75237952($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/fencing-twitter.grpht.co.jp/public_html/lib/mvc/Smarty/plugins/modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ("xx_nimda/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<h3>ツイッター投稿</h3>
<link rel="stylesheet" href="../../app/views/system/css/jquery.toast.css" media="all">
<script src="../../app/views/system/js/store.js"></script>
<script src="../../app/views/system/js/jquery.toast.js"></script>
<br>
<div class="entryOverview">
	表示投稿数：<?php echo $_smarty_tpl->tpl_vars['viewSum']->value;?>
　
	全投稿数：<?php echo $_smarty_tpl->tpl_vars['entrySum']->value;?>

</div>
<br>
<div class="btn-group" role="group" aria-label="...">
	<a href="../twitterList/" class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>btn-primary<?php }?>">非公開投稿</a>
	<a href="../twitterList/?v=1" class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['v']->value==1){?>btn-primary<?php }?>">公開済み投稿</a>
</div>
<br>
<br>
<?php  $_smarty_tpl->tpl_vars['_'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['_']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['entry']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['_']->key => $_smarty_tpl->tpl_vars['_']->value){
$_smarty_tpl->tpl_vars['_']->_loop = true;
?><?php if (mb_strpos($_smarty_tpl->tpl_vars['_']->value['body'],'RT ')!==0){?>
<div class="storeBlock col-md-12" id="entry<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
">
	<p class="col-md-12 <?php if ($_smarty_tpl->tpl_vars['_']->value['entry_num']>1){?>multiUser<?php }?>">
    	<span class="dataId"><?php if ($_smarty_tpl->tpl_vars['_']->value['table']==1){?><?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
<?php }?></span>
    	<span class="dateSpan"><?php if ($_smarty_tpl->tpl_vars['_']->value['table']==1){?><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['_']->value['created_at'],"%y/%m/%d %H:%M");?>
<?php }else{ ?><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['_']->value['posted_at'],"%y/%m/%d %H:%M");?>
<?php }?></span>
        <?php if ($_smarty_tpl->tpl_vars['_']->value['user_id']){?>@<?php echo $_smarty_tpl->tpl_vars['_']->value['user_id'];?>
<?php }else{ ?> <?php if ($_smarty_tpl->tpl_vars['_']->value['ng_name']==1){?><span class="ng-word"><?php }?><?php echo $_smarty_tpl->tpl_vars['_']->value['name'];?>
<?php if ($_smarty_tpl->tpl_vars['_']->value['ng_name']==1){?></span><?php }?><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['_']->value['table']==1){?>
        <!-- $_.category -->
        <span class="postCat">公式HP(<?php if ($_smarty_tpl->tpl_vars['_']->value['category']==1){?>通常投稿<?php }elseif($_smarty_tpl->tpl_vars['_']->value['category']==2){?>Facebook<?php }elseif($_smarty_tpl->tpl_vars['_']->value['category']==3){?>Twitter<?php }elseif($_smarty_tpl->tpl_vars['_']->value['category']==4){?>Instagram<?php }elseif($_smarty_tpl->tpl_vars['_']->value['category']==5){?>飲食店<?php }?>)
		(<?php if ($_smarty_tpl->tpl_vars['_']->value['lang']==0){?>日本語<?php }elseif($_smarty_tpl->tpl_vars['_']->value['lang']==1){?>英語<?php }?>)</span>
        <?php }else{ ?>
         <span class="postCatSns"><?php if ($_smarty_tpl->tpl_vars['_']->value['category']==1){?>Twitter<?php }elseif($_smarty_tpl->tpl_vars['_']->value['category']==2){?>Instagram<?php }?>
        <?php }?></span>
        <span class="storeShow">▼</span><span class="storeHide">▲</span>
    </p>
	<div class="col-md-12 storeDetail" style="display:block;" >
		<dl class="col-md-12">
			<?php if ($_smarty_tpl->tpl_vars['_']->value['nation']){?>
			<dt class="col-md-3">国</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['n']->value[$_smarty_tpl->tpl_vars['_']->value['nation']];?>
</dd>
			<?php }?>
			<dt class="col-md-3"><?php if ($_smarty_tpl->tpl_vars['_']->value['category']==5){?>飲食店名<?php }else{ ?>ユーザー名<?php }?></dt>
			<dd class="col-md-9 <?php if ($_smarty_tpl->tpl_vars['_']->value['ng_name']==1){?>ng-word<?php }?>"><?php echo $_smarty_tpl->tpl_vars['_']->value['name'];?>
</dd>
			<?php if ($_smarty_tpl->tpl_vars['_']->value['prefecture_id']){?>
			<dt class="col-md-3">都道府県</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['p']->value[$_smarty_tpl->tpl_vars['_']->value['prefecture_id']];?>
</dd>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['_']->value['email']){?>
			<dt class="col-md-3">メールアドレス</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['email'];?>
</dd>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['_']->value['tel']){?>
			<dt class="col-md-3">電話番号</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['tel'];?>
</dd>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['_']->value['category']==5){?>
			<dt class="col-md-3">飲食店ID</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['store_id'];?>
</dd>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['_']->value['pic_title']){?>
			<dt class="col-md-3">タイトル</dt>
			<dd class="col-md-9 <?php if ($_smarty_tpl->tpl_vars['_']->value['ng_pic_title']==1){?>ng-word<?php }?>"><?php echo $_smarty_tpl->tpl_vars['_']->value['pic_title'];?>
</dd>
			<?php }?>
			<dt class="col-md-3">本文</dt>
			<dd class="col-md-9 <?php if ($_smarty_tpl->tpl_vars['_']->value['ng_pic_comment']==1||$_smarty_tpl->tpl_vars['_']->value['ng_body']==1){?>ng-word<?php }?>"><?php echo $_smarty_tpl->tpl_vars['_']->value['pic_comment'];?>
<?php echo $_smarty_tpl->tpl_vars['_']->value['body'];?>
</dd>
			<!--<dt class="col-md-3">NG</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['ng_name'];?>
<?php echo $_smarty_tpl->tpl_vars['_']->value['ng_pic_title'];?>
<?php echo $_smarty_tpl->tpl_vars['_']->value['ng_pic_comment'];?>
<?php echo $_smarty_tpl->tpl_vars['_']->value['ng_body'];?>
</dd>-->
			<dt class="col-md-3">投稿日時</dt>
			<dd class="col-md-9"><?php if ($_smarty_tpl->tpl_vars['_']->value['table']==1){?><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['_']->value['created_at'],"%m/%d %H:%M");?>
<?php }else{ ?><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['_']->value['posted_at'],"%m/%d %H:%M");?>
<?php }?></dd>
		</dl>
		<div class="col-md-12 center-block text-center">
			<button class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>btn-primary<?php }else{ ?>btn-danger<?php }?> col-md-3 no-float" onclick="approveEntry(<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
, <?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>1<?php }else{ ?>0<?php }?>, '<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
','<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
');"><?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>公開<?php }else{ ?>非公開<?php }?></button>
			<button class="btn btn-default btn-secondary col-md-3 no-float" onclick="deleteEntry(<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
, true, '<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
','<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
');">削除</button>
		</div>
	</div>
</div>
<?php }?><?php }
if (!$_smarty_tpl->tpl_vars['_']->_loop) {
?>
<div class="storeBlock">
	<p>
		<?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>未承認投稿がありません
		<?php }elseif($_smarty_tpl->tpl_vars['v']->value==1){?>承認済投稿がありません
		<?php }elseif($_smarty_tpl->tpl_vars['v']->value==2){?>非承認投稿がありません
		<?php }elseif($_smarty_tpl->tpl_vars['v']->value==3){?>保留投稿がありません
		<?php }?>
	</p>
</div>
<?php } ?>
<?php echo $_smarty_tpl->getSubTemplate ("xx_nimda/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>