<?php /* Smarty version Smarty-3.1.13, created on 2019-06-10 15:42:29
         compiled from "/home/fencing-twitter.grpht.co.jp/public_html/app/views/templates/xx_nimda/regionList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7680441005cfdfbd5866267-64628931%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '43cecb67af9538f73b0af69e70ec449482bcda51' => 
    array (
      0 => '/home/fencing-twitter.grpht.co.jp/public_html/app/views/templates/xx_nimda/regionList.tpl',
      1 => 1560148341,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7680441005cfdfbd5866267-64628931',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'viewSum' => 0,
    'entrySum' => 0,
    'v' => 0,
    'entry' => 0,
    '_' => 0,
    'table' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5cfdfbd5a02084_63103402',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5cfdfbd5a02084_63103402')) {function content_5cfdfbd5a02084_63103402($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/fencing-twitter.grpht.co.jp/public_html/lib/mvc/Smarty/plugins/modifier.date_format.php';
?>
<?php echo $_smarty_tpl->getSubTemplate ("xx_nimda/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<h3>ご当地投稿</h3>
<link rel="stylesheet" href="../../app/views/system/css/jquery.toast.css" media="all">
<script src="../../app/views/system/js/store.js"></script>
<script src="../../app/views/system/js/jquery.toast.js"></script>
<br>
<div class="entryOverview">
	表示投稿数：<?php echo $_smarty_tpl->tpl_vars['viewSum']->value;?>
　
	全投稿数：<?php echo $_smarty_tpl->tpl_vars['entrySum']->value;?>

</div>
<br>
<div class="btn-group" role="group" aria-label="...">
	<a href="../regionList/" class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>btn-primary<?php }?>">非公開投稿</a>
	<a href="../regionList/?v=1" class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['v']->value==1){?>btn-primary<?php }?>">公開済み投稿</a>
</div>
<br>
<br>
<?php  $_smarty_tpl->tpl_vars['_'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['_']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['entry']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['_']->key => $_smarty_tpl->tpl_vars['_']->value){
$_smarty_tpl->tpl_vars['_']->_loop = true;
?>
<div class="storeBlock col-md-12" id="entry<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
">
	<p class="col-md-12">
    	<span class="dataId"><?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
</span>
    	<span class="dateSpan"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['_']->value['created_at'],"%y/%m/%d %H:%M");?>
</span>
        <?php echo $_smarty_tpl->tpl_vars['_']->value['nm'];?>

        <span class="storeShow">▼</span><span class="storeHide">▲</span>
    </p>
	<div class="col-md-12 storeDetail" >
		<dl class="col-md-12">
			<dt class="col-md-3">自治体名</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['nm'];?>
</dd>
			<dt class="col-md-3">担当者お名前</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['staff_nm'];?>
</dd>
			<dt class="col-md-3">担当者所属</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['staff_belongs'];?>
</dd>
			<dt class="col-md-3">担当者ご連絡先</dt>
			<dd class="col-md-9">
						<?php echo $_smarty_tpl->tpl_vars['_']->value['tel'];?>
<br>
						<?php echo $_smarty_tpl->tpl_vars['_']->value['email'];?>

			</dd>
			<dt class="col-md-3">概要</dt>
			<dd class="col-md-9"><?php echo nl2br(htmlspecialchars($_smarty_tpl->tpl_vars['_']->value['sumally'], ENT_QUOTES, 'UTF-8', true));?>
</dd>
			<dt class="col-md-9"><h4>1ブロック目</h4></dt>
			<dt class="col-md-3">タイトル</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['title1'];?>
</dd>
			<dt class="col-md-3">本文</dt>
			<dd class="col-md-9"><?php echo nl2br(htmlspecialchars($_smarty_tpl->tpl_vars['_']->value['body1'], ENT_QUOTES, 'UTF-8', true));?>
</dd>
			<dt class="col-md-3">写真</dt>
			<dd class="col-md-9">
						  <?php if ($_smarty_tpl->tpl_vars['_']->value['img1']||$_smarty_tpl->tpl_vars['_']->value['img2']||$_smarty_tpl->tpl_vars['_']->value['img3']||$_smarty_tpl->tpl_vars['_']->value['img4']){?>
							<?php if ($_smarty_tpl->tpl_vars['_']->value['img1']){?><img src="/data/region/<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['_']->value['img1'];?>
"><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['_']->value['img2']){?><img src="/data/region/<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['_']->value['img2'];?>
"><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['_']->value['img3']){?><img src="/data/region/<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['_']->value['img3'];?>
"><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['_']->value['img4']){?><img src="/data/region/<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['_']->value['img4'];?>
"><?php }?>
						  <?php }else{ ?><img src="/img/form/noimage.png">
						  <?php }?>
			</dd>
			<dt class="col-md-3">参加者数</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['entry_num1'];?>
</dd>
			<dt class="col-md-3">参加者の声</dt>
			<dd class="col-md-9"><?php echo nl2br(htmlspecialchars($_smarty_tpl->tpl_vars['_']->value['entry_voice1'], ENT_QUOTES, 'UTF-8', true));?>
</dd>
			<dt class="col-md-9"><h4>2ブロック目</h4></dt>
			<dt class="col-md-3">タイトル</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['title2'];?>
</dd>
			<dt class="col-md-3">本文</dt>
			<dd class="col-md-9"><?php echo nl2br(htmlspecialchars($_smarty_tpl->tpl_vars['_']->value['body2'], ENT_QUOTES, 'UTF-8', true));?>
</dd>
			<dt class="col-md-3">写真</dt>
			<dd class="col-md-9">
						  <?php if ($_smarty_tpl->tpl_vars['_']->value['img5']||$_smarty_tpl->tpl_vars['_']->value['img6']||$_smarty_tpl->tpl_vars['_']->value['img7']||$_smarty_tpl->tpl_vars['_']->value['img8']){?>
							<?php if ($_smarty_tpl->tpl_vars['_']->value['img5']){?><img src="/data/region/<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['_']->value['img5'];?>
"><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['_']->value['img6']){?><img src="/data/region/<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['_']->value['img6'];?>
"><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['_']->value['img7']){?><img src="/data/region/<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['_']->value['img7'];?>
"><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['_']->value['img8']){?><img src="/data/region/<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['_']->value['img8'];?>
"><?php }?>
						  <?php }else{ ?><img src="/img/form/noimage.png">
						  <?php }?>
			</dd>
			<dt class="col-md-3">参加者数</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['entry_num2'];?>
</dd>
			<dt class="col-md-3">参加者の声</dt>
			<dd class="col-md-9"><?php echo nl2br(htmlspecialchars($_smarty_tpl->tpl_vars['_']->value['entry_voice2'], ENT_QUOTES, 'UTF-8', true));?>
</dd>
			<dt class="col-md-9"><h4>取組に関するお問合せ先等</h4></dt>
			<dt class="col-md-3">担当</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['belongs'];?>
</dd>
			<dt class="col-md-3">公開用連絡先</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['belongs_contact_info'];?>
</dd>
			<dt class="col-md-3">URL</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['url'];?>
</dd>
			<dt class="col-md-3">備考</dt>
			<dd class="col-md-9"><?php echo nl2br(htmlspecialchars($_smarty_tpl->tpl_vars['_']->value['remarks'], ENT_QUOTES, 'UTF-8', true));?>
</dd>
		</dl>
		<div class="col-md-12 center-block text-center">
			<button class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>btn-primary<?php }else{ ?>btn-danger<?php }?> col-md-3 no-float" onclick="approveEntry(<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
, <?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>1<?php }else{ ?>0<?php }?>, '<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
','<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
');"><?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>公開<?php }else{ ?>非公開<?php }?></button>
			<button class="btn btn-default btn-secondary col-md-3 no-float" onclick="deleteEntry(<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
, true, '<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
','<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
');">削除</button>
		</div>
	</div>
</div>
<?php }
if (!$_smarty_tpl->tpl_vars['_']->_loop) {
?>
<div class="storeBlock">
	<p>
		<?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>未承認投稿はありません
		<?php }elseif($_smarty_tpl->tpl_vars['v']->value==1){?>承認済投稿はありません
		<?php }elseif($_smarty_tpl->tpl_vars['v']->value==2){?>非承認投稿はありません
		<?php }elseif($_smarty_tpl->tpl_vars['v']->value==3){?>保留投稿はありません
		<?php }?>
	</p>
</div>
<?php } ?>
<?php echo $_smarty_tpl->getSubTemplate ("xx_nimda/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>