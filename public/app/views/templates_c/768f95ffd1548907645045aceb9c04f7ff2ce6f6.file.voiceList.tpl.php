<?php /* Smarty version Smarty-3.1.13, created on 2019-06-10 16:00:00
         compiled from "/home/fencing-twitter.grpht.co.jp/public_html/app/views/templates/xx_nimda/voiceList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7407057705cfdfff0ac7339-46339457%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '768f95ffd1548907645045aceb9c04f7ff2ce6f6' => 
    array (
      0 => '/home/fencing-twitter.grpht.co.jp/public_html/app/views/templates/xx_nimda/voiceList.tpl',
      1 => 1560148341,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7407057705cfdfff0ac7339-46339457',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'viewSum' => 0,
    'entrySum' => 0,
    'v' => 0,
    'entry' => 0,
    '_' => 0,
    'table' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5cfdfff0cb9322_84652565',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5cfdfff0cb9322_84652565')) {function content_5cfdfff0cb9322_84652565($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/fencing-twitter.grpht.co.jp/public_html/lib/mvc/Smarty/plugins/modifier.date_format.php';
?>
<?php echo $_smarty_tpl->getSubTemplate ("xx_nimda/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<h3>子育て体験談</h3>
<link rel="stylesheet" href="../../app/views/system/css/jquery.toast.css" media="all">
<script src="../../app/views/system/js/store.js"></script>
<script src="../../app/views/system/js/jquery.toast.js"></script>
<br>
<div class="entryOverview">
	表示投稿数：<?php echo $_smarty_tpl->tpl_vars['viewSum']->value;?>
　
	全投稿数：<?php echo $_smarty_tpl->tpl_vars['entrySum']->value;?>

</div>
<br>
<div class="btn-group" role="group" aria-label="...">
	<a href="../voiceList/" class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>btn-primary<?php }?>">非公開投稿</a>
	<a href="../voiceList/?v=1" class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['v']->value==1){?>btn-primary<?php }?>">公開済み投稿</a>
</div>
<br>
<br>
<?php  $_smarty_tpl->tpl_vars['_'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['_']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['entry']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['_']->key => $_smarty_tpl->tpl_vars['_']->value){
$_smarty_tpl->tpl_vars['_']->_loop = true;
?>
<div class="storeBlock col-md-12" id="entry<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
">
	<p class="col-md-12">
    	<span class="dataId"><?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
</span>
    	<span class="dateSpan"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['_']->value['created_at'],"%y/%m/%d %H:%M");?>
</span>
        <?php echo $_smarty_tpl->tpl_vars['_']->value['nm'];?>

        <span class="storeShow">▼</span><span class="storeHide">▲</span>
    </p>
	<div class="col-md-12 storeDetail" >
		<dl class="col-md-12">
			<dt class="col-md-3">名前</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['nm'];?>
</dd>
			<dt class="col-md-3">メールアドレス</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['email'];?>
</dd>
			<dt class="col-md-3">体験談</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['voice'];?>
</dd>
			<dt class="col-md-3">体験談</dt>
			<dd class="col-md-9"><img src="<?php if ($_smarty_tpl->tpl_vars['_']->value['img']!=''){?>/img/ikumen_experiences/<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['_']->value['img'];?>
<?php }else{ ?>/img/form/noimage.png<?php }?>" /></dd>
			<dt class="col-md-3">年齢</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['age'];?>
代</dd>
			<dt class="col-md-3">男の子人数</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['m_children_num'];?>
人</dd>
			<dt class="col-md-3">女の子人数</dt>
			<dd class="col-md-9"><?php echo $_smarty_tpl->tpl_vars['_']->value['f_children_num'];?>
人</dd>
			<dt class="col-md-3">メルマガ配信</dt>
			<dd class="col-md-9"><?php if ($_smarty_tpl->tpl_vars['_']->value['accept_mail_magazine']==1){?>あり<?php }else{ ?>なし<?php }?></dd>
			<dt class="col-md-3">更新日時</dt>
			<dd class="col-md-9"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['_']->value['modified_at'],"%m/%d %H:%M");?>
</dd>
			<dt class="col-md-3">投稿日時</dt>
			<dd class="col-md-9"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['_']->value['created_at'],"%m/%d %H:%M");?>
</dd>
		</dl>
		<div class="col-md-12 center-block text-center">
			<button class="btn btn-default <?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>btn-primary<?php }else{ ?>btn-danger<?php }?> col-md-3 no-float" onclick="approveEntry(<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
, <?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>1<?php }else{ ?>0<?php }?>, '<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
','<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
');"><?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>公開<?php }else{ ?>非公開<?php }?></button>
			<button class="btn btn-default btn-secondary col-md-3 no-float" onclick="deleteEntry(<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
, true, '<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
','<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
');">削除</button>
		</div>
	</div>
</div>
<?php }
if (!$_smarty_tpl->tpl_vars['_']->_loop) {
?>
<div class="storeBlock">
	<p>
		<?php if ($_smarty_tpl->tpl_vars['v']->value==0){?>未承認投稿はありません
		<?php }elseif($_smarty_tpl->tpl_vars['v']->value==1){?>承認済投稿はありません
		<?php }elseif($_smarty_tpl->tpl_vars['v']->value==2){?>非承認投稿はありません
		<?php }elseif($_smarty_tpl->tpl_vars['v']->value==3){?>保留投稿はありません
		<?php }?>
	</p>
</div>
<?php } ?>
<?php echo $_smarty_tpl->getSubTemplate ("xx_nimda/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>