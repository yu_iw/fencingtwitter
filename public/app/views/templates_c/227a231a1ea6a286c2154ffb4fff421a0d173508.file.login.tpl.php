<?php /* Smarty version Smarty-3.1.13, created on 2019-06-11 15:25:24
         compiled from "/home/fencing-twitter.grpht.co.jp/public_html/app/views/templates/x_nimda/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11890307905cfdfae1725b15-69865514%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '227a231a1ea6a286c2154ffb4fff421a0d173508' => 
    array (
      0 => '/home/fencing-twitter.grpht.co.jp/public_html/app/views/templates/x_nimda/login.tpl',
      1 => 1560234312,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11890307905cfdfae1725b15-69865514',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5cfdfae17580c2_31500067',
  'variables' => 
  array (
    'account_error' => 0,
    'account' => 0,
    'passwd_error' => 0,
    'passwd' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5cfdfae17580c2_31500067')) {function content_5cfdfae17580c2_31500067($_smarty_tpl) {?><!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>システム管理画面</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="robots" content="noindex,nofollow" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/setting.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/common.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/form.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/sys.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/bootstrap-switch.min.css" media="all">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="../../app/views/system/js/bootstrap-switch.min.js"></script>
</head>
<body>
<header>
	<div id="headerInner">
		<h1>Twitter投稿</h1>
	<!--headerInner--></div>
</header>
<div id="container">
	<article>
<!-- :::::::::::::::::::: Contents Area Start :::::::::::::::::::: -->
<div id="login">


<form method="post" name="form" id="form" action="../auth/">
<table id="sys_menu">
<tr>
<th>ID</th>
<td><?php if ($_smarty_tpl->tpl_vars['account_error']->value!=''){?><p class="error-style"><?php echo $_smarty_tpl->tpl_vars['account_error']->value;?>
</p><?php }?>
<input type="text" name="account" id="form_account" value="<?php echo $_smarty_tpl->tpl_vars['account']->value;?>
" size="20" class="form-control textbox1<?php if ($_smarty_tpl->tpl_vars['account_error']->value!=''){?> error<?php }?>"/></td>
</tr>
<tr>
<th>パスワード</th>
<td><?php if ($_smarty_tpl->tpl_vars['passwd_error']->value!=''){?><p class="error-style"><?php echo $_smarty_tpl->tpl_vars['passwd_error']->value;?>
</p><?php }?>
<input type="password" name="passwd" id="form_passwd" value="<?php echo $_smarty_tpl->tpl_vars['passwd']->value;?>
" size="20" class="form-control textbox1<?php if ($_smarty_tpl->tpl_vars['passwd_error']->value!=''){?> error<?php }?>"/></td>
</tr>
</table>
<div id="submit">
<input type="submit" name="next" id="form_next" class="btn btn btn-default" value="ログイン"/>
</div>
</form>


</div>
<!-- :::::::::::::::::::: Contents Area End :::::::::::::::::::: -->
</article>
<!--container--></div>
<footer>COPYRIGHT GRAPHITE INC. All RIGHTS RESERVED.</footer>
</body>
</html><?php }} ?>