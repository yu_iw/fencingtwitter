<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>システム管理画面</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="robots" content="noindex,nofollow" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/setting.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/common.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/form.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/sys.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/bootstrap-switch.min.css" media="all">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="../../app/views/system/js/bootstrap-switch.min.js"></script>
</head>
<body>
<header>
	<div id="headerInner">
		<h1>Twitter投稿</h1>
	<!--headerInner--></div>
</header>
<div id="container">
	<article>
<!-- :::::::::::::::::::: Contents Area Start :::::::::::::::::::: -->
<div id="login">


<form method="post" name="form" id="form" action="../auth/">
<table id="sys_menu">
<tr>
<th>ID</th>
<td>{if $account_error != ""}<p class="error-style">{$account_error}</p>{/if}
<input type="text" name="account" id="form_account" value="{$account}" size="20" class="form-control textbox1{if $account_error != ""} error{/if}"/></td>
</tr>
<tr>
<th>パスワード</th>
<td>{if $passwd_error != ""}<p class="error-style">{$passwd_error}</p>{/if}
<input type="password" name="passwd" id="form_passwd" value="{$passwd}" size="20" class="form-control textbox1{if $passwd_error != ""} error{/if}"/></td>
</tr>
</table>
<div id="submit">
<input type="submit" name="next" id="form_next" class="btn btn btn-default" value="ログイン"/>
</div>
</form>


</div>
<!-- :::::::::::::::::::: Contents Area End :::::::::::::::::::: -->
</article>
<!--container--></div>
<footer>COPYRIGHT GRAPHITE INC. All RIGHTS RESERVED.</footer>
</body>
</html>