<meta property="og:title" content="育てる男が、家族を変える。社会が動く。イクメンプロジェクト" />
<meta property="og:site_name" content="イクメンプロジェクト" />
<meta property="og:description" content="イクメンとは、子育てを楽しみ、自分自身も成長する男性のこと。または、将来そんな人生を送ろうと考えている男性のこと。イクメンがもっと多くなれば、妻である女性の生き方が、子どもたちの可能性が、家族のあり方が大きく変わっていくはず。そして社会全体も、もっと豊かに成長していくはずです。" />
<meta property="og:type" content="website" />
<meta property="og:url" content="http://ikumen-project.mhlw.go.jp/" />
<meta property="og:image" content="http://ikumen-project.mhlw.go.jp/img/common/fb_img.jpg" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@ikumen_project" />
