<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>システム管理画面</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="robots" content="noindex,nofollow" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/setting.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/common.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/form.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/sys.css" media="all">
<link rel="stylesheet" href="../../app/views/system/css/bootstrap-switch.min.css" media="all">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="../../app/views/system/js/bootstrap-switch.min.js"></script>
</head>
<body>
<header>
	<div id="headerInner">
		<ul id="hd_navi" class="nav nav-tabs">
		<li><a href="../twitterList/">ツイッター</a></li>
		<li><a href="../logout/">ログアウト</a></li>
		</ul>
	<!--headerInner--></div>
</header>
<div id="container">
	<article>