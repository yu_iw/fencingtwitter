{include file="xx_nimda/header.tpl"}
<h3>ツイッター投稿</h3>
<link rel="stylesheet" href="../../app/views/system/css/jquery.toast.css" media="all">
<script src="../../app/views/system/js/store.js"></script>
<script src="../../app/views/system/js/jquery.toast.js"></script>
<br>
<div class="entryOverview">
	表示投稿数：{$viewSum}　
	全投稿数：{$entrySum}
</div>
<br>
<div class="btn-group" role="group" aria-label="...">
	<a href="../twitterList/" class="btn btn-default {if $v == 0 }btn-primary{/if}">非公開投稿</a>
	<a href="../twitterList/?v=1" class="btn btn-default {if $v == 1}btn-primary{/if}">公開済み投稿</a>
</div>
<br>
<br>
{foreach from=$entry item=_}{if $_.body|mb_strpos:'RT ' !== 0}
<div class="storeBlock col-md-12" id="entry{$_.id}">
	<p class="col-md-12 {if $_.entry_num > 1}multiUser{/if}">
    	<span class="dataId">{if $_.table == 1}{$_.id}{else}{$_.id}{/if}</span>
    	<span class="dateSpan">{if $_.table == 1}{$_.created_at|date_format:"%y/%m/%d %H:%M"}{else}{$_.posted_at|date_format:"%y/%m/%d %H:%M"}{/if}</span>
        {if $_.user_id}@{$_.user_id}{else} {if $_.ng_name == 1}<span class="ng-word">{/if}{$_.name}{if $_.ng_name == 1}</span>{/if}{/if}
        {if $_.table == 1}
        <!-- $_.category -->
        <span class="postCat">公式HP({if $_.category == 1}通常投稿{else if $_.category == 2}Facebook{else if $_.category == 3}Twitter{else if $_.category == 4}Instagram{else if $_.category == 5}飲食店{/if})
		({if $_.lang == 0}日本語{else if $_.lang== 1}英語{/if})</span>
        {else}
         <span class="postCatSns">{if $_.category == 1}Twitter{else if $_.category == 2}Instagram{/if}
        {/if}</span>
        <span class="storeShow">▼</span><span class="storeHide">▲</span>
    </p>
	<div class="col-md-12 storeDetail" style="display:block;" >
		<dl class="col-md-12">
			{if $_.nation}
			<dt class="col-md-3">国</dt>
			<dd class="col-md-9">{$n[$_.nation]}</dd>
			{/if}
			<dt class="col-md-3">{if $_.category == 5}飲食店名{else}ユーザー名{/if}</dt>
			<dd class="col-md-9 {if $_.ng_name == 1}ng-word{/if}">{$_.name}</dd>
			{if $_.prefecture_id}
			<dt class="col-md-3">都道府県</dt>
			<dd class="col-md-9">{$p[$_.prefecture_id]}</dd>
			{/if}
			{if $_.email}
			<dt class="col-md-3">メールアドレス</dt>
			<dd class="col-md-9">{$_.email}</dd>
			{/if}
			{if $_.tel}
			<dt class="col-md-3">電話番号</dt>
			<dd class="col-md-9">{$_.tel}</dd>
			{/if}
			{if $_.category == 5}
			<dt class="col-md-3">飲食店ID</dt>
			<dd class="col-md-9">{$_.store_id}</dd>
			{/if}
			{if $_.pic_title}
			<dt class="col-md-3">タイトル</dt>
			<dd class="col-md-9 {if $_.ng_pic_title == 1}ng-word{/if}">{$_.pic_title}</dd>
			{/if}
			<dt class="col-md-3">本文</dt>
			<dd class="col-md-9 {if $_.ng_pic_comment == 1 || $_.ng_body == 1}ng-word{/if}">{$_.pic_comment}{$_.body}</dd>
			<!--<dt class="col-md-3">NG</dt>
			<dd class="col-md-9">{$_.ng_name}{$_.ng_pic_title}{$_.ng_pic_comment}{$_.ng_body}</dd>-->
			<dt class="col-md-3">投稿日時</dt>
			<dd class="col-md-9">{if $_.table == 1}{$_.created_at|date_format:"%m/%d %H:%M"}{else}{$_.posted_at|date_format:"%m/%d %H:%M"}{/if}</dd>
		</dl>
		<div class="col-md-12 center-block text-center">
			<button class="btn btn-default {if $v == 0}btn-primary{else}btn-danger{/if} col-md-3 no-float" onclick="approveEntry({$_.id}, {if $v == 0}1{else}0{/if}, '{$_.id}','{$table}');">{if $v == 0}公開{else}非公開{/if}</button>
			<button class="btn btn-default btn-secondary col-md-3 no-float" onclick="deleteEntry({$_.id}, true, '{$_.id}','{$table}');">削除</button>
		</div>
	</div>
</div>
{/if}{foreachelse}
<div class="storeBlock">
	<p>
		{if $v == 0}未承認投稿がありません
		{elseif $v == 1}承認済投稿がありません
		{elseif $v == 2}非承認投稿がありません
		{elseif $v == 3}保留投稿がありません
		{/if}
	</p>
</div>
{/foreach}
{include file="xx_nimda/footer.tpl"}