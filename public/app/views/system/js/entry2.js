$(window).load(function () {

	var rWidth = 710,
	rHeight = 480;
	if($('.template2').length){
		rHeight = 440;
	}
	
	$('#crop-img').cropimg({
		// resultWidth: 633,
		// resultHeight: 389,
		resultWidth: rWidth,
		resultHeight: rHeight,
		textBtnTipRTW: '写真の横幅を合わせる',
		textBtnTipRTH: '写真の縦幅を合わせる',
		textBtnTipFPTL: '左上角に合わせる',
		textBtnTipFPTC: '中央上に合わせる',
		textBtnTipFPTR: '右上角に合わせる',
		textBtnTipFPCL: '左中段に合わせる',
		textBtnTipFPCC: '真ん中に合わせる',
		textBtnTipFPCR: '右中段に合わせる',
		textBtnTipFPBL: '左下角に合わせる',
		textBtnTipFPBC: '中央下に合わせる',
		textBtnTipFPBR: '右下角に合わせる',
    	inputPrefix: 'ci-',
    	onInit: function() {
    		$('#crop-img').css('max-width','611px');
    		$('[name=x]').val(0);
    		$('[name=y]').val(0);
    		$('[name=w]').val(611);
    	}
	});

	$('#form').submit(function(e) { 
		if(!$('[name=x]').val() || $('[name=x]').val() == "NaN"){
			$('[name=x]').val(0);
		}
		if(!$('[name=y]').val() || $('[name=y]').val() == "NaN"){
			$('[name=y]').val(0);
		}
		if(!$('[name=w]').val() || $('[name=w]').val() == "NaN"){
			$('[name=w]').val(0);
		}
		if(!$('[name=h]').val() || $('[name=h]').val() == "NaN"){
			$('[name=h]').val(0);
		}
	});

	var ua = navigator.userAgent;
	if (ua.match('MSIE') || ua.match('Trident')){
		$('#result-img').css('visibility','hidden');
	}

});