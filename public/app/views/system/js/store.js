/* ++++++++++++++++++++++++++++++++++++++++

    Store

++++++++++++++++++++++++++++++++++++++++ */
$(function(){
    $.ajaxSetup({
        cache: false
    });
    $('.storeBlock p').click(function(){
        if($(this).parents('.storeBlock').find('.storeDetail').is(':visible')){
            $(this).parents('.storeBlock').find('.storeDetail').slideUp("fast");
            $(this).find('.storeHide').hide();
            $(this).parents('.storeBlock').find('.storeShow').show();
        }else{
            $(this).parents('.storeBlock').find('.storeDetail').slideDown("fast");
            $(this).find('.storeShow').hide();
            $(this).parents('.storeBlock').find('.storeHide').show();
        } 
    });
})
function approve(sId, viewFlg, sNm){
    $.post('/xx_nimda/storeApprove/',
        {
            s:sId,
            v:viewFlg
        }
    );
	approveAnimate(sId, viewFlg, sNm);
}
function approveEntry(sId, viewFlg, sNm, table){
    $.post('/xx_nimda/entryApprove/',
        {
            s:sId,
            v:viewFlg,
            t:table,
        }
    );
	//console.log(table);
	approveAnimate(sId, viewFlg, sNm);
}
function deleteEntry(sId, deleted, sNm, table){
	// 確認メッセージ
	var msg = sNm+"の投稿を削除しますか？" ;
	
	// 「はい」を選択した場合
	if( window.confirm( msg ) ) {
		$.post('/xx_nimda/entryDelete/',
			{
				s:sId,
				d:deleted,
				t:table,
			}
		);
		deleteAnimate(sId, deleted, sNm);
	}
}
function updateMemo(sId){
    $.post('/xx_nimda/storeMemoUpdate/',
        {
            s:sId,
            m:$("#store"+sId).find('textarea').val()
        }
    );
    location.reload();
}
function updateConfirm(sId,table){
    if(table == 1){
        $("#entry"+sId).animate({opacity: 'hide',}, {duration: 100, easing: 'swing'});
    }else{
        $("#entry"+sId).animate({opacity: 'hide',}, {duration: 100, easing: 'swing'});
    }
    
    $.toast({
        text : "確認人数を更新しています。",
        hideAfter : 500
    });
	setTimeout(function () {
        if(table == 1){
            $("#entry"+sId+" form#confirm").submit();
        }else{
            $("#sns"+sId+" form#confirm").submit();
        }
	}, 600);
}
function approveAnimate(sId, viewFlg, sNm,table){
    $("#entry"+sId).animate({opacity: 'hide',}, {duration: 500, easing: 'swing'});
    var str = "公開";
    if(viewFlg == 0){
        str = "非公開";
    }else if(viewFlg == 3){
        //str = "保留";
    }
    $.toast({
        text : sNm + "を" + str + "にしました。",
        hideAfter : 2500
    });
}
function deleteAnimate(sId, deleted, sNm,table){
    $("#entry"+sId).animate({opacity: 'hide',}, {duration: 500, easing: 'swing'});
    var str = "削除";
    $.toast({
        text : sNm + "を" + str + "しました。",
        hideAfter : 2500
    });
}