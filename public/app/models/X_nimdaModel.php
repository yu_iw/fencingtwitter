<?php

class X_nimdaModel extends ModelBase{

	private $SESSION_ADMIN_LOGIN = 'SESSION_ADMIN_LOGIN';

    public function auth($account,$passwd){
        $sql = "SELECT * FROM t_admin WHERE account = :account and passwd = :passwd";
        $params = array('account' => $account,'passwd' => $passwd);
        $rows = $this->query($sql,$params);
        return count($rows) > 0;
    }

    //void型
    public function setLoginSession($admin){
    	if(isset($admin)){
    		//session開始
    		session_start();
    		$_SESSION[$this->SESSION_ADMIN_LOGIN] = $admin;
    		//echo session_id();
    	}
    }

}

?>
