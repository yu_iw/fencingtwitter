<?php

class Xx_nimdaModel extends ModelBase{

	private $SESSION_ADMIN_LOGIN = 'SESSION_ADMIN_LOGIN';
	private $SESSION_ADMIN_ENTRY = 'SESSION_ADMIN_ENTRY';
	private $SESSION_ADMIN_ENTRYS = 'SESSION_ADMIN_ENTRYS';
    private $SESSION_ADMIN_CATEGORY = 'SESSION_ADMIN_CATEGORY';

    public function getLoginSession(){
    	//session開始
    	session_start();
//     	echo session_id();
    	return  $_SESSION[$this->SESSION_ADMIN_LOGIN];
    }
    public function removeLoginSession(){
    	//session開始
    	session_start();
//     	echo session_id();
    	session_destroy();
    }
    public function setEntrysSession($entrys){
    	session_start();
    	$_SESSION[$this->SESSION_ADMIN_ENTRYS] = $entrys;
    }
    public function getEntrysSession(){
    	session_start();
    	return $_SESSION[$this->SESSION_ADMIN_ENTRYS];
    }
    public function removeEntrysSession(){
    	session_start();
    	unset($_SESSION[$this->SESSION_ADMIN_ENTRYS]);
//     	$_SESSION[$this->SESSION_ADMIN_ENTRYS] = null;
    }
    public function setEntrySession($entry){
    	session_start();
    	$_SESSION[$this->SESSION_ADMIN_ENTRY] = $entry;
    }
    public function getEntrySession(){
    	session_start();
    	return $_SESSION[$this->SESSION_ADMIN_ENTRY];
    }
    public function removeEntrySession(){
    	session_start();
    	unset($_SESSION[$this->SESSION_ADMIN_ENTRY]);
//     	$_SESSION[$this->SESSION_ADMIN_ENTRY] = null;
    }
    public function setCategorySession($category){
        session_start();
        $_SESSION[$this->SESSION_ADMIN_CATEGORY] = $category;
    }
    public function getCategorySession(){
        session_start();
        return $_SESSION[$this->SESSION_ADMIN_CATEGORY];
    }
    public function removeCategorySession(){
        session_start();
        unset($_SESSION[$this->SESSION_ADMIN_CATEGORY]);
//      $_SESSION[$this->SESSION_ADMIN_ENTRY] = null;
    }
    /***********************
     *  Admin系
     **********************/
    public function authId($account,$passwd){
        $sql = "SELECT id FROM t_admin WHERE account = :account and passwd = :passwd";
        $params = array('account' => $account,'passwd' => $passwd);
        $rows = $this->query($sql,$params);
        return $rows[0]["id"];
    }
    /***********************
     *  declare系
     **********************/
    public function selectEntryById($table,$id) {
        $sql = "select * from :table where id = :id";
        $params = array(
                'table'    =>  $table,
                'id'    =>  $id
        );
        $rows = $this->query($sql,$params);
        return $rows;
    }
    public function getEntryList($table,$public){
        $sql = "SELECT * FROM ".$table." WHERE public = ".$public." AND deleted = false ORDER BY created_at desc";
        $rows = $this->query($sql);
        return $rows;
    }
    public function getEntryListTw($public){
        $sql = "SELECT * 
        ,(SELECT count(id) FROM t_twitter as t2 WHERE t2.user_id_number = t.user_id_number AND media_url IS NOT NULL AND body NOT LIKE 'RT %' AND deleted = false GROUP by t2.user_id_number ) as entry_num 
		FROM t_twitter as t WHERE public = ".$public." AND deleted = false ORDER BY created_at desc
		";
        $rows = $this->query($sql);
        return $rows;
    }
    public function updateEntryPublic($sId,$public,$table) {
        $sql = "UPDATE ".$table."
				SET public = ".$public.", modified_at = now()
				WHERE id = ".$sId;
        $rows = $this->query($sql);
        return $rows;
    }
    public function updateDeclareMemo($sId,$memo) {
        $sql = "UPDATE t_declare
                  SET memo = :memo, updated_at = now()
                  WHERE id = :id";
        $params = array(
                'memo'   =>  $memo,
                'id'   =>  $sId
        );
        $rows = $this->query($sql, $params);
        return $rows;
    }
    public function deleteEntry($sId,$deleted,$table) {
        $sql = "UPDATE ".$table." SET deleted = ".$deleted.", modified_at = now() WHERE id = ".$sId;
        $rows = $this->query($sql);
        return $rows;
    }

	
	/***********************
     *  Sum系
     **********************/
	 
    public function sumEntry($table){
        $sql = "SELECT count(*) FROM ".$table." WHERE deleted = false";
        $rows = $this->query($sql);
        $sum = $rows[0]['count'];
		return $sum;
    }
	
    public function sumEntryView($table){
        $sql = "SELECT count(*) FROM ".$table." WHERE public = 1 AND deleted = false";
        $rows = $this->query($sql);
        $sum = $rows[0]['count'];
		return $sum;
    }
    public function sumEntryImg($table){
        $sql = "SELECT count(*) FROM ".$table." WHERE deleted = false AND media_url IS NOT NULL";
        $rows = $this->query($sql);
        $sum = $rows[0]['count'];
		return $sum;
    }
    public function sumEntryImgNotRt($table){
        $sql = "SELECT count(*) FROM ".$table." WHERE deleted = false AND media_url IS NOT NULL AND body NOT LIKE 'RT %'";
        $rows = $this->query($sql);
        $sum = $rows[0]['count'];
		return $sum;
    }
    public function sumEntryNotRt($table){
        $sql = "SELECT count(*) FROM ".$table." WHERE deleted = false AND body NOT LIKE 'RT %'";
        $rows = $this->query($sql);
        $sum = $rows[0]['count'];
		return $sum;
    }

	
	/***********************
     *  CSV系
     **********************/
	 
    public function getCsv($table){
        $sql = "SELECT * FROM ".$table." WHERE deleted = false ORDER BY created_at desc";
        $rows = $this->query($sql);
        return $rows;
    }
}
?>