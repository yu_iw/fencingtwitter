<?php
// require_once ROOT_PATH.'/models/T_admin_vote.php';
class Xx_nimdaController extends ControllerBase{
	public function storeListAction(){
		$v = 0;
		if(isset($_GET['v'])){
			$v = $_GET['v'];
		}
		$this->view->assign('v', $v);

		$xx = new Xx_nimdaModel();
		$store = $xx->getStoreList($v);
		$this->view->assign('store', $store);

	}
	public function storeApproveAction(){
		if(isset($_POST['v']) && isset($_POST['s']) ){
			$v = $_POST['v'];
			$s = $_POST['s'];

			$xx = new Xx_nimdaModel();
			$store = $xx->selectStoreById($s);

			error_log($store[0]['email']);

			if($v == 1){
				$CR = Chr(13).Chr(10);

				$subject = '【全国一斉　日本酒で乾杯！】ご登録いただいた店舗が承認されました';
				$message = $store[0]['store_nm'].'様'.$CR.$CR.
				'お世話になっております。'.$CR.
				'「全国一斉日本酒で乾杯！」事務局です。'.$CR.
				'ご登録いただいた店舗が承認されました。'.$CR.
				'あなたのIDは'.$CR.
				$store[0]['id'].$CR.
				'です。'.$CR.$CR.
				'10月1日(土)当日は下記フォームにアクセスして、'.$CR.
				'乾杯画像を投稿していただけると幸いです。'.$CR.
				'その際、上記IDが必要となります。'.$CR.
				'http://kampai-sake.jp/store-post.html'.$CR.
				'引き続き「全国一斉日本酒で乾杯！」をよろしくお願いいたします。'.$CR.$CR.$CR.
				'---------------------------------------------'.$CR.$CR.
				'「全国一斉日本酒で乾杯！」事務局　'.$CR.$CR.
				'MAIL：jimukyoku@kampai-sake.jp'.$CR.
				'TEL：03-6630-0530'.$CR.
				'http://kampai-sake.jp'.$CR.$CR.
				'---------------------------------------------'.$CR;

				require_once LIB_PATH.'/mvc/qdmail/QdController.php';
				$qc = new QdController();

				try{
					$qc->sendMail($store[0]['email'],$subject,$message);
				}catch(Exception $e){
					error_log($e);
				}
			}

			$store = $xx->updateStoreViewFlg($s, $v);
		}
	}
	public function storeMemoUpdateAction(){
		if(isset($_POST['m']) && isset($_POST['s']) ){
			$m = $_POST['m'];
			$s = $_POST['s'];

			$xx = new Xx_nimdaModel();
			$store = $xx->updateStoreMemo($s, $m);
		}
	}
	public function storeEditAction(){
		if(isset($_GET['id']) && $_GET['id'] != 0){
			require_once ROOT_PATH.'/models/T_store.php';
			$m = new T_store();
			$m->setDefaultTableName();
			$store = $m->selectByPK($_GET['id']);
			$_SESSION['store'] = $store[0];
		}
		$this->view->assign('store',$_SESSION['store']);
		$this->view->assign('e',$_SESSION['error_text']);
		$_SESSION['error_text'] = null;
		$this->view->assign('p', json_decode(PREFECTURE,true));
	}

	public function storeEditConfAction(){

		$store = $this->request->getPost();
		if(isset($store)){
			$_SESSION['store'] = $store;
		}else{
			$store = $_SESSION['store'];
		}

		if(!isset($_SESSION['error_text'])){
			// $v = new Validation();
		
			// $error_text = array(
			// 	'store_nm'=>$v->validate('text',$store['store_nm'], "店舗名",true),
			// 	'name'=>$v->validate('text',$store['name'], "担当者名",true),
			// 	'postal'=>$v->validate('postal_code',$store['postal'], "郵便番号",true),
			// 	'prefecture_id'=>$v->validate('int',$store['prefecture_id'], "都道府県",true),
			// 	'address1'=>$v->validate('text',$store['address1'], "市区町村番地",true),
			// 	'address2'=>$v->validate('text',$store['address2'], "ビル名",false),
			// 	'tel'=>$v->validate('tel',$store['tel'], "電話番号",true),
			// 	'email'=>$v->validate('email',$store['email'], "メールアドレス",true),
			// 	'url'=>$v->validate('text',$store['url'], "HP URL",false)
			// );

			// $temp = array_values($error_text);
			// $e_flg = 0;
			// for($i = 0; $i < count($temp); $i++ ){
			// 	if(!$temp[$i]['bool']){
			// 		$e_flg = 1;
			// 	}
			// }

			// // $m = new MainModel();
			// // $mailFlg = $m->isEmailExist($store['email']);
			// // if($mailFlg){
			// // 	$e_flg = 1;
			// // 	$error_text['email']['error_text'] = "このメールアドレスは登録済みです。";
			// // 	$error_text['email']['bool'] = false;
			// // }
			
			// if($e_flg == 1){
			// 	$_SESSION['error_text'] = $error_text;
			// 	header("Location: " ."../storeEntry/");
			// 	exit();
			// }
		}
		
		$this->view->assign('p', json_decode(PREFECTURE,true));
		$this->view->assign('store', $_SESSION['store']);
		$this->view->assign('e',$_SESSION['error_text']);
		$_SESSION['error_text'] = null;
	}

	public function storeEditCompAction(){
		require_once ROOT_PATH.'/models/T_store.php';
		$s = new T_store();
		$s->setDefaultTableName();
		$store = $this->request->getPost();

		if($store){
			try{
				$result = $s->updateByPK("id",$store);
				if(!$result){
					echo "データの更新に失敗しました。";
				}
			}catch(Exception $e){
				echo "ただ今サーバが混み合っております。しばらく経ってから再度お試しください。<br>".$e;
			}
		}
	}
	
	
	/******************
	 *  イクメン宣言一覧
	******************/
	public function ikumenListAction(){
		$v = 0;
		if(isset($_GET['v'])){
			$v = $_GET['v'];
		}
		
		$table = "t_declare";
		
		$this->view->assign('v', $v);

		$xx = new Xx_nimdaModel();
		
		$sumEntry = $xx->sumEntry($table);
		
		$sumEntryView = $xx->sumEntryView($table);
		$entry = $xx->getEntryList($table,$v);

		/*
		print("<pre>");
		print_r($entry);
		print("</pre>");
		*/
		$this->view->assign('p', json_decode(PREFECTURE,true));
		$this->view->assign('entry', $entry);
		$this->view->assign('table', $table);
		$this->view->assign('entrySum', $sumEntry);
		$this->view->assign('viewSum', $sumEntryView);
	}
	
	public function supporterListAction(){
		$v = 0;
		if(isset($_GET['v'])){
			$v = $_GET['v'];
		}
		
		$table = "t_supporter";
		
		$this->view->assign('v', $v);

		$xx = new Xx_nimdaModel();
		
		$sumEntry = $xx->sumEntry($table);
		
		$sumEntryView = $xx->sumEntryView($table);
		$entry = $xx->getEntryList($table,$v);
		/*
		print("<pre>");
		print_r($entry);
		print("</pre>");
		*/
		$this->view->assign('p', json_decode(PREFECTURE,true));
		$this->view->assign('entry', $entry);
		$this->view->assign('table', $table);
		$this->view->assign('entrySum', $sumEntry);
		$this->view->assign('viewSum', $sumEntryView);
	}
	
	public function voiceListAction(){
		$v = 0;
		if(isset($_GET['v'])){
			$v = $_GET['v'];
		}
		
		$table = "t_voice";
		
		$this->view->assign('v', $v);

		$xx = new Xx_nimdaModel();
		
		$sumEntry = $xx->sumEntry($table);
		
		$sumEntryView = $xx->sumEntryView($table);
		$entry = $xx->getEntryList($table,$v);

		/*
		print("<pre>");
		print_r($entry);
		print("</pre>");
		*/
		$this->view->assign('p', json_decode(PREFECTURE,true));
		$this->view->assign('entry', $entry);
		$this->view->assign('table', $table);
		$this->view->assign('entrySum', $sumEntry);
		$this->view->assign('viewSum', $sumEntryView);
	}
	
	
	public function regionListAction(){
		$v = 0;
		if(isset($_GET['v'])){
			$v = $_GET['v'];
		}
		
		$table = "t_region";
		
		$this->view->assign('v', $v);

		$xx = new Xx_nimdaModel();
		
		$sumEntry = $xx->sumEntry($table);
		
		$sumEntryView = $xx->sumEntryView($table);
		$entry = $xx->getEntryList($table,$v);

		
		/*print("<pre>");
		print_r($entry);
		print("</pre>");*/
		
		$this->view->assign('p', json_decode(PREFECTURE,true));
		$this->view->assign('entry', $entry);
		$this->view->assign('table', $table);
		$this->view->assign('entrySum', $sumEntry);
		$this->view->assign('viewSum', $sumEntryView);
	}
	
	public function supporterGroupListAction(){
		$v = 0;
		if(isset($_GET['v'])){
			$v = $_GET['v'];
		}
		
		$table = "t_supporter_group";
		
		$this->view->assign('v', $v);

		$xx = new Xx_nimdaModel();
		
		$sumEntry = $xx->sumEntry($table);
		
		$sumEntryView = $xx->sumEntryView($table);
		$entry = $xx->getEntryList($table,$v);

		/*
		print("<pre>");
		print_r($entry);
		print("</pre>");
		*/
		$this->view->assign('p', json_decode(PREFECTURE,true));
		
		$this->view->assign('entry', $entry);
		$this->view->assign('table', $table);
		$this->view->assign('entrySum', $sumEntry);
		$this->view->assign('viewSum', $sumEntryView);
	}
	
	public function activeListAction(){
		$v = 0;
		if(isset($_GET['v'])){
			$v = $_GET['v'];
		}
		
		$table = "t_active";
		
		$this->view->assign('v', $v);

		$xx = new Xx_nimdaModel();
		
		$sumEntry = $xx->sumEntry($table);
		
		$sumEntryView = $xx->sumEntryView($table);
		$entry = $xx->getEntryList($table,$v);

		/*
		print("<pre>");
		print_r($entry);
		print("</pre>");
		*/
		$this->view->assign('p', json_decode(PREFECTURE,true));
		
		$this->view->assign('entry', $entry);
		$this->view->assign('table', $table);
		$this->view->assign('entrySum', $sumEntry);
		$this->view->assign('viewSum', $sumEntryView);
	}	
	
	public function instagramListAction(){
		$v = 0;
		if(isset($_GET['v'])){
			$v = $_GET['v'];
		}
		
		$table = "t_sns";
		
		$this->view->assign('v', $v);

		$xx = new Xx_nimdaModel();
		
		$sumEntry = $xx->sumEntry($table);
		
		$sumEntryView = $xx->sumEntryView($table);
		$entry = $xx->getEntryList($table,$v);
/*
		print("<pre>");
		print_r($entry);
		print("</pre>");
*/		
		$this->view->assign('p', json_decode(PREFECTURE,true));
		$this->view->assign('entry', $entry);
		$this->view->assign('table', $table);
		$this->view->assign('entrySum', $sumEntry);
		$this->view->assign('viewSum', $sumEntryView);
	}
	
	public function twitterListAction(){
		$v = 0;
		if(isset($_GET['v'])){
			$v = $_GET['v'];
		}
		
		$table = "t_twitter";
		
		$this->view->assign('v', $v);

		$xx = new Xx_nimdaModel();
		
		$sumEntry = $xx->sumEntryNotRt($table);
		$sumEntryView = $xx->sumEntryView($table);
		$entry = $xx->getEntryListTw($v);

//		print("<pre>");
//		print_r($entry);
//		print("</pre>");
		
		$this->view->assign('p', json_decode(PREFECTURE,true));
		$this->view->assign('entry', $entry);
		$this->view->assign('table', $table);
		$this->view->assign('entrySum', $sumEntry);
		$this->view->assign('viewSum', $sumEntryView);
	}
	
	public function entryApproveAction(){
		if(isset($_POST['v']) && isset($_POST['s']) && isset($_POST['t']) ){
			$v = $_POST['v'];
			$s = $_POST['s'];
			$t = $_POST['t'];

			$xx = new Xx_nimdaModel();
			$entry = $xx->selectEntryById($t,$s);
			$entry = $xx->updateEntryPublic($s, $v, $t);
			
			
			$entry = $xx->getEntryListTw('1');
			//山田修正
			$entry2 = array();
			foreach($entry as $id => $rec){
				$str = $rec['body'] ;
				$rec['body'] = preg_replace('/http\S*/', '', $str);
				array_push($entry2,$rec);
			}

			//$arr = json_encode($entry,JSON_UNESCAPED_UNICODE);
			$arr = json_encode($entry2,JSON_UNESCAPED_UNICODE);
			//山田修正ここまで
			file_put_contents("/home/fencing-twitter.grpht.co.jp/public_html/data/json/twitter.json", $arr);
		}
	}

//★
	public function testAction(){
			$xx = new Xx_nimdaModel();
			$entry = $xx->getEntryListTw('1');
			$entry2 = array();
			//print_r($entry);
			foreach($entry as $id => $rec){
			//	echo $e->body;
				//echo $rec['body'] ;

				$str = $rec['body'] ;
				//$str_grep = preg_replace('/http\S*/', '', $str);
				//$str_grep = preg_replace('/手/', '', $str);
				//echo '<br>';

				//echo $str_grep;
				//echo '<br><br>';
				$rec['body'] = preg_replace('/http\S*/', '', $str);
				//$rec['body'] = preg_replace('/手/', '', $str);
				array_push($entry2,$rec);
				//print_r($rec);
				//echo '<br><br>';

			}
			print_r($entry2);

	}
//★

	public function entryDeleteAction(){
		if(isset($_POST['s']) && isset($_POST['d']) && isset($_POST['t']) ){
			$s = $_POST['s'];
			$d = $_POST['d'];
			$t = $_POST['t'];
			$xx = new Xx_nimdaModel();
			$entry = $xx->selectEntryById($t,$s);
			$entry = $xx->deleteEntry($s, $d, $t);
		}
	}

	public function csvDownloadAction(){
	
	}

	public function csvFileAction(){
		
		$table = $_GET['t'];
		
		$xx = new Xx_nimdaModel();

		$p = json_decode(PREFECTURE,true);

		$entry = $xx->getCsv($table);

		$csvdata =  $table.date("Y/m/d").".csv";
		switch ($table){
				case "t_declare":
					$csv = "id,氏名またはニックネーム,メールアドレス,イクメン宣言,年代,1(true):子供なし、0:(false):子供あり,男の子の数,女の子の数,お住いのエリア（都道府県）,メルマガ配信,0:PC 1:携帯,公開フラグ 1(true):公開、0:(false):非公開,更新日時,登録日時\n";
					foreach ($entry as $e) {
						$declaration = str_replace(',', '，', $e['declaration']);
						$declaration = str_replace(array("\r\n","\n","\r"), '', $e['declaration']);
						$csv .= ''.$e['id'].','.$e['nm'].','.$e['email'].','.$declaration.','.$e['age'].','.$e['no_children'].','.$e['m_children_num'].','.$e['f_children_num'].','.$e['prefecture_id'].','.$e['accept_mail_magazine'].','.$e['device'].','.$e['public'].','.$e['modified_at'].','.$e['created_at'].'';
						$csv .= "\n" ;
					}
					break;
					
				case "t_supporter":
					$csv = "id,氏名またはニックネーム,メールアドレス,性別（1：男、2：女）,年代,サポーター宣言,お住いのエリア（都道府県）,メルマガ配信,0:PC 1:携帯,公開フラグ 1(true):公開、0:(false):非公開,更新日時,登録日時\n";
					foreach ($entry as $e) {
						$declaration = str_replace(',', '，', $e['declaration']);
						$declaration = str_replace(array("\r\n","\n","\r"), '', $declaration);
						$csv .= ''.$e['id'].','.$e['nm'].','.$e['email'].','.$e['sex'].','.$e['age'].','.$declaration.','.$e['prefecture_id'].','.$e['accept_mail_magazine'].','.$e['device'].','.$e['public'].','.$e['modified_at'].','.$e['created_at'].'';
						$csv .= "\n" ;
					}
					break;
					
				case "t_voice":
					$csv = "id,氏名またはニックネーム,メールアドレス,年代,1(true):子供なし、0:(false):子供あり,男の子の数,女の子の数,お住いのエリア（都道府県）,育休・育児体験談,育休・育児中の写真,メルマガ配信,0:PC 1:携帯,公開フラグ 1(true):公開、0:(false):非公開,更新日時,登録日時\n";
					foreach ($entry as $e) {
						$voice = str_replace(',', '，', $e['voice']);
						$voice = str_replace(array("\r\n","\n","\r"), '', $voice);
						$csv .= ''.$e['id'].','.$e['nm'].','.$e['email'].','.$e['age'].','.$e['no_children'].','.$e['m_children_num'].','.$e['f_children_num'].','.$e['prefecture_id'].','.$voice.','.$e['img'].','.$e['accept_mail_magazine'].','.$e['device'].','.$e['public'].','.$e['modified_at'].','.$e['created_at'].'';
						$csv .= "\n" ;
					}
					break;
					
				case "t_supporter_group":
					$csv = "id,グループ名,法人番号,0:企業以外、1:企業,都道府県,担当者名,メールアドレス,企業・団体サイトURL,サポーター宣言,ロゴ,ロゴの仕様書,メルマガ配信,公開フラグ 1(true):公開、0:(false):非公開,更新日時,登録日時\n";
					foreach ($entry as $e) {
						$declaration = str_replace(',', '，', $e['declaration']);
						$declaration = str_replace(array("\r\n","\n","\r"), '', $declaration);
						$csv .= ''.$e['id'].','.$e['group_nm'].','.$e['company_number'].','.$e['is_company'].','.$p[$e['prefecture_id']].','.$e['staff_nm'].','.$e['email'].','.$e['url'].','.$declaration.','.$e['logo_img'].','.$e['logo_spec'].','.$e['accept_mail_magazine'].','.$e['public'].','.$e['modified_at'].','.$e['created_at'].'';	
						$csv .= "\n" ;
					}
					break;	
					
				case "t_active":
					$csv = "id,企業・団体名,法人番号,サポーターID,0:企業以外、1:企業,都道府県,担当者名,メールアドレス,企業・団体サイトURL,活動URL,活動タイトル,活動概要,活動写真,メルマガ配信,公開フラグ 1(true):公開、0:(false):非公開,更新日時,登録日時\n";
					foreach ($entry as $e) {
						$active_body = str_replace(',', '，', $e['active_body']);
						$active_body = str_replace(array("\r\n","\n","\r"), '', $active_body);
						$csv .= ''.$e['id'].','.$e['group_nm'].','.$e['company_number'].','.$e['supporter_id'].','.$e['is_company'].','.$e['prefecture_id'].','.$e['staff_nm'].','.$e['email'].','.$e['url'].','.$e['active_url'].','.$e['active_title'].','.$active_body.','.$e['active_img'].','.$e['accept_mail_magazine'].','.$e['public'].','.$e['modified_at'].','.$e['created_at'].'';
						$csv .= "\n" ;
					}
					break;
					
				case "t_sns":
					$csv = "id,投稿ID,ユーザーID,ユーザー名,投稿本文,画像URL（640*640px）,画像URL（320*320px）,いいね,投稿URL,アイコン画像URL,投稿日時,公開フラグ 1(true):公開、0:(false):非公開,更新日時,登録日時\n";
					foreach ($entry as $e) {
						$body = str_replace(',', '，', $e['body']);
						$body = str_replace(array("\r\n","\n","\r"), '', $body);
						$csv .= ''.$e['id'].','.$e['post_id'].','.$e['user_id'].','.$e['name'].','.$body.','.$e['media_url'].','.$e['media_url_thumb'].','.$e['likes'].','.$e['post_url'].','.$e['icon_url'].','.$e['posted_at'].','.$e['public'].','.$e['modified_at'].','.$e['createsd_at'].'';
						$csv .= "\n" ;
					}
					break;
					
				case "t_twitter":
					$csv = "id,投稿ID,ユーザーID,ユーザー名,投稿本文,画像URL（640*640px）,画像URL（320*320px）,いいね,投稿URL,アイコン画像URL,投稿日時,公開フラグ 1(true):公開、0:(false):非公開,更新日時,登録日時\n";
					foreach ($entry as $e) {
						$body = str_replace(',', '，', $e['body']);
						$body = str_replace(array("\r\n","\n","\r"), '', $body);
						$csv .= ''.$e['id'].','.$e['post_id'].','.$e['user_id'].','.$e['name'].','.$body.','.$e['media_url'].','.$e['media_url_thumb'].','.$e['likes'].','.$e['post_url'].','.$e['icon_url'].','.$e['posted_at'].','.$e['public'].','.$e['modified_at'].','.$e['createsd_at'].'';
						$csv .= "\n" ;
					}
					break;
					
				case "t_social":
					$csv = "id,グループ名,応募者名,フリガナ,性別,学校名,学部／専門,連絡先,メールアドレス,メンバー1,メンバー2,メンバー3,メンバー4,自由記入欄,ファイル送信,更新日時,登録日時\n";
					foreach ($entry as $e) {
						$memo = str_replace(',', '，', $e['memo']);
						$memo = str_replace(array("\r\n","\n","\r"), '', $memo);
						$csv .= ''.$e['id'].','.$e['group_nm'].','.$e['last_nm'].' '.$e['first_nm'].','.$e['last_kana'].' '.$e['first_kana'].','.$e['sex'].','.$e['school'].','.$e['section'].','.$e['tel'].','.$e['email'].','.$e['member1_last_nm'].' '.$e['member1_first_nm'].','.$e['member2_last_nm'].' '.$e['member2_first_nm'].','.$e['member3_last_nm'].' '.$e['member3_first_nm'].','.$e['member4_last_nm'].' '.$e['member4_first_nm'].','.$memo.','.$e['file'].','.$e['modified_at'].','.$e['created_at'].'';
						$csv .= "\n" ;
					}
					break;
				case "t_social_attend":
					$csv = "id,応募者名,フリガナ,性別（1：男、2：女）,所属,連絡先,メールアドレス,懇親会（1：参加、2：不参加）,更新日時,登録日時\n";
					foreach ($entry as $e) {
						$csv .= ''.$e['id'].','.$e['last_nm'].' '.$e['first_nm'].','.$e['last_kana'].' '.$e['first_kana'].','.$e['sex'].','.$e['belong'].','.$e['tel'].','.$e['email'].','.$e['party'].','.$e['modified_at'].','.$e['created_at'].'';
						$csv .= "\n" ;
					}
					break;
				case "t_speech_koshien":
					$kindArr = array('0'=>'選択してください','1'=>'A　農業，林業','2'=>'B　漁業','3'=>'C　鉱業，採石業，砂利採取業','4'=>'D　建設業','5'=>'E　製造業','6'=>'F　電気・ガス・熱供給・水道業','7'=>'G　情報通信業','8'=>'H　運輸業，郵便業','9'=>'I 　卸売業，小売業','10'=>'J　金融業，保険業','11'=>'K　不動産業，物品賃貸業','12'=>'L　学術研究，専門・技術サービス業','13'=>'M　宿泊業，飲食サービス','14'=>'N　生活関連サービス業，娯楽業','15'=>'O　教育，学校支援業','16'=>'P　医療，福祉','17'=>'Q　複合サービス事業','18'=>'R　サービス業（他に分類されないもの）','19'=>'S　公務（他に分類されるものを除く）','20'=>'T　分類不能の産業');
					$question1Arr = array('1'=>'取得済み','2'=>'取得中','3'=>'取得予定あり','4'=>'取得していない','5'=>'自営業者等のため、制度なし');
					$question2Arr = array('1'=>'取得済み','2'=>'取得予定あり','3'=>'取得していない','4'=>'自営業者等のため、制度なし');
					$csv = "id,応募者名,フリガナ,男の子の数,女の子の数,業種,年齢,都道府県,連絡先,メールアドレス,育児休業について,期間,育児目的休暇について,期間,タイトル,エピソード,更新日時,登録日時\n";
					foreach ($entry as $e) {
						$kind = $e['kind'];
						$prefecture_id = $e['prefecture_id'];
						$question1 = $e['question1'];
						$question2 = $e['question2'];
						$csv .= ''.$e['id'].','.$e['last_nm'].' '.$e['first_nm'].','.$e['last_kana'].' '.$e['first_kana'].','.$e['m_children_num'].','.$e['f_children_num'].','.$kindArr[$kind].','.$e['age'].','.$p[$prefecture_id].','.$e['tel'].','.$e['email'].','.$question1Arr[$question1].','.$e['question1_text'].','.$question2Arr[$question2].','.$e['question2_text'].','.$e['title'].',"'.$e['body'].'",'.$e['modified_at'].','.$e['created_at'].'';
						$csv .= "\n" ;
					}
					break;
				case "t_symposium":
					$csv = "id,応募者名,フリガナ,メールアドレス,勤務先,メンバー1氏名,メンバー1会社名,メンバー2氏名,メンバー2会社名,メンバー3氏名,メンバー3会社名,メンバー4氏名,メンバー4会社名,更新日時,登録日時\n";
					foreach ($entry as $e) {
						$prefecture_id = $e['prefecture_id'];
						$csv .= ''.$e['id'].','.$e['last_nm'].' '.$e['first_nm'].','.$e['last_kana'].' '.$e['first_kana'].','.$e['email'].','.$e['business'].','.$e['member1_last_nm'].' '.$e['member1_first_nm'].','.$e['member1_company_nm'].','.$e['member2_last_nm'].' '.$e['member2_first_nm'].','.$e['member2_company_nm'].','.$e['member3_last_nm'].' '.$e['member3_first_nm'].','.$e['member3_company_nm'].','.$e['member4_last_nm'].' '.$e['member4_first_nm'].','.$e['member4_company_nm'].','.$e['modified_at'].','.$e['created_at'].'';
						$csv .= "\n" ;
					}
					break;
		}

		header("Content-Type: application/octet-stream");
	    header("Content-Disposition: attachment; filename=$csvdata");
	    echo mb_convert_encoding($csv,"SJIS", "UTF-8");
	}
	
	/******************
	 *  共通系
	******************/
	public function logoutAction(){
		$x = new Xx_nimdaModel();
		$x->removeLoginSession();
		//ログインページへリダイレクト
		header("Location: ../../x_nimda/login/");
		exit;
	}
	//Actionより前に実行されます。
	protected function preAction(){
		//IP制限
		require_once LIB_PATH.'/mvc/AccessRestriction.php';
		$a = new AccessRestriction();
		if(!$a->ip()){
			//echo "不正なアクセスです。";
			//exit();
		}
		//ログインチェック
		$x = new Xx_nimdaModel();
		$admin = $x->getLoginSession();
		if(!isset($admin)){
			//セッションが無いためログインページへリダイレクト
			header("Location: ../../x_nimda/login/");
			exit;
    	}
    }

}
?>