<?php



class X_nimdaController extends ControllerBase{

	public function loginAction(){
		//IP制限
		require_once LIB_PATH.'/mvc/AccessRestriction.php';
		$a = new AccessRestriction();
		if(!$a->ip()){
			//echo "不正なアクセスです。";
			//exit();
		}
		//全項目を初期化して表示
		$this->view->assign('account', "");
		$this->view->assign('account_error', "");
		$this->view->assign('passwd', "");
		$this->view->assign('passwd_error', "");
	}

	public function authAction(){
		$post = $this->request->getPost();
		$error_flg = false;
		//一度入力されたデータをセットします。
		$this->view->assign('account', $post['account']);
		$this->view->assign('account_error', "");
		$this->view->assign('passwd', $post['passwd']);
		$this->view->assign('passwd_error', "");

		//入力チェック

		if(!isset($post['account']) || "" === $post['account']){
			$this->view->assign('account_error', "IDが未入力です。".$post['account'].$post['passwd']);
			$error_flg = true;
		}
		if(!isset($post['passwd']) ||  "" === $post['passwd']){
			$this->view->assign('passwd_error', "パスワードが未入力です。");
			$error_flg = true;
		}
		//IDとパスを照合
		$x = new X_nimdaModel();
		if(!$error_flg && !$x->auth($post['account'],$post['passwd'])){
			$this->view->assign('account_error', "IDまたはパスワードが正しくありません。");
			$error_flg = true;
		}
		if($error_flg){
			//再度loginページを表示する
			$this->templatePath = 'x_nimda/login.tpl';
		}else{
			//sessionにauthOKの値を追加してリダイレクト
			$admin = array('account'=>$post['account'],'passwd'=>$post['passwd']);
			$x->setLoginSession($admin);
			header("Location: ../../xx_nimda/twitterList/");
			exit;
		}
	}
}
?>
