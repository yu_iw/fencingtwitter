var SerialPort = require('serialport').SerialPort;
var serial = new SerialPort('COM3',{
     baudrate:38400,
     dataBits:8, 
     parity:'none', 
     stopBits: 1,
     flowControl: false
});

//websocket
// 8888番ポートでクライアントの接続を待ち受ける
var ws = require('websocket.io');
var server = ws.listen(8888, function () {
  console.log('\033[96m Server running at 127.0.0.1:8888 \033[39m');
});

// サーバー実装の前に、エラーハンドリングを記載します。
process.on('uncaughtException', function(err) {
    console.log(err);
});

//保管用の変数
var message_data =''; 

//シリアルポートを開く
serial.on('open',function(){
     console.log('open');
});

//シリアルポートで受け付けたデータを処理する
serial.on('data',function(data){
     var buffer = new Buffer(data, 'binary');
     //console.log(buffer.read());
     var hex = buffer.toString("hex");
     //念のため改行コード取り除く
     hex = hex.replace(/\r?\n/g,"");

     if(message_data ==''){
       //最初の状態は01だけか0114はじまりしか許可しない
       if(hex == "01" || hex.indexOf("0114") == 0){
         message_data +=hex;
       }
     }else if(message_data =='01'){
       //message_dataが01だけの場合、14はじまりしか許可しない
       if(hex.indexOf("14") == 0){
         message_data +=hex;
       }
     }else{
       //01だけ以外の何かしら値が入っている状態なので、いったん全て追加してOK
         message_data +=hex;
     }

     
     //0113が先頭から2回出現した直後に04が出たら解析へ
     if(strCount("0113",message_data) >= 2 && message_data.indexOf("04",message_data.indexOf("0113",message_data.indexOf("0113",0) + 1) + 1) >= 0){
       //console.log('\033[94m'+message_data+'\033[39m');
       
       //                                      1     2     3     4         5     6                       7       8       9     10    11    12    13    14    15    16    17
       //message_dataを解析                    Rx    Gx    Wx    wx        Z     MM                      XX   :  YY      A     B     b     C     D     d     P     R     vW
       var result = message_data.match(/011452(..)47(..)57(..)77(..)040113(..)02(..........).*0401134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(.*)02(....)04/);
       //                               011452 30 47 30 57 30 77 30 040113 4e 02    040113440220303a203102 2030203030 02 2030203030 02 31 02 31 02 2020 04         
       //console.log('\033[96m'+result+'\033[39m');
       // 受信したメッセージを全てのクライアントに送信する
       server.clients.forEach(function(client) {
           if(client != null && result != null && result.length > 6){
               //JSONデータ作成
  
               var d = new Date();
               var data = {
                 type: 'data',
                 user: 'FA-07',
                 invalidL: parseInt(convAscii(result[4])),
                 invalidR: parseInt(convAscii(result[3])),
                 hitL: parseInt(convAscii(result[1])),
                 hitR: parseInt(convAscii(result[2])),
                 status: 0,
                 timeRemain: convAscii(result[6]),
                 time: d.getFullYear()  + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + " " + d.getMilliseconds()
               };
  
               //点数用意
               if(result.length > 7){
                 data.scoreR = convAscii(result[7]);
               }
               if(result.length > 8){
                 data.scoreL = convAscii(result[8]);
               }
               
               //右選手イエローカード
               if(result.length > 9){
                 data.yellowR = convAscii(result[9]).replace(" ","");
               }
               //右選手レッドカード
               if(result.length > 10){
                 data.redR = convAscii(result[10]).replace(" ","");
               }
               
               //左選手イエローカード
               if(result.length > 12){
                 data.yellowL = convAscii(result[12]).replace(" ","");
               }
               //左選手レッドカード
               if(result.length > 13){
                 data.redL = convAscii(result[13]).replace(" ","");
               }               
               
               
               //priority用意 
               if(result.length > 15){
                 var priority = convAscii(result[15]);
                 var pL = 0;
                 var pR = 0;
                 if(priority == 1){
                   pR = 1;
                 }else if(priority == 2){
                   pL = 1;
                 }
                 data.priorityL = pL;
                 data.priorityR = pR;
               }

               //Round
               if(result.length > 16){
                 data.round = convAscii(result[16]);
               }               

               //送信
               client.send(JSON.stringify(data));
               console.log('\033[95m'+JSON.stringify(data)+'\033[39m');
  
           }
       });

       //メッセージをリセット
       message_data = '';
     }

});

// クライアントからの接続イベントを処理
server.on('connection', function(socket) {

  socket.on('close',function(){
	  console.log('\033[96mclose\033[39m');
  });

  socket.on('disconnect',function(){
	  console.log('\033[96mdisconnect\033[39m');
  });

  // クライアントからのメッセージ受信イベントを処理
  socket.on('message', function(data) {

    //JSON形式か判別
	if(isJSON(data)){
		// 実行時間を追加
	    var data = JSON.parse(data);
	    var d = new Date();
	    data.time = d.getFullYear()  + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
	    data = JSON.stringify(data);
	    console.log('\033[96m' + data + '\033[39m');

	    // 受信したメッセージを全てのクライアントに送信する
	    server.clients.forEach(function(client) {
	    	if(client != null){
	            client.send(data);
	        }
	    });
	}else{
		//JSON形式ではない
	    console.log('\033[96m不正なメッセージ'+data+'\033[39m');
	}
  });
});

//JSON形式かどうか判定
var isJSON = function(arg) {
    arg = (typeof arg === "function") ? arg() : arg;
    if (typeof arg  !== "string") {
        return false;
    }
    try {
    arg = (!JSON) ? eval("(" + arg + ")") : JSON.parse(arg);
        return true;
    } catch (e) {
        return false;
    }
};

//文字列の出現回数を取得する(探される方、探す文字)
var strCount = function(searchStr, str) {
    if (!searchStr || !str) return 0;
 
    var count = 0, pos = str.indexOf(searchStr);

    while (pos !== -1) {
        count++;
        pos = str.indexOf(searchStr, pos + count);
    }
 
    return count;
};

//2文字ずつ16進数の文字をASCIIコード文字に変換して戻す
var convAscii = function(hex) {
    if (!hex) return null;
    var list = splitByLength(hex,2);
    var rs ="";
    for (var i = 0; i < list.length; i++){
      rs += String.fromCharCode("0x"+list[i]);
    }

    return rs;
 
};

//文字列を、指定した文字数で分割して、配列で返すメソッド
var splitByLength = function (str, length) {
    var resultArr = [];
    if (!str || !length || length < 1) {
        return resultArr;
    }
    var index = 0;
    var start = index;
    var end = start + length;
    while (start < str.length) {
        resultArr[index] = str.substring(start, end);
        index++;
        start = end;
        end = start + length;
    }
    return resultArr;
};