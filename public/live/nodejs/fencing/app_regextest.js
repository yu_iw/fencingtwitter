

//判定用の文字列
//                 1     2     3     4     
//                 Rx    Gx    Wx    wx    
const m1  = /011452(..)47(..)57(..)77(..)04/;
const m1g = /011452(..)47(..)57(..)77(..)04/g;
//               1              2       3       4
//             Z(R |N |J |B )   MM(分)  MM(秒)  MM(ミリ秒)
//const m2 = /0113(52|4E|4A|42)02(.*)04/;
const m2  = /0113(52|4e|4a|42)02(....)..(....)..(....)04/;
const m2g = /0113(52|4e|4a|42)02(....)..(....)..(....)04/g;
//                   1       2       3     4     5     6     7     8     9     10    11
//                   XX   :  YY      A     B     b     C     D     d     P     R     vW      
const m3  = /01134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(....)02(....)04/;
const m3g = /01134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(....)02(....)04/g;
          //0113440220303a20300220302030300220302030300230023102202004
          //01134402 2033 3a 2033 02 2030  2030  30 02 2030  2030  30 02 30 02 2031 02 3030 04

const m4g = /01134902(..)02(..)02(..)02(..)04/g;
const m5g = /01134e4c02(.*)02(.*)02(......)04/g;
const m6g = /01134e5202(.*)02(.*)02(......)04/g;
const m7g = /01134d4302(.*)02(.*)02(.*)02(.*)04/g;
const m8g = /0113554602(........)02(..)02(..)04/g;
const m9g = /0113464302(.*)04/g;

//const m4g = /01134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(....)02(....)04/g;
//const m5g = /01134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(....)02(....)04/g;
//const m6g = /01134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(....)02(....)04/g;
//const m7g = /01134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(....)02(....)04/g;
//const m8g = /01134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(....)02(....)04/g;
//const m9g = /01134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(....)02(....)04/g;


//message_data ="011452304730573077300401134e0220323a34342e3537040113440220303a203002203020303002203020303002300220310230300401134e0220323a34342e353704";

message_d1 ="aaaa0114523047305730773004";
message_d2 ="01134e0220323a34342e353704";
message_d3 ="0113440220303a2030022030203030022030203030023002203102303004";
message_d4 ="011349023002330230023004";
message_d5 ="01134e4c02020204";
message_d6 ="01134e5202020204";
message_d7 ="01134d43020202020";
message_d8 ="0113554602303a30300230023004";
message_d9 ="0113464302020204bbbb";

message_data = message_d1 + message_d2 + message_d3 + message_d4 + message_d5 + message_d6 + message_d7 + message_d8 + message_d9;

       console.log('\033[94m message_data='+message_data+'\033[39m');

       var result = judgeMessage(message_data);
       //使ったメッセージは切り取り、残りは次にまわしてあげる
       restMessage = removeMessage(message_data);
       console.log("restMessage="+restMessage);






function judgeMessage(message_data){

	var data = {};
	if(m1.test(message_data)){
		var r1 = message_data.match(m1g);
		console.log(r1.length);
		//複数入っている場合は配列の一番最後だけを使う
		result1 = r1[r1.length-1].match(m1);
		data.hitL = result1[1];
		data.hitR = result1[2];
		data.invalidR = result1[3];
		data.invalidL = result1[4];
		console.log('\033[96m m1.result1='+result1+'\033[39m');

	};
	if(m2.test(message_data)){
		var r2 = message_data.match(m2g);
		//複数入っている場合は配列の一番最後だけを使う
		result2 = r2[r2.length-1].match(m2);
		data.timeRemainMinute=result2[2];
		data.timeRemainSecond=result2[3];
		data.timeRemainMill=result2[4];
		console.log('\033[96m m2.result2='+result2+'\033[39m');
	}
	if(m3.test(message_data)){
		var r3 = message_data.match(m3g);
		//複数入っている場合は配列の一番最後だけを使う
		result3 = r3[r3.length-1].match(m3);
                data.scoreR = result3[1];
                data.scoreL = result3[2];
                data.yellowR = result3[3];
                data.redR = result3[4];
                data.yellowL = result3[6];
                data.redL = result3[7];
                data.priority = result3[9];
                data.round = result3[10];
		console.log('\033[96m m3.result3='+result3+'\033[39m');
	}
	return data;
}

function removeMessage(message_data){
	var tmp = message_data;
	tmp = tmp.replace(m1g,'');
	tmp = tmp.replace(m2g,'');
	tmp = tmp.replace(m3g,'');
	tmp = tmp.replace(m4g,'');
	tmp = tmp.replace(m5g,'');
	tmp = tmp.replace(m6g,'');
	tmp = tmp.replace(m7g,'');
	tmp = tmp.replace(m8g,'');
	tmp = tmp.replace(m9g,'');
	return tmp;
}
