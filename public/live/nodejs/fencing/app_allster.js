var SerialPort = require('serialport').SerialPort;
var serial = new SerialPort('COM6',{
     baudrate:38400,
     dataBits:8, 
     parity:'none', 
     stopBits: 1,
     flowControl: false
});

//websocket
// 8888番ポートでクライアントの接続を待ち受ける
var ws = require('websocket.io');
var server = ws.listen(8888, function () {
  console.log('\033[96m Server running at 127.0.0.1:8888 \033[39m');
});

// サーバー実装の前に、エラーハンドリングを記載します。
process.on('uncaughtException', function(err) {
    console.log(err);
});

//保管用の変数
var message_data =''; 

//判定用の文字列
//                 1     2     3     4     
//                 Rx    Gx    Wx    wx    
const m1 = /011452(..)47(..)57(..)77(..)04/;
const m1g = /011452(..)47(..)57(..)77(..)04/g;
//               1              2       3       4
//             Z(R |N |J |B )   MM(分)  MM(秒)  MM(ミリ秒)
//const m2 = /0113(52|4E|4A|42)02(.*)04/;
const m2 = /0113(52|4e|4a|42)02(....)..(....)..(....)04/;
const m2g = /0113(52|4e|4a|42)02(....)..(....)..(....)04/g;
//                   1       2       3     4     5     6     7     8     9     10    11
//                   XX   :  YY      A     B     b     C     D     d     P     R     vW      
const m3 = /01134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(....)02(....)04/;
const m3g = /01134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(....)02(....)04/g;
          //0113440220303a20300220302030300220302030300230023102202004
          //01134402 2033 3a 2033 02 2030  2030  30 02 2030  2030  30 02 30 02 2031 02 3030 04
const m4g = /01134902(..)02(..)02(..)02(..)04/g;
const m5g = /01134e4c02(.*)02(.*)02(......)04/g;
const m6g = /01134e5202(.*)02(.*)02(......)04/g;
const m7g = /01134d4302(.*)02(.*)02(.*)02(.*)04/g;
const m8g = /0113554602(........)02(..)02(..)04/g;
const m9g = /0113464302(.*)04/g;

//シリアルポートを開く
serial.on('open',function(){
     console.log('open');
});

//シリアルポートで受け付けたデータを処理する
serial.on('data',function(data){
     var buffer = new Buffer(data, 'binary');
     //console.log(buffer.read());
     var hex = buffer.toString("hex");
     //念のため改行コード取り除く
     hex = hex.replace(/\r?\n/g,"");
     console.log('\033[94m hex='+hex+'\033[39m');
     
     //入力文字列を足しこむ
     message_data +=hex;
     console.log('\033[94m message_data_pre='+message_data+'\033[39m');


     
     //m1/m2/m3どれかに該当したら解析・送信へ
     if(m1.test(message_data) || m2.test(message_data) || m3.test(message_data)){
       
	//0114523047305730773004
	//01134e0220373a303004
	//0113440220303a20300220302030300220302030300230023102202004

       var result = judgeMessage(message_data);
       //console.log('\033[96m result='+result+'\033[39m');
       //送信
       sendJSON(result);

       //使ったメッセージは切り取り、残りは次にまわしてあげる
       message_data = removeMessage(message_data);
       console.log('\033[94m message_data_after='+message_data+'\033[39m');

     }

});

// クライアントからの接続イベントを処理
server.on('connection', function(socket) {

  socket.on('close',function(){
	  console.log('\033[96mclose\033[39m');
  });

  socket.on('disconnect',function(){
	  console.log('\033[96mdisconnect\033[39m');
  });

  // クライアントからのメッセージ受信イベントを処理
  socket.on('message', function(data) {

    //JSON形式か判別
	if(isJSON(data)){
		// 実行時間を追加
	    var data = JSON.parse(data);
	    var d = new Date();
	    data.time = d.getFullYear()  + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
	    data = JSON.stringify(data);
	    console.log('\033[96m' + data + '\033[39m');

	    // 受信したメッセージを全てのクライアントに送信する
	    server.clients.forEach(function(client) {
	    	if(client != null){
	            client.send(data);
	        }
	    });
	}else{
		//JSON形式ではない
	    console.log('\033[96m不正なメッセージ'+data+'\033[39m');
	}
  });
});

var judgeMessage = function(message_data){

	var data = {};
	if(m1.test(message_data)){
		var r1 = message_data.match(m1g);
		//複数入っている場合は配列の一番最後だけを使う
		result1 = r1[r1.length-1].match(m1);
		data.hitL = result1[1];
		data.hitR = result1[2];
		data.invalidR = result1[3];
		data.invalidL = result1[4];
		console.log('\033[96m m1.result1='+result1+'\033[39m');
		//console.log('\033[96m m1.result1[1]='+result1[1]+'\033[39m');
		//console.log('\033[96m m1.result1[2]='+result1[2]+'\033[39m');
		//console.log('\033[96m m1.result1[3]='+result1[3]+'\033[39m');
		//console.log('\033[96m m1.result1[4]='+result1[4]+'\033[39m');
		//console.log('\033[96m m1.data='+data+'\033[39m');
		//console.log('\033[96m m1.data.hitL='+data.hitL+'\033[39m');
		//console.log('\033[96m m1.data.hitR='+data.hitR+'\033[39m');
		//console.log('\033[96m m1.data.invalidR='+data.invalidR+'\033[39m');
		//console.log('\033[96m m1.data.invalidL='+data.invalidL+'\033[39m');
	};
	if(m2.test(message_data)){
		var r2 = message_data.match(m2g);
		//複数入っている場合は配列の一番最後だけを使う
		result2 = r2[r2.length-1].match(m2);
		data.timeRemainMinute=result2[2];
		data.timeRemainSecond=result2[3];
		data.timeRemainMill=result2[4];
		console.log('\033[96m m2.result2[2]='+result2[2]+'\033[39m');
		console.log('\033[96m m2.result2[3]='+result2[3]+'\033[39m');
	}
	if(m3.test(message_data)){
		var r3 = message_data.match(m3g);
		//複数入っている場合は配列の一番最後だけを使う
		result3 = r3[r3.length-1].match(m3);
                data.scoreR = result3[1];
                data.scoreL = result3[2];
                data.yellowR = result3[3];
                data.redR = result3[4];
                data.yellowL = result3[6];
                data.redL = result3[7];
                data.priority = result3[9];
                data.round = result3[10];
		console.log('\033[96m m3.result3[1]='+result3[1]+'\033[39m');
	}
	return data;
}

var removeMessage = function(message_data){
	var tmp = message_data;
	tmp = tmp.replace(m1g,'');
	tmp = tmp.replace(m2g,'');
	tmp = tmp.replace(m3g,'');
	tmp = tmp.replace(m4g,'');
	tmp = tmp.replace(m5g,'');
	tmp = tmp.replace(m6g,'');
	tmp = tmp.replace(m7g,'');
	tmp = tmp.replace(m8g,'');
	tmp = tmp.replace(m9g,'');
	return tmp;
}

var sendJSON = function(result){
       // 受信したメッセージを全てのクライアントに送信する
       server.clients.forEach(function(client) {
           if(client != null && result != null){

               //JSONデータ作成
               console.log('\033[97m result.invalidL= '+result['invalidL']+'\033[39m');
               console.log('\033[97m result.invalidR= '+result['invalidR']+'\033[39m');
               console.log('\033[97m result.hitL= '+result['hitL']+'\033[39m');
               console.log('\033[97m result.hitR= '+result['hitR']+'\033[39m');
               console.log('\033[97m result.timeRemainMinute= '+result['timeRemainMinute']+'\033[39m');
               console.log('\033[97m result.timeRemainSecond= '+result['timeRemainSecond']+'\033[39m');
               console.log('\033[97m result.timeRemainMill= '+result['timeRemainMill']+'\033[39m');
               console.log('\033[97m result.scoreR= '+result['scoreR']+'\033[39m');
               console.log('\033[97m result.scoreL= '+result['scoreL']+'\033[39m');
               console.log('\033[97m result.yellowR= '+result['yellowR']+'\033[39m');
               console.log('\033[97m result.redR= '+result['redR']+'\033[39m');
               console.log('\033[97m result.yellowL= '+result['yellowL']+'\033[39m');
               console.log('\033[97m result.redL= '+result['redL']+'\033[39m');
               console.log('\033[97m result.priority= '+result['priority']+'\033[39m');
               console.log('\033[97m result.round= '+result['round']+'\033[39m');


               var d = new Date();

               var data = {
                 type: 'data',
                 user: 'FA-07',
                 invalidL: result['invalidL'] === undefined ? '' : parseInt(convAscii(result['invalidL'])),
                 invalidR: result['invalidR'] === undefined ? '' : parseInt(convAscii(result['invalidR'])),
                 hitL: result['hitL'] === undefined ? '' : parseInt(convAscii(result['hitL'])),
                 hitR: result['hitR'] === undefined ? '' :parseInt(convAscii(result['hitR'])),
                 status: 0,
                 time: d.getFullYear()  + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + " " + d.getMilliseconds()
               };

		
               //時間調整 isNan(A)はAが数値でない場合にtrue
               if(result['timeRemainMinute'] != null && !isNaN(convAscii(result['timeRemainMinute']))
		  && result['timeRemainSecond'] != null && !isNaN(convAscii(result['timeRemainSecond']))){
               	   //分・秒に数値が入っている状態
		   var minute = parseInt(convAscii(result['timeRemainMinute']));
                   var second = parseInt(convAscii(result['timeRemainSecond']));
                   var millsecond;
                   if(result['timeRemainMill'] != null && !isNaN(convAscii(result['timeRemainMill']))
			&& parseInt(convAscii(result['timeRemainMill'])) > 0){
			//ミリ秒は0を超える数値が入っているので秒を切り上げ
			if(second + 1 >= 60){
				minute = minute + 1;
				second = 0;
			}else{
				second = second + 1;
			}
		   }
		   //時間設定
	           data.timeRemain = minute +":"+ ('0' + second).slice(-2);
	       }



               //点数用意
               data.scoreR = result['scoreR'] === undefined ? '' : convAscii(result['scoreR']).replace(" ","");
               data.scoreL = result['scoreL'] === undefined ? '' : convAscii(result['scoreL']).replace(" ","");
               
               //右選手イエローカード
               data.yellowR = result['yellowR'] === undefined ? '' : convAscii(result['yellowR']).replace(" ","");
               //右選手レッドカード
               data.redR = result['redR'] === undefined ? '' : convAscii(result['redR']).replace(" ","");
               
               //左選手イエローカード
               data.yellowL = result['yellowL'] === undefined ? '' : convAscii(result['yellowL']).replace(" ","");
               //左選手レッドカード
               data.redL = result['redL'] === undefined ? '' : convAscii(result['redL']).replace(" ","");
               
               
               //priority用意 
               var priority = result['priority'] === undefined ? '' : convAscii(result['priority']);
               var pL = "";
               var pR = "";
               if(priority == 1){
                 pR = 1;
               }else if(priority == 2){
                 pL = 1;
               }else if(result['priority'] !== undefined){
                 pL = 0;
                 pR = 0;
               }
               data.priorityL = pL;
               data.priorityR = pR;
               
               //Round
               data.round = result['round'] === undefined ? '' : convAscii(result['round']).replace(" ","");
               
               //送信
               client.send(JSON.stringify(data));
               console.log('\033[95m'+JSON.stringify(data)+'\033[39m');
  
           }
       });
}


//JSON形式かどうか判定
var isJSON = function(arg) {
    arg = (typeof arg === "function") ? arg() : arg;
    if (typeof arg  !== "string") {
        return false;
    }
    try {
    arg = (!JSON) ? eval("(" + arg + ")") : JSON.parse(arg);
        return true;
    } catch (e) {
        return false;
    }
};

//文字列の出現回数を取得する(探される方、探す文字)
var strCount = function(searchStr, str) {
    if (!searchStr || !str) return 0;
 
    var count = 0, pos = str.indexOf(searchStr);

    while (pos !== -1) {
        count++;
        pos = str.indexOf(searchStr, pos + count);
    }
 
    return count;
};

//2文字ずつ16進数の文字をASCIIコード文字に変換して戻す
var convAscii = function(hex) {
    if (!hex) return "";
    var list = splitByLength(hex,2);
    var rs ="";
    for (var i = 0; i < list.length; i++){
      rs += String.fromCharCode("0x"+list[i]);
    }

    return rs;
 
};

//文字列を、指定した文字数で分割して、配列で返すメソッド
var splitByLength = function (str, length) {
    var resultArr = [];
    if (!str || !length || length < 1) {
        return resultArr;
    }
    var index = 0;
    var start = index;
    var end = start + length;
    while (start < str.length) {
        resultArr[index] = str.substring(start, end);
        index++;
        start = end;
        end = start + length;
    }
    return resultArr;
};