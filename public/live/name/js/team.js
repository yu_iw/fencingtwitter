/* ++++++++++++++++++++++++++++++++++++++++

	NAME

++++++++++++++++++++++++++++++++++++++++ */


$(function () {
    //BOS
    var data = {};
    var id = $('.match').attr('id');
    var e = $('#' + id);
    //console.log(e);

    //デフォルト読み込み
    loadData(e, "match1");
    $('.roundName').text("match1");x

    //データ保存
    $('.save').on('click', function () {
        data = setData(e, data);
        data.file = $(this).attr('file');
        //console.log(data);
        saveData(e, data);
    });

    //データ読み込み
    $('.load').on('click', function () {
        if (confirm("現在の値に上書きされます。大丈夫ですか？")) {
        } else {
            return false;
        }
        filename = $(this).attr('file');
        loadData(e, filename);
        $('.roundName').text(filename);
    });

    //プレイヤー並び替え
    $('.arrow').on('click', function () {
        var name = $(this).parents("td").find(".name").val();
        var id = $(this).parents("td").find(".id").val();
        var numberStr = $(this).parents("tr").attr('class');//player1
        number = parseInt(numberStr.replace(/[^0-9]/g, ''));//1

        var side = $(this).parents("td").attr('class');//inputLeft
        if ($(this).attr("value") === "up") {
            targetNum = number - 1;
        } else if ($(this).attr("value") === "down") {
            targetNum = number + 1;
        } else if ($(this).attr("value") === "dummy") {
            return false;
        }
        var target = $(this).parents("tbody").find(".player" + targetNum).find("." + side);

        targetName = target.find(".name").val();
        targetId = target.find(".id").val();
        $(this).parents("td").find(".name").val(targetName);
        $(this).parents("td").find(".id").val(targetId);
        target.find(".name").val(name);
        target.find(".id").val(id);
        data = setData(e, data);
        viewData(e, data);
    });

    //自動テキスト変更の停止
    $('tr[class^="player"] input').keyup(function () {
        if (!$('.manualInput').prop('checked')) {
            data = setData(e, data);
            viewData(e, data);
        } else {
        }
    });

    //左右入れ替え
    $(".reverse").on("click", function () {
        data = setData(e, data);
        e.find(".playerHeader .inputLeft .nation").val(data.right.nation);
        e.find(".playerHeader .inputRight .nation").val(data.left.nation);
        e.find(".player1 .inputLeft .draw").text(data.right.player1.draw);
        e.find(".player1 .inputLeft .name").val(data.right.player1.name);
        e.find(".player1 .inputLeft .id").val(data.right.player1.id);
        e.find(".player1 .inputRight .draw").text(data.left.player1.draw);
        e.find(".player1 .inputRight .name").val(data.left.player1.name);
        e.find(".player1 .inputRight .id").val(data.left.player1.id);
        e.find(".player2 .inputLeft .draw").text(data.right.player2.draw);
        e.find(".player2 .inputLeft .name").val(data.right.player2.name);
        e.find(".player2 .inputLeft .id").val(data.right.player2.id);
        e.find(".player2 .inputRight .draw").text(data.left.player2.draw);
        e.find(".player2 .inputRight .name").val(data.left.player2.name);
        e.find(".player2 .inputRight .id").val(data.left.player2.id);
        e.find(".player3 .inputLeft .draw").text(data.right.player3.draw);
        e.find(".player3 .inputLeft .name").val(data.right.player3.name);
        e.find(".player3 .inputLeft .id").val(data.right.player3.id);
        e.find(".player3 .inputRight .draw").text(data.left.player3.draw);
        e.find(".player3 .inputRight .name").val(data.left.player3.name);
        e.find(".player3 .inputRight .id").val(data.left.player3.id);
        e.find(".playerReserve .inputLeft .draw").text(data.right.playerReserve.draw);
        e.find(".playerReserve .inputLeft .name").val(data.right.playerReserve.name);
        e.find(".playerReserve .inputLeft .id").val(data.right.playerReserve.id);
        e.find(".playerReserve .inputRight .draw").text(data.left.playerReserve.draw);
        e.find(".playerReserve .inputRight .name").val(data.left.playerReserve.name);
        e.find(".playerReserve .inputRight .id").val(data.left.playerReserve.id);
        e.find(".inputLeft .reserve").prop('checked', false);
        e.find(".inputRight .reserve").prop('checked', false);
        e.find("." + data.right.reserve + " .inputLeft .reserve").prop('checked', true);
        e.find("." + data.left.reserve + " .inputRight .reserve").prop('checked', true);

        data = setData(e, data);
        viewData(e, data);
    });
    //リザーブ
    $(".reserve").on("click", function () {
        if ($(this).prop('checked')) {
            var name = $(this).attr('name');
            //console.log(name);
            $('[name="' + name + '"]').prop('checked', false);  //  全部のチェックを外す
            $(this).prop('checked', true);  //  押したやつだけチェックつける
        }
        data = setData(e, data);
        viewData(e, data);
    });
    //全消去
    $(".reset").on("click", function () {
        if (confirm("現在の値に上書きされます。大丈夫ですか？")) {
        } else {
            return false;
        }
        e.find("input").val('');
        data = setData(e, data);
        viewData(e, data);
    });
    //一時左右切り替え
    $(".swap").on("change", function () {
        data = setData(e, data);
        viewData(e, data);
    });

    //match~.jsonからデータを読み込みフォームに入力
    function loadData(e, filename) {
        e.find(".reserve").prop('checked', false);
        /*$.getJSON("http://fencing-twitter.grpht.co.jp/live/data/" + filename + ".json", function (json) {
            console.log(json);
            data = json;
        });*/
        $.ajax({
            type: "POST",
            cache: false,
            scriptCharset: 'utf-8',
            dataType: 'json',
            url: "../data/" + filename + ".json?id=" + Math.random(),
            success: function (res) {
                console.debug(res[0]); //ばっちり読み込み成功！
                data = res;
                e.find(".weapon").val(data.weapon);
                e.find(".playerHeader .inputLeft .nation").val(data.left.nation);
                e.find(".playerHeader .inputRight .nation").val(data.right.nation);
                e.find(".player1 .inputLeft .draw").text(data.left.player1.draw);
                e.find(".player1 .inputLeft .name").val(data.left.player1.name);
                e.find(".player1 .inputLeft .id").val(data.left.player1.id);
                e.find(".player1 .inputRight .draw").text(data.right.player1.draw);
                e.find(".player1 .inputRight .name").val(data.right.player1.name);
                e.find(".player1 .inputRight .id").val(data.right.player1.id);
                e.find(".player2 .inputLeft .draw").text(data.left.player2.draw);
                e.find(".player2 .inputLeft .name").val(data.left.player2.name);
                e.find(".player2 .inputLeft .id").val(data.left.player2.id);
                e.find(".player2 .inputRight .draw").text(data.right.player2.draw);
                e.find(".player2 .inputRight .name").val(data.right.player2.name);
                e.find(".player2 .inputRight .id").val(data.right.player2.id);
                e.find(".player3 .inputLeft .draw").text(data.left.player3.draw);
                e.find(".player3 .inputLeft .name").val(data.left.player3.name);
                e.find(".player3 .inputLeft .id").val(data.left.player3.id);
                e.find(".player3 .inputRight .draw").text(data.right.player3.draw);
                e.find(".player3 .inputRight .name").val(data.right.player3.name);
                e.find(".player3 .inputRight .id").val(data.right.player3.id);
                e.find(".playerReserve .inputLeft .draw").text(data.left.playerReserve.draw);
                e.find(".playerReserve .inputLeft .name").val(data.left.playerReserve.name);
                e.find(".playerReserve .inputLeft .id").val(data.left.playerReserve.id);
                e.find(".playerReserve .inputRight .draw").text(data.right.playerReserve.draw);
                e.find(".playerReserve .inputRight .name").val(data.right.playerReserve.name);
                e.find(".playerReserve .inputRight .id").val(data.right.playerReserve.id);
                e.find("." + data.left.reserve + " .inputLeft .reserve").prop('checked', true);
                e.find("." + data.right.reserve + " .inputRight .reserve").prop('checked', true);
                data = setData(e, data);
                viewData(e, data);
            },
            error: function () { console.log('Miss..'); }
        });
    }
    
    //入力フォーム部分からデータ読み込み
    function setData(e) {
        var reserveLeft = $("input[name='reserveLeft']:checked").parents('tr').attr('class');
        var reserveRight = $("input[name='reserveRight']:checked").parents('tr').attr('class');
        data = {
            "weapon": e.find(".weapon").val(),
            "left": {
                "nation": e.find(".playerHeader .inputLeft .nation").val(),
                "player1": {
                    "draw": e.find(".player1 .inputLeft .draw").text(),
                    "name": e.find(".player1 .inputLeft .name").val(),
                    "id": e.find(".player1 .inputLeft .id").val()
                },
                "player2": {
                    "draw": e.find(".player2 .inputLeft .draw").text(),
                    "name": e.find(".player2 .inputLeft .name").val(),
                    "id": e.find(".player2 .inputLeft .id").val()
                },
                "player3": {
                    "draw": e.find(".player3 .inputLeft .draw").text(),
                    "name": e.find(".player3 .inputLeft .name").val(),
                    "id": e.find(".player3 .inputLeft .id").val()
                },
                "playerReserve": {
                    "draw": e.find(".playerReserve .inputLeft .draw").text(),
                    "name": e.find(".playerReserve .inputLeft .name").val(),
                    "id": e.find(".playerReserve .inputLeft .id").val()
                },
                "reserve": reserveLeft ? reserveLeft : null
            },
            "right": {
                "nation": e.find(".playerHeader .inputRight .nation").val(),
                "player1": {
                    "draw": e.find(".player1 .inputRight .draw").text(),
                    "name": e.find(".player1 .inputRight .name").val(),
                    "id": e.find(".player1 .inputRight .id").val()
                },
                "player2": {
                    "draw": e.find(".player2 .inputRight .draw").text(),
                    "name": e.find(".player2 .inputRight .name").val(),
                    "id": e.find(".player2 .inputRight .id").val()
                },
                "player3": {
                    "draw": e.find(".player3 .inputRight .draw").text(),
                    "name": e.find(".player3 .inputRight .name").val(),
                    "id": e.find(".player3 .inputRight .id").val()
                },
                "playerReserve": {
                    "draw": e.find(".playerReserve .inputRight .draw").text(),
                    "name": e.find(".playerReserve .inputRight .name").val(),
                    "id": e.find(".playerReserve .inputRight .id").val()
                },
                "reserve": reserveRight ? reserveRight : null
            }

        };
        return data;
    }

    //setData()で読み込んだdataを対戦順部分に出力
    function viewData(e, data) {
        console.log(data.left.reserve);
        console.log(data.right.reserve);
        //どの選手にリザーブが適用されているか判別
        reserveLeft = data.left.reserve;
        reserveRight = data.right.reserve;
        if (reserveLeft != null) {
            data.left[reserveLeft] = data.left.playerReserve
        }
        if (reserveRight != null) {
            data.right[reserveRight] = data.right.playerReserve
        }
        //団体戦の対戦順
        if ($(".player1 .inputLeft .draw").text() === "1") {
            align = {
                1: { "l": 3, "r": 3 },
                2: { "l": 1, "r": 2 },
                3: { "l": 2, "r": 1 },
                4: { "l": 1, "r": 3 },
                5: { "l": 3, "r": 1 },
                6: { "l": 2, "r": 2 },
                7: { "l": 1, "r": 1 },
                8: { "l": 2, "r": 3 },
                9: { "l": 3, "r": 2 }
            }
            console.log("並び1");
        } else {
            align = {
                1: { "l": 3, "r": 3 },
                2: { "l": 2, "r": 1 },
                3: { "l": 1, "r": 2 },
                4: { "l": 3, "r": 1 },
                5: { "l": 1, "r": 3 },
                6: { "l": 2, "r": 2 },
                7: { "l": 1, "r": 1 },
                8: { "l": 3, "r": 2 },
                9: { "l": 2, "r": 3 }
            }
            console.log("並び2");

        }
        //console.log($(".arrayShift").prop('checked'));
        /*Object.keys(align).forEach(function (key) {
            //console.log(key + ':' + align[key["l"]]);
            //console.log(key + ':' + align[key["r"]]);
            l = "player" + align[key]["l"];
            r = "player" + align[key]["r"];
            e.find(".match" + key + " .matchLeft").attr("name", l);
            e.find(".match" + key + " .matchRight").attr("name", r);
            e.find(".match" + key + " .matchLeft .name").val(data.left[l].name);
            e.find(".match" + key + " .matchLeft .id").val(data.left[l].id);
            e.find(".match" + key + " .matchRight .name").val(data.right[r].name);
            e.find(".match" + key + " .matchRight .id").val(data.right[r].id);
            e.find(".match" + key + " .matchLeft").attr("portrait", "left_" + l);
            e.find(".match" + key + " .matchRight").attr("portrait", "right_" + r);
        });*/
        //対戦順配列のとおりにBout1-bout9へ出力
        Object.keys(align).forEach(function (key) {
            var swap = false;
            swap = e.find(".match" + key + " input.swap").prop("checked");
            console.log(swap);

            l = "player" + align[key]["l"];
            r = "player" + align[key]["r"];
            console.log(l);
            console.log(r);
            e.find(".match" + key + " .matchLeft").attr("name", l);
            e.find(".match" + key + " .matchRight").attr("name", r);
            e.find(".match" + key + " .matchLeft .draw").text(data.left[l].draw);
            e.find(".match" + key + " .matchLeft .name").val(data.left[l].name);
            e.find(".match" + key + " .matchLeft .id").val(data.left[l].id);
            e.find(".match" + key + " .matchRight .draw").text(data.right[r].draw);
            e.find(".match" + key + " .matchRight .name").val(data.right[r].name);
            e.find(".match" + key + " .matchRight .id").val(data.right[r].id);
            e.find(".match" + key + " .matchLeft").attr("portrait", "left_" + l);
            e.find(".match" + key + " .matchRight").attr("portrait", "right_" + r);
            if (swap) {
                playerL = e.find('.match' + key + ' .matchLeft').attr('name');
                playerR = e.find('.match' + key + ' .matchRight').attr('name');
                drawL = e.find('.match' + key + ' .matchLeft .draw').text();
                drawR = e.find('.match' + key + ' .matchRight .draw').text();
                nameL = e.find('.match' + key + ' .matchLeft .name').val();
                nameR = e.find('.match' + key + ' .matchRight .name').val();
                idL = e.find('.match' + key + ' .matchLeft .id').val();
                idR = e.find('.match' + key + ' .matchRight .id').val();
                portraitL = e.find('.match' + key + ' .matchLeft').attr('portrait');
                portraitR = e.find('.match' + key + ' .matchRight').attr('portrait');
                console.log(playerL);
                console.log('.match' + key + ' .matchLeft')
                e.find('.match' + key + ' .matchLeft').attr('name', playerR);
                e.find('.match' + key + ' .matchRight').attr('name', playerL);
                e.find('.match' + key + ' .matchLeft .draw').text(drawR);
                e.find('.match' + key + ' .matchRight .draw').text(drawL);
                e.find('.match' + key + ' .matchLeft .name').val(nameR);
                e.find('.match' + key + ' .matchRight .name').val(nameL);
                e.find('.match' + key + ' .matchLeft .id').val(idR);
                e.find('.match' + key + ' .matchRight .id').val(idL);
                e.find('.match' + key + ' .matchLeft').attr('portrait', portraitR);
                e.find('.match' + key + ' .matchRight').attr('portrait', portraitL);
            }
        });
        if (reserveLeft != null) {
            e.find('.matchLeft[name="' + reserveLeft + '"]').attr("portrait", "left_playerReserve");
        }
        if (reserveRight != null) {
            e.find('.matchLeft[name="' + reserveLeft + '"]').attr("portrait", "right_playerReserve");
        }
    }

    //match~.jsonに上書き保存
    function saveData(e, data) {
        if (confirm("上書き保存されます。大丈夫ですか？")) {
        } else {
            return false;
        }
        var json = JSON.stringify(data);
        console.log(json);

        //ajax通信
        $.ajax({
            type: "POST",
            url: "team.php",
            contentType: "Content-Type: application/json; charset=UTF-8",
            data: json,
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("ajax通信に失敗しました");
                //失敗した時の処理
            },
            success: function (response) {
                console.log("ajax通信に成功しました");
                //成功した時の処理
            }
        })
    }
    //EOS
});