/* ++++++++++++++++++++++++++++++++++++++++

	Administrator

++++++++++++++++++++++++++++++++++++++++ */

$(function () {
    //BOS

    /* ----------------------------------------
        WebSocketサーバに接続
    ---------------------------------------- */

 //var ws1 = new WebSocket('ws://192.168.100.102:8888/');
    var ws1 = new WebSocket('ws://127.0.0.1:8888/');

    var self = {
        name: 'Administrator'
    };

    /* エラー処理
    ---------------------------------------- */

    ws1.onerror = function (e) {
        $('#log').prepend('<div>サーバ1に接続できませんでした。</div>');
    };

    /* WebSocketサーバ接続イベント
    ---------------------------------------- */

    ws1.onopen = function () {
        ws1.send(JSON.stringify({
            type: 'join',
            user: self.name
        }));
    };


    /* ----------------------------------------
        データ送信
    ---------------------------------------- */

    $('.send').on('click', function (e) {
        console.log('test');
        var data = {
            "green1": {
                "left": "",
                "left_country": "",
                "right": "",
                "right_country": ""
            },
        };

        //var v = parseInt(this.value),
        var v = 1,
            flgSwap2 = $(this).parents('td').find('.swap').prop('checked'),
            flgSwap = true,
            $this = $(this);
        console.log(flgSwap2);
        if (v > 0) {
            var playerR = $this.parents('tr').find('.matchLeft'),
                playerL = $this.parents('tr').find('.matchRight'),
                round = $('.roundName').text(),
                id = {
                    l: playerL.attr('name'),
                    r: playerR.attr('name')
                },
                name = {
                    l: playerL.find('.name').val(),
                    r: playerR.find('.name').val()
                },
                nation = {
                    l: $('.playerHeader .inputLeft .nation').val(),
                    r: $('.playerHeader .inputRight .nation').val()
                },
                portrait = {
                    l: playerL.find('.id').val(),
                    r: playerR.find('.id').val()
                },
                weapon = $('.weapon').val();

            switch (v) {
                case 1:
                    ws1.send(JSON.stringify({
                        type: 'admin',
                        user: self.name,
                        piste: v,
                        weapon: weapon,
                        round: round,
                        idL: (!flgSwap) ? id.l : id.r,
                        nameL2: (!flgSwap) ? name.l : name.r,
                        nationL2: (!flgSwap2) ? nation.l : nation.r,
                        portraitL: (!flgSwap) ? portrait.l : portrait.r,
                        idR: (!flgSwap) ? id.r : id.l,
                        nameR2: (!flgSwap) ? name.r : name.l,
                        nationR2: (!flgSwap2) ? nation.r : nation.l,
                        portraitR: (!flgSwap) ? portrait.r : portrait.l
                    }));
                    data.green1 = {
                        "left": (!flgSwap) ? name.l : name.r,
                        "left_country": (!flgSwap) ? nation.l : nation.r,
                        "right": (!flgSwap) ? name.r : name.l,
                        "right_country": (!flgSwap) ? nation.r : nation.l
                    };
                    break;
            }
        }
        // サイトパブリス対応
        /*
        $.ajax({
            type: "post",
            url: '../../../../request.php',
            // contentType: 'application/json',
            // dataType: "json",
            // cache: false,
            data: data // JSONデータ本体
        })
            // Ajaxリクエストが成功した時発動
            .done((data) => {
                console.log(data);
            })
            // Ajaxリクエストが失敗した時発動
            .fail((data) => {
                alert('error: ' + data);
            })
            // Ajaxリクエストが成功・失敗どちらでも発動
            .always((data) => {

            });
            */

    });

    $('.portraitToggle').on('click', function (e) {
        ws1.send(JSON.stringify({
            type: 'control',
            user: self.name,
            data: "portraitToggle",
        }));
        $(this).toggleClass('disabled');
        console.log("a");
        console.log(JSON);
    });
    $('.signalToggle').on('click', function (e) {
        ws1.send(JSON.stringify({
            type: 'control',
            user: self.name,
            data: "signalToggle",
        }));
        $(this).toggleClass('disabled');
    });

    /* ----------------------------------------
        ブラウザ終了イベント
    ---------------------------------------- */

    window.onbeforeunload = function () {
        for (var i = 0; i < wsLen; i++) {
            ws[i].send(JSON.stringify({
                type: 'defect',
                user: self.name,
            }));
        }
    };


    //EOS
});
