■Macの場合（名前表示システム不使用版）
1.パラニ子機(赤)をUSBポートに接続する（審判機にもパラニ親機(黒)を接続して電源をつけておく）

2.デスクトップのstart.commandをダブルクリックし
"Server running at 127.0.0.1:8888
open"
と出たらnodejs正常起動、7.へ
失敗したらcontrol+Cを入力し、3.へ

3.terminal.appを開き、ls /dev/tty.usbserialまで打ち込み、Tabを押す

4.デスクトップ/Fencing/display_html/data/config.jsを開く

5.port:'***'の***部分を、3.の/dev/tty.usbserial-******に書き換える

6. 2.に戻り同じ手順を行う（失敗が繰り返す場合はパラニ子機の電源が入っているか、しっかりささっているか確認する）

7.選手名は4.のconfig.jsを開き、left:'***' right:'***'となっている部分を書き換える


7.Google chromeを開く

8.デスクトップ/Fencing/display_html/display_j.htmlをドラッグ

9.Command + control + F → Command + Shift + F の順に入力

10. Command + R で更新 （以後、1.〜7. の操作を行ったときはブラウザを更新すると反映される）

