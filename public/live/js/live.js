/* ++++++++++++++++++++++++++++++++++++++++

	Display

++++++++++++++++++++++++++++++++++++++++ */

$(function () {
	//BOS


	/* ----------------------------------------
		Global Variables
	---------------------------------------- */

	var $window = $(window),
		imgPath = 'img/',
		cur = {
			weapon: '',
			idL: '',
			idR: '',
			nameL: '',
			nameR: '',
			nationL: '',
			nationR: '',
			scoreL: -1,
			scoreR: -1,
			yellowL: -1,
			yellowR: -1,
			redL: -1,
			redR: -1,
			status: '',
			timeRemain: '0:00',
			Round: ''
		};

	//$('#statusWrap>div, #timeWrap>div').width($(window).width()-$('.score').eq(0).width()*2-6*2);


	/* ----------------------------------------
		fixfont
	---------------------------------------- */
	var ff = {
		$el: $('.fixfont'),
		loaded: false,
		//loaded: true,//debug
		num: 0,
		len: $('.fixfont').length,
		fnc: function ($el) {
			console.log("fixfont実行");
			$el.each(function () {
				var $this = $(this),
					box = {
						wdt: $this.width(),
						hgt: $this.height(),
					},
					$text = $this.children(),
					text = {
						wdt: $text.width(),
						hgt: $text.height()
					},
					fz = parseInt($this.css('font-size')),
					n = 0;
				//if(window.console && typeof window.console.log==='function') console.log(box.hgt+'font-size:'+fz+' > '+text.hgt);
				while (text.hgt <= box.hgt && text.wdt <= box.wdt && n < 400) {
					fz++;
					$this.css({ fontSize: fz + 'px' });
					text.wdt = $text.width();
					text.hgt = $text.height();
					n++;
				}
				$this.css({ fontSize: fz - 1 + 'px' });
				ff.num++;
				if (ff.num >= ff.len) {
					if (!ff.loaded) {
						ff.loaded = true;
						ff.$el.find('>div>div:not(.stable)').text('');
					}
					ff.$el.css({ opacity: 1 });
				}
			});
		}
	};

	$window.load(function () {
		ff.fnc(ff.$el);
	});


	/* ----------------------------------------
		Config
	---------------------------------------- */

	if (typeof config !== 'undefined') {
		if (window.console && typeof window.console.log === 'function') console.log(config);
		$('.nameL').addClass('stable').text(config.name.left);
		$('.nameR').addClass('stable').text(config.name.right);
	}


	/* ----------------------------------------
		Web Socket
	---------------------------------------- */

	var wsSelect = $('.logWebSocket').text();
	switch (wsSelect) {
		case 'ws1':
			ws = ws1;
			break;
		case 'ws2':
			ws = ws2;
			break;
		case 'ws3':
			ws = ws3;
			break;
		case 'ws4':
			ws = ws4;
			break;
		case 'ws5':
			ws = ws5;
			break;
	}


	// エラー処理
	ws.onerror = function (e) {
		$('.log').text('サーバに接続できませんでした。');
	}

	// WebSocketサーバ接続イベント
	ws.onopen = function () {
		// 入室情報を文字列に変換して送信
		ws.send(JSON.stringify({
			type: 'join',
			user: self.name
		}));
	};

	// メッセージ受信イベントを処理
	if (typeof ws1 !== 'undefined') {
		ws1.onmessage = function (event1) {
			wsSwitch = 1;
			scoreView(event1, wsSwitch, $window, ff);
		}
	}

	if (typeof ws2 !== 'undefined') {
		ws2.onmessage = function (event2) {
			wsSwitch = 2;
			scoreView(event2, wsSwitch, $window, ff);
		};

		ws3.onmessage = function (event3) {
			wsSwitch = 3;
			scoreView(event3, wsSwitch, $window, ff);
		};

		ws4.onmessage = function (event4) {
			wsSwitch = 4;
			scoreView(event4, wsSwitch, $window, ff);
		};

		ws5.onmessage = function (event5) {
			wsSwitch = 5;
			scoreView(event5, wsSwitch, $window, ff);
		};
	}

	/*
	// 操作イベント
	document.onkeydown = function(event){
		// 単一キーを全体に送信
		ws.send(JSON.stringify({
			type: 'control',
			user: self.name,
			keyCode: event.keyCode
		}));
	};
	*/
	//表示テスト debug
	var e = $('.auto .wsSwitch1');
	/*
	$(window).ready(function () {
		console.log("デバッグ入力");
		$('.logDate').text("test");
		$('.logScoreL').text("test");
		$('.logYellowL').text("test");
		$('.logRedL').text("test");
		$('.logScoreR').text("test");
		$('.logYellowR').text("test");
		$('.logRedR').text("test");
		$('.logTime').text("90");
		$('.logRound').text("1");
		$('.scoreL').text("12");
		$('.scoreR').text("5");
		$('.time').text("3:50");
		$('.round').text("1");
		$('.priorityR').addClass('on');
		$('.priorityL').addClass('on');
		$('.redcardL').addClass('on');
		$('.redcardR').addClass('on');
		$('.yellowcardL').addClass('on');
		$('.yellowcardR').addClass('on');
		$('.bgWrapR, .scoreBoxR').addClass('on');
		$('.bgWrapL, .scoreBoxL').addClass('on');
		$('.nameL').text("山田太郎");
		$('.nameR').text("山田太郎");
		$('.nationL').text("鹿児島大");
		$('.flagL').attr('data-nation', "jpn");
		$('.nationR').text("山鹿児島大");
		$('.flagR').attr('data-nation', "jpn");
		$('.weapon').text('FOIL');
	});
	/* */
	// ブラウザ終了イベント
	window.onbeforeunload = function () {
		ws.send(JSON.stringify({
			type: 'defect',
			user: self.name,
		}));
	};

	//EOS
});

var m = 'manual';

$('html').keydown(function (e,ff) {
	var i;
	switch (e.which) {
		case 70: // Key[F]
		ff.fnc(ff.$el);
			break;
		case 77:
			if ($('.roundMax').text() == '2') {
				$('.roundMax').text('3')
			} else {
				$('.roundMax').text('2')
			}
			break;
		case 87: // Key[W]
			$('.roundInner').show();
			$('.roundAdd').show();
			$('.roundDeco').show();
			$('.roundEx').hide();
			break;

		case 69: // Key[E]
			$('.roundInner').hide();
			$('.roundAdd').hide();
			$('.roundDeco').hide();
			$('.roundEx').text('ET');
			$('.roundEx').show();
			break;

		case 79: // Key[O]
			$('.roundInner').hide(); sa
			$('.roundAdd').hide();
			$('.roundDeco').hide();
			$('.roundEx').text('OT');
			$('.roundEx').show();
			break;

		case 72: // Key[H]
			$('body').addClass('auto');
			$('body').removeClass('manual');
			break;

		case 74: // Key[J]
			$('body').removeClass('auto');
			$('body').addClass('manual');
			break;

		case 49: // Key[1]
			$('.logWebSocket').text('ws1');
			$('.container').addClass('hide');;
			$('.wsSwitch1').removeClass('hide');
			break;

		case 50: // Key[2]
			$('.logWebSocket').text('ws2');
			$('.container').addClass('hide');;
			$('.wsSwitch2').removeClass('hide');
			break;

		case 51: // Key[3]
			$('.logWebSocket').text('ws3');
			$('.container').addClass('hide');;
			$('.wsSwitch3').removeClass('hide');
			break;

		case 52: // Key[4]
			$('.logWebSocket').text('ws4');
			$('.container').addClass('hide');;
			$('.wsSwitch4').removeClass('hide');
			break;

		case 53: // Key[5]
			i = 5;
			name(i, 'l');
			name(i, 'r');
			portrait(i, 'l');
			portrait(i, 'r');
			break;

		case 54: // Key[6]
			i = 6;
			name(i, 'l');
			name(i, 'r');
			portrait(i, 'l');
			portrait(i, 'r');
			break;

		case 55: // Key[7]
			i = 7;
			name(i, 'l');
			name(i, 'r');
			portrait(i, 'l');
			portrait(i, 'r');
			break;

		case 56: // Key[8]
			i = 8;
			portrait(i, 'l');
			//portrait(i,'r');
			break;

		case 57: // Key[9]
			i = 9;
			portrait(i, 'l');
			portrait(i, 'r');
			break;

		case 90: // Key[Z]
			$('.' + m + ' .bgWrapL,.' + m + ' .scoreBoxL').addClass('on');
			break;

		case 88: // Key[X]
			$('.' + m + ' .bgWrapL,.' + m + ' .scoreBoxL').addClass('invalid');
			break;

		case 191: // Key[/]
			$('.' + m + ' .bgWrapR,.' + m + ' .scoreBoxR').addClass('invalid');
			break;

		case 226: // Key[\]
			$('.' + m + ' .bgWrapR,.' + m + ' .scoreBoxR').addClass('on');
			break;


		case 65: // Key[A]
			$('.' + m + ' .priorityL').toggleClass('on');
			break;
		case 83: // Key[S]
			$('.' + m + ' .redcardL').toggleClass('on');
			break;
		case 68: // Key[D]
			$('.' + m + ' .yellowcardL').toggleClass('on');
			break;
		case 187: // Key[;]
			$('.' + m + ' .yellowcardR').toggleClass('on');
			break;
		case 186: // Key[:]
			$('.' + m + ' .redcardR').toggleClass('on');
			break;
		case 221: // Key[]]
			$('.' + m + ' .priorityR').toggleClass('on');
			break;

		case 48: // Key[0]
			i = 0;
			$('.portraitL').css('opacity', 0);
			$('.portraitR').css('opacity', 0);
			$('.nameLWrap').css('opacity', 0);
			$('.nameRWrap').css('opacity', 0);
			break;
	}
});


$('html').keyup(function (e) {
	var i;
	//console.log(e.which);
	switch (e.which) {
		case 90: // Key[Z]
			$('.' + m + ' .bgWrapL,.' + m + ' .scoreBoxL').removeClass('on');
			break;

		case 88: // Key[X]
			$('.' + m + ' .bgWrapL,.' + m + ' .scoreBoxL').removeClass('invalid');
			break;

		case 191: // Key[/]
			$('.' + m + ' .bgWrapR,.' + m + ' .scoreBoxR').removeClass('invalid');
			break;

		case 226: // Key[\]
			$('.' + m + ' .bgWrapR,.' + m + ' .scoreBoxR').removeClass('on');
			break;
	}
});

function portrait(i, d) {
	//console.log('test');
	var src = "";
	src = 'img/portrait/portrait' + i + '_' + d + '.jpg'
	if (d == 'l') {
		//src = 'img/portrait'+i+'_l.png'
		$('.portraitL img').attr('src', src);
		if (UrlExists(src)) {
			$('.portraitL').css('opacity', 1);
			//console.log("lt");
		} else {
			$('.portraitL').css('opacity', 0);
			//	console.log("lf");
		}
	} else if (d == 'r') {
		//src = 'img/portrait'+i+'_r.png'
		$('.portraitR img').attr('src', src);
		if (UrlExists(src)) {
			$('.portraitR').css('opacity', 1);
			//	console.log("rt");
		} else {
			$('.portraitR').css('opacity', 0);
			//	console.log("rf");
		}
	} else {
	}
	//console.log(src);
	//console.log(d);
	//console.log(i);
}

function name(i, d) {
	//console.log('test');
	var src = "";
	src = 'img/name/name' + i + '_' + d + '.png'
	if (d == 'l') {
		//src = 'img/portrait'+i+'_l.png'
		$('.nameLWrap img').attr('src', src);
		if (UrlExists(src)) {
			$('.nameLWrap').css('opacity', 1);
			//console.log("lt");
		} else {
			$('.nameLWrap').css('opacity', 0);
			//console.log("lf");
		}
	} else if (d == 'r') {
		//src = 'img/portrait'+i+'_r.png'
		$('.nameRWrap img').attr('src', src);
		if (UrlExists(src)) {
			$('.nameRWrap').css('opacity', 1);
			$('.nameRWrap').removeClass('short');
			//console.log("rt");
		} else {
			$('.nameRWrap').css('opacity', 0); 565
			//console.log("rf");
		}
	} else {
	}
	//console.log(src);
	//console.log(d);
	//console.log(i);
}

function UrlExists(url) {
	var http = new XMLHttpRequest();
	http.open('HEAD', url, false);
	//http.send();
	return http.status != 404;
}

function portraitExist(src) {
	var result;
	src = 'http://stragetest.grpht.co.jp/abema/' + src;
	console.log(src);
	$.ajax({
		url: src,
		type: 'HEAD',
		error: function () {
			//file not exists
			result = false;
		},
		success: function () {
			result = true;
		}
	});
	return result;
}

/* ----------------------------------------
	Idling Mode
---------------------------------------- */

document.onkeydown = function (e) {
	//console.log(e);
	if (e.key == 'Enter') $('.idling').toggleClass('on');
	/*if(e.key=='s'){
		var srcL = $('.portraitL img').attr('src');
		var srcR = $('.portraitR img').attr('src');
		$('.portraitL img').attr('src', srcR);
		$('.portraitR img').attr('src', srcL);
	}*/
};

function scoreView(event, wsSwitch, $window, ff) {
	var $window = $(window),
		imgPath = 'img/',
		cur = {
			nameL: '',
			nameR: '',
			nationL: '',
			nationR: '',
			scoreL: -1,
			scoreR: -1,
			yellowL: -1,
			yellowR: -1,
			redL: -1,
			redR: -1,
			status: '',
			timeRemain: '0:00',
			Round: ''
		};


	var e = $('.auto .wsSwitch' + wsSwitch);
	var data = JSON.parse(event.data);
	//debug
	if (data.type === 'join') {
		$('.log p').text(data.user + 'が入室しました。');
	} else if (data.type === 'defect') {
		$('.log p').text(data.user + 'が退室しました。');
		/*
		debug*/
	} else if (data.type === 'control') {
		if($('body').hasClass('auto')){
			$('body').removeClass('auto');
			$('body').addClass('manual');
		}else{
			$('body').addClass('auto');
			$('body').removeClass('manual');
		}
	} else if (data.type === 'admin') {
		console.log(data);
		//選手データ受信
		$('.logWebSocket').text(data.wsSwitch);
		switch (data.wsSwitch) {
			case "ws1":
				$('.container').addClass('hide');;
				$('.wsSwitch1').removeClass('hide');
				$('.logPiste').text('Green Piste');
				break;
			case "ws2":
				$('.container').addClass('hide');;
				$('.wsSwitch2').removeClass('hide');
				$('.logPiste').text('Red Piste');
				break;
			case "ws3":
				$('.container').addClass('hide');;
				$('.wsSwitch3').removeClass('hide');
				$('.logPiste').text('Yellow Piste');
				break;
			case "ws4":
				$('.container').addClass('hide');;
				$('.wsSwitch4').removeClass('hide');
				$('.logPiste').text('Blue Piste');
				break;
			case "ws5":
				$('.container').addClass('hide');;
				$('.wsSwitch5').removeClass('hide');
				$('.logPiste').text('Podium Piste');
				break;

		}
		$('.nameL').text(data.nameL2);
		$('.nationL').text(data.nationL2);
		$('.nameR').text(data.nameR2);
		$('.nationR').text(data.nationR2);
		$('.weapon').text(data.weapon);
		/*$('.flagL').attr('data-nation', data.nationL.toLowerCase());
		$('.flagR').attr('data-nation', data.nationR.toLowerCase());*/
		$('.nationLWrap .fixfont').removeAttr('style');
		$('.nationRWrap .fixfont').removeAttr('style');
		$('.nameLWrap .fixfont').removeAttr('style');
		$('.nameRWrap .fixfont').removeAttr('style');
		$('.weapon .fixfont').removeAttr('style');
		ff.fnc($('.nationLWrap .fixfont'));
		ff.fnc($('.nationRWrap .fixfont'));
		ff.fnc($('.nameLWrap .fixfont'));
		ff.fnc($('.nameRWrap .fixfont'));
		ff.fnc($('.weapon .fixfont'));
		//console.log(data.nameIdL);
		//console.log(data.nameIdR);
		$('.portraitL img').attr('src', 'img/portrait/' + data.idL.replace('name', 'img') + '.jpg');
		$('.portraitR img').attr('src', 'img/portrait/' + data.idR.replace('name', 'img') + '.jpg');
		//debug
		$('.logPlayerL').text(data.nameL);
		$('.logNationL').text(data.nationL);
		$('.logPlayerR').text(data.nameR);
		$('.logNationR').text(data.nationR);
		/*
		debug*/
	} else if (data.type === 'data') {
		//試合中データ受信
		if (ff.loaded) {
			/*if (cur.nameL != data.nameL) {
				cur.nameL = data.nameL;
				e.find('.nameL').text(cur.nameL);
			}
			if (cur.nameR != data.nameR) {
				cur.nameR = data.nameR;
				e.find('.nameR').text(cur.nameR);
			}
			if (cur.nationL != data.nationL) {
				cur.nationL = data.nationL;
				e.find('.flagL').attr('data-nation', data.nationL.toLowerCase());
				e.find('.nationL').text(cur.nationL);
			}
			if (cur.nationR != data.nationR) {
				cur.nationR = data.nationR;
				e.find('.flagR').attr('data-nation', data.nationR.toLowerCase());
				e.find('.nationR').text(cur.nationR);
			}*/
			if (data.priorityL == 1) {
				$('.priorityL').addClass('on');
			} else if (data.priorityL === 0) {
				$('.priorityL').removeClass('on');
			} else {
				//
			}
			if (data.invalidL == 1) {
				e.find('.bgWrapL, .scoreBoxL').addClass('invalid');
			} else if (data.invalidL === 0) {
				e.find('.bgWrapL, .scoreBoxL').removeClass('invalid');
			} else {
				//
			}
			if (data.hitL == 1) {
				e.find('.bgWrapL, .scoreBoxL').addClass('on');
			} else if (data.hitL === 0) {
				e.find('.bgWrapL, .scoreBoxL').removeClass('on');
			} else {
				//
			}
			if (cur.scoreL != data.scoreL && data.scoreL != "") {
				cur.scoreL = data.scoreL;
				$('.scoreL').text(cur.scoreL);
			} else if (data.scoreL == "" && cur.scoreL >= 0) {
				$('.scoreL').text(cur.scoreL);
			} else {
				//
			}
			//console.log(data.priorityL);
			//console.log(data.priorityR);
			if (data.priorityR == 1) {
				$('.priorityR').addClass('on');
			} else if (data.priorityR === 0) {
				$('.priorityR').removeClass('on');
			} else {
				//
			}
			if (data.invalidR == 1) {
				e.find('.bgWrapR, .scoreBoxR').addClass('invalid');
			} else if (data.invalidR === 0) {
				e.find('.bgWrapR, .scoreBoxR').removeClass('invalid');
			} else {
				//
			}
			if (data.hitR == 1) {
				e.find('.bgWrapR, .scoreBoxR').addClass('on');
			} else if (data.hitR === 0) {
				e.find('.bgWrapR, .scoreBoxR').removeClass('on');
			} else {
				//
			}
			if (cur.scoreR != data.scoreR && data.scoreR != "") {
				cur.scoreR = data.scoreR;
				$('.scoreR').text(cur.scoreR);
			} else if (data.scoreR == "" && cur.scoreR >= 0) {
				$('.scoreR').text(cur.scoreR);
			} else {
				//
			}
			/*
						if(cur.status!=data.status){
							cur.status = data.status;
							e.find('.status').text(cur.status);
						}
			*/
			if (data.yellowL > 0) {
				$('.yellowcardL').addClass('on');
			} else if (data.yellowL === "0") {
				$('.yellowcardL').removeClass('on');
			} else {
				//
			}
			if (data.yellowR > 0) {
				$('.yellowcardR').addClass('on');
			} else if (data.yellowR === "0") {
				$('.yellowcardR').removeClass('on');
			} else {
				//
			}
			if (data.redL > 0) {
				$('.redcardL').addClass('on');
			} else if (data.redL === "0") {
				$('.redcardL').removeClass('on');
			} else {
				//
			}
			if (data.redR > 0) {
				$('.redcardR').addClass('on');
			} else if (data.redR === "0") {
				$('.redcardR').removeClass('on');
			} else {
				//
			}

			if (cur.timeRemain != data.timeRemain && data.timeRemain != "") {
				cur.timeRemain = data.timeRemain;
				$('.time').text(cur.timeRemain);
			} else if (data.timeRemain == "" && cur.timeRemain > 0) {
				$('.time').text(cur.timeRemain);
			} else {
				//
			}
			if (cur.round != data.round && data.round > 0) {
				cur.round = data.round;
				$('.round').text(cur.round);
				$('.roundDeco').addClass("on");
				//console.log("R"+cur.round);
			} else if (data.round == "" && cur.round > 0) {
				$('.round').text(cur.round);
				$('.roundDeco').addClass("on");
				//console.log("R"+cur.round);
			} else {
			}
		}
		//debug
		$('.logDate').text(data.time);
		$('.logScoreL').text(data.scoreL);
		$('.logYellowL').text(data.yellowL);
		$('.logRedL').text(data.redL);
		$('.logScoreR').text(data.scoreR);
		$('.logYellowR').text(data.yellowR);
		$('.logRedR').text(data.redR);
		$('.logTime').text(data.timeRemain);
		$('.logRound').text(data.round);
		/*
		debug*/
	} else if (data.type === 'control') {
		$('.log p').text('キー入力');
	} else {
		$('.log p').text('不正なメッセージを受信しました');
	}

}
