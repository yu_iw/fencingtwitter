/* ++++++++++++++++++++++++++++++++++++++++

	Administrator

++++++++++++++++++++++++++++++++++++++++ */

$(function () {
    //BOS

    /* ----------------------------------------
        WebSocketサーバに接続
    ---------------------------------------- */

    //var ws1 = new WebSocket('ws://192.168.10.170:8888/');
    var ws1 = new WebSocket('ws://192.168.100.107:8888/');
    /*var ws2 = new WebSocket('ws://127.0.0.1:8888/');
    var ws3 = new WebSocket('ws://127.0.0.1:8888/');
    var ws4 = new WebSocket('ws://127.0.0.1:8888/');*/

    var self = {
        name: 'Administrator'
    };

    /* エラー処理
    ---------------------------------------- */

    ws1.onerror = function (e) {
        $('#log').prepend('<div>サーバ1に接続できませんでした。</div>');
    };/*
    ws2.onerror = function (e) {
        $('#log').prepend('<div>サーバ1に接続できませんでした。</div>');
    };
    ws3.onerror = function (e) {
        $('#log').prepend('<div>サーバ1に接続できませんでした。</div>');
    };
    ws4.onerror = function (e) {
        $('#log').prepend('<div>サーバ1に接続できませんでした。</div>');
    };*/

    /* WebSocketサーバ接続イベント
    ---------------------------------------- */

    ws1.onopen = function () {
        ws1.send(JSON.stringify({
            type: 'join',
            user: self.name
        }));
    };/*
    ws2.onopen = function () {
        ws2.send(JSON.stringify({
            type: 'join',
            user: self.name
        }));
    };
    ws3.onopen = function () {
        ws3.send(JSON.stringify({
            type: 'join',
            user: self.name
        }));
    };
    ws4.onopen = function () {
        ws4.send(JSON.stringify({
            type: 'join',
            user: self.name
        }));
    };*/

    $('html').keydown(function (e) {
        var i;
        console.log(e.which);
        $('.container>div').removeClass('on');
        switch (e.which) {

            case 49: // Key[1]
                send('ws1');
                $('.wsSwitch1').addClass('on');
                break;

            case 50: // Key[2]
                send('ws2');
                $('.wsSwitch2').addClass('on');
                break;

            case 51: // Key[3]
                send('ws3');
                $('.wsSwitch3').addClass('on');
                break;

            case 52: // Key[4]
                send('ws4');
                $('.wsSwitch4').addClass('on');
                break;

            case 80: // Key[P]
                send('ws5');
                $('.wsSwitch5').addClass('on');
                break;
        }
    });



    /* ----------------------------------------
        データ送信
    ---------------------------------------- */

    function send(e) {
        ws1.send(JSON.stringify({
            type: 'admin',
            user: self.name,
            wsSwitch: e,
        }));/*
        ws2.send(JSON.stringify({
            type: 'admin',
            user: self.name,
            wsSwitch: e,
        }));
        ws3.send(JSON.stringify({
            type: 'admin',
            user: self.name,
            wsSwitch: e,
        }));
        ws4.send(JSON.stringify({
            type: 'admin',
            user: self.name,
            wsSwitch: e,
        }));*/
    }


    /* ----------------------------------------
        ブラウザ終了イベント
    ---------------------------------------- */

    window.onbeforeunload = function () {
        for (var i = 0; i < wsLen; i++) {
            ws[i].send(JSON.stringify({
                type: 'defect',
                user: self.name,
            }));
        }
    };


    //EOS
});
