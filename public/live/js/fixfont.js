$(function(){
	//BOS


/* ----------------------------------------
	Global Variables
---------------------------------------- */

var $window = $(window),
	imgPath = 'img/',
	cur = {
		nameL: '',
		nameR: '',
		nationL: '',
		nationR: '',
		scoreL: -1,
		scoreR: -1,
		yellowL: -1,
		yellowR: -1,
		redL: -1,
		redR: -1,
		status: '',
		timeRemain: '0:00',
		Round: ''
	};

//$('#statusWrap>div, #timeWrap>div').width($(window).width()-$('.score').eq(0).width()*2-6*2);


/* ----------------------------------------
	fixfont
---------------------------------------- */

var ff = {
	$el: $('.fixfont'),
	loaded: false,
	num: 0,
	len: $('.fixfont').length,
	fnc: function($el){
		$el.each(function(){
			var $this = $(this),
				box ={
					wdt: $this.width(),
					hgt: $this.height(),
				},
				$text = $this.children(),
				text = {
					wdt: $text.width(),
					hgt: $text.height()
				},
				fz = parseInt($this.css('font-size')),
				n = 0;
			//if(window.console && typeof window.console.log==='function') console.log(box.hgt+'font-size:'+fz+' > '+text.hgt);
			while(text.hgt<=box.hgt && text.wdt<=box.wdt && n<200){
				fz ++;
				$this.css({fontSize: fz+'px'});
				text.wdt = $text.width();
				text.hgt = $text.height();
				n++;
			}
			$this.css({fontSize: fz-1+'px'});
			ff.num++;
			if(ff.num>=ff.len){
				if(!ff.loaded){
					ff.loaded = true;
					ff.$el.find('>div>div:not(.stable)').text('');
				}
				ff.$el.css({opacity: 1});
			}
		});
	}
};

$window.load(function(){
	ff.fnc(ff.$el);
});

/* EOS */
});