/* ++++++++++++++++++++++++++++++++++++++++

	Administrator

++++++++++++++++++++++++++++++++++++++++ */

$(function () {
    //BOS

    /* ----------------------------------------
        WebSocketサーバに接続
    ---------------------------------------- */

    var ws1 = new WebSocket('ws://192.168.100.102:8888/');

    var self = {
        name: 'Administrator'
    };

    /* エラー処理
    ---------------------------------------- */

    ws1.onerror = function (e) {
        $('#log').prepend('<div>サーバ1に接続できませんでした。</div>');
    };

    /* WebSocketサーバ接続イベント
    ---------------------------------------- */

    ws1.onopen = function () {
        ws1.send(JSON.stringify({
            type: 'join',
            user: self.name
        }));
    };


    /* ----------------------------------------
        データ送信
    ---------------------------------------- */

    $('.send').on('click', function (e) {
        console.log('test');
        var data = {
            "green1": {
                "left": "",
                "left_country": "",
                "right": "",
                "right_country": ""
            },
        };

        //var v = parseInt(this.value),
        var v = 1,
            //  flgSwap = $(this).parents('td').find('.swap').checked,
            flgSwap = false,
            $this = $(this);
        console.log(flgSwap);
        if (v > 0) {
            var playerR = $this.parents('tr').find('.matchLeft'),
                playerL = $this.parents('tr').find('.matchRight'),
                id = {
                    l: playerL.attr('name'),
                    r: playerR.attr('name')
                },
                name = {
                    l: playerL.find('.name').text(),
                    r: playerR.find('.name').text()
                },
                nation = {
                    l: playerL.find('.nation').text(),
                    r: playerR.find('.nation').text()
                };

            switch (v) {
                case 1:
                    ws1.send(JSON.stringify({
                        type: 'admin',
                        user: self.name,
                        piste: v,
                        idL: (!flgSwap) ? id.l : id.r,
                        nameL: (!flgSwap) ? name.l : name.r,
                        nationL: (!flgSwap) ? nation.l : nation.r,
                        idR: (!flgSwap) ? id.r : id.l,
                        nameR: (!flgSwap) ? name.r : name.l,
                        nationR: (!flgSwap) ? nation.r : nation.l
                    }));
                    data.green1 = {
                        "left": (!flgSwap) ? name.l : name.r,
                        "left_country": (!flgSwap) ? nation.l : nation.r,
                        "right": (!flgSwap) ? name.r : name.l,
                        "right_country": (!flgSwap) ? nation.r : nation.l
                    };
                    break;
            }
        }

        // サイトパブリス対応
        $.ajax({
            type: "post",
            url: '../../../../request.php',
            // contentType: 'application/json',
            // dataType: "json",
            // cache: false,
            data: data // JSONデータ本体
        })
            // Ajaxリクエストが成功した時発動
            .done((data) => {
                alert('ok: ' + data);
                console.log(data);
            })
            // Ajaxリクエストが失敗した時発動
            .fail((data) => {
                alert('error: ' + data);
            })
            // Ajaxリクエストが成功・失敗どちらでも発動
            .always((data) => {

            });

    });


    /* ----------------------------------------
        ブラウザ終了イベント
    ---------------------------------------- */

    window.onbeforeunload = function () {
        for (var i = 0; i < wsLen; i++) {
            ws[i].send(JSON.stringify({
                type: 'defect',
                user: self.name,
            }));
        }
    };


    //EOS
});
