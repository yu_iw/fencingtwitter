var SerialPort = require("serialport");
var config = require('../data/config.js');
var serial = new SerialPort(config.getPort(), {
     baudRate:38400,
     dataBits:8, 
     parity:'none', 
     stopBits: 1,
     flowControl: false
});

//websocket
// 8888”Ôƒ|[ƒg‚ÅƒNƒ‰ƒCƒAƒ“ƒg‚ÌÚ‘±‚ð‘Ò‚¿Žó‚¯‚é
var ws = require('websocket.io');
var server = ws.listen(8888, function () {
  console.log('\033[96m Server running at 127.0.0.1:8888 \033[39m');
});

// ƒT[ƒo[ŽÀ‘•‚Ì‘O‚ÉAƒGƒ‰[ƒnƒ“ƒhƒŠƒ“ƒO‚ð‹LÚ‚µ‚Ü‚·B
process.on('uncaughtException', function(err) {
    console.log(err);
});

//•ÛŠÇ—p‚Ì•Ï”
var message_data =''; 

//ƒVƒŠƒAƒ‹ƒ|[ƒg‚ðŠJ‚­
serial.on('open',function(){
     console.log('open');
});

//ƒVƒŠƒAƒ‹ƒ|[ƒg‚ÅŽó‚¯•t‚¯‚½ƒf[ƒ^‚ðˆ—‚·‚é
serial.on('data',function(data){
     var buffer = new Buffer(data, 'binary');
     //console.log(buffer.read());
     var hex = buffer.toString("hex");
     //”O‚Ì‚½‚ß‰üsƒR[ƒhŽæ‚èœ‚­
     hex = hex.replace(/\r?\n/g,"");

     if(message_data ==''){
       //Å‰‚Ìó‘Ô‚Í01‚¾‚¯‚©0114‚Í‚¶‚Ü‚è‚µ‚©‹–‰Â‚µ‚È‚¢
       if(hex == "01" || hex.indexOf("0114") == 0){
         message_data +=hex;
       }
     }else if(message_data =='01'){
       //message_data‚ª01‚¾‚¯‚Ìê‡A14‚Í‚¶‚Ü‚è‚µ‚©‹–‰Â‚µ‚È‚¢
       if(hex.indexOf("14") == 0){
         message_data +=hex;
       }
     }else{
       //01‚¾‚¯ˆÈŠO‚Ì‰½‚©‚µ‚ç’l‚ª“ü‚Á‚Ä‚¢‚éó‘Ô‚È‚Ì‚ÅA‚¢‚Á‚½‚ñ‘S‚Ä’Ç‰Á‚µ‚ÄOK
         message_data +=hex;
     }

     
     //0113‚ªæ“ª‚©‚ç2‰ñoŒ»‚µ‚½’¼Œã‚É04‚ªo‚½‚ç‰ðÍ‚Ö
     if(strCount("0113",message_data) >= 2 && message_data.indexOf("04",message_data.indexOf("0113",message_data.indexOf("0113",0) + 1) + 1) >= 0){
       //console.log('\033[94m'+message_data+'\033[39m');
       
       //                                      1     2     3     4         5     6                       7       8       9     10    11    12    13    14    15    16    17
       //message_data‚ð‰ðÍ                    Rx    Gx    Wx    wx        Z     MM                      XX   :  YY      A     B     b     C     D     d     P     R     vW
       var result = message_data.match(/011452(..)47(..)57(..)77(..)040113(..)02(..........).*0401134402(....)3a(....)02(....)(....)(..)02(....)(....)(..)02(..)02(..)02(....)04/);
       //                               011452 30 47 30 57 30 77 30 040113 4e 02    040113440220303a203102 2030203030 02 2030203030 02 31 02 31 02 2020 04         
       //console.log('\033[96m'+result+'\033[39m');
       // ŽóM‚µ‚½ƒƒbƒZ[ƒW‚ð‘S‚Ä‚ÌƒNƒ‰ƒCƒAƒ“ƒg‚É‘—M‚·‚é
       server.clients.forEach(function(client) {
           if(client != null && result != null && result.length > 6){
               //JSONƒf[ƒ^ì¬
  
               var d = new Date();
               var data = {
                 type: 'data',
                 user: 'FA-07',
                 invalidL: parseInt(convAscii(result[4])),
                 invalidR: parseInt(convAscii(result[3])),
                 hitL: parseInt(convAscii(result[1])),
                 hitR: parseInt(convAscii(result[2])),
                 status: 0,
                 timeRemain: convAscii(result[6]),
                 time: d.getFullYear()  + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + " " + d.getMilliseconds()
               };
  
               //“_”—pˆÓ
               if(result.length > 7){
                 data.scoreR = convAscii(result[7]);
               }
               if(result.length > 8){
                 data.scoreL = convAscii(result[8]);
               }
               
               //‰E‘IŽèƒCƒGƒ[ƒJ[ƒh
               if(result.length > 9){
                 data.yellowR = convAscii(result[9]).replace(" ","");
               }
               //‰E‘IŽèƒŒƒbƒhƒJ[ƒh
               if(result.length > 10){
                 data.redR = convAscii(result[10]).replace(" ","");
               }
               
               //¶‘IŽèƒCƒGƒ[ƒJ[ƒh
               if(result.length > 12){
                 data.yellowL = convAscii(result[12]).replace(" ","");
               }
               //¶‘IŽèƒŒƒbƒhƒJ[ƒh
               if(result.length > 13){
                 data.redL = convAscii(result[13]).replace(" ","");
               }               
               
               
               //priority—pˆÓ 
               if(result.length > 15){
                 var priority = convAscii(result[15]);
                 var pL = 0;
                 var pR = 0;
                 if(priority == 1){
                   pR = 1;
                 }else if(priority == 2){
                   pL = 1;
                 }
                 data.priorityL = pL;
                 data.priorityR = pR;
               }

               //Round
               if(result.length > 16){
                 data.round = convAscii(result[16]);
               }               

               //‘—M
               client.send(JSON.stringify(data));
               console.log('\033[95m'+JSON.stringify(data)+'\033[39m');
  
           }
       });

       //ƒƒbƒZ[ƒW‚ðƒŠƒZƒbƒg
       message_data = '';
     }

});

// ƒNƒ‰ƒCƒAƒ“ƒg‚©‚ç‚ÌÚ‘±ƒCƒxƒ“ƒg‚ðˆ—
server.on('connection', function(socket) {

  socket.on('close',function(){
	  console.log('\033[96mclose\033[39m');
  });

  socket.on('disconnect',function(){
	  console.log('\033[96mdisconnect\033[39m');
  });

  // ƒNƒ‰ƒCƒAƒ“ƒg‚©‚ç‚ÌƒƒbƒZ[ƒWŽóMƒCƒxƒ“ƒg‚ðˆ—
  socket.on('message', function(data) {

    //JSONŒ`Ž®‚©”»•Ê
	if(isJSON(data)){
		// ŽÀsŽžŠÔ‚ð’Ç‰Á
	    var data = JSON.parse(data);
	    var d = new Date();
	    data.time = d.getFullYear()  + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
	    data = JSON.stringify(data);
	    console.log('\033[96m' + data + '\033[39m');

	    // ŽóM‚µ‚½ƒƒbƒZ[ƒW‚ð‘S‚Ä‚ÌƒNƒ‰ƒCƒAƒ“ƒg‚É‘—M‚·‚é
	    server.clients.forEach(function(client) {
	    	if(client != null){
	            client.send(data);
	        }
	    });
	}else{
		//JSONŒ`Ž®‚Å‚Í‚È‚¢
	    console.log('\033[96m•s³‚ÈƒƒbƒZ[ƒW'+data+'\033[39m');
	}
  });
});

//JSONŒ`Ž®‚©‚Ç‚¤‚©”»’è
var isJSON = function(arg) {
    arg = (typeof arg === "function") ? arg() : arg;
    if (typeof arg  !== "string") {
        return false;
    }
    try {
    arg = (!JSON) ? eval("(" + arg + ")") : JSON.parse(arg);
        return true;
    } catch (e) {
        return false;
    }
};

//•¶Žš—ñ‚ÌoŒ»‰ñ”‚ðŽæ“¾‚·‚é(’T‚³‚ê‚é•ûA’T‚·•¶Žš)
var strCount = function(searchStr, str) {
    if (!searchStr || !str) return 0;
 
    var count = 0, pos = str.indexOf(searchStr);

    while (pos !== -1) {
        count++;
        pos = str.indexOf(searchStr, pos + count);
    }
 
    return count;
};

//2•¶Žš‚¸‚Â16i”‚Ì•¶Žš‚ðASCIIƒR[ƒh•¶Žš‚É•ÏŠ·‚µ‚Ä–ß‚·
var convAscii = function(hex) {
    if (!hex) return null;
    var list = splitByLength(hex,2);
    var rs ="";
    for (var i = 0; i < list.length; i++){
      rs += String.fromCharCode("0x"+list[i]);
    }

    return rs;
 
};

//•¶Žš—ñ‚ðAŽw’è‚µ‚½•¶Žš”‚Å•ªŠ„‚µ‚ÄA”z—ñ‚Å•Ô‚·ƒƒ\ƒbƒh
var splitByLength = function (str, length) {
    var resultArr = [];
    if (!str || !length || length < 1) {
        return resultArr;
    }
    var index = 0;
    var start = index;
    var end = start + length;
    while (start < str.length) {
        resultArr[index] = str.substring(start, end);
        index++;
        start = end;
        end = start + length;
    }
    return resultArr;
};