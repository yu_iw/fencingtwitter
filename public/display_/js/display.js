/* ++++++++++++++++++++++++++++++++++++++++

	Display

++++++++++++++++++++++++++++++++++++++++ */

$(function(){
//BOS

/* ----------------------------------------
	Global Variables
---------------------------------------- */

var $window = $(window),
	imgPath = 'img/',
	cur = {
		nameL: '',
		nameR: '',
		nationL: '',
		nationR: '',
		scoreL: -1,
		scoreR: -1,
		yellowL: -1,
		yellowR: -1,
		redL: -1,
		redR: -1,
		status: '',
		timeRemain: '0:00',
		Round: ''
	};

//$('#statusWrap>div, #timeWrap>div').width($(window).width()-$('.score').eq(0).width()*2-6*2);


/* ----------------------------------------
	fixfont
---------------------------------------- */

var ff = {
		$el: $('.fixfont'),
		loaded: false,
		num: 0,
		len: $('.fixfont').length,
		fnc: function($el){
			$el.each(function(){
				var $this = $(this),
					box ={
						wdt: $this.width(),
						hgt: $this.height(),
					},
					$text = $this.children(),
					text = {
						wdt: $text.width(),
						hgt: $text.height()
					},
					fz = parseInt($this.css('font-size')),
					n = 0;
if(window.console && typeof window.console.log==='function') console.log(box.hgt+'font-size:'+fz+' > '+text.hgt);
				while(text.hgt<=box.hgt && text.wdt<=box.wdt && n<200){
					fz ++;
					$this.css({fontSize: fz+'px'});
					text.wdt = $text.width();
					text.hgt = $text.height();
					n++;
				}
				$this.css({fontSize: fz-1+'px'});
				ff.num++;
				if(ff.num>=ff.len){
					if(!ff.loaded){
						ff.loaded = true;
						ff.$el.find('>div>div:not(.stable)').text('');
					}
					ff.$el.css({opacity: 1});
				}
			});
		}
	};

$window.load(function(){
	ff.fnc(ff.$el);
});


/* ----------------------------------------
	Config
---------------------------------------- */

if(typeof config!=='undefined'){
if(window.console && typeof window.console.log==='function') console.log(config);
	$('#nameL').addClass('stable').text(config.name.left);
	$('#nameR').addClass('stable').text(config.name.right);
}


/* ----------------------------------------
	Web Socket
---------------------------------------- */

// エラー処理
ws.onerror = function(e){
	$('#log').text('サーバに接続できませんでした。');
}

// WebSocketサーバ接続イベント
ws.onopen = function(){
	// 入室情報を文字列に変換して送信
	ws.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};

// メッセージ受信イベントを処理
ws.onmessage = function(event){
	var data = JSON.parse(event.data);
//debug
	if(data.type==='join'){
		$('#log p').text(data.user+'が入室しました。');
	}else if(data.type==='defect'){
		$('#log p').text(data.user+'が退室しました。');
	}else if(data.type==='seiko'){
		if(data.toggle==='on'){
			$('.seiko_logo').show();
		}else{
			$('.seiko_logo').hide();
		}
	}else if(data.type==='fie'){
		if(data.toggle==='on'){
			$('.fie_logo').show();
		}else{
			$('.fie_logo').hide();
		}
/*
debug*/
	}else if(data.type==='admin'){
	//選手データ受信
		$('#nameL').text(data.nameL);
		$('#nationL').text(data.nationL);
		$('#flagL').attr('data-nation', data.nationL.toLowerCase());
		$('#nameR').text(data.nameR);
		$('#nationR').text(data.nationR);
		$('#flagR').attr('data-nation', data.nationR.toLowerCase());
		$('#name .fixfont').removeAttr('style');
		ff.fnc($('#name .fixfont'));
//debug
	$('#logPlayerL').text(data.nameL);
	$('#logNationL').text(data.nationL);
	$('#logPlayerR').text(data.nameR);
	$('#logNationR').text(data.nationR);
/*
debug*/
	}else if(data.type==='data'){
	//試合中データ受信
		if(ff.loaded){
/*
			if(cur.nameL!=data.nameL){
				cur.nameL = data.nameL;
				$('#nameL').text(cur.nameL);
			}
			if(cur.nationL!=data.nationL){
				cur.nationL = data.nationL;
//				nFlagL.src = imgPath+'flag/'+cur.nationL.toLowerCase()+'.png';
				$('#nationL').text(cur.nationL);
			}
*/
			if(data.priorityL){
				$('#priorityL').addClass('on');
				$('.priorityL').addClass('on');
			}else{
				$('#priorityL').removeClass('on');
				$('.priorityL').removeClass('on');
			}
//			$('#scoreBoxL').removeClass('on invalid');
			if(data.invalidL){
				$('#scoreBoxL').addClass('invalid');
			}else{
				$('#scoreBoxL').removeClass('invalid');
			}
			if(data.hitL){
				$('#scoreBoxL').addClass('on');
			}else{
				$('#scoreBoxL').removeClass('on');
			}
			if(cur.scoreL!=data.scoreL){
				cur.scoreL = data.scoreL;
				$('#scoreL').text(cur.scoreL);
			}
/*
			if(cur.nameR!=data.nameR){
				cur.nameR = data.nameR;
				$('#nameR').text(cur.nameR);
			}
			if(cur.nationR!=data.nationR){
				cur.nationR = data.nationR;
				nFlagR.src = imgPath+'flag/'+cur.nationR.toLowerCase()+'.png';
				$('#nationR').text(cur.nationR);
			}
*/
			if(data.priorityR){
				$('#priorityR').addClass('on');
				$('.priorityR').addClass('on');
			}else{
				$('#priorityR').removeClass('on');
				$('.priorityR').removeClass('on');
			}
//			$('#scoreBoxR').removeClass('on invalid');
			if(data.invalidR){
				$('#scoreBoxR').addClass('invalid');
			}else{
				$('#scoreBoxR').removeClass('invalid');
			}
			if(data.hitR){
				$('#scoreBoxR').addClass('on');
			}else{
				$('#scoreBoxR').removeClass('on');
			}
			if(cur.scoreR!=data.scoreR){
				cur.scoreR = data.scoreR;
				$('#scoreR').text(cur.scoreR);
			}
/*
			if(cur.status!=data.status){
				cur.status = data.status;
				$('#status').text(cur.status);
			}
*/
			if(data.yellowL > 0){
				$('#yellowcardL').addClass('on');
				$('.yellowcardL').addClass('on');
			}else{
				$('#yellowcardL').removeClass('on');
				$('.yellowcardL').removeClass('on');				
			}
			if(data.yellowR > 0){
				$('#yellowcardR').addClass('on');
				$('.yellowcardR').addClass('on');
			}else{
				$('#yellowcardR').removeClass('on');	
				$('.yellowcardR').removeClass('on');				
			}
			if(data.redL > 0){
				$('#redcardL').addClass('on');
				$('.redcardL').addClass('on');
			}else{
				$('#redcardL').removeClass('on');
				$('.redcardL').removeClass('on');
			}
			if(data.redR > 0){
				$('#redcardR').addClass('on');
				$('.redcardR').addClass('on');
			}else{
				$('#redcardR').removeClass('on');
				$('.redcardR').removeClass('on');
			}

			if(cur.timeRemain!=data.timeRemain){
				cur.timeRemain = data.timeRemain;
				$('#time').text(cur.timeRemain);
			}
			if(cur.round!=data.round){
				cur.round = data.round;
				$('#round').text('R'+cur.round);
			}
		}
//debug
	$('#logDate').text(data.time);
	$('#logScoreL').text(data.scoreL);
	$('#logYellowL').text(data.yellowL);
	$('#logRedL').text(data.redL);
	$('#logScoreR').text(data.scoreR);
	$('#logYellowR').text(data.yellowR);
	$('#logRedR').text(data.redR);
//	$('#logStatus').text(data.status);
	$('#logTime').text(data.timeRemain);
	$('#logRound').text(data.round);
/*
debug*/
	}else if (data.type==='control'){
		$('#log p').text('キー入力');
	}else{
		$('#log p').text('不正なメッセージを受信しました');
	}
};

/*
// 操作イベント
document.onkeydown = function(event){
	// 単一キーを全体に送信
	ws.send(JSON.stringify({
		type: 'control',
		user: self.name,
		keyCode: event.keyCode
	}));
};
*/

// ブラウザ終了イベント
window.onbeforeunload = function(){
	ws.send(JSON.stringify({
		type: 'defect',
		user: self.name,
	}));
};

//EOS
});


/* ----------------------------------------
	Idling Mode
---------------------------------------- */

document.onkeydown = function(e){
console.log(e);
	if(e.key=='Enter') $('#idling').toggleClass('on');
};