/* ++++++++++++++++++++++++++++++++++++++++

	jQuery GR Slider
	Requires: jquery
	Option: jquery.easing
	Last Update: 9/4, 2018
	Version: 2.4.3

++++++++++++++++++++++++++++++++++++++++ */

;(function($){
//BOS

if(window.console && typeof window.console.log==='function') console.log('%cjQuery GR Slider version 2.4.3', 'color: #39f;');
Number.isFinite = Number.isFinite || function(any){
	return typeof any==='number' && isFinite(any);
};

var $window = $(window),
	isTouchDevice ='ontouchstart' in window || navigator.msPointerEnabled? true: false,
	isIE10 = parseFloat(navigator.userAgent.toLowerCase().replace(/.+msie (\d+)\..+/, '$1'))===10,
	cssEnable = {
		transition: true,
		translate: true,
		rotate: true
	},
	ev =navigator.msPointerEnabled? {
		touchStart:isIE10? 'MSPointerDown': 'pointerdown',
		touchMove:isIE10? 'MSPointerMove': 'pointermove',
		touchEnd:isIE10? 'MSPointerUp': 'pointerup'
	}: {
		touchStart:isTouchDevice? 'touchstart': 'mousedown',
		touchMove:isTouchDevice? 'touchmove': 'mousemove',
		touchEnd:isTouchDevice? 'touchend': 'mouseup'
	},
	transEaseArr = {
		swing: 'ease',
		linear: 'linear',
		easeInQuad: 'cubic-bezier(0.55, 0.085, 0.68, 0.53)',
		easeOutQuad: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
		easeInOutQuad: 'cubic-bezier(0.455, 0.03, 0.515, 0.955)',
		easeInCubic: 'cubic-bezier(0.55, 0.055, 0.675, 0.19)',
		easeOutCubic: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
		easeInOutCubic: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
		easeInQuart: 'cubic-bezier(0.895, 0.03, 0.685, 0.22)',
		easeOutQuart: 'cubic-bezier(0.165, 0.84, 0.44, 1)',
		easeInOutQuart: 'cubic-bezier(0.77, 0, 0.175, 1)',
		easeInQuint: 'cubic-bezier(0.755, 0.05, 0.855, 0.06)',
		easeOutQuint: 'cubic-bezier(0.23, 1, 0.32, 1)',
		easeInOutQuint: 'cubic-bezier(0.86, 0, 0.07, 1)',
		easeInSine: 'cubic-bezier(0.47, 0, 0.745, 0.715)',
		easeOutSine: 'cubic-bezier(0.39, 0.575, 0.565, 1)',
		easeInOutSine: 'cubic-bezier(0.445, 0.05, 0.55, 0.95)',
		easeInExpo: 'cubic-bezier(0.95, 0.05, 0.795, 0.035)',
		easeOutExpo: 'cubic-bezier(0.19, 1, 0.22, 1)',
		easeInOutExpo: 'cubic-bezier(1, 0, 0, 1)',
		easeInCirc: 'cubic-bezier(0.6, 0.04, 0.98, 0.335)',
		easeOutCirc: 'cubic-bezier(0.075, 0.82, 0.165, 1)',
		easeInOutCirc: 'cubic-bezier(0.785, 0.135, 0.15, 0.86)',
		easeInElastic: 'ease',
		easeOutElastic: 'ease',
		easeInOutElastic: 'ease',
		easeInBack: 'cubic-bezier(0.6, -0.28, 0.735, 0.045)',
		easeOutBack: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)',
		easeInOutBack: 'cubic-bezier(0.68, -0.55, 0.265, 1.55)',
		easeInBounce: 'ease',
		easeOutBounce: 'ease',
		easeInOutBounce: 'ease'
	},
	prefix = 'grSlider',
	cls = {
		type: function(t){
			return prefix+t.substring(0, 1).toUpperCase()+t.substring(1);
		},
		act: prefix+'Active',
		last: prefix+'Last',
		navAct: prefix+'NavActive',
		prevented: prefix+'Prevented'
	},
	systemNum = 0;


/* ----------------------------------------
	Checking CSS3 Properties
---------------------------------------- */

$(function(){
	cssEnable.transition = typeof $('body').css('transition')==='string';
	if(!cssEnable.transition){
		cssEnable.translate = false;
		cssEnable.rotate = false;
	}else{
		$('body').append('<div id="'+prefix+'CheckCss3"></div>');
		var $ccss3 = $('#'+prefix+'CheckCss3').css({
				display: 'none',
				position: 'absolute',
				right: '200%'
			});
		$ccss3.css({transform: 'translateX(1px) translateY(1px)'});
		if(typeof $ccss3[0].style.transform==='undefined' && typeof $ccss3[0].style.webkitTransform==='undefined') cssEnable.translate = false;
		$ccss3.css({transform: ''}).css({transform: 'rotateY(1deg)'});
		if(typeof $ccss3[0].style.transform==='undefined' && typeof $ccss3[0].style.webkitTransform==='undefined') cssEnable.rotate = false;
		$ccss3.remove();
	}
});



/* ++++++++++++++++++++++++++++++++++++++++

	methods.init

++++++++++++++++++++++++++++++++++++++++ */

var methods = {
	init: function(options){
	return this.each(function(){
		var slider = {
				$area: $(this),
				original: $(this).html(),
				options: $.extend({}, $.fn.grSlider.defaults),
				repeater: '',
				repeaterCnt: 0,
				flags: {
					initialized: false,
					resize: false,
					moving: false,
					pause: false,
					stop: false,
					destroyed: false,
					last: -1,
					autoCnt: 0,
					dirPositive: true
				}
			},
			dataAttr = $(this).data();
		this.slider = slider;
		slider.$el = slider.$area.children();
		slider.num = slider.$el.length;
		slider.wdt = slider.$el.width();
		if(this.id) slider.name = this.id;
		else{
			systemNum++;
			this.id = slider.name = prefix+systemNum;
		}
		if(!$('#'+slider.name+'Wrap').length){
			slider.$area.before('<div id="'+slider.name+'Wrap" class="'+prefix+'Wrap"><div class="'+prefix+'Contents"></div></div>');
			slider.$wrap = slider.$area.prev();
			slider.$contents = slider.$wrap.find('.'+prefix+'Contents');
			slider.$area.addClass(prefix).appendTo(slider.$contents);
		}
		if(Object.keys(dataAttr).length){
			if(window.console && typeof window.console.log==='function') console.log(dataAttr);
			$.each(dataAttr, function(key, val){
				var v = val;
				if(typeof v==='string' && ~v.indexOf(',')){
					if(~v.indexOf(';')){
						v = val.replace(/\s/g, '').split(/,:|;/);
						for(var i=0, l=v.length; i<l; i=(i+1)|0){
							if(~v[i].indexOf(',')) v[i] = v[i].replace(/:|;/g, '').split(',');
						}
					}else{
						v = val.replace(/\s/g, '').split(',');
					}
				}
				slider.options[key] = v;
			});
		}
		slider.settings = $.extend({}, slider.options, options);


/* -----------------------------------------
	Inner Images
---------------------------------------- */

		slider.imgArr = [];
		slider.$area.each(function(){
			$(this).find('*').each(function(){
				if(this.nodeName.toLowerCase()==='img') slider.imgArr.push(this.src);
				else if($(this).css('backgroundImage')!=='none') slider.imgArr.push($(this).css('backgroundImage').replace(/(\s|"|')/g, '').replace(/url\((.+)\)/, '$1'));
			});
		});


/* ----------------------------------------
	play / pause
---------------------------------------- */

		slider.play = function(){
		if(slider.settings.auto && slider.flags.pause && !slider.flags.stop){
			slider.flags.pause = false;
			slider.repeaterCnt = 0;
			slider.$wrap.removeAttr('data-status');
			if(!slider.flags.moving) slider.timerOn();
		}//End of if(slider.settings.auto && slider.flags.pause && !slider.flags.stop)
		};
		slider.pause = function(){
		if(slider.settings.auto){
			slider.flags.pause = true;
			slider.timerOff();
			slider.$wrap.attr('data-status', 'pause');
		}//End of if(slider.settings.auto)
		};


/* ----------------------------------------
	Fnc slider.setup
---------------------------------------- */

		slider.setup = function(){

/* Options, Variables
---------------------------------------- */

			if(!cssEnable.rotate && slider.settings.type==='flip') slider.settings.type = 'slide';
			slider.settings.changeTime =slider.settings.changeTime>0? parseInt(slider.settings.changeTime): 3000;
			slider.settings.initNum =slider.settings.initNum>1? parseInt(slider.settings.initNum): 1;
			slider.settings.showNum =slider.settings.showNum>1? parseInt(slider.settings.showNum): 1;
			slider.settings.moveAll =slider.settings.type==='slide' && slider.settings.showNum>1? slider.settings.moveAll: true;
			slider.navNum =slider.settings.moveAll? Math.ceil(slider.num/slider.settings.showNum): slider.num;
			slider.settings.slideTime =slider.settings.slideTime>0? parseInt(slider.settings.slideTime): 500;
			var isOnlyAnimateEase = slider.settings.slideEase!=='swing' && transEaseArr[slider.settings.slideEase]==='ease';
			slider.settings.transEase = ' '+transEaseArr[slider.settings.slideEase];
			if(slider.settings.type==='slide' && isOnlyAnimateEase) cssEnable.translate = false;
			if(slider.settings.type==='fade' && isOnlyAnimateEase) cssEnable.transition = false;
			if(typeof $.easing[slider.settings.slideEase]==='undefined' && ((slider.settings.type!=='flip' && !cssEnable.transition) || isOnlyAnimateEase)){
				if(window.console && typeof window.console.log==='function') console.log('%cEasing "'+slider.settings.slideEase+'" requires jQuery Easing Plugin.', 'color: #c33');
				slider.settings.slideEase = 'swing';
			}else if(slider.settings.slideEase!=='swing' && slider.settings.type==='flip' && isOnlyAnimateEase){
				if(window.console && typeof window.console.log==='function') console.log('%cIn "type: \'flip\'" you cannot use easing "Elastic" and "Bounce".', 'color: #c93');
			}
			if(slider.num<=slider.settings.showNum){
				slider.settings.flickable = false;
				slider.settings.auto = false;
				slider.settings.loop = false;
				slider.settings.moveAll = true;
				slider.settings.paging = false;
				slider.settings.nav = false;
				slider.navNum = 1;
			}
			if(slider.settings.changeClass){
				if($.isArray(slider.settings.changeClass)){
					var cClass = {
							name: slider.settings.changeClass[0],
							cnd: false,
							divide:$.isArray(slider.settings.changeClass[1])? slider.settings.changeClass[1]: false
						};
					if(cClass.divide && cClass.divide.length===2){
						cClass.divisor = parseInt(cClass.divide[0]);
						cClass.remainder = parseInt(cClass.divide[1])%cClass.divisor;
					}else{
						for(var i=1, l=slider.settings.changeClass.length; i<l; i=(i+1)|0){
							slider.settings.changeClass[i] = parseInt(slider.settings.changeClass[i]%slider.num);
						}
					}
				}else slider.settings.changeClass = false;
			}
			if(!slider.imgArr.length) slider.settings.loader = false;
			slider.bsStr = String(slider.settings.beforeSlide).replace(/\/\*(.|\s|\r|\n)*\*\//,'').replace(/\/\/.*/,'');
			slider.afStr = String(slider.settings.afterSlide).replace(/\/\*(.|\s|\r|\n)*\*\//,'').replace(/\/\/.*/,'');

/* slider.flags
---------------------------------------- */

			slider.flags.left = slider.settings.direction==='left';
			slider.flags.vertical = slider.settings.direction==='up' || slider.settings.direction==='down';
			slider.flags.cur =slider.settings.moveAll? Math.floor((slider.settings.initNum-1)/slider.settings.showNum): slider.settings.initNum-1;
			if(slider.flags.cur>=slider.navNum) slider.flags.cur = slider.navNum-1;
			else if(slider.flags.cur<0) slider.flags.cur = 0;

/* Array of Times
---------------------------------------- */

			slider.changeTimeArr = [];
			slider.slideTimeArr = [];
			for(var i=0; i<slider.navNum; i=(i+1)|0){
				var idx =slider.settings.moveAll? i*slider.settings.showNum: i,
					ct = slider.$el.eq(idx).data('changeTime'),
					st = slider.$el.eq(idx).data('slideTime');
				if(slider.settings.auto){
					if(!Number.isFinite(ct)) ct = slider.settings.changeTime;
					slider.changeTimeArr.push(ct);
				}
				if(!Number.isFinite(st)) st = slider.settings.slideTime;
				slider.slideTimeArr.push(st);
			}

/* Timer
---------------------------------------- */

			slider.timerOn = function(){
				if(slider.settings.auto){
					if(slider.repeaterCnt<=2){
						if(!Number.isFinite(slider.settings.auto) || slider.settings.auto>slider.flags.autoCnt){
							var ct = slider.changeTimeArr[slider.flags.cur];
							if(!slider.flags.pause && ct>0 &&(slider.settings.loop || (!slider.settings.loop && slider.flags.cur<slider.navNum-1))){
								slider.repeater = setTimeout(function(){
									if(slider.settings.auto) slider.chgSlide();
								}, ct);
							}
						}else slider.settings.auto = false;
						slider.repeaterCnt = 0;
					}else slider.repeaterCnt = 1;
					slider.flags.resize = false;
				}else{
					slider.repeaterCnt = 0;
					slider.flags.resize = false;
				}
			};
			slider.timerOff = function(){
				if(slider.settings.auto){
					clearTimeout(slider.repeater);
					slider.repeaterCnt++;
				}
			};

/* Frame
---------------------------------------- */

			if(!slider.flags.initialized) slider.$wrap.append('<span class="'+prefix+'FrameL"></span><span class="'+prefix+'FrameR"></span>');

/* Paging
---------------------------------------- */

			if(slider.settings.paging && typeof slider.$prev==='undefined'){
				slider.$wrap.append('<a class="'+prefix+'Prev" style="display: none !important;"></a><a class="'+prefix+'Next" style="display: none !important;"></a>');
				slider.$prev = slider.$wrap.find('.'+prefix+'Prev');
				slider.$next = slider.$wrap.find('.'+prefix+'Next');
				slider.$prev.on('click', function(){
					if(!slider.flags.moving){
						slider.flags.cur--;
						slider.chgSlide(slider.flags.cur);
					}
				});
				slider.$next.on('click', function(){
					if(!slider.flags.moving) slider.chgSlide(null);
				});
			}else if(!slider.settings.paging && typeof slider.$prev!=='undefined'){
				slider.$prev.remove();
				slider.$next.remove();
			}

/*Navigation
---------------------------------------- */

			if(slider.settings.nav && typeof slider.$navUl==='undefined'){
				slider.$wrap.append('<div class="'+prefix+'Nav"><div><ul></ul></div></div>');
				slider.$navUl = slider.$wrap.find('.'+prefix+'Nav').find('ul');
				for(var i=0; i<slider.navNum; i=(i+1)|0){
					slider.$navUl.append('<li class="'+prefix+'Nav'+i+'"><a>'+i+'</a></li>');
				}
				slider.$nav = $(slider.$navUl).find('li');
				slider.$nav.on('click', function(){
					if(!slider.flags.moving && !$(this).hasClass(cls.navAct)){
						slider.flags.cur = slider.$nav.index(this);
						slider.$nav.removeClass(cls.navAct)
						.eq(slider.flags.cur).addClass(cls.navAct);
						slider.chgSlide(slider.flags.cur);
					}
				});
			}else if(!slider.settings.nav && typeof slider.$navUl!=='undefined'){
				slider.$wrap.find('.'+prefix+'Nav').remove();
			};

/* Append Empty Slider
---------------------------------------- */

			if(slider.num%slider.settings.showNum!==0 && slider.settings.moveAll && slider.num>slider.settings.showNum){
				var emptyNum = slider.settings.showNum-slider.num%slider.settings.showNum;
				for(var i=0; i<emptyNum; i=(i+1)|0){
					slider.$area.append('<li class="'+prefix+'Empty"></li>');
				}
				slider.$el = slider.$area.children();
				slider.num = slider.$el.length;
			}

/* Type:Slide
---------------------------------------- */

			if(slider.settings.type==='slide'){
				if(slider.settings.loop && slider.num>slider.settings.showNum){
					slider.$el.clone().appendTo(slider.$area);
					slider.$el.clone().prependTo(slider.$area);
					slider.$el = slider.$area.children();
				}
			}

/* Flick
---------------------------------------- */

			var flick = slider.$contents[0];
			slider.$el.find('a').off('click.grSliderFlick');
			flick.moveCnd = 30;
			flick.flags = {
				touch: false
			};
			flick.moveBack = function(pos){
				if(slider.settings.type==='slide'){
					slider.flags.moving = true;
					if(cssEnable.translate){
						slider.$area.css({
							transform: 'translateX('+(-flick.curPos.left)+'px) translateY('+(-flick.curPos.top)+'px)',
							transition: slider.settings.slideTime+'ms transform'
						});
						setTimeout(function(){
							slider.$area.css({transition: '0ms'});
							slider.timerOn();
							slider.flags.moving = false;
						}, slider.settings.slideTime);
					}else{
						$(flick).animate({
							scrollLeft: pos.left,
							scrollTop: pos.top
						}, slider.settings.slideTime, function(){
							slider.timerOn();
							slider.flags.moving = false;
						});
					}
				}else if(slider.settings.type==='flip' && cssEnable.translate){
					slider.flags.moving = true;
					var $el = slider.$el.eq(slider.flags.cur),
						mbStyle =slider.flags.vertical? 'rotateX(0deg)': 'rotateY(0deg)';
					slider.$cur.css({
						transform: mbStyle,
						transition: slider.settings.slideTime+'ms transform'
					});
					setTimeout(function(){
						slider.$cur.css({transition: '0ms'});
						slider.timerOn();
						slider.flags.moving = false;
					}, slider.settings.slideTime);
				}else{
					slider.timerOn();
				}
			};
			flick.comp = function(){
				var self = flick;
				self.flags.touch = false;
				if((!slider.flags.vertical && Math.abs(self.diff.x)<=self.moveCnd) || (slider.flags.vertical && Math.abs(self.diff.y)<=self.moveCnd)){
					self.moveBack(self.curPos);
				}else{
					var isEdge = {
						left:(slider.settings.direction==='left' && slider.flags.cur===0) || (slider.settings.direction==='right' && slider.flags.cur===slider.navNum-1),
						right:(slider.settings.direction==='left' && slider.flags.cur===slider.navNum-1) || (slider.settings.direction==='right' && slider.flags.cur===0),
						top:(slider.settings.direction==='up' && slider.flags.cur===0) || (slider.settings.direction==='down' && slider.flags.cur===slider.navNum-1),
						bottom:(slider.settings.direction==='up' && slider.flags.cur===slider.navNum-1) || (slider.settings.direction==='down' && slider.flags.cur===0)
					};
					if(slider.$el.find('a').length){
						slider.$el.find('a').addClass(cls.prevented).on('click.grSliderFlick', function(e){
							e.preventDefault();
						});
					}
					if(!slider.settings.loop && ((isEdge.left && self.diff.x>0) || (isEdge.right && self.diff.x<0) || (isEdge.top && self.diff.y>0) || (isEdge.bottom && self.diff.y<0))){
						self.moveBack(self.curPos);
					}else{
						switch(slider.settings.direction){
						case 'left':
							if(self.diff.x<0) slider.chgSlide();
							else slider.chgSlide(slider.flags.cur-1);
						break;
						case 'right':
							if(self.diff.x>0) slider.chgSlide();
							else slider.chgSlide(slider.flags.cur-1);
						break;
						case 'up':
							if(self.diff.y<0) slider.chgSlide();
							else slider.chgSlide(slider.flags.cur-1);
						break;
						case 'down':
							if(self.diff.y>0) slider.chgSlide();
							else slider.chgSlide(slider.flags.cur-1);
						break;
						}
					}
				}
			};
			flick.fnc = function(e){
			if(!slider.flags.moving){
				var self = flick;
				if(navigator.msPointerEnabled){
					var touch =e.pointerType==='mouse' || e.pointerType===4? e: e.touches[0];
				}else var touch =isTouchDevice? e.touches[0]: e;

			// Touched
				if(e.type===ev.touchStart){
					if(!isTouchDevice) e.preventDefault();
					else if((e.pointerType==='mouse')) e.preventDefault();
					self.flags.touch = true;
					slider.timerOff();
					if(slider.$el.find('.'+cls.prevented).length) slider.$el.find('.'+cls.prevented).removeClass(cls.prevented).off('click.grSliderFlick');
					self.touchCnt =typeof e.touches!=='undefined'? e.touches.length: 1;
					self.start = {
						x: touch.pageX,
						y: touch.pageY
					};
					self.diff = {
						x: 0,
						y: 0
					};
					self.curPos = slider.curPos;
					self.area = {
						x: slider.$contents.offset().left,
						y: slider.$contents.offset().top
					};
					self.area.x2 = self.area.x+slider.dsplWdt;
					self.area.y2 = self.area.y+slider.dsplHgt;
				}

			// Moving
				if(self.flags.touch && e.type===ev.touchMove){
					self.diff.x = (touch.pageX-self.start.x);
					self.diff.y = (touch.pageY-self.start.y);
					if(self.touchCnt>1){
						self.flags.touch = false;
					}else{
						if(touch.pageX<=self.area.x || touch.pageX>=self.area.x2 || touch.pageY<=self.area.y || touch.pageY>=self.area.y2){
							self.comp();
						}else if(slider.settings.flickable===true){
							if(slider.settings.type==='slide'){
								if(!slider.flags.vertical && Math.abs(self.diff.x)>5){
									if(cssEnable.translate) slider.$area.css({transform: 'translateX('+(-self.curPos.left+self.diff.x)+'px)'});
									else $(self).scrollLeft(self.curPos.left-self.diff.x);
								}else if(slider.flags.vertical && Math.abs(self.diff.y)>5){
									if(cssEnable.translate) slider.$area.css({transform: 'translateY('+(-self.curPos.top+self.diff.y)+'px)'});
									else $(self).scrollTop(self.curPos.top-self.diff.y);
								}
							}else if(slider.settings.type==='flip'){
								if(!slider.flags.vertical && Math.abs(self.diff.x)>5){
									var r = self.diff.x/5;
									if(r>80) r = 80;
									else if(r<-80) r = -80;
									if(cssEnable.translate) slider.$cur.css({transform: 'rotateY('+r+'deg)'});
								}else if(slider.flags.vertical && Math.abs(self.diff.y)>5){
									var r = -self.diff.y/5;
									if(r>80) r = 80;
									else if(r<-80) r = -80;
									if(cssEnable.translate) slider.$cur.css({transform: 'rotateX('+r+'deg)'});
								}
							}
						}
					}
				}

			// Released
				if(self.flags.touch && e.type===ev.touchEnd && slider.settings.flickable) self.comp();
			}//End of if(!slider.flags.moving)
			};

			// addEventListener
			if(slider.settings.flickable){
				flick.addEventListener(ev.touchStart, flick.fnc, false);
				flick.addEventListener(ev.touchMove, flick.fnc, false);
				document.body.addEventListener(ev.touchEnd, flick.fnc, false);
			}

/* pauseOnHover
---------------------------------------- */

			if(slider.settings.auto && slider.settings.pauseOnHover){
				slider.$contents.on({
					'mouseover.grSlider': slider.pause,
					'mouseout.grSlider': slider.play
				});
			}else{
				slider.$contents.off('mouseover.grSlider mouseout.grSlider');
			}
		};
		slider.setup();


/* ----------------------------------------
	Loader
---------------------------------------- */

		if(slider.settings.loader){
			if(!$('#'+slider.name+'Loader').length) slider.$wrap.css({opacity: 0.2}).after('<div id="'+slider.name+'Loader" class="'+prefix+'Loader"><canvas id="'+slider.name+'LoaderCanvas"></canvas><div><div><div><p>Loading</p></div></div></div></div>');
			var loader = {
				$box: $('#'+slider.name+'Loader'),
				canvas: document.getElementById(slider.name+'LoaderCanvas'),
				lineWdt: 12,
				flg: {
					comp: false,
					fade: false
				}
			};
			loader.$box.css({
				top: -(slider.$area.innerHeight()+loader.$box.innerWidth())/2
			});
			if(!loader.canvas.getContext){
				loader.$box.remove();
				return false;
			}
			loader.canvas.width = loader.canvas.height = loader.$box.width();
			loader.canvas.center= {
				x: loader.canvas.width/2,
				y: loader.canvas.height/2
			};
			loader.ctx = loader.canvas.getContext('2d');;
			loader.ctx.strokeStyle = '#005fa0';
			loader.ctx.globalAlpha = 0.9;
			loader.ctx.shadowColor = '#fff';
			loader.ctx.shadowBlur = 6;
			loader.ctx.shadowOffsetX = 0;
			loader.ctx.shadowOffsetY = 0;
			loader.ctx.lineWidth = loader.ctx.shadowBlur*2
			loader.anim = function(){
				var radius = loader.canvas.center.y-loader.ctx.shadowBlur*2,
					startR = -90*Math.PI/180,
					endR = (loader.loadTime/100*360-90)*Math.PI/180;
				loader.ctx.clearRect(0, 0, loader.canvas.width, loader.canvas.height);
				loader.ctx.beginPath();
				loader.ctx.arc(loader.canvas.center.x, loader.canvas.center.y, radius, startR, endR, false);
				loader.ctx.stroke();
				if(!loader.flg.comp){
					if(loader.loadTime<100){
						loader.loadTime += 8;
						if(loader.loadTime>100) loader.loadTime = 100;
					}else{
						loader.loadTime = 1;
					}
					setTimeout(loader.anim, 50);
				}else{
					if(!loader.flg.fade){
						loader.loadTime = 100;
						loader.flg.fade = true;
						loader.anim();
					}else{
						loader.$box.fadeOut(500);
					}
				}
			};
			loader.anim();
		}


/* ----------------------------------------
	Fnc slider.chgBefore
---------------------------------------- */

		slider.chgBefore = function(s, c, n){
			var dBef = new $.Deferred();
			if(typeof slider.settings.beforeSlide==='function' && slider.flags.initialized){
				slider.settings.beforeSlide(s, c, n, dBef);
				if(!~slider.bsStr.indexOf('deferred.resolve')) dBef.resolve();
			}else dBef.resolve();
			return dBef.promise();
		};


/* ----------------------------------------
	Fnc slider.chgAfter
---------------------------------------- */

		slider.chgAfter = function(s, c){
			var dAft = new $.Deferred();
			if(typeof slider.settings.afterSlide==='function'){
				slider.settings.afterSlide(s, c, dAft);
				if(!~slider.afStr.indexOf('deferred.resolve')) dAft.resolve();
			}else dAft.resolve();
			return dAft.promise();
		};


/* ----------------------------------------
	Fnc slider.chgSlide
---------------------------------------- */

		slider.chgSlide = function(clk){
			slider.timerOff();
			slider.flags.moving = true;
			slider.flags.cur =clk || clk===0? clk: slider.flags.cur+1;
			slider.$el.removeClass(cls.last);
			if(slider.$area.find('.'+cls.act).length){
				slider.$area.find('.'+cls.act).addClass(cls.last);
			}
			slider.flags.dirPositive =slider.flags.left || slider.settings.direction==='up'? slider.flags.cur>slider.flags.last: slider.flags.cur<slider.flags.last;
			var st = slider.slideTimeArr[slider.flags.cur];
			if(slider.flags.cur===slider.navNum){
				st = slider.slideTimeArr[0];
				if(slider.settings.type!=='slide') slider.flags.cur = 0;
			}else if(slider.flags.cur<0){
				st = slider.slideTimeArr[slider.navNum-1];
				if(slider.settings.type!=='slide') slider.flags.cur = slider.navNum-1;
			}

/* Activate
---------------------------------------- */

			var startIdx =slider.settings.moveAll? slider.flags.cur*slider.settings.showNum: slider.flags.cur,
				showStart =slider.flags.cur>-1? startIdx: slider.num+startIdx,
				showEnd =slider.settings.moveAll? showStart+slider.settings.showNum: showStart+1;
			slider.$el.removeClass(cls.act);
			for(var i=showStart; i<showEnd; i=(i+1)|0){
				var actIdx = i%slider.num;
				slider.$el.each(function(i){
					if(i%slider.num===actIdx) $(this).addClass(cls.act);
				});
			}
			slider.$cur = slider.$area.children('.'+cls.act);
			slider.$last = slider.$area.children('.'+cls.last);
/*
if(window.console && typeof window.console.log==='function') console.log(
	slider.settings.moveAll+
	' mov:'+slider.mov+
	' showNum:'+slider.settings.showNum+
	' cur:'+ slider.flags.cur+
	' num:'+slider.num+
	' navNum:'+slider.navNum+
	' '+showStart+ '/'+ showEnd
);
*/

/* Show Wrap
---------------------------------------- */

			if(!slider.flags.initialized) slider.$wrap.animate({
				opacity: 1
			}, 200);

/* Before
---------------------------------------- */

			var sbArg = {
					current: {
						idx:!slider.flags.initialized? 0: slider.flags.last,
						$: slider.$last
					},
					next: {
						idx:slider.flags.cur<0? slider.navNum-1:slider.flags.cur<slider.navNum? slider.flags.cur: 0,
						$: slider.$cur
					}
				};
			slider.chgBefore(slider, sbArg.current, sbArg.next).then(function(){
				var dMain = new $.Deferred;
				slider.flags.static = false;
				switch(slider.settings.type){

/* Type:Slide
---------------------------------------- */

				case 'slide':
					var slideMove = function(){
							var s = 0;
							slider.flags.static = !slider.settings.loop && slider.flags.cur+slider.settings.showNum>slider.num;
							if(!slider.flags.vertical){
								if(slider.settings.showNum<slider.num){
									s =slider.flags.left? slider.dsplWdt+slider.mov*slider.flags.cur: slider.mov*(slider.navNum-slider.flags.cur);
									if(slider.settings.loop) s =slider.flags.left? slider.dsplWdt+slider.mov*(slider.navNum+slider.flags.cur): slider.mov*(slider.navNum*2-slider.flags.cur);
									if(slider.flags.static) s =slider.flags.left? s-slider.mov*(slider.flags.cur+slider.settings.showNum-slider.num): s+slider.mov*(slider.flags.cur+slider.settings.showNum-slider.num);
								}
								slider.curPos = {
									left: s,
									top: 0
								};
								return cssEnable.translate? 'translateX('+(-s)+'px)': {
									left: s,
									top: 0
								};
							}else{
								if(slider.settings.showNum<slider.num){
									s =slider.settings.direction==='up'? slider.dsplHgt+slider.mov*slider.flags.cur: slider.mov*(slider.navNum-slider.flags.cur);
									if(slider.settings.loop) s =slider.settings.direction==='up'? slider.dsplHgt+slider.mov*(slider.navNum+slider.flags.cur): slider.mov*(slider.navNum*2-slider.flags.cur);
									if(slider.flags.static) s =slider.settings.direction==='up'? s-slider.mov*(slider.flags.cur+slider.settings.showNum-slider.num): s+slider.mov*(slider.flags.cur+slider.settings.showNum-slider.num);
								}
								slider.curPos = {
									left: 0,
									top: s
								};
								return cssEnable.translate? 'translateY('+(-s)+'px)': {
									left: 0,
									top: s
								};
							}
						};
					if(slider.flags.initialized){
						if(cssEnable.translate){
							slider.$area.css({
								transform: slideMove(),
								transition: st+'ms transform'+slider.settings.transEase
							});
							setTimeout(function(){
								if(slider.flags.initialized) slider.$area.css({transition: '0ms'});
								if(slider.flags.cur>=slider.navNum || slider.flags.cur<0){
									if(slider.flags.cur>=slider.navNum) slider.flags.cur = 0;
									else slider.flags.cur = slider.navNum-1;
									if(slider.flags.initialized) slider.$area.css({transform: slideMove()});
								}
								dMain.resolve();
							}, st);
						}else{
							slider.$contents.stop().animate({
								scrollLeft: slideMove().left,
								scrollTop: slideMove().top
							}, st, slider.settings.slideEase, function(){
								if(slider.flags.cur>=slider.navNum){
									slider.flags.cur = 0;
									slider.$contents.scrollLeft(slideMove().left).scrollTop(slideMove().top);
								}else if(slider.flags.cur<0){
									slider.flags.cur = slider.navNum-1;
									slider.$contents.scrollLeft(slideMove().left).scrollTop(slideMove().top);
								}
								dMain.resolve();
							});
						}
					}else{
						if(cssEnable.translate) slider.$area.css({transform: slideMove()});
						else slider.$contents.stop().scrollLeft(slideMove().left).scrollTop(slideMove().top);
						slider.flags.initialized = true;
						dMain.resolve();
					}
				break;

/* Type:Fade
---------------------------------------- */

				case 'fade':
					if(slider.flags.initialized){
						if(cssEnable.transition){
							slider.$el.not('.'+prefix+'Empty').css({transition: st+'ms opacity'+slider.settings.transEase});
							slider.$cur.css({opacity: 1});
							slider.$area.children('.'+cls.last).css({opacity: 0});
							setTimeout(function(){
								if(slider.flags.moving){
									slider.$el.not('.'+prefix+'Empty').css({transition: '0s'});
									dMain.resolve();
								}
							}, st);
						}else{
							slider.$cur.animate({
								opacity: 1
							}, st, slider.settings.slideEase, function(){
								if(slider.flags.moving){
									dMain.resolve();
								}
							});
							slider.$area.children('.'+cls.last).animate({
								opacity: 0
							}, st, slider.settings.slideEase);
						}
					}else{
						slider.$el.not('.'+prefix+'Empty').css({opacity: 0});
						slider.$cur.css({opacity: 1});
						slider.flags.initialized = true;
						dMain.resolve();
					}
				break;

/* Type:Flip
---------------------------------------- */

				case 'flip':
					var deg =slider.flags.vertical? {
							set:slider.flags.dirPositive? 'rotateX(-180deg)': 'rotateX(180deg)',
							out:slider.flags.dirPositive? 'rotateX(180deg)': 'rotateX(-180deg)',
							act: 'rotateX(0deg)'
						}: {
							set:slider.flags.dirPositive? 'rotateY(180deg)': 'rotateY(-180deg)',
							out:slider.flags.dirPositive? 'rotateY(-180deg)': 'rotateY(180deg)',
							act: 'rotateY(0deg)'
						};
					if(slider.flags.initialized){
						var t = st+'ms transform',
							tEnd = st+100;
						slider.$cur.css({
							transform: deg.set
						});
						setTimeout(function(){
							slider.$el.not('.'+prefix+'Empty').each(function(){
								var $this = $(this);
								if($this.hasClass(cls.act)){
									$this.css({
										transform: deg.act,
										transition: t+slider.settings.transEase
									});
								}else if($this.hasClass(cls.last)){
									$this.css({
										transform: deg.out,
										transition: t+slider.settings.transEase
									});
								}else{
									$this.hide();
								}
							});
						}, 50);
						setTimeout(function(){
							slider.$el.not('.'+prefix+'Empty').css({
								transition: '0ms'
							}).show();
							dMain.resolve();
						}, tEnd);

					}else{
						slider.$cur.css({
							transform: deg.act
						});
						slider.flags.initialized = true;
						dMain.resolve();
					}
				break;
				}

/* Paging Control
---------------------------------------- */

				if(slider.settings.paging){
					slider.$prev.css({display: ''});
					slider.$next.css({display: ''});
					if(!slider.settings.loop){
						if(slider.flags.cur===0) slider.$prev.hide();
						else if(slider.flags.cur===slider.navNum-1) slider.$next.hide();
					}
				}

/* Navigation Control
---------------------------------------- */

				if(slider.settings.nav){
					slider.$nav.removeClass(cls.navAct);
					slider.$nav.eq(slider.flags.cur%(slider.navNum)).addClass(cls.navAct);
				}

/* Change Class Name
---------------------------------------- */

				if(slider.settings.changeClass){
					cClass.cnd = false;
					if(cClass.divisor && cClass.remainder>-1){
						if((slider.flags.cur+1)%cClass.divisor===cClass.remainder) cClass.cnd = true;
					}else{
						if(~slider.settings.changeClass.indexOf((slider.flags.cur+1)%slider.num)) cClass.cnd = true;
					}
					if(cClass.cnd) slider.$wrap.addClass(cClass.name);
					else slider.$wrap.removeClass(cClass.name);
				}
				return dMain.promise();

/* After
---------------------------------------- */

			}).then(function(){
				return slider.chgAfter(slider, {
						idx: slider.flags.cur,
						$: slider.$cur
					});
			}).then(function(){
				slider.flags.last = slider.flags.cur;
				slider.flags.moving = false;
				if(!slider.flags.resize && Number.isFinite(slider.settings.auto) && slider.flags.cur===slider.navNum-1) slider.flags.autoCnt++;
				slider.timerOn();
			});
		};


/* ----------------------------------------
	Fnc slider.appear
---------------------------------------- */

		slider.appear = function(){
			if(slider.settings.loader) loader.flg.comp = true;
			if(slider.flags.initialized){
				slider.timerOff();
				slider.flags.initialized = false;
				slider.$wrap[0].className = prefix+'Wrap';
				slider.$contents.removeAttr('style');
				slider.$area.removeAttr('style');
				slider.$el.removeAttr('style').removeClass(prefix+'El '+cls.act);
			}
			slider.wdt =slider.flags.vertical? slider.$area.width(): slider.$area.width()/slider.settings.showNum;
			slider.hgt =slider.flags.vertical? slider.$area.height()/slider.settings.showNum: slider.$area.height();
			slider.dsplWdt = slider.$contents.width();
			slider.dsplHgt = slider.$contents.height();
			switch(slider.settings.type){

/* Type:Slide
---------------------------------------- */

			case 'slide':
				if(!slider.flags.vertical){
					slider.mov =slider.settings.moveAll? slider.wdt*slider.settings.showNum: slider.wdt;
					var areaWdt =slider.settings.loop? slider.mov*slider.navNum*3: slider.mov*slider.navNum,
						 pad =slider.settings.showNum<slider.num? slider.dsplWdt: 0;
					//slider.$el.width(slider.wdt);
					slider.$area.width(areaWdt).css({padding: '0 '+pad+'px'});
				}else{
					slider.mov =slider.settings.moveAll? slider.hgt*slider.settings.showNum: slider.hgt;
					var areaHgt =slider.settings.loop? slider.mov*slider.navNum*3: slider.mov*slider.navNum,
						 padV =slider.settings.showNum<slider.num? slider.dsplHgt: 0;
					slider.$area.children().each(function(i){
						var $this = $(this);
						if(slider.settings.direction==='up') $this.css({
								top: padV+slider.hgt*i,
								height: slider.hgt
							});
						else $this.css({
							top: 'auto',
							bottom: padV+slider.hgt*i,
							height: slider.hgt
						});
					});
					slider.$area.height(areaHgt).css({padding: padV+'px 0'});
				}
			break;

/* Type:Fade, Flip
---------------------------------------- */

			case 'fade': case 'flip':
				slider.$el.each(function(i){
					var $this = $(this);
					if(slider.settings.showNum>1){
						if(slider.flags.vertical){
							if(slider.settings.direction==='up') $this.css({
								top: slider.hgt*(i%slider.settings.showNum),
								bottom: 'auto'
							});
							else $this.css({
								top: 'auto',
								bottom: slider.hgt*(i%slider.settings.showNum)
							});
						}else{
							if(slider.flags.left) $this.css({
								left: slider.wdt*(i%slider.settings.showNum),
								right: 'auto'
							});
							else $this.css({
								left: 'auto',
								right: slider.wdt*(i%slider.settings.showNum)
							});
						}
					}
					$this.css({
						width: slider.wdt,
						height: slider.hgt
					});
				});
				slider.dsplHgt =slider.flags.vertical? slider.hgt*slider.settings.showNum: slider.hgt;
				slider.$area.height(slider.dsplHgt);
				slider.$wrap.addClass(cls.type(slider.settings.type));
			break;
			}

/* Add Class, slider.chgSlide()
---------------------------------------- */

			if(slider.settings.direction==='up' || slider.settings.direction==='down') slider.$wrap.addClass(prefix+'Vertical');
			if(!slider.flags.left) slider.$wrap.addClass(prefix+slider.settings.direction.substr(0, 1).toUpperCase());
			slider.$el.addClass(prefix+'El');
			slider.chgSlide(slider.flags.cur);
		};

/* Ignite
---------------------------------------- */

		if(slider.imgArr.length){
			slider.imgLoad = function(){
				var img = new Image;
					img.src = slider.imgArr[0];
				img.onload = function(){
					slider.imgArr.shift();
					if(slider.imgArr.length) slider.imgLoad();
					else slider.appear();
				};
				img.onerror = function(){
					slider.appear();
				};
			};
			slider.imgLoad();
		}else slider.appear();
		$window.on('resize', function(){
			if(!slider.flags.destroyed){
				slider.flags.resize = true;
				slider.appear();
			}
		});
	});//End of return
	},



/* ++++++++++++++++++++++++++++++++++++++++

	Other Methods

++++++++++++++++++++++++++++++++++++++++ */

/* ----------------------------------------
	methods.slideTo
---------------------------------------- */

	slideTo: function(idx){
	return this.each(function(){
	if(this.slider.flags.initialized){
		var slideTo =typeof idx!=='undefined' && Number.isFinite(idx)? idx-1: null;
		if(slideTo>=this.slider.navNum) slideTo = this.slider.navNum-1;
		else if(slideTo<0 || (slideTo===null && !this.slider.settings.loop && this.slider.flags.cur>=this.slider.navNum-1)) slideTo = 0;
		this.slider.chgSlide(slideTo);
	}//End of if(this.slider.flags.initialized)
	});//End of return
	},


/* ----------------------------------------
	methods.play
---------------------------------------- */

	play: function(idx){
	return this.each(function(){
		if(typeof this.slider!=='undefined'){
			this.slider.flags.stop = false;
			this.slider.play();
		}
	});//End of return
	},


/* ----------------------------------------
	methods.pause
---------------------------------------- */

	pause: function(idx){
	return this.each(function(){
		if(typeof this.slider!=='undefined'){
			this.slider.flags.stop = true;
			this.slider.pause();
		}
	});//End of return
	},


/* ----------------------------------------
	methods.setOption
---------------------------------------- */

	setOption: function(options){
	return this.each(function(){
		if(typeof this.slider!=='undefined' && this.slider.flags.initialized){
			if(Object.prototype.toString.call(options)==='[object Object]') $.extend(this.slider.settings, this.slider.options, options);
			else if(typeof options ==='undefined'){
				$.extend(this.slider.options, $.fn.grSlider.defaults);
				$.extend(this.slider.settings, this.slider.options);
			}
			this.slider.setup();
			this.slider.appear();
		}
	});//End of return
	},


/* ----------------------------------------
	methods.destroy
---------------------------------------- */

	destroy: function(){
	return this.each(function(){
		if(typeof this.slider!=='undefined' && this.slider.flags.initialized && !this.slider.flags.destroyed){
			this.slider.pause();
			this.slider.flags.initialized = false;
			this.slider.flags.destroyed = true;
			this.slider.$contents.siblings().remove();
			this.slider.$wrap.after(this.slider.$area.removeAttr('style').html(this.slider.original)).remove();
		}
	});//End of return
	},


/* ----------------------------------------
	methods.rebuild
---------------------------------------- */

	rebuild: function(options){
	return this.each(function(){
		var newOptions =Object.prototype.toString.call(options)==='[object Object]'? options: null;
		if(typeof this.slider!=='undefined' && !this.slider.flags.destroyed) $(this).grSlider('destroy');
		$(this).grSlider(newOptions);
	});//End of return
	}
};



/* ++++++++++++++++++++++++++++++++++++++++

	Plug-in

++++++++++++++++++++++++++++++++++++++++ */

$.fn.grSlider = function(method){
	if(methods[method]){
		return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
	}else if(typeof method==='object' || !method){
		return methods.init.apply(this, arguments);
	}else{
		$.error('Method '+method+' does not exist on jQuery.grSlider');
	}
};



/* ++++++++++++++++++++++++++++++++++++++++

	Default Settings

++++++++++++++++++++++++++++++++++++++++ */

$.fn.grSlider.defaults = {
	auto: true,//閾ｪ蜍募�逕�
	changeTime: 3000,//閾ｪ蜍募�逕滄俣髫�
	slideTime: 500,//繧ｹ繝ｩ繧､繝牙柑譫懈凾髢�
	loop: true,//郢ｰ繧願ｿ斐＠
	type: 'slide',//繧ｹ繝ｩ繧､繝牙ｽ｢蠑�
	initNum: 1,//蛻晄悄陦ｨ遉ｺ繧ｹ繝ｩ繧､繝臥分蜿ｷ
	showNum: 1,//陦ｨ遉ｺ繧ｹ繝ｩ繧､繝画焚
	moveAll: false,//陦ｨ遉ｺ蛻�∪縺ｨ繧√※繧ｹ繝ｩ繧､繝�
	slideEase: 'swing',//繧ｹ繝ｩ繧､繝牙柑譫�
	direction: 'left',//繧ｹ繝ｩ繧､繝画婿蜷�
	paging: true,//蜑榊ｾ後�繧ｿ繝ｳ譛臥┌
	nav: true,//繝翫ン譛臥┌
	changeClass: false,//繧ｯ繝ｩ繧ｹ螟画峩
	flickable: true,//繝輔Μ繝�け謫堺ｽ�
	pauseOnHover: false,//繝槭え繧ｹ繧ｪ繝ｼ繝舌�縺ｧ荳譎ょ●豁｢
	loader: true,//繝ｭ繝ｼ繝�ぅ繝ｳ繧ｰ貍泌�
	beforeSlide: false,//繧ｹ繝ｩ繧､繝牙燕縺ｫ螳溯｡後☆繧矩未謨ｰ
	afterSlide: false//繧ｹ繝ｩ繧､繝牙ｾ後↓螳溯｡後☆繧矩未謨ｰ
};

//EOS
})(jQuery);