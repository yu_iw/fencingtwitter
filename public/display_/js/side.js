/* ++++++++++++++++++++++++++++++++++++++++

	LED

++++++++++++++++++++++++++++++++++++++++ */

$(function(){
//BOS

/* ----------------------------------------
	Global Variables
---------------------------------------- */

var $window = $(window),
	$player = {
		l: $('.playerL'),
		r: $('.playerR')
	},
	cur = {
		nameL: '',
		nameR: '',
		nationL: '',
		nationR: '',
		scoreL: -1,
		scoreR: -1,
		yellowL: -1,
		yellowR: -1,
		redL: -1,
		redR: -1,
		status: '',
		timeRemain: '0:00',
		Round: ''
	};

/* ----------------------------------------
	reload
---------------------------------------- */
var reloading = sessionStorage.getItem("reloading");
if (reloading) {
    sessionStorage.removeItem("reloading");
    refreshTweet();
}

/* ----------------------------------------
	fixfont
---------------------------------------- */

var ff = {
		$el: $('.fixfont'),
		loaded: false,
		num: 0,
		len: $('.fixfont').length,
		fnc: function($el){
			$el.each(function(){
				var $this = $(this),
					box ={
						wdt: $this.width(),
						hgt: $this.height(),
					},
					$text = $this.children(),
					text = {
						wdt: $text.width(),
						hgt: $text.height()
					},
					fz = parseInt($this.css('font-size')),
					n = 0;
if(window.console && typeof window.console.log==='function') console.log(box.hgt+'font-size:'+fz+' > '+text.hgt);
				while(text.hgt>box.hgt && n<50){
					fz --;
					$this.css({fontSize: fz+'px'});
					text.wdt = $text.width();
					text.hgt = $text.height();
					n++;
				}
				$this.css({fontSize: fz+'px'});
				ff.num++;
				if(ff.num>=ff.len){
					if(!ff.loaded){
						ff.loaded = true;
//						ff.$el.find('>div>div:not(.stable)').text('');
					}
					ff.$el.css({opacity: 1});
				}
			});
		}
	};

/*
$window.load(function(){
	ff.fnc(ff.$el);
});
*/


/* ----------------------------------------
	Web Socket
---------------------------------------- */

// エラー処理
ws.onerror = function(e){
	console.log('サーバに接続できませんでした。');
}

// WebSocketサーバ接続イベント
ws.onopen = function(){
	// 入室情報を文字列に変換して送信
	ws.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};

// メッセージ受信イベントを処理
ws.onmessage = function(event){
	var data = JSON.parse(event.data);
	if(data.type==='admin'){
	//選手データ受信
		$('.nameL').text(data.nameL);
		$('.flagL').attr('data-nation', data.nationL.toLowerCase());
		$('.nationL').text(data.nationL)
		.siblings('i').attr('data-nation', data.nationL.toLowerCase());
		$('.nameR').text(data.nameR);
		$('.flagR').attr('data-nation', data.nationR.toLowerCase());
		$('.nationR').text(data.nationR)
		.siblings('i').attr('data-nation', data.nationR.toLowerCase());
		$('.name').removeAttr('style');
		ff.fnc($('.name'));
	}else if(data.type==='data'){
	//試合中データ受信
		//if(ff.loaded){
			if(data.priorityL){
				$('.priorityL').addClass('on');
			}else{
				$('.priorityL').removeClass('on');
			}
		if(data.invalidL){
			$player.l.addClass('invalid');
		}else{
			$player.l.removeClass('invalid');
		}
		if(data.hitL){
			$player.l.addClass('on');
		}else{
			$player.l.removeClass('on');
		}
		if(cur.scoreL!=data.scoreL){
			cur.scoreL = data.scoreL;
			$('.scoreL').text(cur.scoreL);
		}
			if(data.priorityR){
				$('#priorityR').addClass('on');
			}else{
				$('#priorityR').removeClass('on');
			}
		if(data.invalidR){
			$player.r.addClass('invalid');
		}else{
			$player.r.removeClass('invalid');
		}
		if(data.hitR){
			$player.r.addClass('on');
		}else{
			$player.r.removeClass('on');
		}
		if(cur.scoreR!=data.scoreR){
			cur.scoreR = data.scoreR;
			$('.scoreR').text(cur.scoreR);
		}
		if(data.yellowL > 0){
			$('.yellowcardL').addClass('on');
		}else{
			$('.yellowcardL').removeClass('on');				
		}
		if(data.yellowR > 0){
			$('.yellowcardR').addClass('on');
		}else{
			$('.yellowcardR').removeClass('on');				
		}
		if(data.redL > 0){
			$('.redcardL').addClass('on');
		}else{
			$('.redcardL').removeClass('on');
		}
		if(data.redR > 0){
			$('.redcardR').addClass('on');
		}else{
			$('.redcardR').removeClass('on');
		}
		if(cur.timeRemain!=data.timeRemain){
			cur.timeRemain = data.timeRemain;
			$('.time').text(cur.timeRemain);
		}
		if(cur.round!=data.round){
			cur.round = data.round;
			$('.round').text('R'+cur.round);
		}
	}
}

// ブラウザ終了イベント
window.onbeforeunload = function(){
	ws.send(JSON.stringify({
		type: 'defect',
		user: self.name,
	}));
};


/* ----------------------------------------
	Sponsor
---------------------------------------- */
				
	$('.loopslider').each(function(){
		var $ul = $(this).children('ul');
		//console.log($ul.width());
		var $li = $ul.children('li');

		var sum = 0;
		for (var i = 0; i < $li.length; i++) {
		var $liW = $($li.get(i)).outerWidth();
			sum += $liW;
		}
		
		var loopsliderWidth = $(this).width();
		var loopsliderHeight = $(this).height();
		var $wrap = $(this).children('ul').wrapAll('<div class="loopslider_wrap"></div>');

		//var listWidth = $('.loopslider_wrap').children('ul').children('li').width();
		var $wrap = $(this).children('.loopslider_wrap');
		var listCount = $wrap.children('ul').children('li').length;


		console.log(listCount);

		var loopWidth = (sum)+listCount*20;

		$wrap.css({
			top: '0',
			left: '0',
			width: ((loopWidth) * 2),
			height: (loopsliderHeight),
			overflow: 'hidden',
			position: 'absolute'
		});

		$wrap.children('ul').css({
			width: (loopWidth)
		});
		loopsliderPosition();

		function loopsliderPosition(){
			$wrap.css({left:'0'});
			$wrap.stop().animate({left:'-' + (loopWidth) + 'px'},250000,'linear');
			setTimeout(function(){
				loopsliderPosition();
			},250000);
		}
		$wrap.children('ul').clone().appendTo($wrap);
  });
	

	
/* ----------------------------------------
	AutoPlay
---------------------------------------- */
	
var $sec = $('li'),
	program = [
		['', ''],
		['on', ''],
		['', 'on'],
		['on', 'on'],
		['on', 'invalid'],
		['invalid', 'on'],
		['invalid', 'invalid'],
		['invalid', ''],
		['', 'invalid']
	],
	cnt = 0,
	len = program.length,
	time = 1000,
	flg = {
		p: true,
		loop: false,
		key: false
	},
	fnc = function(){
		if(!flg.loop){
			if(cnt===0) ff.fnc(ff.$el);
			flg.loop = true;
			var pIdx = cnt%len;
			$sec.eq(0)[0].className = program[pIdx][0];
			$sec.eq(1)[0].className = program[pIdx][0];
			$sec.eq(2)[0].className = program[pIdx][1];
			$sec.eq(3)[0].className = program[pIdx][1];
			cnt++;
			setTimeout(function(){
				flg.loop = false;
				flg.key = false;
				if(flg.p) fnc();
			}, time);
		}
	};
/*fnc(); /*debug*/

/* ----------------------------------------
	Message
---------------------------------------- */
//updateMessage();
	
function updateMessage(){
	
	var messages = [];

	$.getJSON('../../data/json/message.json') // json読み込み開始
		.done(function(json){ // jsonの読み込みに成功した時
			for(var i=0; i<json.length; i++){
				console.log(json[i]['message']);
				messages.push(json[i]['message'])
			}
			updateMessageDisplay(messages);

		})
	.fail(function(){ // jsonの読み込みに失敗した時
		//console.log('失敗');
	})
	.always(function(){ // 成功/失敗に関わらず実行
		//console.log('必ず実行される');
	});
}
function updateMessageDisplay(messages){
	var i = 0;
	var mslen = messages.length;
	var msl = [];
	var msr = [];
	while(i < mslen){
		var mlen = messages[i].length;
		var cutlen = mlen;
		if(mlen>36){
			while(cutlen > 36){
				cutlen = Math.floor(cutlen / 2)+1;
				//console.log(cutlen);
			}
		}else{
			cutlen = Math.floor(cutlen / 2)+1;
				//console.log(cutlen);
		}
		//console.log(cutlen);
		//var message = messages[i].substring(cutlen);
		var message = splitByLength(messages[i],cutlen);
		//console.log(message);
		i++;
		var j = 0;
		var lrlen = message.length;
		while(j < lrlen){
			msl.push("<li><span>" + message[j] + "</span></li>");
			msr.push("<li><span>" + message[j+1] + "</span></li>");
			j++;j++;
		}
	}
	$('.messageL ul').each(function(){
		this.innerHTML = msl.join("");
		//console.log(msl);
	})
	$('.messageR ul').each(function(){
		this.innerHTML = msr.join("");
		//console.log(msr);
	})
}
	
/* ----------------------------------------
	Twitter
---------------------------------------- */
//updateTweets();
	
function updateTweets(){
	
	var tweets = [];

	$.getJSON('../../data/json/twitter.json') // json読み込み開始
		.done(function(json){ // jsonの読み込みに成功した時
			for(var i=0; i<json.length; i++){
				console.log(json[i]['body']);
				console.log(json[i]['user_id']);
				tweets.push(json[i]['body']+" @"+json[i]['user_id'])
			}
			updateTweetsDisplay(tweets);

		})
	.fail(function(){ // jsonの読み込みに失敗した時
		//console.log('失敗');
	})
	.always(function(){ // 成功/失敗に関わらず実行
		//console.log('必ず実行される');
	});
}
	
function updateTweetsDisplay(tweets){
i = 0;
var twlen = tweets.length;
var twl = [];
var twr = [];
while(i < twlen){
	var tlen = tweets[i].length;
	var cutlen = tlen;
	if(cutlen>36){
		while(cutlen > 36){
			cutlen = Math.floor(cutlen / 2)+1;
			//console.log(cutlen);
		}
	}else{
		cutlen = Math.floor(cutlen / 2)+1;
			//console.log(cutlen);
	}
	//console.log(cutlen);
	//var message = messages[i].substring(cutlen);
	var tweet = splitByLength(tweets[i],cutlen);
	//var tweet = tweets[i];
	//console.log(message);
	i++;
	var j = 0;
	var lrlen = tweet.length;
	while(j < lrlen){
		if(i % 2 == 0){
			twl.push("<li><span class='evenTweet'>" + tweet[j] + "</span></li>");
			twr.push("<li><span class='evenTweet'>" + tweet[j+1] + "</span></li>");
		}else{
			twl.push("<li><span>" + tweet[j] + "</span></li>");
			twr.push("<li><span>" + tweet[j+1] + "</span></li>");
		}
		j++;j++;
	}
}
	$('.twitterL ul').each(function(){
		this.innerHTML = twl.join("");
		//console.log(twl);
	})
	$('.twitterR ul').each(function(){
		this.innerHTML = twr.join("");
		//console.log(twr);
	})
}
	
function splitByLength(str, length) {
    var resultArr = [];
    if (!str || !length || length < 1) {
        return resultArr;
    }
    var index = 0;
    var start = index;
    var end = start + length;
    while (start < str.length) {
        resultArr[index] = str.substring(start, end);
        index++;
        start = end;
        end = start + length;
    }
    return resultArr;
}

function refreshTweet() {
	console.log('refresh');

	function tweetLoad(){
		console.log('tweetLoad');
		$('.loopmessage').html('<ul></ul>');
		updateMessage();
		$('.looptweet').html('<ul></ul>');
		updateTweets();
	}
	function tweetSlider(){
		console.log('tweetSlider');
		$('.looptweet >ul').grSlider({
			loop: true,
			direction: 'up',
			paging: false,
			nav: false,
			flickable: false
		});
	}
	function displayTweet(){
		console.log('displayTweet');
		$("#sideTwitter").css("z-index","1000");
		$("#sideTwitter").siblings().css("z-index","1");
	}

	setTimeout(function(){
		tweetLoad();
    },500);
	setTimeout(function(){
		tweetSlider();
    },500);
	setTimeout(function(){
		displayTweet();
    },500);
}


/* ----------------------------------------
	Mode
---------------------------------------- */

document.onkeydown = function(e){
		if(e.key=='p'){
			if(!flg.key){
				flg.key = true;
				flg.p = !flg.p;
				fnc();
			}
		}
		if(e.key=='1'){
			$("#sideLed").css("z-index","1000");
			$("#sideLed").siblings().css("z-index","1");
		}
		if(e.key=='2'){
			$("#sideSponsor").css("z-index","1000");
			$("#sideSponsor").siblings().css("z-index","1");
		}
		if(e.key=='3'){
			$("#sideMessage").css("z-index","1000");
			$("#sideMessage").siblings().css("z-index","1");
		}
		if(e.key=='4'){
			$("#sideTwitter").css("z-index","1000");
			$("#sideTwitter").siblings().css("z-index","1");
		}
		if(e.key=='5'){
			$("#sideTwitter2").css("z-index","1000");
			$("#sideTwitter2").siblings().css("z-index","1");
		}
		if(e.key=='m'){
			$('.loopmessage >ul').grSlider({
				loop: false,
				direction: 'up',
				paging: false,
				nav: false,
				flickable: false
			});
		}
		if(e.key=='t'){
			$('.looptweet >ul').grSlider({
				loop: true,
				direction: 'up',
				paging: false,
				nav: false,
				flickable: false
			});
		}
		if(e.key=='l'){
			console.log('reboot');
			sessionStorage.setItem("reloading", "true");
		    document.location.reload();
		}
		if(e.key=='u'){
			$('.loopmessage').html('<ul></ul>');
			updateMessage();
			$('.looptweet').html('<ul></ul>');
			updateTweets();
		}
		if(e.key=='s'){
			$('.seiko').toggle();
		}
		if(e.key=='r'){
			$('.rotate').toggleClass('rotate');
		}
};

//EOS
});