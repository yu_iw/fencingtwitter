<?php

date_default_timezone_set('Asia/Tokyo');
// システムのルートディレクトリパス
define('ROOT_PATH', realpath(dirname(__FILE__) . '/app'));
// ライブラリのディレクトリパス
define('LIB_PATH', realpath(dirname(__FILE__) . '/lib'));
// フレームワークのURL上のルートディレクトリパス
define('URL_ROOT', '/');
// 現在のURLを取得
define('URL_FULL', 'http://'.$_SERVER['HTTP_HOST']);
//ディレクトリパス
define('MAIN_PATH', realpath(dirname(__FILE__)));
//twitter設定
// define('T_USERID', '1943878981');
//Display Errors
// ini_set( 'display_errors', 0 );

// 定数領域
$prefecture = '{"-1":"都道府県の選択","1":"北海道","2":"青森県","3":"岩手県","4":"宮城県","5":"秋田県","6":"山形県","7":"福島県","8":"茨城県","9":"栃木県","10":"群馬県","11":"埼玉県","12":"千葉県","13":"東京都","14":"神奈川県","15":"新潟県","16":"富山県","17":"石川県","18":"福井県","19":"山梨県","20":"長野県","21":"岐阜県","22":"静岡県","23":"愛知県","24":"三重県","25":"滋賀県","26":"京都府","27":"大阪府","28":"兵庫県","29":"奈良県","30":"和歌山県","31":"鳥取県","32":"島根県","33":"岡山県","34":"広島県","35":"山口県","36":"徳島県","37":"香川県","38":"愛媛県","39":"高知県","40":"福岡県","41":"佐賀県","42":"長崎県","43":"熊本県","44":"大分県","45":"宮崎県","46":"鹿児島県","47":"沖縄県"}';
define('PREFECTURE', $prefecture);
$sex = '{"1":"男性","2":"女性"}';
define('SEX', $sex);

// ライブラリとモデルのディレクトリをinclude_pathに追加
$includes = array(LIB_PATH . '/mvc', ROOT_PATH . '/models');
$incPath = implode(PATH_SEPARATOR, $includes);
set_include_path(get_include_path() . PATH_SEPARATOR . $incPath);

// クラスのオートロード
function __autoload($className){
    require_once $className . ".php";
}

 //データベース接続情報設定
$connInfo = array(
'host'     => 'localhost',
'dbname'   => 'fencing-twitter',
'dbuser'   => 'fencing-twitter.grpht.co.jp',
'password' => 'grpht0601'
);
 // $connInfo = array(
 //    'host'     => 'localhost',
 //    'dbname'   => 'ikumen',
 //    'dbuser'   => 'ikumen.grpht.co.jp',
 //    'password' => 'grpht0601'
 // );

ModelBase::setConnectionInfo($connInfo);


$connInfo2 = array(
 'host'     => '111.89.176.217',
 'dbname'   => 'ikumen2',
 'dbuser'   => 'ikumenadmin',
 'password' => 'DwkPqyeGk'
);

ModelBase2::setConnectionInfo($connInfo2);

// require_once LIB_PATH.'/mvc/qdmail/QdController.php';
// $mailprof = array(
// 		'host'=>'',
// 		'port'=>587,
// 		'from'=>'',
// 		'user'=>'',
// 		'pass'=>'',
// 		'protocol'=>'SMTP'
// );

require_once LIB_PATH.'/mvc/qdmail/QdController.php';
$mailprof = array(
		'host'=>'mail.ikumen-project.jp',
		'port'=>25,
		'from'=>'no_reply@ikumen-project.jp',
		'user'=>'pop@ikumen-project.jp',
		'pass'=>'Dg3QpUe',
		'protocol'=>'POP_BEFORE'
);
QdController::setMailprof($mailprof);

// require_once LIB_PATH.'/mvc/phpmailer/PHPMailController.php';
// PHPMailController::setMailprof($mailprof);

// リクエスト処理
$dispatcher = new Dispatcher();
$dispatcher->setSystemRoot(ROOT_PATH);
$dispatcher->dispatch();

?>