<?php
		// curlを初期化
		$curl = curl_init ();
		curl_setopt ( $curl, CURLOPT_HTTPHEADER, array (
				'Content-Type: application/json' 
		) );
		curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true ); 
		curl_setopt ( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );
		curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); 
		$url = "https://104.215.0.251/api/data/json/1.0/realtimematchcard"; // APIのURL
		curl_setopt ( $curl, CURLOPT_URL, $url ); 
		$param = array (
				"green1" => array (
						"left" => "Taro Yamada",
						"left_country" => "JPN",
						"right" => "Taro Yamada",
						"right_country" => "USA" 
				),
				"green2" => array (
						"left" => "Taro Yamada",
						"left_country" => "JPN",
						"right" => "Taro Yamada",
						"right_country" => "USA" 
				),
				"red1" => array (
						"left" => "Taro Yamada",
						"left_country" => "JPN",
						"right" => "Taro Yamada",
						"right_country" => "USA" 
				),
				"red2" => array (
						"left" => "Taro Yamada",
						"left_country" => "JPN",
						"right" => "Taro Yamada",
						"right_country" => "USA" 
				),
				"yellow1" => array (
						"left" => "Taro Yamada",
						"left_country" => "JPN",
						"right" => "Taro Yamada",
						"right_country" => "USA" 
				),
				"yellow2" => array (
						"left" => "Taro Yamada",
						"left_country" => "JPN",
						"right" => "Taro Yamada",
						"right_country" => "USA" 
				),
				"blue1" => array (
						"left" => "Taro Yamada",
						"left_country" => "JPN",
						"right" => "Taro Yamada",
						"right_country" => "USA" 
				),
				"blue2" => array (
						"left" => "Taro Yamada",
						"left_country" => "JPN",
						"right" => "Taro Yamada",
						"right_country" => "USA" 
				),
				"podium" => array (
						"left" => "Taro Yamada",
						"left_country" => "JPN",
						"right" => "Taro Yamada",
						"right_country" => "USA" 
				) 
		);
		$data_json = json_encode ( $param );
		curl_setopt ( $curl, CURLOPT_POSTFIELDS, $data_json );
		$response = curl_exec ( $curl ); 
		echo $response; // メッセージの表示（成功ならOKになる）
		curl_close ( $curl );
?>
