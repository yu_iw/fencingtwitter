/* ++++++++++++++++++++++++++++++++++++++++

	Administrator

++++++++++++++++++++++++++++++++++++++++ */

$(function(){
//BOS

/* ----------------------------------------
	WebSocketサーバに接続
---------------------------------------- */

var ws1 = new WebSocket('ws://192.168.10.198:8888/'),//BLUE
	ws2 = new WebSocket('ws://192.168.10.164:8888/'),
	ws3 = new WebSocket('ws://192.168.10.164:8888/'),
	ws4 = new WebSocket('ws://192.168.10.164:8888/'),
	ws5 = new WebSocket('ws://192.168.10.164:8888/'),
	ws6 = new WebSocket('ws://192.168.10.164:8888/'),
	ws7 = new WebSocket('ws://192.168.10.164:8888/'),
	ws8 = new WebSocket('ws://192.168.10.164:8888/'),
	ws9 = new WebSocket('ws://192.168.10.164:8888/'),//Podium
	ws10 = new WebSocket('ws://192.168.10.164:8888/'),//Abema
	ws11 = new WebSocket('ws://192.168.10.164:8888/'),//国際
	ws0 = new WebSocket('ws://127.0.0.1:8888/'),//トーナメント表表示
	wsLen = 2;

var self = {
		name: 'Administrator'
	};

/* エラー処理
---------------------------------------- */

ws0.onerror = function(e){
	$('#log').prepend('<div>サーバ1に接続できませんでした。</div>');
};
ws1.onerror = function(e){
	$('#log').prepend('<div>サーバ1に接続できませんでした。</div>');
};
ws2.onerror = function(e){
	$('#log').prepend('<div>サーバ2に接続できませんでした。</div>');
};
ws3.onerror = function(e){
	$('#log').prepend('<div>サーバ3に接続できませんでした。</div>');
};
ws4.onerror = function(e){
	$('#log').prepend('<div>サーバ4に接続できませんでした。</div>');
};
ws5.onerror = function(e){
	$('#log').prepend('<div>サーバ5に接続できませんでした。</div>');
};
ws6.onerror = function(e){
	$('#log').prepend('<div>サーバ6に接続できませんでした。</div>');
};
ws7.onerror = function(e){
	$('#log').prepend('<div>サーバ7に接続できませんでした。</div>');
};
ws8.onerror = function(e){
	$('#log').prepend('<div>サーバ8に接続できませんでした。</div>');
};
ws9.onerror = function(e){
	$('#log').prepend('<div>サーバ9に接続できませんでした。</div>');
};
ws10.onerror = function(e){
	$('#log').prepend('<div>サーバ10に接続できませんでした。</div>');
};
ws11.onerror = function(e){
	$('#log').prepend('<div>サーバ11に接続できませんでした。</div>');
};

/* WebSocketサーバ接続イベント
---------------------------------------- */

ws0.onopen = function() {
	ws0.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};
ws1.onopen = function() {
	ws1.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};
ws2.onopen = function() {
	ws2.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};
ws3.onopen = function() {
	ws3.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};
ws4.onopen = function() {
	ws4.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};
ws5.onopen = function() {
	ws5.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};
ws6.onopen = function() {
	ws6.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};
ws7.onopen = function() {
	ws7.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};
ws8.onopen = function() {
	ws8.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};
ws9.onopen = function() {
	ws9.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};
ws10.onopen = function() {
	ws10.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};
ws11.onopen = function() {
	ws11.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};


/* ----------------------------------------
	データ送信
---------------------------------------- */

btnSend.onclick = function(event){
		var data = {
			"green1": {
				"left": "",
				"left_country": "",
				"right":"",
				"right_country": ""
			},
			"green2": {
				"left": "",
				"left_country": "",
				"right":"",
				"right_country": ""
			},
			"red1": {
				"left": "",
				"left_country": "",
				"right":"",
				"right_country": ""
			},
			"red2": {
				"left": "",
				"left_country": "",
				"right":"",
				"right_country": ""
			},
			"yellow1": {
				"left": "",
				"left_country": "",
				"right":"",
				"right_country": ""
			},
			"yellow2": {
				"left": "",
				"left_country": "",
				"right":"",
				"right_country": ""
			},
			"blue1": {
				"left": "",
				"left_country": "",
				"right":"",
				"right_country": ""
			},
			"blue2": {
				"left": "",
				"left_country": "",
				"right":"",
				"right_country": ""
			},
			"podium": {
				"left": "",
				"left_country": "",
				"right":"",
				"right_country": ""
			}
		};
		
		var formData = $('form').serialize();
		//console.log(formData);
		var game = {}
		$('.inputArea.on,.sectionLine.on').each(function(e){
			//console.log($(this).find('.court').attr('id'));
			//console.log($(this).find('.court').val());
			var pos = $(this).attr('id');
			var color = $(this).find('.court').val();
			game[pos] = color;
		});
		ws0.send(JSON.stringify({
			type: 'admin',
			user: self.name,
			piste: 'all',
			data: formData,
			game: game
		}));

	$('select.court').each(function(i){
		var v = parseInt(this.value),
			$this = $(this),
			flgSwap = $this.siblings('.swap').find('input')[0].checked,
			$parent = $this.parent().parent();
		if(v>0 && !$parent.hasClass('win1st') && !$parent.hasClass('win2nd')){
			var playerR = $this.parents('.status').siblings('.player1st').find('.winner').val(),
				playerL = $this.parents('.status').siblings('.player2nd').find('.winner').val(),
				name = {
						l: $('.winner[value="'+playerL+'"]').siblings('.name').val(),
						r: $('.winner[value="'+playerR+'"]').siblings('.name').val()
					},
				nation = {
					l: $('.winner[value="'+playerL+'"]').siblings('.nation').val(),
					r: $('.winner[value="'+playerR+'"]').siblings('.nation').val()
				};

			switch(v){
			case 1:
				ws1.send(JSON.stringify({
					type: 'admin',
					user: self.name,
					piste: v,
					nameL:(!flgSwap)? name.l: name.r,
					nationL:(!flgSwap)? nation.l: nation.r,
					nameR:(!flgSwap)? name.r: name.l,
					nationR:(!flgSwap)? nation.r: nation.l
				}));
				data.green1 = {
					"left":(!flgSwap)? name.l: name.r,
					"left_country":(!flgSwap)? nation.l: nation.r,
					"right":(!flgSwap)? name.r: name.l,
					"right_country":(!flgSwap)? nation.r: nation.l
				};
			break;
			case 2:
				ws2.send(JSON.stringify({
					type: 'admin',
					user: self.name,
					piste: v,
					nameL:(!flgSwap)? name.l: name.r,
					nationL:(!flgSwap)? nation.l: nation.r,
					nameR:(!flgSwap)? name.r: name.l,
					nationR:(!flgSwap)? nation.r: nation.l
				}));
				data.yellow1 = {
					"left":(!flgSwap)? name.l: name.r,
					"left_country":(!flgSwap)? nation.l: nation.r,
					"right":(!flgSwap)? name.r: name.l,
					"right_country":(!flgSwap)? nation.r: nation.l
				};
			break;
			case 3:
				ws3.send(JSON.stringify({
					type: 'admin',
					user: self.name,
					piste: v,
					nameL:(!flgSwap)? name.l: name.r,
					nationL:(!flgSwap)? nation.l: nation.r,
					nameR:(!flgSwap)? name.r: name.l,
					nationR:(!flgSwap)? nation.r: nation.l
				}));
				data.red1 = {
					"left":(!flgSwap)? name.l: name.r,
					"left_country":(!flgSwap)? nation.l: nation.r,
					"right":(!flgSwap)? name.r: name.l,
					"right_country":(!flgSwap)? nation.r: nation.l
				};
			break;
			case 4:
				ws4.send(JSON.stringify({
					type: 'admin',
					user: self.name,
					piste: v,
					nameL:(!flgSwap)? name.l: name.r,
					nationL:(!flgSwap)? nation.l: nation.r,
					nameR:(!flgSwap)? name.r: name.l,
					nationR:(!flgSwap)? nation.r: nation.l
				}));
				data.blue1 = {
					"left":(!flgSwap)? name.l: name.r,
					"left_country":(!flgSwap)? nation.l: nation.r,
					"right":(!flgSwap)? name.r: name.l,
					"right_country":(!flgSwap)? nation.r: nation.l
				};
			break;
			case 5:
				ws5.send(JSON.stringify({
					type: 'admin',
					user: self.name,
					piste: v,
					nameL:(!flgSwap)? name.l: name.r,
					nationL:(!flgSwap)? nation.l: nation.r,
					nameR:(!flgSwap)? name.r: name.l,
					nationR:(!flgSwap)? nation.r: nation.l
				}));
				data.green2 = {
					"left":(!flgSwap)? name.l: name.r,
					"left_country":(!flgSwap)? nation.l: nation.r,
					"right":(!flgSwap)? name.r: name.l,
					"right_country":(!flgSwap)? nation.r: nation.l
				};
			break;
			case 6:
				ws6.send(JSON.stringify({
					type: 'admin',
					user: self.name,
					piste: v,
					nameL:(!flgSwap)? name.l: name.r,
					nationL:(!flgSwap)? nation.l: nation.r,
					nameR:(!flgSwap)? name.r: name.l,
					nationR:(!flgSwap)? nation.r: nation.l
				}));
				data.yellow2 = {
					"left":(!flgSwap)? name.l: name.r,
					"left_country":(!flgSwap)? nation.l: nation.r,
					"right":(!flgSwap)? name.r: name.l,
					"right_country":(!flgSwap)? nation.r: nation.l
				};
			break;
			case 7:
				ws7.send(JSON.stringify({
					type: 'admin',
					user: self.name,
					piste: v,
					nameL:(!flgSwap)? name.l: name.r,
					nationL:(!flgSwap)? nation.l: nation.r,
					nameR:(!flgSwap)? name.r: name.l,
					nationR:(!flgSwap)? nation.r: nation.l
				}));
				data.red2 = {
					"left":(!flgSwap)? name.l: name.r,
					"left_country":(!flgSwap)? nation.l: nation.r,
					"right":(!flgSwap)? name.r: name.l,
					"right_country":(!flgSwap)? nation.r: nation.l
				};
			break;
			case 8:
				ws8.send(JSON.stringify({
					type: 'admin',
					user: self.name,
					piste: v,
					nameL:(!flgSwap)? name.l: name.r,
					nationL:(!flgSwap)? nation.l: nation.r,
					nameR:(!flgSwap)? name.r: name.l,
					nationR:(!flgSwap)? nation.r: nation.l
				}));
				data.blue2 = {
					"left":(!flgSwap)? name.l: name.r,
					"left_country":(!flgSwap)? nation.l: nation.r,
					"right":(!flgSwap)? name.r: name.l,
					"right_country":(!flgSwap)? nation.r: nation.l
				};
			break;
			case 9:
				ws9.send(JSON.stringify({
					type: 'admin',
					user: self.name,
					piste: v,
					nameL:(!flgSwap)? name.l: name.r,
					nationL:(!flgSwap)? nation.l: nation.r,
					nameR:(!flgSwap)? name.r: name.l,
					nationR:(!flgSwap)? nation.r: nation.l
				}));
				data.podium = {
					"left":(!flgSwap)? name.l: name.r,
					"left_country":(!flgSwap)? nation.l: nation.r,
					"right":(!flgSwap)? name.r: name.l,
					"right_country":(!flgSwap)? nation.r: nation.l
				};
			break;
			case 10:
				ws10.send(JSON.stringify({
					type: 'admin',
					user: self.name,
					piste: v,
					nameL:(!flgSwap)? name.l: name.r,
					nationL:(!flgSwap)? nation.l: nation.r,
					nameR:(!flgSwap)? name.r: name.l,
					nationR:(!flgSwap)? nation.r: nation.l
				}));
			break;
			case 11:
				ws11.send(JSON.stringify({
					type: 'admin',
					user: self.name,
					piste: v,
					nameL:(!flgSwap)? name.l: name.r,
					nationL:(!flgSwap)? nation.l: nation.r,
					nameR:(!flgSwap)? name.r: name.l,
					nationR:(!flgSwap)? nation.r: nation.l
				}));
			break;
			}
		}
	});

	// サイトパブリス対応
	/*
	$.ajax({
		type: "post",
		url: '../../../../request.php',
		// contentType: 'application/json',
		// dataType: "json",
    // cache: false,
		data: data // JSONデータ本体
		})
		// Ajaxリクエストが成功した時発動
		.done( (data) => {
			alert('ok: ' + data);
			console.log(data);
		})
		// Ajaxリクエストが失敗した時発動
		.fail( (data) => {
			alert('error: ' + data);
		})
		// Ajaxリクエストが成功・失敗どちらでも発動
		.always( (data) => {

		});
		*/
};


/* ----------------------------------------
	ブラウザ終了イベント
---------------------------------------- */
/*
window.onbeforeunload = function () {
	for(var i=0; i<wsLen; i++){
		ws[i].send(JSON.stringify({
			type: 'defect',
			user: self.name,
		}));
	}
};*/


/* ----------------------------------------
	Form
---------------------------------------- */

/* winner
---------------------------------------- */

/*
//debug
$('.winner').each(function(i){
	if(~this.id.indexOf('w6')) this.value = Math.floor(Math.random()*10000);
});
debug*/

var $winner = $('.winner');

$winner.each(function(i){
	var v = this.value;
	if(v){
		var loc = parseInt(this.name.replace(/w(\d)_.+/,'$1')), //w6_2 なら 6
			id = parseInt(this.name.replace('w'+loc+'_', '')), //w6_2 なら2
			$next = $('#w'+(loc-1)+'_'+Math.ceil(id/2)); //w5_
		if(~this.id.indexOf('w0')){
			$('#name').text($('.winner[value="'+v+'"]').siblings('.name').val());
		}else{
			if(this.checked){
				var winClass =(id%2==1)? 'win1st': 'win2nd';
				$(this).parent().parent().addClass(winClass).removeClass('on');
//				.find('.status select, .status input').attr('disabled', 'disabled');
				if(!$next.val()) $next.val(v),
					$next.siblings('.name').val($('.winner[value="'+v+'"]').siblings('.name').val()),
					$next.siblings('.nation').val($('.winner[value="'+v+'"]').siblings('.nation').val());
			}
		}
	}
}).click(function(){
	var v = this.value;
	if(v){
		var $this = $(this),
			$parent = $this.parent().parent(),
			loc = parseInt(this.name.replace(/w(\d)_.+/,'$1')),
			id = parseInt(this.name.replace('w'+loc+'_', '')),
			pair =(id%2==1)? document.getElementById('w'+loc+'_'+(id+1)): document.getElementById('w'+loc+'_'+(id-1)),
			$next = $('#w'+(loc-1)+'_'+Math.ceil(id/2));
//debug
$('#log').prepend('<div>location:'+loc+' id:'+id+' playerID:'+v+'</div>');
//if(window.console && typeof window.console.log==='function') console.log(pair);
//if(window.console && typeof window.console.log==='function') console.log($next);
/*
debug*/
		if(pair.checked) pair.checked = false;
		$parent.removeClass('win1st win2nd');
		if(this.checked && pair.value){
			$next.val(v);
			
			if($next.parent().parent().find('.court').val() != 0) $next.parent().parent().addClass('on');
			if(id%2==1) $parent.addClass('win1st').removeClass('on');
			else $parent.addClass('win2nd').removeClass('on');
//			$parent.find('.status select, .status input').attr('disabled', 'disabled');
			if(loc-1==0) $('#name').text($('.winner[value="'+v+'"]').siblings('.name').val());
			else  $next.siblings('.name').val($('.winner[value="'+v+'"]').siblings('.name').val()),
				$next.siblings('.nation').val($('.winner[value="'+v+'"]').siblings('.nation').val());
				for(i=0;i<6;i++){
					$prev = $next;
					$next = $('#w'+(loc-(i+2))+'_'+Math.ceil(id/(2**(i+2))));
					//console.log($next);
					if($prev.prop('checked')){
						if(loc-(i+2)==0) $('#name').text($('.winner[value="'+v+'"]').siblings('.name').val());
						else  $next.siblings('.name').val($('.winner[value="'+v+'"]').siblings('.name').val()),
							$next.siblings('.nation').val($('.winner[value="'+v+'"]').siblings('.nation').val());
					}else{
						break;
					}
				}
		}else{
			this.checked = false;
			$next.val('');
//			$parent.find('.status select, .status input').removeAttr('disabled');
			$next.siblings('.name').val('');
			$next.siblings('.nation').val('');
			$next.parent().parent().removeClass('on');
			if($parent.find('.court').val() != 0) $parent.addClass('on');
		}
	}else{
		this.checked = false;
	}
});

/* court
---------------------------------------- */

var $court = $('select.court'),
	cSelected = 0,
	cChange = function(el){
		var _this = el,
		$this = $(_this);
		if($this.siblings('.swap').find('input').length){
			var flgSwap = $this.siblings('.swap').find('input')[0].checked,
			playerR =(!flgSwap)? $this.parents('.status').siblings('.player1st').find('.winner').val(): $this.parents('.status').siblings('.player2nd').find('.winner').val(),
			playerL =(!flgSwap)? $this.parents('.status').siblings('.player2nd').find('.winner').val(): $this.parents('.status').siblings('.player1st').find('.winner').val();
		}
		if(el){
				v = parseInt(_this.value);
				if(v>0 && playerL && playerR){
					$court.not(_this).each(function(){
						var $parent = $(this).parent().parent();
						if(this.value==v && !$parent.hasClass('win1st') && !$parent.hasClass('win2nd')){
							this.value = 0;
							$(this).parents('.inputArea, .sectionLine').removeClass('on');
						}
					});
					var name = {
							l: $('.winner[value="'+playerL+'"]').siblings('.name').val(),
							r: $('.winner[value="'+playerR+'"]').siblings('.name').val()
						},
						nation = {
							l: $('.winner[value="'+playerL+'"]').siblings('.nation').val(),
							r: $('.winner[value="'+playerR+'"]').siblings('.nation').val()
						};
					$('#ci'+v+'NameL').text(name.l);
					$('#ci'+v+'NameR').text(name.r);
					$('#ci'+v+'NationL').text(nation.l);
					$('#ci'+v+'NationR').text(nation.r);
					$this.parents('.inputArea, .sectionLine').addClass('on');
				}else{
					_this.value = 0;
					$this.parents('.inputArea, .sectionLine').removeClass('on');
				}
		}
	},
	$swap = $('.swap');

$court.each(function(){
	var $parent = $(this).parent().parent();
	if(this.value && !$parent.hasClass('win1st') && !$parent.hasClass('win2nd')){
		cChange(this);
	}
}).change(function(){
if(window.console && typeof window.console.log==='function') /*console.log('courtChange');*/
	cChange(this);
	var cur = [
		false,
		false,
		false,
		false,
		false,
		false
	];
	$court.each(function(){
		var $parent = $(this).parent().parent();
		if(this.value && !$parent.hasClass('win1st') && !$parent.hasClass('win2nd')){
			cur[this.value] = true;
		}
	});
	for(var i=1; i<=11; i++){
		if(!cur[i]){
			$('#ci'+i+'NameL').text('');
			$('#ci'+i+'NameR').text('');
			$('#ci'+i+'NationL').text('');
			$('#ci'+i+'NationR').text('');
		}
	}
});

$swap.click(function(){
	cChange($(this).prev()[0]);
});

$winner.click(function(){
	//cChange($(this).prev()[0]); 勝利チェックつけると国籍消えるバグ
});


/* ----------------------------------------
	info
---------------------------------------- */

$('#infoClose').click(function(){
	$(this).toggleClass('close');
	var pos =($(this).hasClass('close'))? -$('#info').width(): 0;
	$('#info').css({right: pos});
});

window.onload = function () {
var getParams = para_get(location.search);
//console.log(getParams);
if(getParams.info === 'close'){
	$(this).toggleClass('close');
	var pos =($(this).hasClass('close'))? -$('#info').width(): 0;
	$('#info').css({right: pos});
}
}


/* ----------------------------------------
	color
---------------------------------------- */

$('.color').change(function(){
	bg = $(this).val();
	$(this).closest('.break').find('.color').each(function(){
		$(this).val(bg);
		$(this).parent().parent().css("background-color",bg);
	});
});
$('.color').each(function(){
	$(this).parent().parent().css("background-color",$(this).val());
});


/* ----------------------------------------
	label
---------------------------------------- */

$('.label').change(function(){
	str = $(this).val();
	$(this).closest('.break').find('.label').each(function(){
		$(this).val(str);
	});
});
//EOS
});

function para_get(data)
{
	// URLパラメータを"&"で分離する
	var params = data.substr(1).split('&');
	// パラメータ連想配列エリア初期化
	var para = [];
	// キーエリア初期化
	var key = null;
	for(var i = 0 ; i < params.length ; i++)
	{
		// "&"で分離したパラメータを"="で再分離
		key = params[i].split("=");
		// パラメータをURIデコードして連想配列でセット
		para[key[0]] = decodeURI(key[1]);
	}
	// 連想配列パラメータを返す
	return (para);
}

function nChange (e){}