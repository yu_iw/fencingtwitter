<?php
class ModelBase
{
    private static $connInfo;
    protected $db;
    protected $name;
    public function __construct()
    {
        $this->initDb();
    }
    public function initDb()
    {
        $dsn = sprintf(
            'pgsql:host=%s;dbname=%s;port=5432;',
            self::$connInfo['host'],
            self::$connInfo['dbname']
        );
        $this->db = new PDO($dsn, self::$connInfo['dbuser'], self::$connInfo['password']);
    }
    public static function setConnectionInfo($connInfo)
    {
        self::$connInfo = $connInfo;
    }
    //次行の最初のカラムを返す
    public function queryColumn($sql, array $params = array())
    {
        $stmt = $this->db->prepare($sql);
        if ($params != null) {
            foreach ($params as $key => $val) {
                $stmt->bindValue(':' . $key, $val);
            }
        }
        $stmt->execute();
        $scholar = $stmt->fetchColumn();
        return $scholar;
    }
    // クエリ結果を取得
    // select以外にもinsertやupdateなども実行できます。
    // $sql = "UPDATE t_entry set status = :status WHERE id = :id";
    // $params = array('status' => $status,'id' => $id);
    // $this->query($sql,$params);
    public function query($sql, array $params = array())
    {
    	$stmt = $this->db->prepare($sql);
    	if ($params != null) {
    		foreach ($params as $key => $val) {
    			$stmt->bindValue(':' . $key, $val);
    		}
    	}
    	$stmt->execute();
    	$rows = $stmt->fetchAll();
    	return $rows;
    }
    // INSERTを実行
    public function insert($data)
    {
        $fields = array();
        $values = array();
        foreach ($data as $key => $val) {
            $fields[] = $key;
            $values[] = ':' . $key;
        }
        $sql = sprintf(
            "INSERT INTO %s (%s) VALUES (%s);",
            $this->name,
            implode(',', $fields),
            implode(',', $values)
        );
        $stmt = $this->db->prepare($sql);
        foreach ($data as $key => $val) {
            $stmt->bindValue(':'.$key, $val);
        }
        $res = $stmt->execute();
        return $res;
    }
    // UPDATEを実行
    // 使用例 ↓主キー
    // update("id",idも含めた更新したいパラメータの連想配列)
    public function updateByPK($pk,$params)
    {
        $sql = sprintf("UPDATE %s set ", $this->name);
        if ($pk != "" && $params != null) {
        	$count =1;
        	foreach ($params as $key => $val) {
        		if($key === $pk){
        			//主キーはsetしないので除く
        			continue;
        		}
            	if($count >= 2){
            		//最初以外は区切りのため頭に,をつける
            		$sql .= ',';
	        	}
	        	//更新用のパラメータをセット
	        	$sql .= $key.' = :'.$key;
	        	$count ++;
  		    }
            $sql .= " WHERE ".$pk." = :".$pk;
        }
        $stmt = $this->db->prepare($sql);
        if ($params != null) {
            foreach ($params as $key => $val) {
                $stmt->bindValue(':' . $key, $val);
            }
        }
        $res = $stmt->execute();
        return $res;
    }
    // DELETEを実行
    // 使用例  ↓where句　　　　　　　　　　　↓パラメータ
    // delete("id",idのみの連想配列)
    public function deleteByPK($pk, $params)
    {
        $sql = sprintf("DELETE FROM %s", $this->name);
        if ($pk != "" && $params != null) {
            $sql .= " WHERE ".$pk." = :".$pk;
        }
        $stmt = $this->db->prepare($sql);
        if ($params != null) {
            foreach ($params as $key => $val) {
                $stmt->bindValue(':' . $key, $val);
            }
        }
        $res = $stmt->execute();
        return $res;
    }    
    // maxId
    public function maxId()
    {
        $sql = sprintf("SELECT max(id) FROM %s;", $this->name);
        $rows = $this->query($sql);
		//print_r($rows);
        //return $rows['0']['max(id)']; 
        return $rows['0']['max']; //postgres用
    }
    //{"id"=>value}のセットで指定
    //id以外は未実装
    public function selectByPK($pk, $deleted = false)
    {
        $sql = sprintf("SELECT * FROM %s WHERE id = :id;", $this->name);
        if($deleted){
            $sql .= " AND deleted = false";
        }
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':id', $pk);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        return $rows;
    }
    public function setDefaultTableName()
    {
        $className = get_class($this);
        $len = strlen($className);
        $tableName = '';
        for ($i = 0; $i < $len; $i++) {
            $char = substr($className, $i, 1);
            $lower = strtolower($char);
            if ($i > 0 && $char != $lower) {
                $tableName .= '_';
            }
            $tableName .= $lower;
        }
        $this->name  = $tableName;
    }
}
?>