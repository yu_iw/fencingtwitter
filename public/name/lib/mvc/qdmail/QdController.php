<?php

require_once('qdmail.php');
require_once('qdsmtp.php');

class QdController
{
	private static $mailprof;

	public static function setMailprof($mailprof)
	{
		self::$mailprof = $mailprof;
	}

    public function sendMail($mailto, $subject, $message)
    {
    	mb_language("japanese");
    	mb_internal_encoding("UTF-8");

    	$param = self::$mailprof;

    	$mail = new Qdmail();

    	$mail->to($mailto);
    	$mail->subject($subject);
    	$mail->from($param['from']);
    	$mail->text($message);
//     	$mail->attach($attach);
//     	$mail->simpleAttach(true);

    	$qdsmtp = new Qdsmtp();
    	$qdsmtp->server($param);
    	$qdsmtp = $mail->smtpObject();
    	$qdsmtp->pop3UseFile(false);

		$mail->setSmtpObject($qdsmtp);

    	return $mail->send();

    }

    public function sendMailWithAttach($mailto, $subject, $message, $attach = null){

    	mb_language("japanese");
    	mb_internal_encoding("UTF-8");

    	$param = self::$mailprof;

    	$mail = new Qdmail();

    	$mail->to($mailto);
    	$mail->subject($subject);
    	$mail->from($param['from']);
    	$mail->text($message);
    	$mail->attach($attach);
    	$mail->simpleAttach(true);

//     	$mail->smtp(true);

    	$qdsmtp = new Qdsmtp();
    	$qdsmtp->server($param);
    	$qdsmtp = $mail->smtpObject();
    	$qdsmtp->pop3UseFile(false);

		$mail->setSmtpObject($qdsmtp);

    	return $mail->send();

//     	qd_send_mail('text',$mailto,$subject,$message,$param['from'],$attach);
    }
}

?>
