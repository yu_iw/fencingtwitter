<?php

class UrlParameter extends RequestVariables
{
    protected function setValues()
    {
        // パラメーター取得（末尾の / は削除）
        //$param = ereg_replace('/?$', '', $_GET['param']);
        $param = ereg_replace('/?$', '', $_SERVER['REQUEST_URI']);
		$param = ereg_replace(URL_ROOT, '', $param);
        $param = trim($param, '/');

        $params = array();
        if ('' != $param) {
            // パラメーターを / で分割
            $params = explode('/', $param);
        }

        if (2 < count($params)) {
          foreach ($params as $param) {
              // "_"で分割
              $splited = explode('_', $param);
              if (2 == count($splited)) {
                  $key = $splited[0];
                  $val = $splited[1];
				$this->_values[$key] = $this->myhtmlspecialchars($val);//エスケープ処理
              }
          }
      }

    }
	
	private function myhtmlspecialchars($string) {
		if (is_array($string)) {
			return array_map(array($this, 'myhtmlspecialchars'), $string);
		} else {
			return htmlspecialchars($string, ENT_QUOTES);
		}
	}
}

?>
