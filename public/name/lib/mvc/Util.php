<?php

class Util{

	const UTF8 = 'UTF-8';
	const SJIS = 'SJIS';

	//CSV形式のデータを1件だけファイルに追記する
	function csv1write($file,$data,$to_encoding,$from_encoding){

		if( $this->file_lock( $file )){

			if( $handle = fopen( $file, 'a' ) ){
				//配列の文字コードを変換
				//string mb_convert_variables ( string $to_encoding , mixed $from_encoding , mixed &$vars [, mixed &$... ] )
				// 				echo $to_encoding;
// 				mb_convert_encoding(  $to_encoding ,$from_encoding, $data );

				$this->_fputcsv( $handle, $data,$to_encoding,$from_encoding);
			}
			fclose( $handle );

			$this->file_unlock( $file );
		}
	}

	function _fputcsv($fp, $data, $toEncoding='Shift-JIS', $srcEncoding='UTF-8') {
		require_once LIB_PATH.'/mvc/mb_str_replace.php';
		$csv = '';
		foreach ($data as $col) {
			if (is_numeric($col)) {
				$csv .= '="' . $col . '"';
			} else {
				$col = mb_convert_encoding($col, $toEncoding, $srcEncoding);
				$col = mb_str_replace('"', '""', $col, $toEncoding);
				$csv .= '"' . $col . '"';
			}
			$csv .= ',';
		}
		fwrite($fp, $csv);
		fwrite($fp, "\r\n");
	}

// 	function csv1write($file,$data,$to_encoding,$from_encoding){
//
// 		if( $this->file_lock( $file )){
//
// 			if( $handle = fopen( $file, 'a' ) ){
// 				//配列の文字コードを変換
// 				//string mb_convert_variables ( string $to_encoding , mixed $from_encoding , mixed &$vars [, mixed &$... ] )
// 				mb_convert_variables(  $to_encoding ,$from_encoding, $data );
//
// 				fputcsv( $handle, $data);
// 			}
// 			fclose( $handle );
//
// 			$this->file_unlock( $file );
// 		}
// 	}
	function getUTF8(){
		return self::UTF8;
	}
	function getSJIS(){
		return self::SJIS;
	}

	// ファイルロックチェック＆生成
	function file_lock( $file ){
		$lock = $file . ".lock"; // ロック用ディレクトリ名
		$count = 5;	// 再トライする回数
		$time = 60;	// ロックディレクトリの有効期限

		for( ; $count > 0; $count-- )
		{
			if( ! @mkdir( $lock, 0777 ) )
			{
				sleep( 1 );
			}
			else
			{
				break;
			}
		}

		if( ! $count )
		{
			if( filemtime( $lock ) <= time() - $time )
			{
				if( $this->file_unlock( $file ) && $this->file_lock( $file ) )
				{
					return true;
				}
			}
			return false;
		}
		return true;
	}


	// ファイルロック解除
	function file_unlock( $file ){
		if( @rmdir( $file . ".lock" ) ) return true;
		return false;
	}


}
?>