<?php

class OneTimeCreate
{
    public function createPass()
    {
    	// ランダム文字列の生成
    	for ($i = 0, $str = null; $i < 10; ) {
    		$num = mt_rand(0x30, 0x7A);
    		if ((0x30 <= $num && $num <= 0x39)||        // 0 ～ 9
    				(0x41 <= $num && $num <= 0x5A)||    // A ～ Z
    				(0x61 <= $num && $num <= 0x7A)){    // a ～ z
    			$str .= chr($num);
    			$i++;
    		}
    	}

    	return $str;
    }
}


?>
