<?php

// GET変数クラス
class QueryString extends RequestVariables
{
    protected function setValues()
    {
        foreach ($_GET as $key => $value) {
			$this->_values[$key] = $this->myhtmlspecialchars($value);//エスケープ処理
        }		
    }		
	
	private function myhtmlspecialchars($string) {
		if (is_array($string)) {
			return array_map(array($this, 'myhtmlspecialchars'), $string);
		} else {
			return htmlspecialchars($string, ENT_QUOTES);
		}
	}
}

?>
