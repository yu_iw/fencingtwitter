<?php

//アップロードファイルクラス
class UploadUtil
{
    function GetMimeType ($upload_file)
    {
		//MIMEタイプの取得
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime_type = finfo_buffer($finfo, file_get_contents($upload_file));
		finfo_close($finfo);
		//echo $mime_type;
		return $mime_type;
    }		
}


?>
