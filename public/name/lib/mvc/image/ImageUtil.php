<?php

class ImageUtil{
	function ImageUtil(){
	}
	function ImageResize($fromImage,$toImage,$width,$height,$method){
		 // load the image manipulation class
		 // phpのクラスをロードする
		require_once LIB_PATH  . '/mvc/image/Zebra_Image.php';
		//入力データのパスを指定
		$source_path = '/data/temp/';
		//出力データのパスを指定
		$target_path = '/data/post/';
		//リサイズ後の幅、高さ、圧縮品質を指定する。
		//$width   = 150;
		//$height  = 213;
		$quality =  100;
		// create a new instance of the class
		//インスタンスを生成する
		$image = new Zebra_Image();
		// indicate a source image (a GIF, PNG or JPEG file)
		//画像ファイルのソースを入力する
		$image->source_path = $fromImage;
		/* indicate a target image
		// note that there's no extra property to set in order to specify the target
		// image's type -simply by writing '.jpg' as extension will instruct the script
		// to create a 'jpg' file
		//生成したいファイル名を入力する
		*/
		$image->target_path = $toImage;
		$image->jpeg_quality = $quality;
		$image->preserve_aspect_ratio = true;
		$image->enlarge_smaller_images = true;
		$image->sharpen_images = true;
		$image->preserve_time = true;
		
		if(!$method){
			$method = ZEBRA_IMAGE_CROP_CENTER;
		}
		if (!$image->resize($width, $height, $method)) :
			// if there was an error, let's see what the error is about
			// エラー処理
			switch ($image->error) {
				case 1:
					echo '<p class="text-primary">' .$filename. '入力ファイルが見つかりません</p>';
					break;
				case 2:
					echo '<p class="text-primary">' .$filename. '入力ファイルを読み取れません</p>';
					break;
				case 3:
					echo '<p class="text-primary">' .$filename. '出力ファイルを書き込めません</p>';
					break;
				case 4:
					echo '<p class="text-primary">' .$filename. '入力ファイルフォーマットがサポート外です</p>';
					break;
				case 5:
					echo '<p class="text-primary">' .$filename. '出力ファイルフォーマットがサポート外です</p>';
					break;
				case 6:
					echo '<p class="text-primary">' .$filename. 'GD library のバージョンが出力フォーマットに対応していません</p>';
					break;
				case 7:
					echo '<p class="text-primary">' .$filename. 'GD library がインストールされていません</p>';
					break;
			}
		endif;
	}
	function ImageCompose ( $img1, $img2, $dest, $id){
		require_once LIB_PATH  . '/mvc/image/Zebra_Image.php';
		$exif_data = exif_read_data($img2);
		if(isset($exif_data['Orientation'])  && $exif_data['Orientation'] == 6 ){
			error_log("here_img");
			$source = imagecreatefromstring(file_get_contents($img2));
			$rotate = imagerotate($source, 270, 0);
			imagejpeg($rotate, $img2, 100);
		}
		$img1_info = getimagesize($img1);
		$img2_info = getimagesize($img2);

		$img1_tmp = @imagecreatefromstring(file_get_contents($img1));
		$img2_tmp = @imagecreatefromstring(file_get_contents($img2));

		$img1_width = $img1_info['0'];
		$img1_height = $img1_info['1'];
		$img2_width = $img2_info['0'];
		$img2_height = $img2_info['1'];

		//ファイルサイズが3M超えたら1/3に縮小
		if( (int)filesize($img2) > 3145728){
			error_log("-----------FileSize:".filesize($img2));
			$baseImg_tmp = ImageCreateTrueColor($img2_width/3,$img2_height/3);
			imagecopyresampled($baseImg_tmp, $img2_tmp, 0, 0, 0, 0, $img2_width/3, $img2_height/3, $img2_width, $img2_height);
			ImagePNG($baseImg_tmp,$img2,3);
			$img2_tmp = @imagecreatefromstring(file_get_contents($img2));
			$img2_info = getimagesize($img2);
		}
		if( $img2_width/$img2_height > 328/477 ){
			$cut = new Zebra_Image();
			$cut->source_path = $img2;
			$cut->target_path = MAIN_PATH.'/tmp/temp/'.$id.'_trim.jpg';
			$cut->resize(328 * ($img2_height/477), $img2_height,ZEBRA_IMAGE_CROP_CENTER);
			$img2_tmp = @imagecreatefromstring(file_get_contents(MAIN_PATH.'/tmp/temp/'.$id.'_trim.jpg'));
			$img2_info = getimagesize(MAIN_PATH.'/tmp/temp/'.$id.'_trim.jpg');
			$img2_width = $img2_info['0'];
			$img2_height = $img2_info['1'];
		}
		$img2_size = $img2_info['bits'];
		error_log("size:".$img2_size);

		imagefilter($img2_tmp,IMG_FILTER_GRAYSCALE);
		$baseImg = ImageCreateTrueColor(662,476);
		$white = imagecolorallocatealpha($baseImg, 255, 255, 255, 127);
		imagealphablending($baseImg, false);
		imagefilledrectangle($baseImg, 0, 0, 662, 476, $white);
		imagecolordeallocate($baseImg, $white);
		imagealphablending($baseImg, true);
		imagecopyresampled($baseImg, $img1_tmp, (328 - $img1_width*(477/$img1_height))/2, 0, 0, 0, $img1_width*(477/$img1_height), 477, $img1_width, $img1_height);
		imagecopyresampled($baseImg, $img2_tmp, 334, 0, 0, 0, $img2_width*(477/$img2_height), 477,$img2_width, $img2_height);
		imagedestroy($img1_tmp);
		imagedestroy($img2_tmp);
		error_log("test".$img2_size);
// 		header('Content-Type: image/png');
		ImagePNG($baseImg,$dest,3);
		imagedestroy($baseImg);
	}
	function WriteTag($img1, $detail, $dest){
		$font_size = 12;
		$font_angle = 0;
		$font_path = ROOT_PATH."/views/assets/ArmedLemon.TTF";
		$img1_info = getimagesize($img1);
		$img1_tmp = @imagecreatefromstring(file_get_contents($img1));
		$baseImg = ImageCreateTrueColor(245,155);
		$gray = imagecolorallocate($baseImg, 241, 239, 243);
		$font_color = ImageColorAllocate($baseImg, 0, 0, 0);
		imagefilledrectangle($baseImg, 0, 0, 245, 155, $gray);
		imagecopy($baseImg, $img1_tmp, 0, 0, 0, 0, $img1_info['0'], $img1_info['1']);
		$text = array(imagettfbbox($font_size, 0, $font_path, $detail['nm']),
						imagettfbbox($font_size, 0, $font_path, $detail['job']),
						imagettfbbox($font_size, 0, $font_path, $detail['style']));
		$subject = array('nm','job','style');
		$h = array(25,50,85);
		for($i=0; $i<count($text); $i++){
			if($text[$i][2] > 220){
				$font_size = 10;
				$text[$i] = imagettfbbox($font_size, 0, $font_path, $detail[$subject[$i]]);
			}else{
				$font_size = 12;
			}
			ImageTTFText($baseImg, $font_size, 0, (245 - ($text[$i][2] - $text[$i][0])) / 2, $h[$i], $font_color, $font_path, $detail[$subject[$i]]);
		}
		$transColor = imagecolorallocatealpha($baseImg, 255, 255, 255, 127);
		$baseImg = imagerotate($baseImg, 5, $transColor);
		imagesavealpha($baseImg, true);
		ImagePNG($baseImg,$dest,3);
	}
	function ImageCompose2 ( $img1, $img2, $dest){
		$img1_info = getimagesize($img1);
		$img2_info = getimagesize($img2);

		$img1_tmp = @imagecreatefromstring(file_get_contents($img1));
		$img2_tmp = @imagecreatefromstring(file_get_contents($img2));

		$img1_width = $img1_info['0'];
		$img1_height = $img1_info['1'];
		$img2_width = $img2_info['0'];
		$img2_height = $img2_info['1'];

		imagefilter($img2_tmp,IMG_FILTER_GRAYSCALE);

		$baseImg = ImageCreateTrueColor(693,496);
		$white = imagecolorallocate($baseImg, 255, 255, 255);
		imagefilledrectangle($baseImg, 0, 0, 693,496, $white);

		imagecopy($baseImg, $img1_tmp, 0, 0, 0, 0, $img1_width, $img1_height);
		imagecopy($baseImg, $img2_tmp, 422, 315, 0, 0, $img2_width, $img2_height);

		imagedestroy($img1_tmp);
		imagedestroy($img2_tmp);

		// 		header('Content-Type: image/png');
		ImagePNG($baseImg,$dest,3);
		imagedestroy($baseImg);
	}
}
?>