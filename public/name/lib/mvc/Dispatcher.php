<?php
//require_once '/home/adidas.jp/public_html/gf/lib/mvc/Smarty/Smarty.class.php';
require_once 'Request.php';
class Dispatcher
{
    private $sysRoot;
    /**
     * システムのルートディレクトリを設定
     */
    public function setSystemRoot($path)
    {
        $this->sysRoot = rtrim($path, '/');
    }
    /**
     * 振分け処理実行
     */
    public function dispatch()
    {
        // パラメーター取得（末尾の / は削除）
        //$param = ereg_replace('/?$', '', $_GET['param']);
        //echo $_SERVER['REQUEST_URI'].'<br>';
        $param = ereg_replace('/?$', '', $_SERVER['REQUEST_URI']);
        if(URL_ROOT != "/"){
            $param = ereg_replace(URL_ROOT, '', $param);
        }
        $param = trim($param, '/');
        $params = array();
        if ('' != $param) {
            // パラメーターを"/"で分割
            $params = explode('/', $param);
        }
        // １番目のパラメーターをコントローラーとして取得
        $controller = 'index';
        if (0 < count($params)) {
            $controller = $params[0];
        }
        // １番目のパラメーターをもとにコントローラークラスインスタンス取得
        $controllerInstance = $this->getControllerInstance($controller);
        if (null == $controller) {
            //header("HTTP/1.0 404 Not Found");
            exit;
        }
        // 2番目のパラメーターをアクションとして取得
        $action= "index";
        if (1 < count($params)) {
            $action = $params[1];
        }
        // アクションメソッドの存在確認
        if (false == method_exists($controllerInstance, $action . 'Action')) {
            //header("HTTP/1.0 404 Not Found");
            exit;
        }
        // コントローラー初期設定
        $controllerInstance->setSystemRoot($this->sysRoot);
        $controllerInstance->setControllerAction($controller, $action);
        //echo $this->sysRoot.'<br>';
        //echo $controller.'<br>';
        //echo $action.'<br>';
        //exit;

        // 処理実行
        $controllerInstance->run();
        //exit;
    }
    // コントローラークラスのインスタンスを取得
    private function getControllerInstance($controller)
    {
        // 一文字目のみ大文字に変換＋"Controller"
        $className = ucfirst(strtolower($controller)) . 'Controller';
        // コントローラーファイル名
        $controllerFileName = sprintf(
           '%s/controllers/%s.php',
           $this->sysRoot,
           $className
           );
        // ファイル存在チェック
        if (false == file_exists($controllerFileName)) {
            return null;
        }
        // クラスファイルを読込
        require_once $controllerFileName;
        // クラスが定義されているかチェック
        if (false == class_exists($className)) {
            return null;
        }
        // クラスインスタンス生成
        $controllerInstarnce = new $className();
        return $controllerInstarnce;
    }
}
?>