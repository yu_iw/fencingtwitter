<?php

class tweet{

	public function tweetMessage($message){

		session_start();
		require_once('twitteroauth.php');
		//設定ファイルの読み込み
		$ini = parse_ini_file('twitter.ini');

// 		error_log($_SESSION['oauth_token'],0);
// 		error_log($_SESSION['oauth_token_secret'],0);

		define('CONSUMER_KEY', $ini['CONSUMER_KEY']);
		define('CONSUMER_SECRET', $ini['CONSUMER_SECRET']);
		define('ACCESS_TOKEN', $_SESSION['oauth_token']);
		define('ACCESS_TOKEN_SECRET', $_SESSION['oauth_token_secret']);

		// request token取得
		$tw = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

		$vRequest = $tw->OAuthRequest("http://api.twitter.com/1.1/statuses/update.json","POST",array('status' => $message));

		//XMLデータをsimplexml_load_string関数を使用してオブジェクトに変換
// 		$oXml = simplexml_load_string($vRequest);
		$json = json_decode($vRequest);

// 		header("Content-Type: application/json");

		return $json;

	}



}


?>