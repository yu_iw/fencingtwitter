<?php

class sendDM{

	public function sendDirectMessage($user_id, $message){

		session_start();
		require_once('twitteroauth.php');
		//設定ファイルの読み込み
		$ini = parse_ini_file('twitter.ini');

		define('CONSUMER_KEY', $ini['CONSUMER_KEY']);
		define('CONSUMER_SECRET', $ini['CONSUMER_SECRET']);
		define('ACCESS_TOKEN', $ini['ACCESS_TOKEN']);
		define('ACCESS_TOKEN_SECRET', $ini['ACCESS_TOKEN_SECRET']);

		// request token取得
		$tw = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

		$vRequest = $tw->OAuthRequest("https://api.twitter.com/1/direct_messages/new.xml","POST",array('user_id' => $user_id, 'text' => $message));

		//XMLデータをsimplexml_load_string関数を使用してオブジェクトに変換
		$oXml = simplexml_load_string($vRequest);

		return $oXml;

	}



}


?>