<?php
require_once('twitteroauth/autoload.php');
use Abraham\TwitterOAuth\TwitterOAuth;
session_start();
//設定ファイルの読み込み
$ini = parse_ini_file('twitter.ini');

define('CONSUMER_KEY', $ini['CONSUMER_KEY']);
define('CONSUMER_SECRET', $ini['CONSUMER_SECRET']);

// getToken.php でセットした oauth_token と一致するかチェック
if ($_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
	unset($_SESSION);
// 	echo '<a href="/">token不一致。最初からどうぞ</a>';
	header("Location: " ."/");
	exit;
}

error_log($_SESSION['oauth_token'],0);
error_log($_SESSION['oauth_token_secret'],0);

// access token 取得
$tw = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET,
		$_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
$access_token = $tw->oauth("oauth/access_token", array("oauth_verifier" => $_REQUEST['oauth_verifier']));

// Twitter の user_id + screen_name(表示名)
$user_id     = $access_token['user_id'];
$screen_name = $access_token['screen_name'];

//セッション内のトークンを上書き
$_SESSION['oauth_token'] = $access_token['oauth_token'];
$_SESSION['oauth_token_secret'] = $access_token['oauth_token_secret'];

// error_log(print_r($access_token,true));
// session内にuser_idとscreen_nameを保持
$_SESSION['user_id']        = $user_id;
$_SESSION['screen_name'] = $screen_name;


$tw2 = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET,
		$_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

$users_params = array('screen_name' => $_SESSION['screen_name']);
$users = $tw2->get('users/show', $users_params);

$_SESSION['tw_users'] = json_decode(json_encode($users), true);

header("Location: " . $ini['callback']);

?>