<?php


    $content_type = explode(';', trim(strtolower($_SERVER['CONTENT_TYPE'])));
//var_dump($_SERVER);
    $media_type = $content_type[0];

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && $media_type == 'application/json') {
        // application/json で送信されてきた場合の処理
        $request = json_decode(file_get_contents('php://input'), true);
    } else {
        // application/x-www-form-urlencoded で送信されてきた場合の処理
        $request = $_REQUEST;
    }


		// curlを初期化
		$curl = curl_init ();
		curl_setopt ( $curl, CURLOPT_HTTPHEADER, array (
				'Content-Type: application/json' 
		) );
		curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true ); 
		curl_setopt ( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );
		curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); 
		$url = "https://104.215.0.251/api/data/json/1.0/realtimematchcard"; // APIのURL
		curl_setopt ( $curl, CURLOPT_URL, $url ); 

		$data_json = json_encode ( $request );
		curl_setopt ( $curl, CURLOPT_POSTFIELDS, $data_json );
		$response = curl_exec ( $curl ); 
		curl_close ( $curl );




    header('Content-type: text/plain; charset= UTF-8');
    $str = "AJAX REQUEST SUCCESS\nstatus:".$response;
    $result = nl2br($str);
    echo $result;
?>