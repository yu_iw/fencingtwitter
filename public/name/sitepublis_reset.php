<?php
		// curlを初期化
		$curl = curl_init ();
		curl_setopt ( $curl, CURLOPT_HTTPHEADER, array (
				'Content-Type: application/json' 
		) );
		curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true ); 
		curl_setopt ( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );
		curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); 
		$url = "https://104.215.0.251/api/data/json/1.0/realtimematchcard"; // APIのURL
		curl_setopt ( $curl, CURLOPT_URL, $url ); 
		$param = array (
				"green1" => array (
						"left" => "",
						"left_country" => "",
						"right" => "",
						"right_country" => "" 
				),
				"green2" => array (
						"left" => "",
						"left_country" => "",
						"right" => "",
						"right_country" => "" 
				),
				"red1" => array (
						"left" => "",
						"left_country" => "",
						"right" => "",
						"right_country" => "" 
				),
				"red2" => array (
						"left" => "",
						"left_country" => "",
						"right" => "",
						"right_country" => "" 
				),
				"yellow1" => array (
						"left" => "",
						"left_country" => "",
						"right" => "",
						"right_country" => "" 
				),
				"yellow2" => array (
						"left" => "",
						"left_country" => "",
						"right" => "",
						"right_country" => "" 
				),
				"blue1" => array (
						"left" => "",
						"left_country" => "",
						"right" => "",
						"right_country" => "" 
				),
				"blue2" => array (
						"left" => "",
						"left_country" => "",
						"right" => "",
						"right_country" => "" 
				),
				"podium" => array (
						"left" => "",
						"left_country" => "",
						"right" => "",
						"right_country" => "" 
				) 
		);
		$data_json = json_encode ( $param );
		curl_setopt ( $curl, CURLOPT_POSTFIELDS, $data_json );
		$response = curl_exec ( $curl ); 
		echo $response; // メッセージの表示（成功ならOKになる）
		curl_close ( $curl );
?>
