<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Tournament 1</title>
<link rel="stylesheet" href="/fencing/css/setting.css">
<link rel="stylesheet" href="/fencing/css/admin.css">
</head>
<body>
<form class="" enctype="multipart/form-data"  action="../bracket/?tid={$q['tid']}" method="post">
<h1>大会作成＆選択</h1>

<div id="container">
<table>
{foreach from=$p key=k item=_}
	<tr>
		<th><input type="text" name="name61" id="name61" class="name" value="{$_.name}"></th>
		<td><input type="text" name="name61" id="name61" class="name" value="{$_.nation}"></td>
		<td><input type="text" name="name61" id="name61" class="name" value="{$_.id}"></td>
	</tr>
{/foreach}
</table>
</div>
<div id="btnWrap"><input type="submit" id="btnSend" value="更新 / Send"></div>
</form>
<style>
#log {
	display: none;
	position: fixed;
	left: 10px;
	bottom: 80px;
	z-index: 1000;
	padding: 10px;
	background: rgba(255,255,255,.7);
	line-height: 2;
}
</style>
<div id="log"></div>

<script src="/fencing/js/jquery-2.2.3.min.js"></script>
<script src="/fencing/js/admin.js"></script>
</body>
</html>