
<div id="l{$p1}_{$p1}" class="player player1st">
    <input type="text" name="name{$p1}" id="name{$p1}" class="name" value="{$p[$p1-1].name}">
    <input type="text" maxlength="3" name="nation{$p1}" id="nation{$p1}" class="nation" value="{$p[$p1-1].nation}">
    <input type="checkbox" name="w6_{$p1}" id="w6_{$p1}" class="winner" value="{$t[location|cat:$p1]}" {if $t[w6_|cat:$p1]}checked{/if}>
<!--player{$p1}--></div>

<div class="status">
	{html_options name="c$num" class='court' id="c$num" options=$cArr selected=$t[c|cat:$num]}
	<div class="swap"><label><input type="checkbox" name="s{$num}" id="s{$num}" value="1" {if $t[s|cat:$num]}checked{/if}></label></div>
	<input type="color" name="f{$num}" id="f{$num}" class="color" value="{$t[f|cat:$num]}" >
	<input type="text" name="l{$num}" id="l{$num}" class="label" placeholder="ラベル" value="{$t[l|cat:$num]}" >
<!--game{$num} status--></div>

<div id="l6_{$p2}" class="player player2nd">
    <input type="text" name="name{$p2}" id="name{$p2}" class="name" value="{$p[$p2-1].name}">
    <input type="text" maxlength="3" name="nation{$p2}" id="nation{$p2}" class="nation" value="{$p[$p2-1].nation}">
    <input type="checkbox" name="w6_{$p2}" id="w6_{$p2}" class="winner" value="{$t[location|cat:$p2]}" {if $t[w6_|cat:$p2]}checked{/if}>
 <!--player{$p2}--></div>