
<div id="l{$p1}" class="line player1st">
	<input type="checkbox" name="w{$p1}" id="w{$p1}" class="winner" value="" {if $t[w|cat:$p1]}checked{/if}>
	<input type="text" name="name{$p1}" id="name{$p1}" class="name" value="">
	<input type="text" name="nation{$p1}" id="nation{$p1}" class="nation" value="">
</div>
<div class="status">
	{html_options name="c$num" class='court' id="c$num" options=$cArr selected=$t[c|cat:$num]}
	<div class="swap"><label><input type="checkbox" name="s{$num}" id="s{$num}" value="1" {if $t[s|cat:$num]}checked{/if}></label></div>
	<input type="color" name="f{$num}" id="f{$num}" class="color" value="{$t[f|cat:$num]}" >
	<input type="text" name="l{$num}" id="l{$num}" class="label" placeholder="ラベル" value="{$t[l|cat:$num]}" >
	<div class="positionMatch">{$p1}</div>
<!--game{$num} status--></div>
<div id="l{$p2}" class="line player2nd">
	<input type="checkbox" name="w{$p2}" id="w{$p2}" class="winner" value="" {if $t[w|cat:$p2]}checked{/if}>
	<input type="text" name="name{$p2}" id="name{$p2}" class="name" value="">
	<input type="text" name="nation{$p2}" id="nation{$p2}" class="nation" value="">
</div>