<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Tournament 1</title>
<link rel="stylesheet" href="/name/css/setting.css">
<link rel="stylesheet" href="/name/css/admin.css">
</head>
<body>
<form class="" enctype="multipart/form-data"  action="../bracket/?tid={$q['tid']}" method="post">
<h1>{$t.tournament_name}</h1>

{foreach from=$w key=k item=_}
	<input type="hidden" name="{$k}" value="{$_}">
{/foreach}
{foreach from=$s key=k item=_}
	<input type="hidden" name="{$k}" value="{$_}">
{/foreach}
	<div id="container" class="break">
		<div class=""><a href="../list/">大会リストに戻る</a><br><br><br></div>
		<div class="">大会名：<input type="text" name="tournament_name" id="tournament_name" class="tournament_name" value="{$t.tournament_name}"></div>
		<div class="section">
			<div class="break">
			<div class="section">
				<div class="break">
				<div class="section">
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_1" class="inputArea break">
								{include file="tournament/init.tpl" num="6_1" p1="1" p2="2"}
							<!--best64 game1--></div>
							<div id="sec6_2" class="inputArea break">
								{include file="tournament/init.tpl" num="6_2" p1="3" p2="4"}
							<!--best64 game2--></div>
						<!--best64 game1, 2---></div><!--
						--><div id='sec5_1' class="sectionLine">
							{include file="tournament/match.tpl" num="5_1" p1="5_1" p2="5_2"}
							<!--best32 game1--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_3" class="inputArea break">
								{include file="tournament/init.tpl" num="6_3" p1="5" p2="6"}
							<!--best64 game3--></div>
							<div id="sec6_4" class="inputArea break">
								{include file="tournament/init.tpl" num="6_4" p1="7" p2="8"}
							<!--best64 game4--></div>
						<!--best64 game3, 4--></div><!--
						--><div id='sec5_2' class="sectionLine">
							{include file="tournament/match.tpl" num="5_2" p1="5_3" p2="5_4"}
							<!--best32 game2--></div>
						<!--break--></div>
					<!--best32 game1, 2--></div><!--
					--><div id='sec4_1' class="sectionLine secLine4">
							{include file="tournament/match.tpl" num="4_1" p1="4_1" p2="4_2"}
					<!--best16 game1--></div>
					<!--break--></div>
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_5" class="inputArea break">
								{include file="tournament/init.tpl" num="6_5" p1="9" p2="10"}
							<!--best64 game5--></div>
							<div id="sec6_6" class="inputArea break">
								{include file="tournament/init.tpl" num="6_6" p1="11" p2="12"}
							<!--best64 game6--></div>
						<!--best64 game5, 6---></div><!--
						--><div id='sec5_3' class="sectionLine">
							{include file="tournament/match.tpl" num="5_3" p1="5_5" p2="5_6"}
						<!--best32 game3--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_7" class="inputArea break">
								{include file="tournament/init.tpl" num="6_7" p1="13" p2="14"}
							<!--best64 game7--></div>
							<div id="sec6_8" class="inputArea break">
								{include file="tournament/init.tpl" num="6_8" p1="15" p2="16"}
							<!--best64 game8--></div>
						<!--best64 game7, 8--></div><!--
						--><div id='sec5_4' class="sectionLine">
							{include file="tournament/match.tpl" num="5_4" p1="5_7" p2="5_8"}
						<!--best32 game4--></div>
						<!--break--></div>
					<!--best32 game3, 4--></div><!--
					--><div id='sec4_2' class="sectionLine secLine4">
						{include file="tournament/match.tpl" num="4_2" p1="4_3" p2="4_4"}
					<!--best16 game2--></div>
					<!--break--></div>
				<!--best16 game1, 2--></div><!--
				--><div id='sec3_1' class="sectionLine secLine3">
					{include file="tournament/match.tpl" num="3_1" p1="3_1" p2="3_2"}
				<!--best8 game1--></div>
				<!--break--></div>
				<div class="break">
				<div class="section">
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_9" class="inputArea break">
								{include file="tournament/init.tpl" num="6_9" p1="17" p2="18"}
							<!--best64 game9--></div>
							<div id="sec6_10" class="inputArea break">
								{include file="tournament/init.tpl" num="6_10" p1="19" p2="20"}
							<!--best64 game10--></div>
						<!--best64 game9, 10---></div><!--
						--><div id='sec5_5' class="sectionLine">
							{include file="tournament/match.tpl" num="5_5" p1="5_9" p2="5_10"}
						<!--best32 game5--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_11" class="inputArea break">
								{include file="tournament/init.tpl" num="6_11" p1="21" p2="22"}
							<!--best64 game11--></div>
							<div id="sec6_12" class="inputArea break">
								{include file="tournament/init.tpl" num="6_12" p1="23" p2="24"}
							<!--best64 game12--></div>
						<!--best64 game11, 12--></div><!--
						--><div id='sec5_6' class="sectionLine">
							{include file="tournament/match.tpl" num="5_6" p1="5_11" p2="5_12"}
						<!--best32 game6--></div>
						<!--break--></div>
					<!--best32 game5, 6--></div><!--
					--><div id='sec4_3' class="sectionLine secLine4">
							{include file="tournament/match.tpl" num="4_3" p1="4_5" p2="4_6"}
					<!--best16 game3--></div>
					<!--break--></div>
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_13" class="inputArea break">
								{include file="tournament/init.tpl" num="6_13" p1="25" p2="26"}
							<!--best64 game13--></div>
							<div id="sec6_14" class="inputArea break">
								{include file="tournament/init.tpl" num="6_14" p1="27" p2="28"}
							<!--best64 game14--></div>
						<!--best64 game13, 14---></div><!--
						--><div id='sec5_7' class="sectionLine">
							{include file="tournament/match.tpl" num="5_7" p1="5_13" p2="5_14"}
						<!--best32 game7--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_15" class="inputArea break">
								{include file="tournament/init.tpl" num="6_15" p1="29" p2="30"}
							<!--best64 game15--></div>
							<div id="sec6_16" class="inputArea break">
								{include file="tournament/init.tpl" num="6_16" p1="31" p2="32"}
							<!--best64 game16--></div>
						<!--best64 game15, 16--></div><!--
						--><div id='sec5_8' class="sectionLine">
							{include file="tournament/match.tpl" num="5_8" p1="5_15" p2="5_16"}
						<!--best32 game8--></div>
						<!--break--></div>
					<!--best32 game7, 8--></div><!--
					--><div id='sec4_4' class="sectionLine secLine4">
							{include file="tournament/match.tpl" num="4_4" p1="4_7" p2="4_8"}
					<!--best16 game4--></div>
					<!--break--></div>
				<!--best16 game3, 4--></div><!--
				--><div id='sec3_2' class="sectionLine secLine3">
					{include file="tournament/match.tpl" num="3_2" p1="3_3" p2="3_4"}
				<!--best8 game2--></div>
				<!--break--></div>
			<!--best8 game1, 2--></div><!--
				--><div id='sec2_1' class="sectionLine secLine2">
					{include file="tournament/match.tpl" num="2_1" p1="2_1" p2="2_2"}
				<!--best4 game1--></div>
				<!--break--></div>
				<div class="break">
			<div class="section">
				<div class="break">
				<div class="section">
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_17" class="inputArea break">
								{include file="tournament/init.tpl" num="6_17" p1="33" p2="34"}
							<!--best64 game17--></div>
							<div id="sec6_18" class="inputArea break">
								{include file="tournament/init.tpl" num="6_18" p1="35" p2="36"}
							<!--best64 game18--></div>
						<!--best64 game17, 18---></div><!--
						--><div id='sec5_9' class="sectionLine">
							{include file="tournament/match.tpl" num="5_9" p1="5_17" p2="5_18"}
						<!--best32 game9--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_19" class="inputArea break">
								{include file="tournament/init.tpl" num="6_19" p1="37" p2="38"}
							<!--best64 game19--></div>
							<div id="sec6_20" class="inputArea break">
								{include file="tournament/init.tpl" num="6_20" p1="39" p2="40"}
							<!--best64 game20--></div>
						<!--best64 game19, 20--></div><!--
						--><div id='sec5_10' class="sectionLine">
							{include file="tournament/match.tpl" num="5_10" p1="5_19" p2="5_20"}
						<!--best32 game10--></div>
						<!--break--></div>
					<!--best32 game9, 10--></div><!--
					--><div id='sec4_5' class="sectionLine secLine4">
							{include file="tournament/match.tpl" num="4_5" p1="4_9" p2="4_10"}
					<!--best16 game5--></div>
					<!--break--></div>
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_21" class="inputArea break">
								{include file="tournament/init.tpl" num="6_21" p1="41" p2="42"}
							<!--best64 game21--></div>
							<div id="sec6_22" class="inputArea break">
								{include file="tournament/init.tpl" num="6_22" p1="43" p2="44"}
							<!--best64 game22--></div>
						<!--best64 game21, 22---></div><!--
						--><div id='sec5_11' class="sectionLine">
							{include file="tournament/match.tpl" num="5_11" p1="5_21" p2="5_22"}
						<!--best32 game11--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_23" class="inputArea break">
								{include file="tournament/init.tpl" num="6_23" p1="45" p2="46"}
							<!--best64 game23--></div>
							<div id="sec6_24" class="inputArea break">
								{include file="tournament/init.tpl" num="6_24" p1="47" p2="48"}
							<!--best64 game24--></div>
						<!--best64 game23, 24--></div><!--
						--><div id='sec5_12' class="sectionLine">
							{include file="tournament/match.tpl" num="5_12" p1="5_23" p2="5_24"}
						<!--best32 game12--></div>
						<!--break--></div>
					<!--best32 game11, 12--></div><!--
					--><div id='sec4_6' class="sectionLine secLine4">
							{include file="tournament/match.tpl" num="4_6" p1="4_11" p2="4_12"}
					<!--best16 game6--></div>
					<!--break--></div>
				<!--best16 game5, 6--></div><!--
				--><div id='sec3_3' class="sectionLine secLine3">
					{include file="tournament/match.tpl" num="3_3" p1="3_5" p2="3_6"}
				<!--best8 game3--></div>
				<!--break--></div>
				<div class="break">
				<div class="section">
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_25" class="inputArea break">
								{include file="tournament/init.tpl" num="6_25" p1="49" p2="50"}
							<!--best64 game25--></div>
							<div id="sec6_26" class="inputArea break">
								{include file="tournament/init.tpl" num="6_26" p1="51" p2="52"}
							<!--best64 game26--></div>
						<!--best64 game25, 26---></div><!--
						--><div id='sec5_13' class="sectionLine">
							{include file="tournament/match.tpl" num="5_13" p1="5_25" p2="5_26"}
						<!--best32 game13--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_27" class="inputArea break">
								{include file="tournament/init.tpl" num="6_27" p1="53" p2="54"}
							<!--best64 game27--></div>
							<div id="sec6_28" class="inputArea break">
								{include file="tournament/init.tpl" num="6_28" p1="55" p2="56"}
							<!--best64 game28--></div>
						<!--best64 game27, 28--></div><!--
						--><div id='sec5_14' class="sectionLine">
							{include file="tournament/match.tpl" num="5_14" p1="5_27" p2="5_28"}
						<!--best32 game14--></div>
						<!--break--></div>
					<!--best32 game13, 14--></div><!--
					--><div id='sec4_7' class="sectionLine secLine4">
							{include file="tournament/match.tpl" num="4_7" p1="4_13" p2="4_14"}
					<!--best16 game7--></div>
					<!--break--></div>
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_29" class="inputArea break">
								{include file="tournament/init.tpl" num="6_29" p1="57" p2="58"}
							<!--best64 game29--></div>
							<div id="sec6_30" class="inputArea break">
								{include file="tournament/init.tpl" num="6_30" p1="59" p2="60"}
							<!--best64 game14--></div>
						<!--best64 game29, 30---></div><!--
						--><div id='sec5_15' class="sectionLine">
							{include file="tournament/match.tpl" num="5_15" p1="5_29" p2="5_30"}
						<!--best32 game15--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_31" class="inputArea break">
								{include file="tournament/init.tpl" num="6_31" p1="61" p2="62"}
							<!--best64 game31--></div>
							<div id="sec6_32" class="inputArea break">
								{include file="tournament/init.tpl" num="6_32" p1="63" p2="64"}
							<!--best64 game32--></div>
						<!--best64 game31, 32--></div><!--
						--><div id='sec5_16' class="sectionLine">
							{include file="tournament/match.tpl" num="5_16" p1="5_31" p2="5_32"}
						<!--best32 game16--></div>
						<!--break--></div>
					<!--best32 game15, 16--></div><!--
					--><div id='sec4_8' class="sectionLine secLine4">
						{include file="tournament/match.tpl" num="4_8" p1="4_15" p2="4_16"}
					<!--best16 game8--></div>
					<!--break--></div>
				<!--best16 game7, 8--></div><!--
				--><div id='sec3_4' class="sectionLine secLine3">
						{include file="tournament/match.tpl" num="3_4" p1="3_7" p2="3_8"}
				<!--best8 game4--></div>
				<!--break--></div>
			<!--best8 game3, 4--></div><!--
				--><div id='sec2_2' class="sectionLine secLine2">
						{include file="tournament/match.tpl" num="2_2" p1="2_3" p2="2_4"}
				<!--best4 game2--></div>
				<!--break--></div>
		<!--best4 game1, 2--></div><!--
		--><div id='sec1_1' class="sectionLine secLine1">
						{include file="tournament/match.tpl" num="1_1" p1="1_1" p2="1_2"}
		<!--final--></div><!--
		--><div class="sectionWinner">
			<div id="winnerName">Winner</div>
			<input type="hidden" name="w0_1" id="w0_1" class="winner" value="">
		<!--sectionWinner--></div>
	<!--container--></div>

	<div id="info">
		<div><ul id="courtInfo">
			<li id="ci1">
				<p class="courtGreen">Piste Green1</p>
				<span>Left Player：</span>
				<span id="ci1NameL" class="name"></span>
				<span id="ci1NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci1NameR" class="name"></span>
				<span id="ci1NationR" class="nation"></span><br>
			</li>
			<li id="ci5">
				<p class="courtGreen">Piste Green2</p>
				<span>Left Player：</span>
				<span id="ci5NameL" class="name"></span>
				<span id="ci5NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci5NameR" class="name"></span>
				<span id="ci5NationR" class="nation"></span><br>
			</li>
			<li id="ci2">
				<p class="courtYellow">Piste Yellow1</p>
				<span>Left Player：</span>
				<span id="ci2NameL" class="name"></span>
				<span id="ci2NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci2NameR" class="name"></span>
				<span id="ci2NationR" class="nation"></span><br>
			</li>
			<li id="ci6">
				<p class="courtYellow">Piste Yellow2</p>
				<span>Left Player：</span>
				<span id="ci6NameL" class="name"></span>
				<span id="ci6NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci6NameR" class="name"></span>
				<span id="ci6NationR" class="nation"></span><br>
			</li>
			<li id="ci3">
				<p class="courtRed">Piste Red1</p>
				<span>Left Player：</span>
				<span id="ci3NameL" class="name"></span>
				<span id="ci3NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci3NameR" class="name"></span>
				<span id="ci3NationR" class="nation"></span><br>
			</li>
			<li id="ci7">
				<p class="courtRed">Piste Red2</p>
				<span>Left Player：</span>
				<span id="ci7NameL" class="name"></span>
				<span id="ci7NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci7NameR" class="name"></span>
				<span id="ci7NationR" class="nation"></span><br>
			</li>
			<li id="ci4">
				<p class="courtBlue">Piste Blue1</p>
				<span>Left Player：</span>
				<span id="ci4NameL" class="name"></span>
				<span id="ci4NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci4NameR" class="name"></span>
				<span id="ci4NationR" class="nation"></span><br>
			</li>
			<li id="ci8">
				<p class="courtBlue">Piste Blue2</p>
				<span>Left Player：</span>
				<span id="ci8NameL" class="name"></span>
				<span id="ci8NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci8NameR" class="name"></span>
				<span id="ci8NationR" class="nation"></span><br>
			</li>
			<li id="ci9">
				<p>Piste Podium</p>
				<span>Left Player：</span>
				<span id="ci9NameL" class="name"></span>
				<span id="ci9NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci9NameR" class="name"></span>
				<span id="ci9NationR" class="nation"></span><br>
			</li>
			<li id="ci10">
				<p>Abema</p>
				<span>Left Player：</span>
				<span id="ci10NameL" class="name"></span>
				<span id="ci10NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci10NameR" class="name"></span>
				<span id="ci10NationR" class="nation"></span><br>
			</li>
			<li id="ci11">
				<p>Kokusai</p>
				<span>Left Player：</span>
				<span id="ci11NameL" class="name"></span>
				<span id="ci11NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci11NameR" class="name"></span>
				<span id="ci11NationR" class="nation"></span><br>
			</li>
		</ul></div>
		<a id="infoClose"></a>
	<!--info--></div>
{foreach from=$pp key=k item=_}
	<input type="hidden" name="id{$k+1}" value="{$_.id}">
{/foreach}
	<div id="btnWrap"><input type="submit" id="btnSend" value="更新 / Send"></div>
</form>

<style>
#log {
	display: none;
	position: fixed;
	left: 10px;
	bottom: 80px;
	z-index: 1000;
	padding: 10px;
	background: rgba(255,255,255,.7);
	line-height: 2;
}
</style>
<div id="log"></div>

<script src="/name/js/jquery-2.2.3.min.js"></script>
<script src="/name/js/admin.js"></script>
</body>
</html>
