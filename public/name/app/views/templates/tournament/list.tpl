<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Tournament 1</title>
<link rel="stylesheet" href="/fencing/css/setting.css">
<link rel="stylesheet" href="/fencing/css/admin.css">
</head>
<body>
<h1>大会作成＆選択</h1>

<div id="container">
<div class=""><a href="../logout/">ログアウト</a><br><br></div>
<table>
{foreach from=$t key=k item=_}
	<tr>
		<th><a href ="../bracket/?tid={$_.id}">「{$_.tournament_name}」</a>：</th>
		<td>ID:{$_.id}</td>
		<td><a href="../delete/?tid={$_.id}"  class="confirm_link">削除</a></td>
	</tr>
{/foreach}
</table>
	<a href ="../create/">新規大会作成</a><br>
</div>

<style>
#log {
	display: none;
	position: fixed;
	left: 10px;
	bottom: 80px;
	z-index: 1000;
	padding: 10px;
	background: rgba(255,255,255,.7);
	line-height: 2;
}
table {
	margin: auto;
	display: block;
}
th,td {
	padding: 10px;	
}
</style>
<div id="log"></div>

<script src="/fencing/js/jquery-2.2.3.min.js"></script>
<script src="/fencing/js/admin.js"></script>
<SCRIPT language="JavaScript">
<!--
// 警告ウィンドウを表示する
$('.confirm_link').click(function() {
	if (!confirm('この大会を削除します\nよろしいですか？')) {
		return false;
	}
});
//-->
</SCRIPT>
</body>
</html>