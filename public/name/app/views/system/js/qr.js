/* ++++++++++++++++++++++++++++++++++++++++

	QR
	
++++++++++++++++++++++++++++++++++++++++ */
function qrGenerate(text){
	$('#qr').qrcode({
		render: 'image',
		text: text,
		size : 200,
		minVersion: 6,
	    ecLevel: 'Q',
	})
};
