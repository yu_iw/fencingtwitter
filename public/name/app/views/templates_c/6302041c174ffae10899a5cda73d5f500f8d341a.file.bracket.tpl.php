<?php /* Smarty version Smarty-3.1.13, created on 2020-07-20 16:24:13
         compiled from "/home/fencing-name.grpht.co.jp/public_html/fencing/app/views/templates/tournament/bracket.tpl" */ ?>
<?php /*%%SmartyHeaderCode:194090515f15469dc8f651-07752560%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6302041c174ffae10899a5cda73d5f500f8d341a' => 
    array (
      0 => '/home/fencing-name.grpht.co.jp/public_html/fencing/app/views/templates/tournament/bracket.tpl',
      1 => 1559705204,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '194090515f15469dc8f651-07752560',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'q' => 0,
    't' => 0,
    'w' => 0,
    'k' => 0,
    '_' => 0,
    's' => 0,
    'p' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5f15469e7d2d16_61504388',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f15469e7d2d16_61504388')) {function content_5f15469e7d2d16_61504388($_smarty_tpl) {?><!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Tournament 1</title>
<link rel="stylesheet" href="/fencing/css/setting.css">
<link rel="stylesheet" href="/fencing/css/admin.css">
</head>
<body>
<form class="" enctype="multipart/form-data"  action="../bracket/?tid=<?php echo $_smarty_tpl->tpl_vars['q']->value['tid'];?>
" method="post">
<h1><?php echo $_smarty_tpl->tpl_vars['t']->value['tournament_name'];?>
</h1>

<?php  $_smarty_tpl->tpl_vars['_'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['_']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['w']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['_']->key => $_smarty_tpl->tpl_vars['_']->value){
$_smarty_tpl->tpl_vars['_']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['_']->key;
?>
	<input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['_']->value;?>
">
<?php } ?>
<?php  $_smarty_tpl->tpl_vars['_'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['_']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['s']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['_']->key => $_smarty_tpl->tpl_vars['_']->value){
$_smarty_tpl->tpl_vars['_']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['_']->key;
?>
	<input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['_']->value;?>
">
<?php } ?>
	<div id="container" class="break">
		<div class=""><a href="../list/">大会リストに戻る</a><br><br><br></div>
		<div class="">大会名：<input type="text" name="tournament_name" id="tournament_name" class="tournament_name" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['tournament_name'];?>
"></div>
		<div class="section">
			<div class="break">
			<div class="section">
				<div class="break">
				<div class="section">
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_1" class="inputArea break">
							<div id="l6_1" class="player player1st">
								<input type="text" name="name1" id="name1" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[0]['name'];?>
">
								<input type="text" maxlength="3" name="nation1" id="nation1" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[0]['nation'];?>
">
								<input type="checkbox" name="w6_1" id="w6_1" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location1'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_1']){?>checked<?php }?>>
							<!--player1--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_1"), 0);?>

							<div id="l6_2" class="player player2nd">
								<input type="text" name="name2" id="name2" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[1]['name'];?>
">
								<input type="text" maxlength="3" name="nation2" id="nation2" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[1]['nation'];?>
">
								<input type="checkbox" name="w6_2" id="w6_2" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location2'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_2']){?>checked<?php }?>>
							<!--player2--></div>
							<!--best64 game1--></div>
							<div id="sec6_2" class="inputArea break">
							<div id="l6_3" class="player player1st">
								<input type="text" name="name3" id="name3" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[2]['name'];?>
">
								<input type="text" maxlength="3" name="nation3" id="nation3" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[2]['nation'];?>
">
								<input type="checkbox" name="w6_3" id="w6_3" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location3'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_3']){?>checked<?php }?>>
							<!--player3--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_2"), 0);?>

							<div id="l6_4" class="player player2nd">
								<input type="text" name="name4" id="name4" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[3]['name'];?>
">
								<input type="text" maxlength="3" name="nation4" id="nation4" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[3]['nation'];?>
">
								<input type="checkbox" name="w6_4" id="w6_4" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location4'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_4']){?>checked<?php }?>>
							<!--player4--></div>
							<!--best64 game2--></div>
						<!--best64 game1, 2---></div><!--
						--><div class="sectionLine">
							<div id="l5_1" class="line player1st">
								<input type="checkbox" name="w5_1" id="w5_1" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_1']){?>checked<?php }?>>
							</div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_1"), 0);?>

							<div id="l5_2" class="line player2nd">
								<input type="checkbox" name="w5_2" id="w5_2" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_2']){?>checked<?php }?>>
							</div>
						<!--best32 game1--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_3" class="inputArea break">
								<div id="l6_5" class="player player1st">
									<input type="text" name="name5" id="name5" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[4]['name'];?>
">
									<input type="text" maxlength="3" name="nation5" id="nation5" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[4]['nation'];?>
">
									<input type="checkbox" name="w6_5" id="w6_5" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location5'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_5']){?>checked<?php }?>>
								<!--player5--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_3"), 0);?>

								<div id="l6_6" class="player player2nd">
									<input type="text" name="name6" id="name6" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[5]['name'];?>
">
									<input type="text" maxlength="3" name="nation6" id="nation6" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[5]['nation'];?>
">
									<input type="checkbox" name="w6_6" id="w6_6" class="winner"value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location6'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_6']){?>checked<?php }?>>
								<!--player6--></div>
							<!--best64 game3--></div>
							<div id="sec6_4" class="inputArea break">
							<div id="l6_7" class="player player1st">
								<input type="text" name="name7" id="name7" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[6]['name'];?>
">
								<input type="text" maxlength="3" name="nation7" id="nation7" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[6]['nation'];?>
">
								<input type="checkbox" name="w6_7" id="w6_7" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location7'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_7']){?>checked<?php }?>>
							<!--player7--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_4"), 0);?>

							<div id="l6_8" class="player player2nd">
								<input type="text" name="name8" id="name8" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[7]['name'];?>
">
								<input type="text" maxlength="3" name="nation8" id="nation8" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[7]['nation'];?>
">
								<input type="checkbox" name="w6_8" id="w6_8" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location8'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_8']){?>checked<?php }?>>
							<!--player8--></div>
							<!--best64 game4--></div>
						<!--best64 game3, 4--></div><!--
						--><div class="sectionLine">
							<div id="l5_3" class="line player1st"><input type="checkbox" name="w5_3" id="w5_3" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_3']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_2"), 0);?>

							<div id="l5_4" class="line player2nd"><input type="checkbox" name="w5_4" id="w5_4" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_4']){?>checked<?php }?>></div>
						<!--best32 game2--></div>
						<!--break--></div>
					<!--best32 game1, 2--></div><!--
					--><div class="sectionLine secLine4">
						<div id="l4_1" class="line player1st"><input type="checkbox" name="w4_1" id="w4_1" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_1']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"4_1"), 0);?>

						<div id="l4_2" class="line player2nd"><input type="checkbox" name="w4_2" id="w4_2" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_2']){?>checked<?php }?>></div>
					<!--best16 game1--></div>
					<!--break--></div>
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_5" class="inputArea break">
							<div id="l6_9" class="player player1st">
								<input type="text" name="name9" id="name9" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[8]['name'];?>
">
								<input type="text" maxlength="3" name="nation9" id="nation9" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[8]['nation'];?>
">
								<input type="checkbox" name="w6_9" id="w6_9" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location9'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_9']){?>checked<?php }?>>
							<!--player9--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_5"), 0);?>

							<div id="l6_10" class="player player2nd">
								<input type="text" name="name10" id="name10" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[9]['name'];?>
">
								<input type="text" maxlength="3" name="nation10" id="nation10" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[9]['nation'];?>
">
								<input type="checkbox" name="w6_10" id="w6_10" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location10'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_10']){?>checked<?php }?>>
							<!--player10--></div>
							<!--best64 game5--></div>
							<div id="sec6_6" class="inputArea break">
							<div id="l6_11" class="player player1st">
								<input type="text" name="name11" id="name11" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[10]['name'];?>
">
								<input type="text" maxlength="3" name="nation11" id="nation11" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[10]['nation'];?>
">
								<input type="checkbox" name="w6_11" id="w6_11" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location11'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_11']){?>checked<?php }?>>
							<!--player11--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_6"), 0);?>

							<div id="l6_12" class="player player2nd">
								<input type="text" name="name12" id="name12" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[11]['name'];?>
">
								<input type="text" maxlength="3" name="nation12" id="nation12" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[11]['nation'];?>
">
								<input type="checkbox" name="w6_12" id="w6_12" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location12'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_12']){?>checked<?php }?>>
							<!--player12--></div>
							<!--best64 game6--></div>
						<!--best64 game5, 6---></div><!--
						--><div class="sectionLine">
							<div id="l5_5" class="line player1st"><input type="checkbox" name="w5_5" id="w5_5" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_5']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_3"), 0);?>

							<div id="l5_6" class="line player2nd"><input type="checkbox" name="w5_6" id="w5_6" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_6']){?>checked<?php }?>></div>
						<!--best32 game3--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_7" class="inputArea break">
								<div id="l6_13" class="player player1st">
									<input type="text" name="name13" id="name13" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[12]['name'];?>
">
									<input type="text" maxlength="3" name="nation13" id="nation13" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[12]['nation'];?>
">
									<input type="checkbox" name="w6_13" id="w6_13" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location13'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_13']){?>checked<?php }?>>
								<!--player13--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_7"), 0);?>

								<div id="l6_14" class="player player2nd">
									<input type="text" name="name14" id="name14" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[13]['name'];?>
">
									<input type="text" maxlength="3" name="nation14" id="nation14" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[13]['nation'];?>
">
									<input type="checkbox" name="w6_14" id="w6_14" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location14'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_14']){?>checked<?php }?>>
								<!--player14--></div>
							<!--best64 game7--></div>
							<div id="sec6_8" class="inputArea break">
							<div id="l6_15" class="player player1st">
								<input type="text" name="name15" id="name15" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[14]['name'];?>
">
								<input type="text" maxlength="3" name="nation15" id="nation15" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[14]['nation'];?>
">
								<input type="checkbox" name="w6_15" id="w6_15" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location15'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_15']){?>checked<?php }?>>
							<!--player15--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_8"), 0);?>

							<div id="l6_16" class="player player2nd">
								<input type="text" name="name16" id="name16" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[15]['name'];?>
">
								<input type="text" maxlength="3" name="nation16" id="nation16" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[15]['nation'];?>
">
								<input type="checkbox" name="w6_16" id="w6_16" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location16'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_16']){?>checked<?php }?>>
							<!--player16--></div>
							<!--best64 game8--></div>
						<!--best64 game7, 8--></div><!--
						--><div class="sectionLine">
							<div id="l5_7" class="line player1st"><input type="checkbox" name="w5_7" id="w5_7" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_7']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_4"), 0);?>

							<div id="l5_8" class="line player2nd"><input type="checkbox" name="w5_8" id="w5_8" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_8']){?>checked<?php }?>></div>
						<!--best32 game4--></div>
						<!--break--></div>
					<!--best32 game3, 4--></div><!--
					--><div class="sectionLine secLine4">
						<div id="l4_3" class="line player1st"><input type="checkbox" name="w4_3" id="w4_3" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_3']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"4_2"), 0);?>

						<div id="l4_4" class="line player2nd"><input type="checkbox" name="w4_4" id="w4_4" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_4']){?>checked<?php }?>></div>
					<!--best16 game2--></div>
					<!--break--></div>
				<!--best16 game1, 2--></div><!--
				--><div class="sectionLine secLine3">
					<div id="l3_1" class="line player1st"><input type="checkbox" name="w3_1" id="w3_1" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w3_1']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"3_1"), 0);?>

					<div id="l3_2" class="line player2nd"><input type="checkbox" name="w3_2" id="w3_2" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w3_2']){?>checked<?php }?>></div>
				<!--best8 game1--></div>
				<!--break--></div>
				<div class="break">
				<div class="section">
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_9" class="inputArea break">
							<div id="l6_17" class="player player1st">
								<input type="text" name="name17" id="name17" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[16]['name'];?>
">
								<input type="text" maxlength="3" name="nation17" id="nation17" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[16]['nation'];?>
">
								<input type="checkbox" name="w6_17" id="w6_17" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location17'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_17']){?>checked<?php }?>>
							<!--player17--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_9"), 0);?>

							<div id="l6_18" class="player player2nd">
								<input type="text" name="name18" id="name18" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[17]['name'];?>
">
								<input type="text" maxlength="3" name="nation18" id="nation18" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[17]['nation'];?>
">
								<input type="checkbox" name="w6_18" id="w6_18" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location18'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_18']){?>checked<?php }?>>
							<!--player18--></div>
							<!--best64 game9--></div>
							<div id="sec6_10" class="inputArea break">
							<div id="l6_19" class="player player1st">
								<input type="text" name="name19" id="name19" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[18]['name'];?>
">
								<input type="text" maxlength="3" name="nation19" id="nation19" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[18]['nation'];?>
">
								<input type="checkbox" name="w6_19" id="w6_19" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location19'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_19']){?>checked<?php }?>>
							<!--player19--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_10"), 0);?>

							<div id="l6_20" class="player player2nd">
								<input type="text" name="name20" id="name20" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[19]['name'];?>
">
								<input type="text" maxlength="3" name="nation20" id="nation20" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[19]['nation'];?>
">
								<input type="checkbox" name="w6_20" id="w6_20" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location20'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_20']){?>checked<?php }?>>
							<!--player20--></div>
							<!--best64 game10--></div>
						<!--best64 game9, 10---></div><!--
						--><div class="sectionLine">
							<div id="l5_9" class="line player1st"><input type="checkbox" name="w5_9" id="w5_9" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_9']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_5"), 0);?>

							<div id="l5_10" class="line player2nd"><input type="checkbox" name="w5_10" id="w5_10" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_10']){?>checked<?php }?>></div>
						<!--best32 game5--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_11" class="inputArea break">
								<div id="l6_21" class="player player1st">
									<input type="text" name="name21" id="name21" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[20]['name'];?>
">
									<input type="text" maxlength="3" name="nation21" id="nation21" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[20]['nation'];?>
">
									<input type="checkbox" name="w6_21" id="w6_21" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location21'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_21']){?>checked<?php }?>>
								<!--player21--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_11"), 0);?>

								<div id="l6_22" class="player player2nd">
									<input type="text" name="name22" id="name22" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[21]['name'];?>
">
									<input type="text" maxlength="3" name="nation22" id="nation22" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[21]['nation'];?>
">
									<input type="checkbox" name="w6_22" id="w6_22" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location22'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_22']){?>checked<?php }?>>
								<!--player22--></div>
							<!--best64 game11--></div>
							<div id="sec6_12" class="inputArea break">
							<div id="l6_23" class="player player1st">
								<input type="text" name="name23" id="name23" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[22]['name'];?>
">
								<input type="text" maxlength="3" name="nation23" id="nation23" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[22]['nation'];?>
">
								<input type="checkbox" name="w6_23" id="w6_23" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location23'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_23']){?>checked<?php }?>>
							<!--player23--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_12"), 0);?>

							<div id="l6_24" class="player player2nd">
								<input type="text" name="name24" id="name24" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[23]['name'];?>
">
								<input type="text" maxlength="3" name="nation24" id="nation24" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[23]['nation'];?>
">
								<input type="checkbox" name="w6_24" id="w6_24" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location24'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_24']){?>checked<?php }?>>
							<!--player24--></div>
							<!--best64 game12--></div>
						<!--best64 game11, 12--></div><!--
						--><div class="sectionLine">
							<div id="l5_11" class="line player1st"><input type="checkbox" name="w5_11" id="w5_11" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_11']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_6"), 0);?>

							<div id="l5_12" class="line player2nd"><input type="checkbox" name="w5_12" id="w5_12" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_12']){?>checked<?php }?>></div>
						<!--best32 game6--></div>
						<!--break--></div>
					<!--best32 game5, 6--></div><!--
					--><div class="sectionLine secLine4">
						<div id="l4_5" class="line player1st"><input type="checkbox" name="w4_5" id="w4_5" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_5']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"4_3"), 0);?>

						<div id="l4_6" class="line player2nd"><input type="checkbox" name="w4_6" id="w4_6" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_6']){?>checked<?php }?>></div>
					<!--best16 game3--></div>
					<!--break--></div>
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_13" class="inputArea break">
							<div id="l6_25" class="player player1st">
								<input type="text" name="name25" id="name25" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[24]['name'];?>
">
								<input type="text" maxlength="3" name="nation25" id="nation25" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[24]['nation'];?>
">
								<input type="checkbox" name="w6_25" id="w6_25" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location25'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_25']){?>checked<?php }?>>
							<!--player25--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_13"), 0);?>

							<div id="l6_26" class="player player2nd">
								<input type="text" name="name26" id="name26" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[25]['name'];?>
">
								<input type="text" maxlength="3" name="nation26" id="nation26" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[25]['nation'];?>
">
								<input type="checkbox" name="w6_26" id="w6_26" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location26'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_26']){?>checked<?php }?>>
							<!--player26--></div>
							<!--best64 game13--></div>
							<div id="sec6_14" class="inputArea break">
							<div id="l6_27" class="player player1st">
								<input type="text" name="name27" id="name27" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[26]['name'];?>
">
								<input type="text" maxlength="3" name="nation27" id="nation27" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[26]['nation'];?>
">
								<input type="checkbox" name="w6_27" id="w6_27" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location27'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_27']){?>checked<?php }?>>
							<!--player27--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_14"), 0);?>

							<div id="l6_28" class="player player2nd">
								<input type="text" name="name28" id="name28" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[27]['name'];?>
">
								<input type="text" maxlength="3" name="nation28" id="nation28" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[27]['nation'];?>
">
								<input type="checkbox" name="w6_28" id="w6_28" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location28'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_28']){?>checked<?php }?>>
							<!--player28--></div>
							<!--best64 game14--></div>
						<!--best64 game13, 14---></div><!--
						--><div class="sectionLine">
							<div id="l5_13" class="line player1st"><input type="checkbox" name="w5_13" id="w5_13" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_13']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_7"), 0);?>

							<div id="l5_14" class="line player2nd"><input type="checkbox" name="w5_14" id="w5_14" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_14']){?>checked<?php }?>></div>
						<!--best32 game7--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_15" class="inputArea break">
								<div id="l6_29" class="player player1st">
									<input type="text" name="name29" id="name29" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[28]['name'];?>
">
									<input type="text" maxlength="3" name="nation29" id="nation29" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[28]['nation'];?>
">
									<input type="checkbox" name="w6_29" id="w6_29" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location29'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_29']){?>checked<?php }?>>
								<!--player29--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_15"), 0);?>

								<div id="l6_30" class="player player2nd">
									<input type="text" name="name30" id="name30" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[29]['name'];?>
">
									<input type="text" maxlength="3" name="nation30" id="nation30" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[29]['nation'];?>
">
									<input type="checkbox" name="w6_30" id="w6_30" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location30'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_30']){?>checked<?php }?>>
								<!--player30--></div>
							<!--best64 game15--></div>
							<div id="sec6_16" class="inputArea break">
							<div id="l6_31" class="player player1st">
								<input type="text" name="name31" id="name31" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[30]['name'];?>
">
								<input type="text" maxlength="3" name="nation31" id="nation31" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[30]['nation'];?>
">
								<input type="checkbox" name="w6_31" id="w6_31" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location31'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_31']){?>checked<?php }?>>
							<!--player31--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_16"), 0);?>

							<div id="l6_32" class="player player2nd">
								<input type="text" name="name32" id="name32" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[31]['name'];?>
">
								<input type="text" maxlength="3" name="nation32" id="nation32" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[31]['nation'];?>
">
								<input type="checkbox" name="w6_32" id="w6_32" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location32'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_32']){?>checked<?php }?>>
							<!--player32--></div>
							<!--best64 game16--></div>
						<!--best64 game15, 16--></div><!--
						--><div class="sectionLine">
							<div id="l5_15" class="line player1st"><input type="checkbox" name="w5_15" id="w5_15" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_15']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_8"), 0);?>

							<div id="l5_16" class="line player2nd"><input type="checkbox" name="w5_16" id="w5_16" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_16']){?>checked<?php }?>></div>
						<!--best32 game8--></div>
						<!--break--></div>
					<!--best32 game7, 8--></div><!--
					--><div class="sectionLine secLine4">
						<div id="l4_7" class="line player1st"><input type="checkbox" name="w4_7" id="w4_7" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_7']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"4_4"), 0);?>

						<div id="l4_8" class="line player2nd"><input type="checkbox" name="w4_8" id="w4_8" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_8']){?>checked<?php }?>></div>
					<!--best16 game4--></div>
					<!--break--></div>
				<!--best16 game3, 4--></div><!--
				--><div class="sectionLine secLine3">
					<div id="l3_3" class="line player1st"><input type="checkbox" name="w3_3" id="w3_3" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w3_3']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"3_2"), 0);?>

					<div id="l3_4" class="line player2nd"><input type="checkbox" name="w3_4" id="w3_4" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w3_4']){?>checked<?php }?>></div>
				<!--best8 game2--></div>
				<!--break--></div>
			<!--best8 game1, 2--></div><!--
				--><div class="sectionLine secLine2">
					<div id="l2_1" class="line player1st"><input type="checkbox" name="w2_1" id="w2_1" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w2_1']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"2_1"), 0);?>

					<div id="l2_2" class="line player2nd"><input type="checkbox" name="w2_2" id="w2_2" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w2_2']){?>checked<?php }?>></div>
				<!--best4 game1--></div>
				<!--break--></div>
				<div class="break">
			<div class="section">
				<div class="break">
				<div class="section">
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_17" class="inputArea break">
							<div id="l6_33" class="player player1st">
								<input type="text" name="name33" id="name33" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[32]['name'];?>
">
								<input type="text" maxlength="3" name="nation33" id="nation33" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[32]['nation'];?>
">
								<input type="checkbox" name="w6_33" id="w6_33" class="winner"value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location33'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_33']){?>checked<?php }?>>
							<!--player33--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_17"), 0);?>

							<div id="l6_34" class="player player2nd">
								<input type="text" name="name34" id="name34" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[33]['name'];?>
">
								<input type="text" maxlength="3" name="nation34" id="nation34" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[33]['nation'];?>
">
								<input type="checkbox" name="w6_34" id="w6_34" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location34'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_34']){?>checked<?php }?>>
							<!--player34--></div>
							<!--best64 game17--></div>
							<div id="sec6_18" class="inputArea break">
							<div id="l6_35" class="player player1st">
								<input type="text" name="name35" id="name35" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[34]['name'];?>
">
								<input type="text" maxlength="3" name="nation35" id="nation35" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[34]['nation'];?>
">
								<input type="checkbox" name="w6_35" id="w6_35" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location35'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_35']){?>checked<?php }?>>
							<!--player35--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_18"), 0);?>

							<div id="l6_36" class="player player2nd">
								<input type="text" name="name36" id="name36" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[35]['name'];?>
">
								<input type="text" maxlength="3" name="nation36" id="nation36" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[35]['nation'];?>
">
								<input type="checkbox" name="w6_36" id="w6_36" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location36'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_36']){?>checked<?php }?>>
							<!--player36--></div>
							<!--best64 game18--></div>
						<!--best64 game17, 18---></div><!--
						--><div class="sectionLine">
							<div id="l5_17" class="line player1st">
								<input type="checkbox" name="w5_17" id="w5_17" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_17']){?>checked<?php }?>>
							</div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_9"), 0);?>

							<div id="l5_18" class="line player2nd">
								<input type="checkbox" name="w5_18" id="w5_18" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_18']){?>checked<?php }?>>
							</div>
						<!--best32 game9--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_19" class="inputArea break">
								<div id="l6_37" class="player player1st">
									<input type="text" name="name37" id="name37" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[36]['name'];?>
">
									<input type="text" maxlength="3" name="nation37" id="nation37" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[36]['nation'];?>
">
									<input type="checkbox" name="w6_37" id="w6_37" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location37'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_37']){?>checked<?php }?>>
								<!--player37--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_19"), 0);?>

								<div id="l6_38" class="player player2nd">
									<input type="text" name="name38" id="name38" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[37]['name'];?>
">
									<input type="text" maxlength="3" name="nation38" id="nation38" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[37]['nation'];?>
">
									<input type="checkbox" name="w6_38" id="w6_38" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location38'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_38']){?>checked<?php }?>>
								<!--player38--></div>
							<!--best64 game19--></div>
							<div id="sec6_20" class="inputArea break">
							<div id="l6_39" class="player player1st">
								<input type="text" name="name39" id="name39" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[38]['name'];?>
">
								<input type="text" maxlength="3" name="nation39" id="nation39" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[38]['nation'];?>
">
								<input type="checkbox" name="w6_39" id="w6_39" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location39'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_39']){?>checked<?php }?>>
							<!--player39--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_20"), 0);?>

							<div id="l6_40" class="player player2nd">
								<input type="text" name="name40" id="name40" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[39]['name'];?>
">
								<input type="text" maxlength="3" name="nation40" id="nation40" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[39]['nation'];?>
">
								<input type="checkbox" name="w6_40" id="w6_40" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location40'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_40']){?>checked<?php }?>>
							<!--player40--></div>
							<!--best64 game20--></div>
						<!--best64 game19, 20--></div><!--
						--><div class="sectionLine">
							<div id="l5_19" class="line player1st"><input type="checkbox" name="w5_19" id="w5_19" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_19']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_10"), 0);?>

							<div id="l5_20" class="line player2nd"><input type="checkbox" name="w5_20" id="w5_20" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_20']){?>checked<?php }?>></div>
						<!--best32 game10--></div>
						<!--break--></div>
					<!--best32 game9, 10--></div><!--
					--><div class="sectionLine secLine4">
						<div id="l4_9" class="line player1st"><input type="checkbox" name="w4_9" id="w4_9" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_9']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"4_5"), 0);?>

						<div id="l4_10" class="line player2nd"><input type="checkbox" name="w4_10" id="w4_10" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_10']){?>checked<?php }?>></div>
					<!--best16 game5--></div>
					<!--break--></div>
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_21" class="inputArea break">
							<div id="l6_41" class="player player1st">
								<input type="text" name="name41" id="name41" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[40]['name'];?>
">
								<input type="text" maxlength="3" name="nation41" id="nation41" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[40]['nation'];?>
">
								<input type="checkbox" name="w6_41" id="w6_41" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location41'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_41']){?>checked<?php }?>>
							<!--player41--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_21"), 0);?>

							<div id="l6_42" class="player player2nd">
								<input type="text" name="name42" id="name42" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[41]['name'];?>
">
								<input type="text" maxlength="3" name="nation42" id="nation42" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[41]['nation'];?>
">
								<input type="checkbox" name="w6_42" id="w6_42" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location42'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_42']){?>checked<?php }?>>
							<!--player42--></div>
							<!--best64 game21--></div>
							<div id="sec6_22" class="inputArea break">
							<div id="l6_43" class="player player1st">
								<input type="text" name="name43" id="name43" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[42]['name'];?>
">
								<input type="text" maxlength="3" name="nation43" id="nation43" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[42]['nation'];?>
">
								<input type="checkbox" name="w6_43" id="w6_43" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location43'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_43']){?>checked<?php }?>>
							<!--player43--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_22"), 0);?>

							<div id="l6_44" class="player player2nd">
								<input type="text" name="name44" id="name44" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[43]['name'];?>
">
								<input type="text" maxlength="3" name="nation44" id="nation44" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[43]['nation'];?>
">
								<input type="checkbox" name="w6_44" id="w6_44" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location44'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_44']){?>checked<?php }?>>
							<!--player44--></div>
							<!--best64 game22--></div>
						<!--best64 game21, 22---></div><!--
						--><div class="sectionLine">
							<div id="l5_21" class="line player1st"><input type="checkbox" name="w5_21" id="w5_21" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_21']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_11"), 0);?>

							<div id="l5_22" class="line player2nd"><input type="checkbox" name="w5_22" id="w5_22" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_22']){?>checked<?php }?>></div>
						<!--best32 game11--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_23" class="inputArea break">
								<div id="l6_45" class="player player1st">
									<input type="text" name="name45" id="name45" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[44]['name'];?>
">
									<input type="text" maxlength="3" name="nation45" id="nation45" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[44]['nation'];?>
">
									<input type="checkbox" name="w6_45" id="w6_45" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location45'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_45']){?>checked<?php }?>>
								<!--player45--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_23"), 0);?>

								<div id="l6_46" class="player player2nd">
									<input type="text" name="name46" id="name46" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[45]['name'];?>
">
									<input type="text" maxlength="3" name="nation46" id="nation46" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[45]['nation'];?>
">
									<input type="checkbox" name="w6_46" id="w6_46" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location46'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_46']){?>checked<?php }?>>
								<!--player46--></div>
							<!--best64 game23--></div>
							<div id="sec6_24" class="inputArea break">
							<div id="l6_47" class="player player1st">
								<input type="text" name="name47" id="name47" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[46]['name'];?>
">
								<input type="text" maxlength="3" name="nation47" id="nation47" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[46]['nation'];?>
">
								<input type="checkbox" name="w6_47" id="w6_47" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location47'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_47']){?>checked<?php }?>>
							<!--player47--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_24"), 0);?>

							<div id="l6_48" class="player player2nd">
								<input type="text" name="name48" id="name48" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[47]['name'];?>
">
								<input type="text" maxlength="3" name="nation48" id="nation48" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[47]['nation'];?>
">
								<input type="checkbox" name="w6_48" id="w6_48" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location48'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_48']){?>checked<?php }?>>
							<!--player48--></div>
							<!--best64 game24--></div>
						<!--best64 game23, 24--></div><!--
						--><div class="sectionLine">
							<div id="l5_23" class="line player1st"><input type="checkbox" name="w5_23" id="w5_23" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_23']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_12"), 0);?>

							<div id="l5_24" class="line player2nd"><input type="checkbox" name="w5_24" id="w5_24" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_24']){?>checked<?php }?>></div>
						<!--best32 game12--></div>
						<!--break--></div>
					<!--best32 game11, 12--></div><!--
					--><div class="sectionLine secLine4">
						<div id="l4_11" class="line player1st"><input type="checkbox" name="w4_11" id="w4_11" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_11']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"4_6"), 0);?>

						<div id="l4_12" class="line player2nd"><input type="checkbox" name="w4_12" id="w4_12" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_12']){?>checked<?php }?>></div>
					<!--best16 game6--></div>
					<!--break--></div>
				<!--best16 game5, 6--></div><!--
				--><div class="sectionLine secLine3">
					<div id="l3_5" class="line player1st"><input type="checkbox" name="w3_5" id="w3_5" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w3_5']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"3_3"), 0);?>

					<div id="l3_6" class="line player2nd"><input type="checkbox" name="w3_6" id="w3_6" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w3_6']){?>checked<?php }?>></div>
				<!--best8 game3--></div>
				<!--break--></div>
				<div class="break">
				<div class="section">
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_25" class="inputArea break">
							<div id="l6_49" class="player player1st">
								<input type="text" name="name49" id="name49" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[48]['name'];?>
">
								<input type="text" maxlength="3" name="nation49" id="nation49" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[48]['nation'];?>
">
								<input type="checkbox" name="w6_49" id="w6_49" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location49'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_49']){?>checked<?php }?>>
							<!--player49--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_25"), 0);?>

							<div id="l6_50" class="player player2nd">
								<input type="text" name="name50" id="name50" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[49]['name'];?>
">
								<input type="text" maxlength="3" name="nation50" id="nation50" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[49]['nation'];?>
">
								<input type="checkbox" name="w6_50" id="w6_50" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location50'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_50']){?>checked<?php }?>>
							<!--player50--></div>
							<!--best64 game25--></div>
							<div id="sec6_26" class="inputArea break">
							<div id="l6_51" class="player player1st">
								<input type="text" name="name51" id="name51" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[50]['name'];?>
">
								<input type="text" maxlength="3" name="nation51" id="nation51" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[50]['nation'];?>
">
								<input type="checkbox" name="w6_51" id="w6_51" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location51'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_51']){?>checked<?php }?>>
							<!--player51--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_26"), 0);?>

							<div id="l6_52" class="player player2nd">
								<input type="text" name="name52" id="name52" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[51]['name'];?>
">
								<input type="text" maxlength="3" name="nation52" id="nation52" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[51]['nation'];?>
">
								<input type="checkbox" name="w6_52" id="w6_52" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location52'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_52']){?>checked<?php }?>>
							<!--player52--></div>
							<!--best64 game26--></div>
						<!--best64 game25, 26---></div><!--
						--><div class="sectionLine">
							<div id="l5_25" class="line player1st"><input type="checkbox" name="w5_25" id="w5_25" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_25']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_13"), 0);?>

							<div id="l5_26" class="line player2nd"><input type="checkbox" name="w5_26" id="w5_26" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_26']){?>checked<?php }?>></div>
						<!--best32 game13--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_27" class="inputArea break">
								<div id="l6_53" class="player player1st">
									<input type="text" name="name53" id="name53" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[52]['name'];?>
">
									<input type="text" maxlength="3" name="nation53" id="nation53" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[52]['nation'];?>
">
									<input type="checkbox" name="w6_53" id="w6_53" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location53'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_53']){?>checked<?php }?>>
								<!--player53--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_27"), 0);?>

								<div id="l6_54" class="player player2nd">
									<input type="text" name="name54" id="name54" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[53]['name'];?>
">
									<input type="text" maxlength="3" name="nation54" id="nation54" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[53]['nation'];?>
">
									<input type="checkbox" name="w6_54" id="w6_54" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location54'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_54']){?>checked<?php }?>>
								<!--player54--></div>
							<!--best64 game27--></div>
							<div id="sec6_28" class="inputArea break">
							<div id="l6_55" class="player player1st">
								<input type="text" name="name55" id="name55" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[54]['name'];?>
">
								<input type="text" maxlength="3" name="nation55" id="nation55" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[54]['nation'];?>
">
								<input type="checkbox" name="w6_55" id="w6_55" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location55'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_55']){?>checked<?php }?>>
							<!--player55--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_28"), 0);?>

							<div id="l6_56" class="player player2nd">
								<input type="text" name="name56" id="name56" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[55]['name'];?>
">
								<input type="text" maxlength="3" name="nation56" id="nation56" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[55]['nation'];?>
">
								<input type="checkbox" name="w6_56" id="w6_56" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location56'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_56']){?>checked<?php }?>>
							<!--player56--></div>
							<!--best64 game28--></div>
						<!--best64 game27, 28--></div><!--
						--><div class="sectionLine">
							<div id="l5_27" class="line player1st"><input type="checkbox" name="w5_27" id="w5_27" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_27']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_14"), 0);?>

							<div id="l5_28" class="line player2nd"><input type="checkbox" name="w5_28" id="w5_28" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_28']){?>checked<?php }?>></div>
						<!--best32 game14--></div>
						<!--break--></div>
					<!--best32 game13, 14--></div><!--
					--><div class="sectionLine secLine4">
						<div id="l4_13" class="line player1st"><input type="checkbox" name="w4_13" id="w4_13" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_13']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"4_7"), 0);?>

						<div id="l4_14" class="line player2nd"><input type="checkbox" name="w4_14" id="w4_14" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_14']){?>checked<?php }?>></div>
					<!--best16 game7--></div>
					<!--break--></div>
					<div class="break">
					<div class="section">
						<div class="break">
						<div class="section">
							<div id="sec6_29" class="inputArea break">
							<div id="l6_57" class="player player1st">
								<input type="text" name="name57" id="name57" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[56]['name'];?>
">
								<input type="text" maxlength="3" name="nation57" id="nation57" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[56]['nation'];?>
">
								<input type="checkbox" name="w6_57" id="w6_57" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location57'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_57']){?>checked<?php }?>>
							<!--player57--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_29"), 0);?>

							<div id="l6_58" class="player player2nd">
								<input type="text" name="name58" id="name58" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[57]['name'];?>
">
								<input type="text" maxlength="3" name="nation58" id="nation58" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[57]['nation'];?>
">
								<input type="checkbox" name="w6_58" id="w6_58" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location58'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_58']){?>checked<?php }?>>
							<!--player58--></div>
							<!--best64 game29--></div>
							<div id="sec6_30" class="inputArea break">
							<div id="l6_59" class="player player1st">
								<input type="text" name="name59" id="name59" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[58]['name'];?>
">
								<input type="text" maxlength="3" name="nation59" id="nation59" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[58]['nation'];?>
">
								<input type="checkbox" name="w6_59" id="w6_59" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location59'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_59']){?>checked<?php }?>>
							<!--player59--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_30"), 0);?>

							<div id="l6_60" class="player player2nd">
								<input type="text" name="name60" id="name60" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[59]['name'];?>
">
								<input type="text" maxlength="3" name="nation60" id="nation60" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[59]['nation'];?>
">
								<input type="checkbox" name="w6_60" id="w6_60" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location60'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_60']){?>checked<?php }?>>
							<!--player60--></div>
							<!--best64 game14--></div>
						<!--best64 game29, 30---></div><!--
						--><div class="sectionLine">
							<div id="l5_29" class="line player1st"><input type="checkbox" name="w5_29" id="w5_29" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_29']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_15"), 0);?>

							<div id="l5_30" class="line player2nd"><input type="checkbox" name="w5_30" id="w5_30" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_30']){?>checked<?php }?>></div>
						<!--best32 game15--></div>
						<!--break--></div>
						<div class="break">
						<div class="section">
							<div id="sec6_31" class="inputArea break">
							<div id="l6_61" class="player player1st">
								<input type="text" name="name61" id="name61" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[60]['name'];?>
">
								<input type="text" maxlength="3" name="nation61" id="nation61" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[60]['nation'];?>
">
								<input type="checkbox" name="w6_61" id="w6_61" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location61'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_61']){?>checked<?php }?>>
							<!--player61--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_31"), 0);?>

							<div id="l6_62" class="player player2nd">
								<input type="text" name="name62" id="name62" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[61]['name'];?>
">
								<input type="text" maxlength="3" name="nation62" id="nation62" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[61]['nation'];?>
">
								<input type="checkbox" name="w6_62" id="w6_62" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location62'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_62']){?>checked<?php }?>>
							<!--player62--></div>
							<!--best64 game31--></div>
							<div id="sec6_32" class="inputArea break">
							<div id="l6_63" class="player player1st">
								<input type="text" name="name63" id="name63" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[62]['name'];?>
">
								<input type="text" maxlength="3" name="nation63" id="nation63" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[62]['nation'];?>
">
								<input type="checkbox" name="w6_63" id="w6_63" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location63'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_63']){?>checked<?php }?>>
							<!--player63--></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"6_32"), 0);?>

							<div id="l6_64" class="player player2nd">
								<input type="text" name="name64" id="name64" class="name" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[63]['name'];?>
">
								<input type="text" maxlength="3" name="nation64" id="nation64" class="nation" value="<?php echo $_smarty_tpl->tpl_vars['p']->value[63]['nation'];?>
">
								<input type="checkbox" name="w6_64" id="w6_64" class="winner" value="<?php echo $_smarty_tpl->tpl_vars['t']->value['location64'];?>
" <?php if ($_smarty_tpl->tpl_vars['t']->value['w6_64']){?>checked<?php }?>>
							<!--player64--></div>
							<!--best64 game32--></div>
						<!--best64 game31, 32--></div><!--
						--><div class="sectionLine">
							<div id="l5_31" class="line player1st"><input type="checkbox" name="w5_31" id="w5_31" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_31']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"5_16"), 0);?>

							<div id="l5_32" class="line player2nd"><input type="checkbox" name="w5_32" id="w5_32" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w5_32']){?>checked<?php }?>></div>
						<!--best32 game16--></div>
						<!--break--></div>
					<!--best32 game15, 16--></div><!--
					--><div class="sectionLine secLine4">
						<div id="l4_15" class="line player1st"><input type="checkbox" name="w4_15" id="w4_15" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_15']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"4_8"), 0);?>

						<div id="l4_16" class="line player2nd"><input type="checkbox" name="w4_16" id="w4_16" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w4_16']){?>checked<?php }?>></div>
					<!--best16 game8--></div>
					<!--break--></div>
				<!--best16 game7, 8--></div><!--
				--><div class="sectionLine secLine3">
					<div id="l3_7" class="line player1st"><input type="checkbox" name="w3_7" id="w3_7" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w3_7']){?>checked<?php }?>></div>
							<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"3_4"), 0);?>

					<div id="l3_8" class="line player2nd"><input type="checkbox" name="w3_8" id="w3_8" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w3_8']){?>checked<?php }?>></div>
				<!--best8 game4--></div>
				<!--break--></div>
			<!--best8 game3, 4--></div><!--
				--><div class="sectionLine secLine2">
					<div id="l2_3" class="line player1st"><input type="checkbox" name="w2_3" id="w2_3" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w2_3']){?>checked<?php }?>></div>
					<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"2_2"), 0);?>

					<div id="l2_4" class="line player2nd"><input type="checkbox" name="w2_4" id="w2_4" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w2_4']){?>checked<?php }?>></div>
				<!--best4 game2--></div>
				<!--break--></div>
		<!--best4 game1, 2--></div><!--
		--><div class="sectionLine secLine1">
			<div id="l1_1" class="line player1st"><input type="checkbox" name="w1_1" id="w1_1" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w1_1']){?>checked<?php }?>></div>
			<?php echo $_smarty_tpl->getSubTemplate ("tournament/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('num'=>"1_1"), 0);?>

			<div id="l1_2" class="line player2nd"><input type="checkbox" name="w1_2" id="w1_2" class="winner" value="" <?php if ($_smarty_tpl->tpl_vars['t']->value['w1_2']){?>checked<?php }?>></div>
		<!--final--></div><!--
		--><div class="sectionWinner">
			<div id="winnerName">Winner</div>
			<input type="hidden" name="w0_1" id="w0_1" class="winner" value="">
		<!--sectionWinner--></div>
	<!--container--></div>

	<div id="info">
		<div><ul id="courtInfo">
			<li id="ci1">
				<p class="courtGreen">Piste Green1</p>
				<span>Left Player：</span>
				<span id="ci1NameL" class="name"></span>
				<span id="ci1NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci1NameR" class="name"></span>
				<span id="ci1NationR" class="nation"></span><br>
			</li>
			<li id="ci5">
				<p class="courtGreen">Piste Green2</p>
				<span>Left Player：</span>
				<span id="ci5NameL" class="name"></span>
				<span id="ci5NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci5NameR" class="name"></span>
				<span id="ci5NationR" class="nation"></span><br>
			</li>
			<li id="ci2">
				<p class="courtYellow">Piste Yellow1</p>
				<span>Left Player：</span>
				<span id="ci2NameL" class="name"></span>
				<span id="ci2NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci2NameR" class="name"></span>
				<span id="ci2NationR" class="nation"></span><br>
			</li>
			<li id="ci6">
				<p class="courtYellow">Piste Yellow2</p>
				<span>Left Player：</span>
				<span id="ci6NameL" class="name"></span>
				<span id="ci6NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci6NameR" class="name"></span>
				<span id="ci6NationR" class="nation"></span><br>
			</li>
			<li id="ci3">
				<p class="courtRed">Piste Red1</p>
				<span>Left Player：</span>
				<span id="ci3NameL" class="name"></span>
				<span id="ci3NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci3NameR" class="name"></span>
				<span id="ci3NationR" class="nation"></span><br>
			</li>
			<li id="ci7">
				<p class="courtRed">Piste Red2</p>
				<span>Left Player：</span>
				<span id="ci7NameL" class="name"></span>
				<span id="ci7NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci7NameR" class="name"></span>
				<span id="ci7NationR" class="nation"></span><br>
			</li>
			<li id="ci4">
				<p class="courtBlue">Piste Blue1</p>
				<span>Left Player：</span>
				<span id="ci4NameL" class="name"></span>
				<span id="ci4NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci4NameR" class="name"></span>
				<span id="ci4NationR" class="nation"></span><br>
			</li>
			<li id="ci8">
				<p class="courtBlue">Piste Blue2</p>
				<span>Left Player：</span>
				<span id="ci8NameL" class="name"></span>
				<span id="ci8NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci8NameR" class="name"></span>
				<span id="ci8NationR" class="nation"></span><br>
			</li>
			<li id="ci9">
				<p>Piste Podium</p>
				<span>Left Player：</span>
				<span id="ci9NameL" class="name"></span>
				<span id="ci9NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci9NameR" class="name"></span>
				<span id="ci9NationR" class="nation"></span><br>
			</li>
			<li id="ci10">
				<p>Abema</p>
				<span>Left Player：</span>
				<span id="ci10NameL" class="name"></span>
				<span id="ci10NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci10NameR" class="name"></span>
				<span id="ci10NationR" class="nation"></span><br>
			</li>
			<li id="ci11">
				<p>Kokusai</p>
				<span>Left Player：</span>
				<span id="ci11NameL" class="name"></span>
				<span id="ci11NationL" class="nation"></span><br>
				<div class="vs"></div>
				<span>Right Player：</span>
				<span id="ci11NameR" class="name"></span>
				<span id="ci11NationR" class="nation"></span><br>
			</li>
		</ul></div>
		<a id="infoClose"></a>
	<!--info--></div>
<?php  $_smarty_tpl->tpl_vars['_'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['_']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['p']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['_']->key => $_smarty_tpl->tpl_vars['_']->value){
$_smarty_tpl->tpl_vars['_']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['_']->key;
?>
	<input type="hidden" name="id<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
" value="<?php echo $_smarty_tpl->tpl_vars['_']->value['id'];?>
">
<?php } ?>
	<div id="btnWrap"><input type="submit" id="btnSend" value="更新 / Send"></div>
</form>

<style>
#log {
	display: none;
	position: fixed;
	left: 10px;
	bottom: 80px;
	z-index: 1000;
	padding: 10px;
	background: rgba(255,255,255,.7);
	line-height: 2;
}
</style>
<div id="log"></div>

<script src="/fencing/js/jquery-2.2.3.min.js"></script>
<script src="/fencing/js/admin.js"></script>
</body>
</html>
<?php }} ?>