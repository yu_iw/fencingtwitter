<?php
class T_tournament extends ModelBase{
	private $SESSION_ADMIN_LOGIN = 'SESSION_ADMIN_LOGIN';
	private $SESSION_ADMIN_ENTRY = 'SESSION_ADMIN_ENTRY';
	private $SESSION_ADMIN_ENTRYS = 'SESSION_ADMIN_ENTRYS';
    private $SESSION_ADMIN_CATEGORY = 'SESSION_ADMIN_CATEGORY';

    public function getLoginSession(){
    	//session開始
    	session_start();
//     	echo session_id();
    	return  $_SESSION[$this->SESSION_ADMIN_LOGIN];
    }
    public function removeLoginSession(){
    	//session開始
    	session_start();
//     	echo session_id();
    	session_destroy();
    }
    public function getPlayer(){
        $sql = "SELECT * FROM t_player order by id";
        $rows = $this->query($sql);
        return $rows;
    }
    public function getTournament(){
        $sql = "SELECT * FROM t_tournament order by id";
        $rows = $this->query($sql);
        return $rows;
    }
    public function getPlayerByTournamentId($tid){
        $sql = "SELECT * FROM t_player where tournament_id = :tid order by id";
        $params = array('tid' => $tid);
        $rows = $this->query($sql,$params);
        return $rows;
    }
    public function getTournamentById($id){
        $sql = "SELECT * FROM t_tournament where id = :id";
        $params = array('id' => $id);
        $rows = $this->query($sql,$params);
        return $rows[0];
    }
    public function insertNewTournament($tournament_name){
        $sql = "insert into t_tournament (tournament_name) values (:tournament_name);";
        $params = array('tournament_name' => $tournament_name);
        $rows = $this->query($sql,$params);
        return $rows;
    }
    public function insertInitPlayer($tournament_id,$name,$nation){
        $sql = "insert into t_player (tournament_id,name,nation) values (:tournament_id,:name,:nation);";
        $params = array(
			'tournament_id' => $tournament_id,
			'name' => $name,
			'nation' => $nation,
		);
        $rows = $this->query($sql,$params);
        return $rows;
    }
    public function deleteTournamentById($id){
        $sql = "delete FROM t_tournament where id = :id";
        $params = array('id' => $id);
        $rows = $this->query($sql,$params);
        return $rows;
    }
    public function updatePlayer($id,$name,$nation) {
        $sql = "UPDATE t_player
				SET name = :name, nation = :nation, updated_at = now()
				WHERE id = :id";
        $params = array(
			'id' => $id,
			'name' => $name,
			'nation' => $nation,
		);
        $rows = $this->query($sql,$params);
        return $rows;
    }
    public function updateTournament($id,$key,$value) {
        $sql = "UPDATE t_tournament
				SET ".$key." = :value
				WHERE id = :id";
        $params = array(
			'id' => $id,
			'value' => $value,
		);
		//print_r($params);
        $rows = $this->query($sql,$params);
        return $rows;
    }
    public function maxIdByTable($table)
    {
        $sql = sprintf("SELECT max(id) FROM ".$table.";");
        $rows = $this->query($sql);
        return $rows;
    }
}
?>