<?php
ini_set("default_charset","UTF-8");
ini_set("display_errors", "On");

require_once LIB_PATH.'/mvc/Validation.php';
require_once LIB_PATH.'/mvc/Functions.php';
require_once ROOT_PATH.'/models/T_tournament.php';
class TournamentController extends ControllerBase{

	protected function listAction(){
		//echo "<br><br><br><br><br><br><br>";//仮空白
		$tt = new T_tournament();
		$t = $tt->getTournament();/*
		print_r("<pre>");
		print_r($t);
		print_r("</pre>");*/
		$this->view->assign('t',$t);
	}
	protected function createAction(){
		echo "tournament作成";
		$tt = new T_tournament();
		$n = $tt->insertNewTournament('新しいトーナメント');

		header("Location: ../../tournament/list/");
	}

	protected function deleteAction(){
		echo "tournament削除";
		$q = $this->request->getQuery();
			$tt = new T_tournament();
			$n = $tt->deleteTournamentById($q['tid']);

			header("Location: ../../tournament/list/");
	}

	protected function playerAction(){
		echo "tournament削除";
		$q = $this->request->getQuery();
	}

	protected function bracketAction(){
		//echo "<br><br><br><br><br><br><br>";//仮空白
		$tt = new T_tournament();

		$q= $this->request->getQuery();
		if(!empty($_POST)){
			ini_set('xdebug.var_display_max_children', -1);
ini_set('xdebug.var_display_max_data', -1);
ini_set('xdebug.var_display_max_depth', -1);
			//var_dump($_POST);
			if($s = $this->request->getPost()){
	//			print_r("<pre>");
	//			print_r($s);
	//			print_r("</pre>");
				foreach($s as $key => $val){
					if(strpos($key,'id') !== false){
						$key = preg_replace('/[^0-9]/', '', $key);
						/*
						echo "<br>".$key."<br>";
						echo $s["id".$key]."<br>";
						echo 'test';
						echo $s["name6_".$key]."<br>";
						echo $s["nation6_".$key]."<br>";
						*/
						$result = $tt->updatePlayer($s["id".$key],$s["name6_".$key],$s["nation6_".$key]);
						//print_r($result);
					}else if(strpos($key,'l') !== false || strpos($key,'f') !== false ||strpos($key,'w') !== false || strpos($key,'s') !== false || strpos($key,'c') !== false || strpos($key,'tournament_name') !== false){
						//echo "".$key."＝".$val."<br>";

						$result = $tt->updateTournament($q['tid'],$key,$val);
						//print_r($result);
					}
				}
			}
		}
			if($q['tid']){
				$pp = $tt->getPlayerByTournamentId($q['tid']);
				$t = $tt->getTournamentById($q['tid']);
			}
			if(empty($pp)){
	//			echo "t_player未設定";
				if($s = $this->request->getPost()){
	//				echo "トーナメント".$q['tid'];
	//				$maxPId = $tt->maxIdByTable('t_player');
	//				$nextPId = $maxPId['0']['max'] + 1;
	//				echo $nextPId;
					for($i = 1; $i <= 64; $i++){
						//echo $i."<br>";
						$result = $tt->insertInitPlayer($q['tid'],$s["name6_".$i],$s["nation6_".$i]);
					}
				}
			}

			if($t['id']){
				$w = array();
				$s = array();
				foreach($t as $key => $val){
					if(strpos($key,'w') !== false){
						$w += array(
							$key => 0
						);
					}
					if(strpos($key,'s') !== false){
						$s += array(
							$key => 0
						);
					}
					if(strpos($key,'f') !== false){
						if($t[$key] == null){
							$t[$key] = "#ffffcc";
						}
					}
				}
				foreach($pp as $key => $val){ //プレイヤーID初期配置
						$lk = "location6_".($key+1);
						/*$l += array(
							$lk => $pp[$key]['id']
						);*/
						if(!$t[$lk]){
							//echo 'location'.($key);
							$t[$lk] = $pp[$key]['id'];
						}
				}
	//			print_r("<pre>");
				//print_r($t);
				//print_r($pp);
	//			print_r("</pre>");
				$this->view->assign('pp',$pp);
				$this->view->assign('t',$t);
				$this->view->assign('w',$w);
				$this->view->assign('s',$s);
				$this->view->assign('q',$q);
				$cArr = array(0 => 'Select Piste', 1 => 'Piste Green1', 2 => 'Piste Yellow1', 3 => 'Piste Red1', 4 => 'Piste Blue1', 5 => 'Piste Green2', 6 => 'Piste Yellow2', 7 => 'Piste Red2', 8 => 'Piste Blue2', 9 => 'Piste Podium', 10 => 'Abema', 11 => 'Kokusai');
				$this->view->assign('cArr',$cArr);
			}else{
				header("Location: ../../tournament/list/");
				exit;
			}
	}

	protected function reportAction(){
	}
	protected function reportViewAction(){
	}
	protected function receiveAction(){
		// file_get_contents(): ファイルの内容を全て文字列に読み込む
		// php://input POSTの生データの読み込みを許可
		$obj = file_get_contents('php://input');
		$data = json_decode($obj,true);
		$json = fopen('../data/json/report.json', 'w+b');
		fwrite($json, json_encode($data, JSON_UNESCAPED_UNICODE));
		fclose($json);
	}
	/******************
	 *  共通系
	******************/
	public function logoutAction(){
		$x = new T_tournament();
		$x->removeLoginSession();
		//ログインページへリダイレクト
		header("Location: ../../x_nimda/login/");
		exit;
	}
	protected function preAction(){
		//session_start();
		//ログインチェック
		//$x = new T_tournament();
		//$admin = $x->getLoginSession();
		/*if(!isset($admin)){
			//セッションが無いためログインページへリダイレクト
			header("Location: ../../x_nimda/login/");
			exit;
    	}*/
		$this->view->assign('encode', 'UTF-8');
	}


}
?>
