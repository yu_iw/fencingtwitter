var self = {
		name: 'Head Quarters'
	};

// エラー処理
ws.onerror = function(e){
	$('#chat-area').empty()
	.addClass('alert alert-error')
	.append('<button type="button" class="close" data-dismiss="alert">×</button>',
		$('<i/>').addClass('icon-warning-sign'),
		'サーバに接続できませんでした。'
	);
}
 
// WebSocketサーバ接続イベント
ws.onopen = function() {
	$('#textbox').focus();
	// 入室情報を文字列に変換して送信
	ws.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};


/* ----------------------------------------
	ダミーデータ送信
---------------------------------------- */

var timer,
	timerTime = 500,
	nationArr = ['JPN', 'USA', 'ENG', 'FRA', 'GER', 'ITA', 'ESP', 'CHN', 'KOR'],
	nm1 = 'player ',
	nm2 = 'player ',
	nn1 = 0,
	nn2 = 0,
	priArr1 = [0, 0, 1],
	priArr2 = [0, 1, 0],
	pri = 0,
	invalid1 = 0,
	invalid2 = 0,
	hit1 = 0,
	hit2 = 0,
	scoreArr1 = [0, 0, 0, 1, 1],
	scoreArr2 = [0, 0, 1, 0, 1],
	sc = 0,
	sc1 = 0,
	sc2 = 0,
	statusArr = ['', 'pause'],
	st = 0,
	timeRemain = 3*60*1000;//3分 ミリ秒;

function restart(){
	nm1 = 'player '+Math.ceil(Math.random()*20);
	nm2 = 'player '+Math.ceil(Math.random()*20);
	nn1 = Math.floor(Math.random()*nationArr.length);
	nn2 = Math.floor(Math.random()*nationArr.length);
	pri = 0;
	sc1 = 0;
	sc2 = 0;
	timeRemain = 3*60*1000;//3分 ミリ秒;
	sendToServer();
}

function sendToServer(){
	var sendTime =(Math.floor(timeRemain/1000%60)<10)? Math.floor(timeRemain/1000/60)+':0'+Math.floor(timeRemain/1000%60): Math.floor(timeRemain/1000/60)+':'+Math.floor(timeRemain/1000%60);
	ws.send(JSON.stringify({
		type: 'data',
		user: self.name,
/*
		nameL: nm1,
		nameR: nm2,
		nationL: nationArr[nn1],
		nationR: nationArr[nn2],
*/
		priorityL: priArr1[pri],
		priorityR: priArr2[pri],
		invalidL: invalid1,
		invalidR: invalid2,
		hitL: hit1,
		hitR: hit2,
		scoreL: sc1,
		scoreR: sc2,
		status: statusArr[st],
		timeRemain: sendTime
	}));

	$('#log').prepend(
		'<li>'+
		'player '+nm1+
		' Nation:'+nationArr[nn1]+
		' Priority:'+priArr1[pri]+
		' Invalid:'+invalid1+
		' Hit:'+hit1+
		' Score:'+sc1+
		' ／ player '+nm2+
		' Nation:'+ nationArr[nn2]+
		' Priority:'+priArr2[pri]+
		' Invalid:'+invalid2+
		' Hit:'+hit2+
		' Score:'+sc2+
		' ／ Status:'+statusArr[st]+
		' ／ Time:'+sendTime+
		'</li>'
	);
	if(timeRemain<=0){
		clearInterval(timer);
		$('#log').prepend('Time is up.');
	}
}

function sendLoop(){
	sc = Math.floor(Math.random()*scoreArr1.length);
	st = Math.floor(Math.random()*statusArr.length);
	pri = Math.floor(Math.random()*priArr1.length);
	invalid1 = Math.floor(Math.random()*2);
	invalid2 = Math.floor(Math.random()*2);
	hit1 = Math.floor(Math.random()*3);
	hit2 = Math.floor(Math.random()*3);
	sc1 += scoreArr1[sc];
	sc2 += scoreArr2[sc];
	if(sc1>99) sc1 = 0;
	if(sc2>99) sc2 = 0;
	timeRemain -= timerTime;
	if(timeRemain<0) timeRemain = 0;
	sendToServer();
}

btnStart.onclick = function(event) {
	restart();
	timer = setInterval(sendLoop, 500);
};
btnStop.onclick = function(event) {
	clearInterval(timer);
};
btnRestart.onclick = function(event) {
	sendLoop();
	timer = setInterval(sendLoop, 500);
};
btnReset.onclick = function(){
	restart();
};


/* ----------------------------------------
	ブラウザ終了イベント
---------------------------------------- */

window.onbeforeunload = function () {
	ws.send(JSON.stringify({
		type: 'defect',
		user: self.name,
	}));
};