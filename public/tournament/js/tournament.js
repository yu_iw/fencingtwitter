/* ++++++++++++++++++++++++++++++++++++++++

	Display

++++++++++++++++++++++++++++++++++++++++ */

$(function(){
	//BOS
	
	/* ----------------------------------------
		Global Variables
	---------------------------------------- */
	
	var $window = $(window),
		imgPath = 'img/',
		cur = {
			nameL: '',
			nameR: '',
			nationL: '',
			nationR: '',
			scoreL: -1,
			scoreR: -1,
			yellowL: -1,
			yellowR: -1,
			redL: -1,
			redR: -1,
			status: '',
			timeRemain: '0:00',
			Round: ''
		};
	
	//$('#statusWrap>div, #timeWrap>div').width($(window).width()-$('.score').eq(0).width()*2-6*2);
	
	
	/* ----------------------------------------
		fixfont
	---------------------------------------- */
	
	var ff = {
			$el: $('.fixfont'),
			loaded: false,
			num: 0,
			len: $('.fixfont').length,
			fnc: function($el){
				$el.each(function(){
					var $this = $(this),
						box ={
							wdt: $this.width(),
							hgt: $this.height(),
						},
						$text = $this.children(),
						text = {
							wdt: $text.width(),
							hgt: $text.height()
						},
						fz = parseInt($this.css('font-size')),
						n = 0;
	if(window.console && typeof window.console.log==='function') console.log(box.hgt+'font-size:'+fz+' > '+text.hgt);
					while(text.hgt<=box.hgt && text.wdt<=box.wdt && n<200){
						fz ++;
						$this.css({fontSize: fz+'px'});
						text.wdt = $text.width();
						text.hgt = $text.height();
						n++;
					}
					$this.css({fontSize: fz-1+'px'});
					ff.num++;
					if(ff.num>=ff.len){
						if(!ff.loaded){
							ff.loaded = true;
							ff.$el.find('>div>div:not(.stable)').text('');
						}
						ff.$el.css({opacity: 1});
					}
				});
			}
		};
	
	$window.load(function(){
		ff.fnc(ff.$el);
	});
	
	
	/* ----------------------------------------
		Config
	---------------------------------------- */
	
	if(typeof config!=='undefined'){
	if(window.console && typeof window.console.log==='function') console.log(config);
		$('#nameL').addClass('stable').text(config.name.left);
		$('#nameR').addClass('stable').text(config.name.right);
	}
	
	
	/* ----------------------------------------
		Web Socket
	---------------------------------------- */
	
	// エラー処理
	ws.onerror = function(e){
		$('#log').text('サーバに接続できませんでした。');
	}
	
	// WebSocketサーバ接続イベント
	ws.onopen = function(){
		// 入室情報を文字列に変換して送信
		ws.send(JSON.stringify({
			type: 'join',
			user: self.name
		}));
	};
	
	// メッセージ受信イベントを処理
	ws.onmessage = function(event){
		var data = JSON.parse(event.data);
	//debug
		if(data.type==='join'){
			$('#log p').text(data.user+'が入室しました。');
		}else if(data.type==='defect'){
			$('#log p').text(data.user+'が退室しました。');
		}else if(data.type==='seiko'){
			if(data.toggle==='on'){
				$('.seiko_logo').show();
			}else{
				$('.seiko_logo').hide();
			}
		}else if(data.type==='fie'){
			if(data.toggle==='on'){
				$('.fie_logo').show();
			}else{
				$('.fie_logo').hide();
			}
	/*
	debug*/
		}else if(data.type==='admin'){
		//選手データ受信
			if(data.data){
				$params = para_get(data.data);
			}
			console.log(data.data)
			console.log(data.game)
			console.log($params)
			var arg = new Object;
			var pair=location.search.substring(1).split('&');
			for(var i=0;pair[i];i++) {
				var kv = pair[i].split('=');
				arg[kv[0]]=kv[1];
			}
			console.log(arg);
			var column = +arg.column;//数値化
			var row = +arg.row;
			var match = +arg.match;
			$('.inputArea,.sectionLine').removeClass('on');
			$('.player,.line').removeClass('win');
			for(var key in data.game) {
				if(data.game.hasOwnProperty(key)) {
					var val = data.game[key];
					var gamePos = key.match(/[0-9]+\.?[0-9]*/g);
					console.log(gamePos);
					console.log('row:'+row);
					gamePosInit0 = gamePos[0];
					gamePos[0] = gamePos[0]-column+4-(3-match);
					gamePos[1] = gamePos[1]-(row-1)/(2**(7-gamePosInit0-(3-match)));
					console.log(gamePos);
					$('#sec'+gamePos[0]+'_'+gamePos[1]).addClass('on');
				}
			}
			if(match == 3){
				var row1 = ((row-1)/1)+1;//そのまま
				var row2 = ((row-1)/2)+1;
				var row3 = ((row-1)/4)+1;
				var row4 = ((row-1)/8)+1;
				//console.log(row2);
				for (var i = 0; i < 2**match; i++) {
					var pos = column + '_' + (row1+i);
					//console.log(pos);
					$('.name4_'+(i+1)).text($params['name' + pos]);
					$('.nation4_'+(i+1)).text($params['nation' + pos])
					if($params['w' + pos] != 0){
						$('#l4_'+(i+1)).addClass('win').siblings().removeClass('win');
					}
				}
				for (var i = 0; i < (2**(match-1)); i++) {
					var pos = (column-1) + '_' + (row2+i);
					//console.log(pos);
					$('.name3_'+(i+1)).text($params['name' + pos]);
					$('.nation3_'+(i+1)).text($params['nation' + pos])
					if($params['w' + pos] != 0){
						$('#l3_'+(i+1)).addClass('win').siblings().removeClass('win');
					}
				}
				for (var i = 0; i < (2**(match-2)); i++) {
					var pos = (column-2) + '_' + (row3+i);
					//console.log(pos);
					$('.name2_'+(i+1)).text($params['name' + pos]);
					$('.nation2_'+(i+1)).text($params['nation' + pos])
					if($params['w' + pos] != 0){
						$('#l2_'+(i+1)).addClass('win').siblings().removeClass('win');
					}
				}
				var pos =  (column-3) + '_' + (row4);
				$('.name1_1').text($params['name' + pos])
				$('.nation1_1').text($params['nation' + pos])
			}
			if(match == 2){
				var row1 = ((row-1)/1)+1;//そのまま
				var row2 = ((row-1)/2)+1;
				var row3 = ((row-1)/4)+1;
				//console.log(row2);
				for (var i = 0; i < 2**match; i++) {
					var pos = column + '_' + (row1+i);
					//console.log(pos);
					$('.name3_'+(i+1)).text($params['name' + pos]);
					$('.nation3_'+(i+1)).text($params['nation' + pos])
					if($params['w' + pos] != 0){
						$('#l3_'+(i+1)).addClass('win').siblings().removeClass('win');
					}
				}
				for (var i = 0; i < (2**(match-1)); i++) {
					var pos = (column-1) + '_' + (row2+i);
					//console.log(pos);
					$('.name2_'+(i+1)).text($params['name' + pos]);
					$('.nation2_'+(i+1)).text($params['nation' + pos])
					if($params['w' + pos] != 0){
						$('#l2_'+(i+1)).addClass('win').siblings().removeClass('win');
					}
				}
				var pos =  (column-2) + '_' + (row3);
				$('.name1_1').text($params['name' + pos])
				$('.nation1_1').text($params['nation' + pos])
			}
			if(match == 1){
				var row1 = ((row-1)/1)+1;//そのまま
				var row2 = ((row-1)/2)+1;
				//console.log(row2);
				for (var i = 0; i < 2**match; i++) {
					var pos = column + '_' + (row1+i);
					console.log(pos);
					$('.name2_'+(i+1)).text($params['name' + pos]);
					$('.nation2_'+(i+1)).text($params['nation' + pos])
					if($params['w' + pos] != 0){
						$('#l2_'+(i+1)).addClass('win').siblings().removeClass('win');
					}
				}
				var pos =  (column-1) + '_' + (row2);
				$('.name1_1').text($params['name' + pos])
				$('.nation1_1').text($params['nation' + pos])
				/*
				$('.name1_1').text($params.name3_1)
				$('.nation1_1').text($params.nation3_1)
				$('.name3_1').text($params.name5_1)
				$('.name3_2').text($params.name5_2)
				$('.name3_3').text($params.name5_3)
				$('.name3_4').text($params.name5_4)
				$('.nation3_1').text($params.nation5_1)
				$('.nation3_2').text($params.nation5_2)
				$('.nation3_3').text($params.nation5_3)
				$('.nation3_4').text($params.nation5_4)
				$('.name2_1').text($params.name4_1)
				$('.name2_2').text($params.name4_2)
				$('.nation2_1').text($params.nation4_1)
				$('.nation2_2').text($params.nation4_2)
				$('.name1_1').text($params.name3_1)
				$('.nation1_1').text($params.nation3_1)
				*/
			}
			ff.fnc($('#name .fixfont'));
	//debug
		$('#logPlayerL').text(data.nameL);
		$('#logNationL').text(data.nationL);
		$('#logPlayerR').text(data.nameR);
		$('#logNationR').text(data.nationR);
	/*
	debug*/
		
			
	//debug
		$('#logDate').text(data.time);
		$('#logScoreL').text(data.scoreL);
		$('#logYellowL').text(data.yellowL);
		$('#logRedL').text(data.redL);
		$('#logScoreR').text(data.scoreR);
		$('#logYellowR').text(data.yellowR);
		$('#logRedR').text(data.redR);
	//	$('#logStatus').text(data.status);
		$('#logTime').text(data.timeRemain);
		$('#logRound').text(data.round);
	/*
	debug*/
		}else if (data.type==='control'){
			$('#log p').text('キー入力');
		}else{
			$('#log p').text('不正なメッセージを受信しました');
		}
	};
	
	/*
	// 操作イベント
	document.onkeydown = function(event){
		// 単一キーを全体に送信
		ws.send(JSON.stringify({
			type: 'control',
			user: self.name,
			keyCode: event.keyCode
		}));
	};
	*/
	
	// ブラウザ終了イベント
	window.onbeforeunload = function(){
		ws.send(JSON.stringify({
			type: 'defect',
			user: self.name,
		}));
	};
	
	//EOS
	});
	
	
	/* ----------------------------------------
		Idling Mode
	---------------------------------------- */
	
	document.onkeydown = function(e){
	console.log(e);
		if(e.key=='Enter') $('#idling').toggleClass('on');
	};

function para_get(data)
{
	// URLパラメータを"&"で分離する
	var params = data.substr(1).split('&');
	// パラメータ連想配列エリア初期化
	var para = [];
	// キーエリア初期化
	var key = null;
	for(var i = 0 ; i < params.length ; i++)
	{
		// "&"で分離したパラメータを"="で再分離
		key = params[i].split("=");
		// パラメータをURIデコードして連想配列でセット
		para[key[0]] = decodeURI(key[1]);
	}
	// 連想配列パラメータを返す
	return (para);
}