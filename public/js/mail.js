$(document).ready(function () {
    $("#contact-form input").floatlabel();
});
function mailSend(fromId, toId){
    console.log('test');
    var title = $("#mail-title").val();
    var body = $("#mail-body").val();
    jQuery.ajax({
        cache:false,
        type: 'POST',
        data: {
                "f": fromId,
                "t": toId,
                "title": title,
                "body": body
            },
        url: '/my/mailSend/',
        success: function(rs){
            console.log("return: " + rs);
            if(jQuery.trim(rs) == "success"){
                $('.popup-detail-area').empty();
                $('.popup-detail-area').load('/my/mailSendSuccess/');
            }
        }
    });
};