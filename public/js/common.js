/* ++++++++++++++++++++++++++++++++++++++++

	Common Scripts

++++++++++++++++++++++++++++++++++++++++ */

/* ----------------------------------------
	UA
---------------------------------------- */

var ua = navigator.userAgent.toLowerCase(),
	isSP = /android.+mobile/.test(ua) || /i(phone|pod)/.test(ua),
	isLtIE9 = ua.replace(/.+msie (\d+)\..+/, '$1')<9;

$(function() {
	// $('#slider li.active img.pic').lazyload({
	// 	event: "lazyload",
	// 	effect : "fadeIn",	//表示の効果
	// 	effectspeed: 500,	//効果時間
	// });
});
	
$(window).load(function(){
	$(window).resize();
//	BOS


/* ----------------------------------------
    gr slider
---------------------------------------- */

$('#slider').grSlider({
	type: 'slide',// スライド形式
	dsplNum: 1,// 表示スライド数
	moveAll: false,// 表示分まとめてスライド
	auto: false,// 自動再生
	loop: true,// 繰り返し
	chgTime: 3000,// 自動再生時間
	slideTime: 500,// スライド効果時間
	slideEase: 'easeOutCubic',// スライド効果
	direction: 'left',// スライド方向
	paging: true,// 前後ボタン有無
	navReverse: false,// 前後ボタンを逆方向に
	pagingClass: false,// 前後ボタンのスタイル変更 'クラス名, 除数, 余り'
	nav: false,// スライドナビ有無
	resize: false// リサイズ有無
});
// $('#slider li.active img.pic').lazyload({
// 	event: "lazyload",
// 	effect : "fadeIn",	//表示の効果
// 	effectspeed: 500,	//効果時間
// });


/* ----------------------------------------
	SP menu
---------------------------------------- */

$("#toggle a, #menu a").click(function(){
$("#menu").slideToggle();
return false;
});

/* ----------------------------------------
	参加店舗一覧
---------------------------------------- */

$('#area li').click(function(){
	var index = $('#area li').index(this);
	$('#area li').removeClass('on').eq(index).addClass('on');
	$('#shop li').removeClass('on').eq(index).addClass('on');
});


/* ----------------------------------------
    HashTag Copy
---------------------------------------- */

/*var clipboard = new Clipboard('.btn_instagram');
clipboard.on('success', function(e) {
	alert('コピーしました。');
});*/

/* ----------------------------------------
	Fixed Header
---------------------------------------- */

var $window = $(window),
	pos = $('nav').offset().top;

$window.scroll(function(){
	if ($window.scrollTop()>pos){ 
		$('nav').addClass('fixed');
	} else {
		$('nav').removeClass('fixed');
	}
});


var scrollMenu = function() {
        // 配列宣言
        // ここにスクロールで点灯させる箇所のidを記述する
        // 数値は全て0でOK
        var array = {
            '#top': 0,
            '#about': 0,
            '#about-kampai': 0,
            '#04': 0,
            '#shop-list': 0,
            '#06': 0
        };
 
        var $globalNavi = new Array();
 
        // 各要素のスクロール値を保存
        for (var key in array) {
            if ($(key).offset()) {
                array[key] = $(key).offset().top - 10; // 数値丁度だとずれるので10px余裕を作る
                $globalNavi[key] = $('nav ul li a[href="' + key + '"]');
            }
        }
 
        // スクロールイベントで判定
        $(window).scroll(function () {
            for (var key in array) {
                if ($(window).scrollTop() > array[key] - 50) {
                    $('nav ul li a').each(function() {
                        $(this).removeClass('active');
                    });
                    $globalNavi[key].addClass('active');
                }
            }
        });
    }
 
    // 実行
    scrollMenu();


/* ----------------------------------------
	Animation
---------------------------------------- */

var targetPos = $window.height()/2,
	anim ={
		$el: $('.anim'),
		lng: $('.anim').length,
		flg: true,
		arr: [],
		init: function(){
			anim.$el.each(function(i){
				var $this = $(this),
					type = 'fade';
				if($this.hasClass('animL')) type = 'left';
				if($this.hasClass('animR')) type = 'right';
				if($this.hasClass('animP')) type = 'parallax';
				var val = {
					'type': type,
					pos: 0
				}
				if(type=='fade') $this.css({opacity: 0, 'margin-top': '0'});
				anim.arr.push(val);
			});
			this.setup();
			this.fnc();
		},
		setup: function(){
			anim.flg =($window.width()>=768)? true: false;
			if(anim.flg){
				anim.$el.each(function(i){
					var $this = $(this),
						pos =($this.data('tarpos'))? $this.offset().top-$window.height()/$this.data('tarpos'): $this.offset().top-targetPos;
					anim.arr[i].pos = pos;
//if(window.console && typeof window.console.log==='function') console.log(pos+' w:'+targetPos+' w2:'+$window.height()/$this.data('tarpos'));
				});
			}else{
				anim.$el.removeAttr('style');
			}
		},
		fnc: function(){
			$window.bind('scroll', function(){
				if(anim.flg){
					for(var i=0; i<anim.lng; i++){
						var $this = anim.$el.eq(i),
							type = anim.arr[i].type,
							pos = anim.arr[i].pos;

					/* fade
					---------------------------------------- */

						if(type=='fade'){
							if($(this).scrollTop()>=pos){
								$this.velocity({opacity: 1}, 1500, 'outQuart');
								$this.css({'margin-top': (pos-$(this).scrollTop())/2});
							}

					/* parallax
					---------------------------------------- */

						}else if(type=='parallax'){
							if($(this).scrollTop()>=pos) $this.velocity({translateY: ($(this).scrollTop()-pos)/3}, 0);
							else $this.velocity({translateY: 0}, 0);

					/* scroll
					---------------------------------------- */	

						}else{
							var dir =(type=='left')? -1: 1,
								nowPos = dir*(pos-$(this).scrollTop());
							if((nowPos>=0 && dir<0) || (nowPos<=0 && dir>0)) nowPos = 0;
							$this.velocity({translateX: nowPos}, 0);
						}
					}
				}
			});
		}
	};
anim.init();
//if(window.console && typeof window.console.log==='function') console.log(anim.arr);
$window.resize(function(){
	anim.setup();
});


/* ----------------------------------------
	roll over
---------------------------------------- */

	var cache = [];
	function onmouseover() {
		if (/_on\.(?:gif|png|jpg)$/.test(this.src)) {
			return;
		}
		this.src = this.src.replace(/\.(gif|png|jpg)$/, '_on.$1'); 
	}
	function onmouseout() {
		this.src = this.src.replace(/_on\.(gif|png|jpg)$/, '.$1'); 
	}
	$('img.rollover, input.rollover')
	.hover(onmouseover, onmouseout)
	.click(onmouseout)
	.each(function() {
		var img = new Image();
		img.src = this.src.replace(/\.(gif|png|jpg)$/, '_on.$1'); 
		cache.push(img);
	});


/* ----------------------------------------
	smooth scroll
---------------------------------------- */

$('a[href^=#]').click(function(){
	var speed = 600;
	var href= $(this).attr("href");
	var target = $(href == "#" || href == "" ? 'html' : href);
	var position = target.offset().top;
	$("html, body").animate({scrollTop:position}, speed, "swing");
	return false;
});



/* ----------------------------------------
	modal window
---------------------------------------- */

var $list = $('#movieList li a.modal, #nihonshudance a.modal, #forum a.modal'),
	$modal = {
		wrap: $('#modal'),
		li:  $('#modal li'),
		close: $('#modal .close, #modal .btnClose, #modal .btnStop')
	};

$('#movieList a.modal').each(function(i){
	var thisUrl = $(this).attr('href');
	$(this).removeAttr('href').click(function(){
		$('#modal .movie').eq(i).find('iframe').attr('src', thisUrl);
	});
});
$('#nihonshudance a.modal').each(function(i){
	var thisUrl = $(this).attr('href');
	$(this).removeAttr('href').click(function(){
		$('#modal .movie').eq(i).find('iframe').attr('src', thisUrl);
	});
});
	
$list.click(function(){
	var idx = $list.index(this);
	$modal.li.removeClass('on').eq(idx).addClass('on');
	$modal.wrap.css({
		opacity: 0
	}).show().animate({
		opacity: 1
	}, 500, 'easeOutQuint');
});

$("#forum a.modal").click(function(){
	$modal.li.removeClass('on');
	$("li.schedule").addClass('on');
	$("body").css("overflow", "hidden");
	$modal.wrap.css({
		opacity: 0
	}).show().animate({
		opacity: 1
	}, 500, 'easeOutQuint');
});	
	
$modal.close.click(function(){
	$('#modal .movie.on').length;
	$modal.wrap.animate({
		opacity: 0
	}, 300, function(){
		$(this).hide();
	});
});
$("#modal a.btn, .close").click(function(){
	$("body").css("overflow", "");
	$modal.wrap.animate({
		opacity: 0
	}, 300, function(){
		$(this).hide();
	});
});
	
	
/* ----------------------------------------
	modal window photo
---------------------------------------- */

var $list2 = $('#slider .item:not(#sakeTitle)'),
	$modal2 = {
		wrap: $('#modal'),
		li:  $('#modal li'),
		close: $('#modal .close, #modal .btnClose, #modal .btnStop')
	};

$('#slider .item').each(function(){
	var thisName = $(this).find('.name').text();
	var thisTime = $(this).find('.time').text();
	var thisProfImg = $(this).find('.prof_img').find('img').attr('src');
	var thisTitle = $(this).find('.title').text();
	var thisCaption = $(this).find('.caption').text();
	$(this).click(function(){
		var thisUrl = $(this).find('.pic').attr('data-original2');
		$('#modal .photo .pic').attr('src', thisUrl).show();
		$('#modal .photo .name').text(thisName).show();
		$('#modal .photo .time').text(thisTime).show();
		$('#modal .photo .profImg').show();
		$('#modal .photo .profImg > img').attr('src',thisProfImg).show();
		$('#modal .photo .text .title').text(thisTitle).show();
		$('#modal .photo .text .caption').text(thisCaption).show();
		if(thisName == "")
			$('#modal .photo .name').hide();
		if(!thisProfImg)
			$('#modal .photo .profImg').hide();
		if(thisTitle == "")
			$('#modal .photo .text .title').hide();
	});
});

$list2.click(function(){
	var idx = $list.index(this);
	$modal.li.removeClass('on').eq(idx).addClass('on');
	$modal.wrap.css({
		opacity: 0
	}).show().animate({
		opacity: 1
	}, 500, 'easeOutQuint');
});

$('#next').click(function(){
	var $next = $list.domNext().addClass('on');
});

$modal2.close.click(function(){
	$modal.wrap.animate({
		opacity: 0
	}, 300, function(){
		$(this).hide();
		if($('#modal .photo.on').length) {
			$('#modal .photo.on img').removeAttr('src');
			$('#modal .photo .name').text("");
			$('#modal .photo .time').text("");
			$('#modal .photo .caption span').text("");
		}
	});
});


/* ----------------------------------------
    post image
---------------------------------------- */

$('.my-container').sortablePhotos({
  selector: '> .my-item',
  sortable: true,
  padding: 2
});


/* ----------------------------------------
	Page top
---------------------------------------- */

$(window).bind("scroll", function(){
if ($(this).scrollTop() > 500) { 
	$(".pageTop").fadeIn();
} else {
	$(".pageTop").fadeOut();
}

	

$(".pageTop a").css({"position":"fixed","bottom": "20px"});


});


/* ----------------------------------------
	Modal with Param
---------------------------------------- */

var url   = location.href;
params    = url.split("?");
spparams   = params[1].split("&");
 
var paramArray = [];
for ( i = 0; i < spparams.length; i++ ) {
    vol = spparams[i].split("=");
    paramArray.push(vol[0]);
    paramArray[vol[0]] = vol[1];
}

if ( paramArray["i"].length > 0) {
	$entry = $('#entry_' + paramArray["i"]);

	if($entry.length){
		var thisUrl = $entry.find('.pic').attr('src');
		var thisName = $entry.find('.name').text();
		var thisTime = $entry.find('.time').text();
		var thisProfImg = $entry.find('.prof_img').find('img').attr('src');
		var thisTitle = $entry.find('.title').text();
		var thisCaption = $entry.find('.caption').text();
		$('#modal .photo .pic').attr('src', thisUrl);
		$('#modal .photo .name').text(thisName);
		$('#modal .photo .time').text(thisTime);
		$('#modal .photo .profImg > img').attr('src',thisProfImg);
		$('#modal .photo .text .title').text(thisTitle);
		$('#modal .photo .text .caption').text(thisCaption);

		$('.modal-photo .photo').addClass('on');
		$modal.wrap.css({
			opacity: 0
		}).show().animate({
			opacity: 1
		}, 500, 'easeOutQuint');
	}

}

// EOS
});