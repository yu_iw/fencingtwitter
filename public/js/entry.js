$(function(){

	if ($('#agreement').prop('checked') == false) {
		$('#submitBtn').attr('disabled', 'disabled');
	} else {
		$('#submitBtn').removeAttr('disabled');
	};

	$('#agreement').click(function() {
		if ($(this).prop('checked') == false) {
			$('#submitBtn').attr('disabled', 'disabled');
		} else {
			$('#submitBtn').removeAttr('disabled');
		}
	});

	$('#openAccompany1').click(function() {
		$('#accompany1').slideDown("slow");
		$(this).hide();
	});

	$('#openAccompany2').click(function() {
		$('#accompany2').slideDown("slow");
		$(this).hide();
	});

	$('#openAccompany3').click(function() {
		$('#accompany3').slideDown("slow");
		$(this).hide();
	});

	$('#openAccompany4').click(function() {
		$('#accompany4').slideDown("slow");
		$(this).hide();
	});

})