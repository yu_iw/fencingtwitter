$(document).ready(function () {
    $("#contact-form input").floatlabel();
});
function requestSend(fromId, toId){
    var comment = $("#request-comment").val();
    jQuery.ajax({
        cache:false,
        type: 'POST',
        data: {
                "f": fromId,
                "t": toId,
                "comment": comment
            },
        url: '/my/requestSend/',
        success: function(rs){
            console.log(rs);
            if(jQuery.trim(rs) == "success"){
                $('.popup-detail-area').empty();
                $('.popup-detail-area').load('/my/requestSendSuccess/');
            }
        }
    });
};
function friendAllow(fId){
    jQuery.ajax({
        cache:false,
        type: 'POST',
        data: {
                "f": fId,
                "allow": 1
            },
        url: '/my/requestAllow/',
        success: function(rs){
            console.log(rs);
            if(jQuery.trim(rs) == "success"){
                $('.popup-detail-area').empty();
                $('.popup-detail-area').load('/my/requestAllowSuccess/');
            }
        }
    });
};
function friendDisAllow(fId){
    jQuery.ajax({
        cache:false,
        type: 'POST',
        data: {
                "f": fId,
                "allow": 0
            },
        url: '/my/requestAllow/',
        success: function(rs){
            console.log(rs);
            if(jQuery.trim(rs) == "success"){
                $('.popup-detail-area').empty();
                $('.popup-detail-area').load('/my/requestDisAllowSuccess/');
            }
        }
    });
};