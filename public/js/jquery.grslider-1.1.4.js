/* ++++++++++++++++++++++++++++++++++++++++

	jQuery GR Slider
	Requires: jquery, jquery.easing
	Last Update: 4/22, 2015
	Version: 1.1.4

++++++++++++++++++++++++++++++++++++++++ */

;(function($){


/* ----------------------------------------
	Start
---------------------------------------- */

var cls = {
		act: 'active',
		fd: 'fading'
	};

$.fn.grSlider = function(options){

var $self = this;

$self.each(function(){


/* ----------------------------------------
	Initialize
---------------------------------------- */
	
	var slider = {
			$area: $(this)
		},
		settings = $.extend({}, $.fn.grSlider.defaults, options);

	slider.$area.before('<div id="'+slider.$area.attr('id')+'Wrap" class="sliderWrap"></div>');
	slider.$wrap = slider.$area.prev('.sliderWrap');
	slider.$area.appendTo(slider.$wrap);

/* Options, Variables
---------------------------------------- */

	settings.moveAll =(settings.type=='fade' || settings.dsplNum==1)? true: settings.moveAll;// 表示分まとめてスライド
	settings.direction =(settings.type=='fade')? 'left': settings.direction;// 表示分まとめてスライド
	slider.$el = slider.$area.children();
	slider.num = slider.$el.length,
	slider.navNum =(settings.moveAll)? Math.ceil(slider.num/settings.dsplNum): slider.num,
	slider.mov =(settings.moveAll)? slider.$area.width(): slider.$el.width(),
	slider.wdt = slider.$el.width();
	var flags = {
		left: settings.direction=='left',
		cur: 0,// 何番目か
		moving: false// 移動中かどうか
	}
	var slideRepeat;

/* Set up
---------------------------------------- */

// Slide Directions
	if(settings.paging){
		slider.$wrap.append('<p class="sliderPrev"><a>Previous</a></p><p class="sliderNext"><a>Next</a></p>');
		slider.$prev =(!settings.navReverse)? $('.sliderPrev a', slider.$wrap): $('.sliderNext a', slider.$wrap),
		slider.$next =(!settings.navReverse)? $('.sliderNext a', slider.$wrap): $('.sliderPrev a', slider.$wrap);
	}
	
// Slide Navigation
	if(settings.nav){
		slider.$wrap.append('<div class="sliderNav"><ul></ul></div>');
		slider.$navUl = $('.sliderNav ul', slider.$wrap);
		for(i=0; i<slider.navNum; i++){
			slider.$navUl.append('<li class="sliderNav'+i+'"><a>'+i+'</a></li>');
		}
		slider.$nav = $('li', slider.$navUl);
		slider.$nav.eq(0).addClass(cls.act);
	}

// Append Empty Slider
	if(slider.num%settings.dsplNum!=0 && settings.moveAll){
		var emptyNum = settings.dsplNum-slider.num%settings.dsplNum;
		for(i=0; i<emptyNum; i++){
			slider.$area.append('<li class="empty"></li>')
		}
		slider.$el = slider.$area.children();
		slider.num = slider.$el.length;
	}
	slider.$el.addClass('slided');

// Mode:Slide
	switch(settings.type){
	case 'slide':
	if(settings.loop){
		slider.$el.clone().appendTo(slider.$area);
		for(i=0; i<settings.dsplNum; i++){
			slider.$el.eq(i).addClass(cls.act);
		}
		slider.$el = slider.$area.children();
		slider.num = slider.$el.length;
	}

	// Set Initial Positions
		function setInitPosSlide(){
			slider.$el.each(function(){
			var $this = $(this),
				slideInd = slider.$el.index(this);
				if(flags.left){
					$this.css({
						left: slider.wdt*slideInd
					});
				}else{
					if(settings.dsplNum>1){
						$this.css({
							left: slider.wdt*(settings.dsplNum-slideInd-1)
						});
					}else{
						$this.css({
							left: -slider.wdt*slideInd
						});
					}
				}
			});
		}
		setInitPosSlide();

	// Resized
		if(settings.resize){
			$(window).bind('resize load', function(){
				slider.mov =(settings.moveAll)? slider.$area.width(): slider.$el.width();
				slider.wdt = slider.$el.width();
				var newPos =(settings.direction=='left')? -slider.wdt*flags.cur: slider.wdt*flags.cur;
				slider.$area.stop().css({left: newPos});
				flags.moving = false;
				setInitPosSlide();
			});
		}
	break;

// Mode:Fade
	case 'fade':
		for(i=0; i<settings.dsplNum; i++){
			slider.$el.eq(i).addClass(cls.act);
		}
	// Initial DispaPositions
		slider.$el.each(function(){
			var $this =  $(this);
			if(!$this.hasClass(cls.act)) $this.hide();
			if(settings.dsplNum>1){
				slideInitPos = slider.$el.index(this)%settings.dsplNum;
				$this.css({
					left: slider.wdt*slideInitPos
				});
			}
		});
	break;
	}


/* ----------------------------------------
	Fnc chgNav
---------------------------------------- */

function chgNav(){

/* Slide Directions
---------------------------------------- */

	if(settings.paging){
		slider.$prev.show();
		slider.$next.show();
		if(!settings.loop){
			if(flags.cur==0){
				slider.$prev.hide();
			}
			if(flags.cur==slider.navNum-settings.dsplNum || slider.navNum<=settings.dsplNum){
				slider.$next.hide();
			}
		}
	}

/* Slide Navigation
---------------------------------------- */

	if(settings.nav){
		slider.$nav.removeClass(cls.act);
		slider.$nav.eq(flags.cur%(slider.navNum)).addClass(cls.act);

	// Change Drirection Color
		if(settings.pagingClass){
			var arr = settings.pagingClass.split(','),
				pClass = arr[0],
				pDivisor = arr[1],
				pRemainder = arr[2];
			if((flags.cur+1)%pDivisor==pRemainder){
				slider.$prev.addClass(pClass);
				slider.$next.addClass(pClass);
			}else{
				slider.$prev.removeClass(pClass);
				slider.$next.removeClass(pClass);
			}
		}
	}
}

chgNav();


/* ----------------------------------------
	Fnc chgSlide
---------------------------------------- */

	function chgSlide(clk){
		tmrOff();
		flags.moving = true;// スライドが終わるまで他の動きを禁止
		flags.cur =(clk || clk==0)? clk: flags.cur+1;
		if(settings.type=='fade' && flags.cur==slider.navNum){
			flags.cur = 0;
		}

/* Activate
---------------------------------------- */

		var dsplStart = flags.cur*settings.dsplNum;
		if(settings.type=='slide' && !settings.moveAll){
			dsplStart = flags.cur;
		}
		if(settings.moveAll){
			if(dsplStart>slider.navNum*settings.dsplNum) dsplStart %= slider.navNum*settings.dsplNum;
		}else{
			if(dsplStart>slider.navNum) dsplStart %= slider.navNum;
		}
		var dsplEnd = dsplStart+settings.dsplNum;
		slider.$el.removeClass(cls.act);
		for(i=dsplStart; i<dsplEnd; i++){
			var actInd = i;
			if(dsplStart>=slider.navNum && settings.type=='slide'){
				slider.$el.eq(actInd%slider.navNum).addClass(cls.act);
			}
			slider.$el.eq(actInd).addClass(cls.act);
		}

		switch(settings.type){
	
/* Mode:Slide
---------------------------------------- */

		case 'slide':
			var slideMove =(flags.left)? -slider.mov*flags.cur: slider.mov*flags.cur;
			slider.$area.animate({
				left : slideMove
			}, settings.slideTime, settings.slideEase, function(){
				if(flags.cur==slider.navNum){
					slider.$area.css({
						left: 0
					});
					flags.cur = 0;
				}
				flags.moving = false;
				tmrOn();
			});
		break;
	
/* Mode:Fade
---------------------------------------- */

		case 'fade':
			slider.$el.not('.empty').each(function(){
				var $this = $(this);
				if($this.hasClass(cls.act)){
					$this.show().css({
						opacity: 0
					}).animate({
						opacity: 1
					}, settings.slideTime, settings.slideEase, function(){
						tmrOn();
					});
				}else{
					$this.delay(100).css({
						zIndex: 11,
						opacity: 1
					}).animate({
						opacity: 0
					}, settings.slideTime, settings.slideEase, function(){
						$this.css({
							zIndex: ''
						}).hide();
						flags.moving = false;
					});
				}
			});
		break;
		}
		chgNav();
	}


/* ----------------------------------------
	Click Navi
---------------------------------------- */

	if(settings.nav){
		slider.$nav.click(function(){
			if(!flags.moving && !$(this).hasClass(cls.act)){
				flags.cur = slider.$nav.index(this);
		
				if(flags.cur==slider.navNum){
					slider.$area.css({
						left: 0
					});
				}
				chgSlide(flags.cur);
				slider.$nav.removeClass(cls.act);
				slider.$nav.eq(flags.cur % slider.num).addClass(cls.act);
			}
		});
	}


/* ----------------------------------------
	Click Previous, Next
---------------------------------------- */

	if(settings.paging){
		slider.$prev.click(function(){
			if(!flags.moving){
				flags.cur -= 1;
				if(flags.cur<0){
					flags.cur = slider.navNum-1;
					switch(settings.type){
					case 'slide':
						if(flags.left){
							slider.$area.css({
								left: -slider.mov*slider.navNum
							});
						}else{
							slider.$area.css({
								left: slider.mov*slider.navNum
							});
						}
					break;
					}
				}
				chgSlide(flags.cur);
			}
		});

/* Click Next
---------------------------------------- */

		slider.$next.click(function(){
			if(!flags.moving){
				chgSlide(null);
			}
		});
	}


/* ----------------------------------------
	Timer Switch
---------------------------------------- */
	
	function tmrOn(){
		if(settings.auto) slideRepeat = setInterval(chgSlide, settings.chgTime);
	}
	function tmrOff(){
		if(settings.auto) clearInterval(slideRepeat);
	}

	$(window).load(tmrOn);


/* ----------------------------------------
	Return
---------------------------------------- */

	return this;
});
}


/* ----------------------------------------
	Default Settings
---------------------------------------- */

$.fn.grSlider.defaults = {
	type: 'slide',// スライド形式
	dsplNum: 1,// 表示スライド数
	moveAll: false,// 表示分まとめてスライド
	auto: true,// 自動再生
	loop: true,// 繰り返し
	chgTime: 3000,// 自動再生時間
	slideTime: 500,// スライド効果時間
	slideEase: 'easeOutCubic',// スライド効果
	direction: 'left',// スライド方向
	paging: true,// 前後ボタン有無
	navReverse: false,// 前後ボタンを逆方向に
	pagingClass: false,// 前後ボタンのスタイル変更 'クラス名, 除数, 余り'
	nav: false,// スライドナビ有無
	resize: false// リサイズ有無
}

})(jQuery);
