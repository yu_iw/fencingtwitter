<?php
class InstagramApiBatch{
  const TAG = 'イクメンフォト';  # 取得したいタグ名
  const ACCESS_TOKEN = '3273157635.16269bf.f67a97211e164c17b77a5ae592d10128';  # アクセストークン
  const MAX_PAGE = 5;  # 最大取得回数（6が限界？）
  private $base_url;
  private $exclude_link_list;
 
  function __construct() {
    # 33件が一度に取得できる最大値のようなのでcount=33
    $this->base_url = 'https://api.instagram.com/v1/tags/'. self::TAG . '/media/recent?access_token=' . self::ACCESS_TOKEN . '&count=33';
    # 除外したい写真のURLを配列内に記述 : 'http://instagram.com/p/{一意なキー}/'
    $this->exclude_link_list = array();
  }
 
  public function run() {
    $this->get_tags($this->base_url, 1);
  }
 
  private function get_tags($next_url, $page) {
    $obj = json_decode(file_get_contents($next_url));
	print_r($obj);
    

    //DB接続
    $dsn = 'pgsql:dbname=ikumen;host=localhost';
    $user = 'ikumen-project.mhlw.go.jp';//本番
    //$user = 'ikumen.grpht.co.jp';//テスト
    $password = '';
  
    try{
      $dbh = new PDO($dsn, $user, $password);
  
      if ($dbh == null){
          print('接続に失敗しました。<br>');
      }else{
          print('接続に成功しました。<br>');
      }

	  
		//$ng = 'select * from t_word';
		if($ng){
		$stmt = $dbh->query($ng);
			while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
				//print($result['ng']);
				$ng[] = $result['ng'];
			}
		}
  
      //instagramAPI検索結果分だけチェックしてまわす
      foreach($obj->data as $data) {
        if (in_array($data->link, $this->exclude_link_list)) {
          continue;
        }

        //$image_url = strstr($data->images->standard_resolution->url , '?' , true);
        //$image_url_low = strstr($data->images->low_resolution->url , '?' , true);
        $image_url = $data->images->standard_resolution->url;
        $image_url_low = $data->images->low_resolution->url;

		
        //DB用にパラメータを用意
        $params = array(
			2,//category
			0,//ng_name
			0,//ng_body
			$data->id, 
			$data->user->username,
			$data->user->full_name,
			$data->caption->text,
			$image_url,
			$image_url_low,
			$data->likes->count,
			$data->link,
			$data->created_time,
			$data->user->profile_picture,
			$data->created_time,
			$data->id,
		);
		if($ng){
			foreach($ng as $value){
				if(strpos($params['3'],$value)!== false){
					$params[5] = 1;
					//echo "NGネーム";
				}
				if(strpos($params['4'],$value)!== false){
					$params[6] = 1;
					//echo "NGボディ";
				}
			}
		}
        print_r($params);

        //prepared statementで接続
        $stmt = $dbh->prepare("INSERT INTO t_sns (
			category,
			ng_name,
			ng_body,
			post_id,
			user_id,
			name,
			body,
			media_url,
			media_url_thumb,
			likes,
			post_url,
			posted_at,
			icon_url,
			created_at
			) SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, to_timestamp(?), ?,to_timestamp(?) WHERE NOT EXISTS ( SELECT id FROM t_sns WHERE post_id = ? )"
		);
        //データ挿入
        $stmt->execute($params);
	
		print_r($dbh->errorInfo());			//データ挿入


      }
    }catch (PDOException $e){
      print('Error:'.$e->getMessage());
      die();
    }


    if ($page >= self::MAX_PAGE) {
      //print('MAX_PAGEです<br>');
      return;
    }
 
    if (isset($obj->pagination->next_url)) {
      //print('まだページあります<br>');
      $this->get_tags($obj->pagination->next_url, $page + 1);
    }
    
    print('終了しました<br>');
  }

}


//ここから実際の実行部分→json出力
header( "Content-Type: application/json; charset=utf-8" ) ;

$instagram = new InstagramApiBatch();
$instagram->run();