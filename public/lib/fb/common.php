<?php

define('APP_ID', '1764345840444718');
define('APP_SECRET', '423f8094fa175e44bdad0de10e6697d4');


//https://www.facebook.com/dialog/oauth/?
//    client_id=YOUR_APP_ID
//    &redirect_uri=YOUR_REDIRECT_URL
//    &state=YOUR_STATE_VALUE
//    &scope=COMMA_SEPARATED_LIST_OF_PERMISSION_NAMES

define('FB_URL', 'https://www.facebook.com');
define('REDIRECT_URI', 'https://kampai-sake.jp/main/fbCallback/');
define('FB_ACCESS_TOKEN_URL', 'https://graph.facebook.com/oauth/access_token');
define('FB_POST_URL', 'https://graph.facebook.com/me/feed');
define('FB_FEED_URL', 'https://graph.facebook.com/me/home');
define('FB_USER_INFO_URL', 'https://graph.facebook.com/me');
function get_fb_oauth_url($options = array())
{
    $q_array = array();
    foreach (array(
        'client_id',
        'redirect_uri',
        'response_type',
        'state',
        'scope',
    ) as $k) {
        if (!$v = array_get($options, $k)) continue;
        $q_array[] = "{$k}=" . $v;
    }
    $query_string = implode($q_array, '&');
    return FB_URL . "/dialog/oauth/?{$query_string}";
}

function post($url, $params)
{
    $headers = array('Content-Type: application/x-www-form-urlencoded');

    $reqOptions = array(
        'http' => array(
            'method' => 'POST',
            'header' => implode('\r\n', $headers),
            'content' => http_build_query($params),
        ),
    );
    return file_get_contents($url, false, stream_context_create($reqOptions));

}

function generateConsumerKey()
{
    $fp = fopen('/dev/urandom', 'rb');
    $entropy = fread($fp, 32);

    fclose($fp);

    $entropy .= uniqid(mt_rand(), true);
    $hash = sha1($entropy);

    return array(substr($hash,0,30),substr($hash,30,10));
}

function array_get($array, $key, $default = null)
{
    if (!is_array($key)) {
        return is_array($array) && array_key_exists($key, $array) ? $array[$key] : $default;
    }

    if (!$key) return $default;

    foreach ($key as $k) {
        if (is_array($array) && array_key_exists($k, $array)) {
            $array = $array[$k];
        } else {
            return $default;
        }
    }
    return $array;
}

session_start();