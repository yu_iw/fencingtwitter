<?php

require_once('PHPMailer/PHPMailerAutoload.php');

class PHPMailController
{
	private static $mailprof;

	public static function setMailprof($mailprof)
	{
		self::$mailprof = $mailprof;
	}

    public function sendMail($mailto, $subject, $message)
    {
    	mb_language("japanese");
    	mb_internal_encoding("UTF-8");

        $param = self::$mailprof;

        $pop = POP3::popBeforeSmtp('mail.ikumen-project.jp', 110, 30, 'pop@ikumen-project.jp', 'Dg3QpUe', 0);

        $mailer = new PHPMailer();


        try {

            $mailer -> CharSet = "iso-2022-jp";
            $mailer -> Encoding = "7bit";

            $mailer -> IsSMTP();
            $mailer -> SMTPAuth = false;
            // $mailer -> SMTPDebug = 2;

            $mailer -> Host = "mail.ikumen-project.jp:25";
            $mailer -> From     = "no_reply@ikumen-project.jp";
            $mailer -> FromName = mb_encode_mimeheader ( mb_convert_encoding ( "イクメンプロジェクト事務局", "JIS", "UTF-8" ) );
            $mailer -> Subject  = mb_encode_mimeheader ( mb_convert_encoding ( $subject, "JIS", "UTF-8" ) );
            $mailer -> Body     = mb_convert_encoding ( $message, "JIS", "UTF-8" );
            $mailer -> AddAddress($mailto);

            $result = $mailer -> Send();

        } catch (phpmailerException $e) {
          $result = $e->errorMessage();
        } catch (Exception $e) {
          $result = $e->getMessage();
        }

        return $result;

    }

}

?>
