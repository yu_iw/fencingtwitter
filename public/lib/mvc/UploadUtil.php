<?php

//アップロードファイルクラス
class UploadUtil
{
    function GetMimeType ($upload_file)
    {
		$path = $upload_file['tmp_name'];
		//echo $path."<br>";
		$mime = shell_exec('file -bi '.escapeshellcmd($path));
		$mime = trim($mime);
		$mime = preg_replace("/ [^ ]*/", "", $mime);
		$mime = str_replace(";","",$mime);
		//print($mime."<br>");
		return $mime;
    }
	
	function MimeTypeFilter($mimeArr,$mime_type)
	{	
		$type['jpg'] = array(
			'image/jpeg',
			'image/pjpeg',
		);
		
		$type['png'] = array(
			'image/png',
		);
		
		$type['gif'] = array(
			'image/gif',
		);
		
		$type['text'] = array(
			'application/plain',
			'text/plain',
		);
		
		$type['pdf'] = array(
			'application/pdf',
			'application/x-pdf',
			'application/acrobat',
			'applications/vnd.pdf',
			'text/pdf',
			'text/x-pdf',
		);
		
		$type['word'] = array(
			'application/msword',
			'application/doc',
			'application/vnd.msword',
			'application/vnd.ms-word',
			'application/winword',
			'application/word',
			'application/x-msw6',
			'application/x-msword',
		);
		
		$type['excel'] = array(
			'application/vnd.ms-excel',
			'application/msexcel',
			'application/x-msexcel',
			'application/x-ms-excel',
			'application/vnd.ms-excel',
			'application/x-excel',
			'application/x-dos_ms_excel',
			'application/xls',
		);
		
		$type['ppt'] = array(
			'application/vnd.ms-powerpoint',
			'application/mspowerpoint',
			'application/ms-powerpoint',
			'application/mspowerpnt',
			'application/vnd-mspowerpoint',
			'application/powerpoint',
			'application/x-powerpoint',
			'application/x-m',
		);
		
		$result = false;
		
		foreach($mimeArr as $val){
			foreach ($type as $k => $v){
				if($val == $k){
					$result = in_array($mime_type, $v);
					//echo $val."判別：".$result;
					if ($result == true){
						return $result;
					}
				}
			}
		}
	}
}


?>
