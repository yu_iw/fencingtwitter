<?php

//回転する角度-180～180
$angle = $_GET['a'];
error_log("angle=".$angle);
//コピー元
$src = $_GET['src'];
error_log("src=".$src);
//コピー先
$dst = $_GET['dst'];
error_log("dst=".$dst);
//リサイズする際の横幅
$width = $_GET['w'];
error_log("width=".$width);

$ir = new ImageRotate();
$ir->rotateImage($angle, $dst, $src, $width);


// 画像回転クラス
class ImageRotate
{
    public function rotateImage($angle, $dst, $src, $width )
    {
		//パーミッション777
//     	chmod($dst, 0777);

		$extension = substr($src, strlen($src)-4);

		if ($extension == '.jpg' || $extension == 'jpeg' ){
			$src_image = imagecreatefromjpeg($src);
		}elseif ($extension == '.gif'){
			$src_image = imagecreatefromgif($src);
		}elseif ($extension == '.png'){
			$src_image = imagecreatefrompng($src);
		}

		$src_image = $this->imagerotateEquivalent($src_image, -$angle, 0);
		echo($src_image);

		$src_w = imagesx($src_image);
		$src_h = imagesy($src_image);
		$dst_w = $width;
		$dst_h = $width*($src_h/$src_w);

		$dst_image = imagecreatetruecolor($dst_w, $dst_h);

		if( !imagecopyresampled($dst_image, $src_image, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h)){
// 			echo' 画像のリサイズ・移動を完了できません。<br>';
			return false;
		}else if( $extension == '.jpg' || $extension == 'jpeg' ){
			if(imagejpeg($dst_image, $dst,100)){
// 				echo "<br>画像の回転・移動が完了しました。<br><a href='#' onClick='window.close(); return false;'>閉じる</a>";
				return true;
			}
		}else if( $extension == '.gif' ){
			if(imagegif($dst_image, $dst,100)){
// 				error_log('リサイズ・移動が完了しました。');
				return true;
			}
		}else if( $extension == '.png' ){
			if(imagepng($dst_image, $dst,100)){
// 				error_log('リサイズ・移動が完了しました。');
				return true;
			}
		}

    }

    function imagerotateEquivalent($srcImg, $angle, $bgcolor, $ignore_transparent = 0) {
    	function rotateX($x, $y, $theta){
    		return $x * cos($theta) - $y * sin($theta);
    	}
    	function rotateY($x, $y, $theta){
    		return $x * sin($theta) + $y * cos($theta);
    	}

    	$srcw = imagesx($srcImg);
    	$srch = imagesy($srcImg);

    	//Normalize angle
    	$angle %= 360;
    	//Set rotate to clockwise
    	$angle = -$angle;

    	if($angle == 0) {
    		if ($ignore_transparent == 0) {
    			imagesavealpha($srcImg, true);
    		}
    		return $srcImg;
    	}

    	// Convert the angle to radians
    	$theta = deg2rad ($angle);

    	//Standart case of rotate
    	if ( (abs($angle) == 90) || (abs($angle) == 270) ) {
    		$width = $srch;
    		$height = $srcw;
    		if ( ($angle == 90) || ($angle == -270) ) {
    			$minX = 0;
    			$maxX = $width;
    			$minY = -$height+1;
    			$maxY = 1;
    		} else if ( ($angle == -90) || ($angle == 270) ) {
    			$minX = -$width+1;
    			$maxX = 1;
    			$minY = 0;
    			$maxY = $height;
    		}
    	} else if (abs($angle) === 180) {
    		$width = $srcw;
    		$height = $srch;
    		$minX = -$width+1;
    		$maxX = 1;
    		$minY = -$height+1;
    		$maxY = 1;
    	} else {
    		// Calculate the width of the destination image.
    		$temp = array (rotateX(0, 0, 0-$theta),
    				rotateX($srcw, 0, 0-$theta),
    				rotateX(0, $srch, 0-$theta),
    				rotateX($srcw, $srch, 0-$theta)
    		);
    		$minX = floor(min($temp));
    		$maxX = ceil(max($temp));
    		$width = $maxX - $minX;

    		// Calculate the height of the destination image.
    		$temp = array (rotateY(0, 0, 0-$theta),
    				rotateY($srcw, 0, 0-$theta),
    				rotateY(0, $srch, 0-$theta),
    				rotateY($srcw, $srch, 0-$theta)
    		);
    		$minY = floor(min($temp));
    		$maxY = ceil(max($temp));
    		$height = $maxY - $minY;
    	}

    	$destimg = imagecreatetruecolor($width, $height);
    	if ($ignore_transparent == 0) {
    		imagefill($destimg, 0, 0, imagecolorallocatealpha($destimg, 255,255, 255, 127));
    		imagesavealpha($destimg, true);
    	}

    	// sets all pixels in the new image
    	for($x=$minX; $x<$maxX; $x++) {
    		for($y=$minY; $y<$maxY; $y++) {
    			// fetch corresponding pixel from the source image
    			$srcX = round(rotateX($x, $y, $theta));
    			$srcY = round(rotateY($x, $y, $theta));
    			if($srcX >= 0 && $srcX < $srcw && $srcY >= 0 && $srcY < $srch) {
    				$color = imagecolorat($srcImg, $srcX, $srcY );
    			} else {
    				$color = $bgcolor;
    			}
    			imagesetpixel($destimg, $x-$minX, $y-$minY, $color);
    		}
    	}
    	return $destimg;
    }

}

?>
