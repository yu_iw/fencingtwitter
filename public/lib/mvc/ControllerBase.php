<?php
abstract class ControllerBase
{
    protected $systemRoot;
    protected $controller = 'index';
    protected $action = 'index';
    protected $view;
    protected $request;
    protected $templatePath;

    // コンストラクタ
    public function __construct()
    {
        $this->request = new Request();
        session_start();
    }
    // システムのルートディレクトリパスを設定
    public function setSystemRoot($path)
    {
        $this->systemRoot = $path;
    }
    // コントローラーとアクションの文字列設定
    public function setControllerAction($controller, $action)
    {
        $this->controller = $controller;
        $this->action = $action;
    }
	
    // 処理実行
    public function run()
    {
        try {
            // ビューの初期化
	require_once LIB_PATH.'/mvc/Smarty/Smarty.class.php';
		$this->initializeView();
        // モデルの初期化
        $this->initializeModel();
        // 共通前処理
        $this->preAction();
        // アクションメソッド実行
        $methodName = sprintf('%sAction', $this->action);
        $this->$methodName();

		// $this->view->registerFilter("output",array("ControllerBase","filterSjis"));
		// ini_set("default_charset", "Shift_JIS");
        //ini_set('mbstring.http_output', 'SJIS');
        //ini_set('mbstring.detect_order', 'auto');

        // 表示Smarty実行
        $this->view->display($this->templatePath,null);
        exit;
        } catch (Exception $e) {
        	echo $e->getMessage();
            // ログ出力等の処理を記述
        }
    }
    // ビューの初期化
    protected function initializeView()
    {
        $this->view = new Smarty();
        $this->view->template_dir = sprintf('%s/views/templates', $this->systemRoot);
        $this->view->compile_dir = sprintf('%s/views/templates_c', $this->systemRoot);
	
        $this->templatePath = sprintf('%s/%s.tpl', $this->controller, $this->action);
    }
    // モデルインスタンス生成処理
    protected function initializeModel()
    {
        $className = ucfirst($this->controller) . 'Model';
        $classFile = sprintf('%s/models/%s.php', $this->systemRoot, $className);
        if (false == file_exists($classFile)) {
            return;
        }
        require_once $classFile;
        if (false == class_exists($className)) {
            return;
        }
        $this->model = new $className();
    }
    // 共通前処理（オーバーライド前提）
    protected function preAction()
    {
    }
	
	//smartyアウトプットフィルタ
	public function filterSjis($buff){
		return mb_convert_encoding($buff,"SJIS","UTF-8");
	}
	
}
?>