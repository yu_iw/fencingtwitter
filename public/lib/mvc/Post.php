<?php

// POST変数クラス
class Post extends RequestVariables
{
    protected function setValues()
    {
        foreach ($_POST as $key => $value) {
			//print_r($value."<br>");
			$this->_values[$key] = $this->myhtmlspecialchars($value);//エスケープ処理
			//print_r($this->_values[$key]);
            //$this->_values[$key] = $value;
        }		
    }
	private function myhtmlspecialchars($string) {
		if (is_array($string)) {
			return array_map(array($this, 'myhtmlspecialchars'), $string);
		} else {
			return htmlspecialchars($string, ENT_QUOTES);
		}
	}
}


?>
