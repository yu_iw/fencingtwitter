<?php
class Validation{
    public function validate($type, $subject, $subject_text, $requred = true, $length = 9999, $start_range = 0, $end_range = 999){
    	/***********************
    	 *  subject
    	 *  	text
    	 *  	zenkaku
    	 *  	katakana
    	 *  	hiragana
    	 *  	int
         *      prefecture
    	 *  	email
    	 *  	postal_code
    	 *  	tel
    	 *  	radio
         *      url
    	 *
    	 *  return	1:boolean
    	 *  		2:message
    	**********************/
    	//text
    	if($type === 'text'){
            $subject = str_replace(array("\r\n", "\r", "\n"), '', $subject);
    		$s_length = mb_strlen($subject);
            // error_log("文字数:".$s_length);
    		if($s_length > $length){
    			return array('bool'=>false,'error_text'=>'入力可能文字数を超えています。');
    		}else if($requred && $s_length == 0){
    			return array('bool'=>false,'error_text'=>$subject_text.'が入力されていません。');
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
    	//text English
    	if($type === 'text_en'){
            $subject = str_replace(array("\r\n", "\r", "\n"), '', $subject);
    		$s_length = mb_strlen($subject);
    		if($s_length > $length){
    			return array('bool'=>false,'error_text'=>'Maximum Allowed: '.$length.' characters.');
    		}else if($requred && $s_length == 0){
    			return array('bool'=>false,'error_text'=>'This field is required. Please enter a value.');
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}

    	//全角
    	if ($type === 'zenkaku') {
            $subject = str_replace(array("\r\n", "\r", "\n"), '', $subject);
    		$s_length = mb_strlen($subject);
    		$mb_length = mb_strlen($subject, "UTF-8") * 3;
    		$match = preg_match("/[^\x01-\x7E\xA1-\xDF]/", $subject);
    		if($s_length == $mb_length){
	    		if($s_length == 0){
	    			if($requred){
		    			return array('bool'=>false,'error_text'=>'入力してください。');
	    			}else{
			    		return array('bool'=>true,'error_text'=>'');
	    			}
	    		}else{
	    			if($s_length > $length){
	    				return array('bool'=>false,'error_text'=>'入力可能文字数を超えています。');
	    			}else if(!$match){
		    			return array('bool'=>false,'error_text'=>'全角で入力してください。');
		    		}
	    		}
	    		return array('bool'=>true,'error_text'=>'');
	    	}else{
	    		return array('bool'=>false,'error_text'=>'全て全角で入力してください。');
	    	}
    	}
    	//katakana
    	if($type === 'katakana'){
            $subject = str_replace(array("\r\n", "\r", "\n"), '', $subject);
    		$s_length = mb_strlen($subject);
    		$match = preg_match("/^([ァ-ヶー 　])+$/u",$subject);
    		if($s_length == 0){
    			if($requred){
                    return array('bool'=>false,'error_text'=>$subject_text.'が入力されていません。');
    			}else{
    				return array('bool'=>true,'error_text'=>'');
    			}
    		}else{
    			if($s_length > $length){
    				return array('bool'=>false,'error_text'=>'入力可能文字数を超えています。');
    			}else if(!$match){
    				return array('bool'=>false,'error_text'=>'カタカナで入力してください。');
    			}
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
    	//hiragana
    	if($type === 'hiragana'){
            $subject = str_replace(array("\r\n", "\r", "\n"), '', $subject);
    		$s_length = mb_strlen($subject);
    		$match = preg_match("/^([ぁ-んー 　])+$/u",$subject);
    		if($s_length == 0){
    			if($requred){
    				return array('bool'=>false,'error_text'=>'入力してください。');
    			}else{
    				return array('bool'=>true,'error_text'=>'');
       			}
    		}else{
    			if($s_length > $length){
    				return array('bool'=>false,'error_text'=>'入力可能文字数を超えています。');
    			}else if(!$match){
    				return array('bool'=>false,'error_text'=>'ひらがなで入力して下さい。');
    			}
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
    	//int
    	if($type === 'int'){
    		$s_length = mb_strlen($subject);
    		$match = preg_match("/^[0-9]+$/", $subject);
    		if($s_length == 0){
    			if($requred){
    				return array('bool'=>false,'error_text'=>$subject_text.'が入力されていません。');
    			}else{
    				return array('bool'=>true,'error_text'=>'');
    			}
    		}else{
    			if($s_length > $length){
    				return array('bool'=>false,'error_text'=>'入力可能文字数を超えています。');
    			}else if(!$match){
    				return array('bool'=>false,'error_text'=>'数字を入力してください。');
    			}
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
    	//int English
    	if($type === 'int_en'){
    		$s_length = mb_strlen($subject);
    		$match = preg_match("/^[0-9]+$/", $subject);
    		if($s_length == 0){
    			if($requred){
					return array('bool'=>false,'error_text'=>'This field is required. Please enter a value.');
    			}else{
    				return array('bool'=>true,'error_text'=>'');
    			}
    		}else{
    			if($s_length > $length){
    				return array('bool'=>false,'error_text'=>'Maximum Allowed: '.$length.' characters.');
    			}else if(!$match){
    				return array('bool'=>false,'error_text'=>'Please enter a numeric value.');
    			}
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
        //prefecture
        if($type === 'prefecture'){
            $s_length = mb_strlen($subject);
            $match = preg_match("/^[0-9]+$/", $subject);
            if($s_length == 0){
                if($requred){
                    return array('bool'=>false,'error_text'=>$subject_text.'が選択されていません。');
                }else{
                    return array('bool'=>true,'error_text'=>'');
                }
            }else{
                if($s_length > $length){
                    return array('bool'=>false,'error_text'=>'入力可能文字数を超えています。');
                }else if(!$match){
                    return array('bool'=>false,'error_text'=>'選択してください。');
                }
            }
            return array('bool'=>true,'error_text'=>'');
        }
        //prefecture_en
        if($type === 'prefecture_en'){
            $s_length = mb_strlen($subject);
            $match = preg_match("/^[0-9]+$/", $subject);
            if($s_length == 0){
                if($requred){
                    return array('bool'=>false,'error_text'=>'This field is required. Please select.');
                }else{
                    return array('bool'=>true,'error_text'=>'');
                }
            }else{
                if($s_length > $length){
                    return array('bool'=>false,'error_text'=>'入力可能文字数を超えています。');
                }else if(!$match){
                    return array('bool'=>false,'error_text'=>'Please select.');
                }
            }
            return array('bool'=>true,'error_text'=>'');
        }
    	//email
       	if($type === 'email'){
    		$s_length = mb_strlen($subject);
    		$match = preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $subject);
    		if($s_length == 0){
    			if($requred){
    				return array('bool'=>false,'error_text'=>$subject_text.'が入力されていません。');
    			}else{
    				return array('bool'=>true,'error_text'=>'');
    			}
    		}else{
    			if($s_length > $length){
    				return array('bool'=>false,'error_text'=>'入力可能文字数を超えています。');
    			}else if(!$match){
    				return array('bool'=>false,'error_text'=>'入力形式を確認してください。');
    			}
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
    	//email English
       	if($type === 'email_en'){
    		$s_length = mb_strlen($subject);
    		$match = preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $subject);
    		if($s_length == 0){
    			if($requred){
					return array('bool'=>false,'error_text'=>'This field is required. Please enter a value.');
    			}else{
    				return array('bool'=>true,'error_text'=>'');
    			}
    		}else{
    			if($s_length > $length){
    				return array('bool'=>false,'error_text'=>'Maximum Allowed: '.$length.' characters.');
    			}else if(!$match){
    				return array('bool'=>false,'error_text'=>'Please enter a valid email address.');
    			}
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
    	//postal_code
    	if($type === 'postal_code'){
    		$s_length = mb_strlen($subject);
    		if($s_length == 7){
    			$match = preg_match("/^\d{7}$/", $subject);
    		}elseif($s_length == 8){
    			$match = preg_match("/^\d{3}\-\d{4}$/", $subject);
    		}else{
    			return array('bool'=>false,'error_text'=>'入力形式を確認してください。');
    		}
    		if(!$match){
    			return array('bool'=>false,'error_text'=>'入力形式を確認してください。');
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
    	//tel
    	if($type === 'tel'){
    		$s_length = mb_strlen($subject);
    		$match = preg_match("/^0\d{1,4}-\d{1,4}-\d{4}$/", $subject);
    		if($requred && $s_length == 0){
    			return array('bool'=>false,'error_text'=>'入力してください。');
    		}
    		if(!$match){
    			$match2 = preg_match("/^0\d{9,10}$/", $subject);
    			if($s_length > 0 && !$match2){
    				return array('bool'=>false,'error_text'=>'入力形式を確認してください。');
    			}
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
    	//tel international
    	if($type === 'tel_i'){
    		$s_length = mb_strlen($subject);
    		$match = preg_match("/^[!-@≠\[-`{-~]+$/", $subject);
    		if($s_length == 0){
    			if($requred){
    				return array('bool'=>false,'error_text'=>$subject_text.'が選択されていません。');
    			}else{
    				return array('bool'=>true,'error_text'=>'');
    			}
    		}else{
    			if($s_length > $length){
    				return array('bool'=>false,'error_text'=>'入力可能文字数を超えています。');
    			}else if(!$match){
    				return array('bool'=>false,'error_text'=>'入力形式を確認してください。');
    			}
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
    	//int international Engl
    	if($type === 'tel_i_en'){
    		$s_length = mb_strlen($subject);
    		$match = preg_match("/^[!-@≠\[-`{-~]+$/", $subject);
    		if($s_length == 0){
    			if($requred){
					return array('bool'=>false,'error_text'=>'This field is required. Please enter a value.');
    			}else{
    				return array('bool'=>true,'error_text'=>'');
    			}
    		}else{
    			if($s_length > $length){
    				return array('bool'=>false,'error_text'=>'Maximum Allowed: '.$length.' characters.');
    			}else if(!$match){
    				return array('bool'=>false,'error_text'=>'Please enter a valid phone number.');
    			}
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
		//radio
    	if($type === 'radio'){
    		if( (!isset($subject) || $subject === "" ) && $requred ){
    			return array('bool'=>false,'error_text'=>'選択してください。');
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
    	//select
    	if ($type == 'select') {
    		if( (!isset($subject) || $subject === "" || $subject == "0" || $subject == "-1") && $requred ){
    			return array('bool'=>false,'error_text'=>'選択してください。');
    		}
       		return array('bool'=>true,'error_text'=>'');
    	}
        //url
        if($type === 'url'){
            $s_length = mb_strlen($subject);
            $match = preg_match("/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/", $subject);
            if($requred && $s_length == 0){
                return array('bool'=>false,'error_text'=>'入力してください。');
            }
            if(!$match){
                $match2 = preg_match("/^0\d{9,10}$/", $subject);
                if($s_length > 0 && !$match2){
                    return array('bool'=>false,'error_text'=>'入力形式を確認してください。');
                }
            }
            return array('bool'=>true,'error_text'=>'');
        }
    	//age
    	if($type === 'age'){
    	    $s_length = mb_strlen($subject);
    		$match = preg_match("/^[0-9]+$/", $subject);
    		if($match){
    			if($requred && $s_length == 0){
    				return array('bool'=>false,'error_text'=>'入力してください。');
    			}elseif($s_length > $length){
    				return array('bool'=>false,'error_text'=>'入力可能文字数を超えています。');
    			}elseif($subject < $start_range || $subject > $end_range){
    				return array('bool'=>false,'error_text'=>$start_range.'歳以上'.$end_range.'歳以下の方が対象となります。');
    			}
    		}else{
    			return array('bool'=>false,'error_text'=>'数字を入力してください。');
    		}
    		return array('bool'=>true,'error_text'=>'');
    	}
    }
}
?>