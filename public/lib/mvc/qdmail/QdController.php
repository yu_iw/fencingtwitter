<?php

require_once('qdmail.php');
require_once('qdsmtp.php');

class QdController
{
	private static $mailprof;

	public static function setMailprof($mailprof)
	{
		self::$mailprof = $mailprof;
	}

    public function sendMail($mailto, $subject, $message)
    {
    	mb_language("japanese");
    	mb_internal_encoding("UTF-8");

    	$param = self::$mailprof;

    	$mail = new Qdmail();

    	$mail->to($mailto);
    	$mail->subject($subject);
    	$mail->from($param['from']);
    	$mail->text($message);
//     	$mail->attach($attach);
//     	$mail->simpleAttach(true);

    	$qdsmtp = new Qdsmtp();
    	$qdsmtp->server($param);
    	$qdsmtp = $mail->smtpObject();
    	$qdsmtp->pop3UseFile(false);

		$mail->setSmtpObject($qdsmtp);

    	return $mail->send();

    }

    public function sendMailWithAttach($mailto, $subject, $message, $attach = null){

    	mb_language("japanese");
    	mb_internal_encoding("UTF-8");

    	$param = self::$mailprof;

    	$mail = new Qdmail();

    	$mail->to($mailto);
    	$mail->subject($subject);
    	$mail->from($param['from']);
    	$mail->text($message);
    	$mail->attach($attach);
    	$mail->simpleAttach(true);

//     	$mail->smtp(true);

    	$qdsmtp = new Qdsmtp();
    	$qdsmtp->server($param);
    	$qdsmtp = $mail->smtpObject();
    	$qdsmtp->pop3UseFile(false);

		$mail->setSmtpObject($qdsmtp);

    	$mail->send();

        return $mail -> errorStatment();
    }


    public function sendMailWithAttach2($mailto, $subject, $message, $attach = null){

        mb_language("japanese");
        mb_internal_encoding("UTF-8");

//         $param = array(
//                 'host'=>'mail.grpht.co.jp',
//                 'port'=>25,
//                 'from'=>'no_reply@ikumen-project.jp',
//                 'user'=>'kazuyuki@grpht.co.jp',
//                 'pass'=>'Bys1Gzar',
//                 'protocol'=>'SMTP'
//         );

//         $mail = new Qdmail();

//         $mail->to($mailto);
//         $mail->subject($subject);
//         $mail->from($param['from']);
//         $mail->text($message);
//         $mail->attach($attach);
//         $mail->simpleAttach(true);

// //      $mail->smtp(true);

//         $qdsmtp = new Qdsmtp();
//         $qdsmtp->server($param);
//         $qdsmtp = $mail->smtpObject();
//         $qdsmtp->pop3UseFile(false);

//         $mail->setSmtpObject($qdsmtp);

//         $mail->send();

//         return $mail -> errorStatment();

        $headers = 'From: webmaster@example.com' . "\r\n" .
            'Reply-To: webmaster@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($mailto, $subject, $message, $headers);

        return "success";

//      qd_send_mail('text',$mailto,$subject,$message,$param['from'],$attach);
    }
}

?>
