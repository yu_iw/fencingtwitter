<?php

class Functions
{
	/*public function h($s) { //htmlspecialchars
		return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
	}*/
	public function h($s) {
		if (is_array($s)) {
			return array_map(array($this, 'myhtmlspecialchars'), $s);
		} else {
			return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
		}
	}

}


?>
