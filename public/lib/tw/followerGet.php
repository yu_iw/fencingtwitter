<?php

class followerGet{

	public function followerGet($screen_name){

		session_start();
		require_once('twitteroauth.php');
		//設定ファイルの読み込み
		$ini = parse_ini_file('twitter.ini');

		define('CONSUMER_KEY', $ini['CONSUMER_KEY']);
		define('CONSUMER_SECRET', $ini['CONSUMER_SECRET']);
		define('ACCESS_TOKEN', $ini['ACCESS_TOKEN']);
		define('ACCESS_TOKEN_SECRET', $ini['ACCESS_TOKEN_SECRET']);

		// request token取得
		$tw = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

		$vRequest = $tw->OAuthRequest("http://api.twitter.com/1/friends/ids.xml","GET",array('screen_name' => $screen_name));

		//XMLデータをsimplexml_load_string関数を使用してオブジェクトに変換
		$oXml = simplexml_load_string($vRequest);

		return $oXml;

	}



}


?>