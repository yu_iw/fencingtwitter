<?php
require_once('twitteroauth/autoload.php');
use Abraham\TwitterOAuth\TwitterOAuth;
class getToken{
	public function getTwitterToken($callback_url){
		//callback_url:Twitterからの戻り先
		session_start();
		//設定ファイルの読み込み
		$ini = parse_ini_file('twitter.ini');
		define('CONSUMER_KEY', $ini['CONSUMER_KEY']);
		define('CONSUMER_SECRET', $ini['CONSUMER_SECRET']);
		define('CALLBACK_URL', $callback_url);
		// request token取得
		$tw = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
		$token = $tw->oauth('oauth/request_token', array('oauth_callback' => CALLBACK_URL));
		if(!isset($token['oauth_token'])){
			echo "error: getRequestToken\n";
			exit;
		}
		// callback.php で使うので session に突っ込む
		$_SESSION['oauth_token']        = $token['oauth_token'];
		$_SESSION['oauth_token_secret'] = $token['oauth_token_secret'];
		// 認証用URL取得してredirect
		$authURL = $tw->url('oauth/authenticate', array('oauth_token' => $token['oauth_token']));
		header("Location: " . $authURL);
	}
}
?>