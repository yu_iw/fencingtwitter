<?php
require_once('twitteroauth/autoload.php');
use Abraham\TwitterOAuth\TwitterOAuth;
class tweetImage{

	public function tweetMessageAndImage($message, $image){

		session_start();
		// require_once('twitteroauth.php');
		//設定ファイルの読み込み
		$ini = parse_ini_file('twitter.ini');

		define('CONSUMER_KEY', $ini['CONSUMER_KEY']);
		define('CONSUMER_SECRET', $ini['CONSUMER_SECRET']);
		define('ACCESS_TOKEN', $_SESSION['oauth_token']);
		define('ACCESS_TOKEN_SECRET', $_SESSION['oauth_token_secret']);

		// request token取得
		$tw = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

		$mediaId = $tw->upload("media/upload", array("media" => $image));
		$parameters = array(
			'status' => $message,
			'media_ids' => $mediaId->media_id_string,
		);
		$result = $tw->post('statuses/update', $parameters);

		return $result;
	}



}


?>