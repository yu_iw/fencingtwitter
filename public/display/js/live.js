/* ++++++++++++++++++++++++++++++++++++++++

	Display

++++++++++++++++++++++++++++++++++++++++ */

$(function(){
//BOS

/* ----------------------------------------
	Global Variables
---------------------------------------- */

var $window = $(window),
	imgPath = 'img/',
	cur = {
		nameL: '',
		nameR: '',
		nationL: '',
		nationR: '',
		scoreL: -1,
		scoreR: -1,
		yellowL: -1,
		yellowR: -1,
		redL: -1,
		redR: -1,
		status: '',
		timeRemain: '0:00',
		Round: ''
	};

//$('#statusWrap>div, #timeWrap>div').width($(window).width()-$('.score').eq(0).width()*2-6*2);


/* ----------------------------------------
	fixfont
---------------------------------------- */

var ff = {
		$el: $('.fixfont'),
		loaded: false,
		num: 0,
		len: $('.fixfont').length,
		fnc: function($el){
			$el.each(function(){
				var $this = $(this),
					box ={
						wdt: $this.width(),
						hgt: $this.height(),
					},
					$text = $this.children(),
					text = {
						wdt: $text.width(),
						hgt: $text.height()
					},
					fz = parseInt($this.css('font-size')),
					n = 0;
				//if(window.console && typeof window.console.log==='function') console.log(box.hgt+'font-size:'+fz+' > '+text.hgt);
				while(text.hgt<=box.hgt && text.wdt<=box.wdt && n<200){
					fz ++;
					$this.css({fontSize: fz+'px'});
					text.wdt = $text.width();
					text.hgt = $text.height();
					n++;
				}
				$this.css({fontSize: fz-1+'px'});
				ff.num++;
				if(ff.num>=ff.len){
					if(!ff.loaded){
						ff.loaded = true;
						ff.$el.find('>div>div:not(.stable)').text('');
					}
					ff.$el.css({opacity: 1});
				}
			});
		}
	};

$window.load(function(){
	ff.fnc(ff.$el);
});


/* ----------------------------------------
	Config
---------------------------------------- */

if(typeof config!=='undefined'){
if(window.console && typeof window.console.log==='function') console.log(config);
	$('#nameL').addClass('stable').text(config.name.left);
	$('#nameR').addClass('stable').text(config.name.right);
}


/* ----------------------------------------
	Web Socket
---------------------------------------- */

// エラー処理
ws.onerror = function(e){
	$('#log').text('サーバに接続できませんでした。');
}

// WebSocketサーバ接続イベント
ws.onopen = function(){
	// 入室情報を文字列に変換して送信
	ws.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};

// メッセージ受信イベントを処理
ws.onmessage = function(event){
	var data = JSON.parse(event.data);
//debug
	if(data.type==='join'){
		$('#log p').text(data.user+'が入室しました。');
	}else if(data.type==='defect'){
		$('#log p').text(data.user+'が退室しました。');
/*
debug*/
	}else if(data.type==='admin'){
		console.log(data);
	//選手データ受信
		$('#nameL').text(data.nameL);
		$('#nationL').text(data.nationL);
		$('#flagL').attr('data-nation', data.nationL.toLowerCase());
		$('#nameR').text(data.nameR);
		$('#nationR').text(data.nationR);
		$('#flagR').attr('data-nation', data.nationR.toLowerCase());
		$('#nameLWrap .fixfont').removeAttr('style');
		ff.fnc($('#nameLWrap .fixfont'));
		$('#nameRWrap .fixfont').removeAttr('style');
		ff.fnc($('#nameRWrap .fixfont'));
		$('#portraitL img').attr('src',  'img/portrait/'+data.nameIdL.replace('name','img')+'.jpg');
		$('#portraitR img').attr('src',  'img/portrait/'+data.nameIdR.replace('name','img')+'.jpg');
//debug
	$('#logPlayerL').text(data.nameL);
	$('#logNationL').text(data.nationL);
	$('#logPlayerR').text(data.nameR);
	$('#logNationR').text(data.nationR);
/*
debug*/
	}else if(data.type==='data'){
	//試合中データ受信
		if(ff.loaded){
/*
			if(cur.nameL!=data.nameL){
				cur.nameL = data.nameL;
				$('#nameL').text(cur.nameL);
			}
			if(cur.nationL!=data.nationL){
				cur.nationL = data.nationL;
//				nFlagL.src = imgPath+'flag/'+cur.nationL.toLowerCase()+'.png';
				$('#nationL').text(cur.nationL);
			}
*/
			if(data.priorityL == 1){
				$('#priorityL').addClass('on');
			}else if(data.priorityL === 0){
				$('#priorityL').removeClass('on');
			}else{
                //
			}
			if(data.invalidL == 1){
				$('#scoreBoxL').addClass('invalid');
			}else if(data.invalidL === 0){
				$('#scoreBoxL').removeClass('invalid');
			}else{
                //
			}
			if(data.hitL == 1){
				$('#scoreBoxL').addClass('on');
			}else if(data.hitL === 0){
				$('#scoreBoxL').removeClass('on');
			}else{
                //
			}
			if(cur.scoreL != data.scoreL && data.scoreL != ""){
				cur.scoreL = data.scoreL;
				$('#scoreL').text(cur.scoreL);
			}else if(data.scoreL == "" && cur.scoreL >= 0){
				$('#scoreL').text(cur.scoreL);
			}else {
                //
			}
			console.log(data.priorityL);
			console.log(data.priorityR);
			if(data.priorityR == 1){
				$('#priorityR').addClass('on');
			}else if(data.priorityR === 0){
				$('#priorityR').removeClass('on');
			}else{
                //
			}
			if(data.invalidR == 1){
				$('#scoreBoxR').addClass('invalid');
			}else if(data.invalidR === 0){
				$('#scoreBoxR').removeClass('invalid');
			}else{
                //
			}
			if(data.hitR == 1){
				$('#scoreBoxR').addClass('on');
			}else if(data.hitR === 0){
				$('#scoreBoxR').removeClass('on');
			}else{
                //
			}
			if(cur.scoreR != data.scoreR && data.scoreR != ""){
				cur.scoreR = data.scoreR;
				$('#scoreR').text(cur.scoreR);
			}else if(data.scoreR == "" && cur.scoreR >= 0){
				$('#scoreR').text(cur.scoreR);
			}else {
                //
			}
/*
			if(cur.status!=data.status){
				cur.status = data.status;
				$('#status').text(cur.status);
			}
*/
			if(data.yellowL > 0){
				$('#yellowcardL').addClass('on');
			}else if(data.yellowL === "0"){
				$('#yellowcardL').removeClass('on');				
			}else {
                //
			}
			if(data.yellowR > 0){
				$('#yellowcardR').addClass('on');
			}else if(data.yellowR === "0"){
				$('#yellowcardR').removeClass('on');				
			}else {
                //
			}
			if(data.redL > 0){
				$('#redcardL').addClass('on');
			}else if(data.redL === "0"){
				$('#redcardL').removeClass('on');
			}else {
                //
			}
			if(data.redR > 0){
				$('#redcardR').addClass('on');
			}else if(data.redR === "0"){
				$('#redcardR').removeClass('on');
			}else {
                //
			}

			if(cur.timeRemain != data.timeRemain && data.timeRemain != ""){
				cur.timeRemain = data.timeRemain;
				$('#time').text(cur.timeRemain);
			}else if(data.timeRemain == "" && cur.timeRemain > 0){
				$('#time').text(cur.timeRemain);
			}else {
                //
			}
			if(cur.round != data.round && data.round > 0){
				cur.round = data.round;
				$('#round').text(cur.round);
				$('.roundDeco').addClass("on");
				//console.log("R"+cur.round);
			}else if(data.round == "" && cur.round > 0){
				$('#round').text(cur.round);
				$('.roundDeco').addClass("on");
				//console.log("R"+cur.round);
			}else {
			}
		}
//debug
	$('#logDate').text(data.time);
	$('#logScoreL').text(data.scoreL);
	$('#logYellowL').text(data.yellowL);
	$('#logRedL').text(data.redL);
	$('#logScoreR').text(data.scoreR);
	$('#logYellowR').text(data.yellowR);
	$('#logRedR').text(data.redR);
	$('#logTime').text(data.timeRemain);
	$('#logRound').text(data.round);
/*
debug*/
	}else if (data.type==='control'){
		$('#log p').text('キー入力');
	}else{
		$('#log p').text('不正なメッセージを受信しました');
	}
};

/*
// 操作イベント
document.onkeydown = function(event){
	// 単一キーを全体に送信
	ws.send(JSON.stringify({
		type: 'control',
		user: self.name,
		keyCode: event.keyCode
	}));
};
*/
//表示テスト
/*$(window).load(function(){
	$('#logDate').text("test");
	$('#logScoreL').text("test");
	$('#logYellowL').text("test");
	$('#logRedL').text("test");
	$('#logScoreR').text("test");
	$('#logYellowR').text("test");
	$('#logRedR').text("test");
	$('#logTime').text("90");
	$('#logRound').text("1");
	$('#scoreL').text("91");
	$('#scoreR').text("25");
	$('#time').text("3:50");
	$('#round').text("1");
	$('#priorityR').addClass('on');
	$('#priorityL').addClass('on');
	$('#redcardL').addClass('on');
	$('#redcardR').addClass('on');
	$('#yellowcardL').addClass('on');
	$('#yellowcardR').addClass('on');
	$('#nameL').text("選手名 ああ");
	$('#nameR').text("選手名 右欄");
});*/
	
// ブラウザ終了イベント
window.onbeforeunload = function(){
	ws.send(JSON.stringify({
		type: 'defect',
		user: self.name,
	}));
};

//EOS
});


    $('html').keydown(function(e){
		var i;
		console.log(e.which);
        switch(e.which){
			case 77:
				if($('.roundMax').text() == '2'){
					$('.roundMax').text('3')
				}else{
					$('.roundMax').text('2')
				}
				break;
            case 87: // Key[W]
				$('.roundInner').show();
				$('.roundAdd').show();
				$('.roundDeco').show();
				$('.roundEx').hide();
            break;

            case 69: // Key[E]
				$('.roundInner').hide();
				$('.roundAdd').hide();
				$('.roundDeco').hide();
				$('.roundEx').text('ET');
				$('.roundEx').show();
            break;

            case 79: // Key[O]
				$('.roundInner').hide();
				$('.roundAdd').hide();
				$('.roundDeco').hide();
				$('.roundEx').text('OT');
				$('.roundEx').show();
            break;
            case 49: // Key[1]
				i = 1;
				name(i,'l');
				name(i,'r');
				portrait(i,'l');
				portrait(i,'r');
            break;

            case 50: // Key[2]
				i = 2;
				name(i,'l');
				name(i,'r');
				portrait(i,'l');
				portrait(i,'r');
            break;

            case 51: // Key[3]
				i = 3;
				name(i,'l');
				name(i,'r');
				portrait(i,'l');
				portrait(i,'r');
            break;

            case 52: // Key[4]
				i = 4;
				name(i,'l');
				name(i,'r');
				portrait(i,'l');
				portrait(i,'r');
            break;

            case 53: // Key[5]
				i = 5;
				name(i,'l');
				name(i,'r');
				portrait(i,'l');
				portrait(i,'r');
            break;

            case 54: // Key[6]
				i = 6;
				name(i,'l');
				name(i,'r');
				portrait(i,'l');
				portrait(i,'r');
            break;

            case 55: // Key[7]
				i = 7;
				name(i,'l');
				name(i,'r');
				portrait(i,'l');
				portrait(i,'r');
            break;

            /*case 56: // Key[8]
			i = 8;
				portrait(i,'l');
				//portrait(i,'r');
            break;

            case 57: // Key[9]
				i = 9;
				portrait(i,'l');
				portrait(i,'r');
			break;*/
			
            case 48: // Key[0]
			i = 0;
			$('#portraitL').css('opacity',0);
			$('#portraitR').css('opacity',0);
			$('#nameLWrap').css('opacity',0);
			$('#nameRWrap').css('opacity',0);
		break;
        }
	});
	function portrait(i,d){
		//console.log('test');
		var src = "";
		src = 'img/portrait/portrait'+i+'_'+d+'.jpg'
		if(d == 'l'){
			//src = 'img/portrait'+i+'_l.png'
			$('#portraitL img').attr('src', src);
			if(UrlExists(src)){
				$('#portraitL').css('opacity',1);
				//console.log("lt");
			}else{
				$('#portraitL').css('opacity',0);
			//	console.log("lf");
			}
		}else if(d == 'r'){
			//src = 'img/portrait'+i+'_r.png'
			$('#portraitR img').attr('src', src);
			if(UrlExists(src)){
				$('#portraitR').css('opacity',1);
			//	console.log("rt");
			}else{
				$('#portraitR').css('opacity',0);
			//	console.log("rf");
			}
		}else{
		}
		//console.log(src);
		//console.log(d);
		//console.log(i);
	}
	function name(i,d){
		//console.log('test');
		var src = "";
		src = 'img/name/name'+i+'_'+d+'.png'
		if(d == 'l'){
			//src = 'img/portrait'+i+'_l.png'
			$('#nameLWrap img').attr('src', src);
			if(UrlExists(src)){
				$('#nameLWrap').css('opacity',1);
				//console.log("lt");
			}else{
				$('#nameLWrap').css('opacity',0);
				//console.log("lf");
			}
		}else if(d == 'r'){
			//src = 'img/portrait'+i+'_r.png'
			$('#nameRWrap img').attr('src', src);
			if(UrlExists(src)){
				$('#nameRWrap').css('opacity',1);
				$('#nameRWrap').removeClass('short');
				//console.log("rt");
			}else{
				$('#nameRWrap').css('opacity',0);565
				//console.log("rf");
			}
		}else{
		}
		//console.log(src);
		//console.log(d);
		//console.log(i);
	}
function UrlExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    //http.send();
    return http.status!=404;
}

function portraitExist(src){
	var result;
	src = 'http://stragetest.grpht.co.jp/abema/'+src;
	console.log(src);
	$.ajax({
		url: src,
		type:'HEAD',
		error: function()
		{
			//file not exists
			result = false;
		},
		success: function()
		{
			result = true;
		}
	});
	return result;
}

/* ----------------------------------------
	Idling Mode
---------------------------------------- */

document.onkeydown = function(e){
//console.log(e);
	if(e.key=='Enter') $('#idling').toggleClass('on');
	/*if(e.key=='s'){
		var srcL = $('#portraitL img').attr('src');
		var srcR = $('#portraitR img').attr('src');
		$('#portraitL img').attr('src', srcR);
		$('#portraitR img').attr('src', srcL);
	}*/
};


/* debug */
$('.hitTestL').click(function(){
			$('#scoreBoxL').toggleClass('on');
});
$('.hitTestR').click(function(){
			$('#scoreBoxR').toggleClass('on');
});