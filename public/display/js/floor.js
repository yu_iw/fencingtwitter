/* ++++++++++++++++++++++++++++++++++++++++

	LED

++++++++++++++++++++++++++++++++++++++++ */

$(function(){
//BOS

/* ----------------------------------------
	Global Variables
---------------------------------------- */

var $window = $(window),
	$player = {
		l: $('#playerL'),
		r: $('#playerR')
	};


/* ----------------------------------------
	Web Socket
---------------------------------------- */

// エラー処理
ws.onerror = function(e){
	console.log('サーバに接続できませんでした。');
}

// WebSocketサーバ接続イベント
ws.onopen = function(){
	// 入室情報を文字列に変換して送信
	ws.send(JSON.stringify({
		type: 'join',
		user: self.name
	}));
};

// メッセージ受信イベントを処理
ws.onmessage = function(event){
	var data = JSON.parse(event.data);
	if(data.type==='data'){
		if(data.invalidL){
			$player.l.addClass('invalid');
		}else{
			$player.l.removeClass('invalid');
		}
		if(data.hitL){
			$player.l.addClass('on');
		}else{
			$player.l.removeClass('on');
		}
		if(data.invalidR){
			$player.r.addClass('invalid');
		}else{
			$player.r.removeClass('invalid');
		}
		if(data.hitR){
			$player.r.addClass('on');
		}else{
			$player.r.removeClass('on');
		}
	}
}

// ブラウザ終了イベント
window.onbeforeunload = function(){
	ws.send(JSON.stringify({
		type: 'defect',
		user: self.name,
	}));
};


/* ----------------------------------------
	AutoPlay
---------------------------------------- */

var $sec = $('#playerL, #playerR'),
	program = [
		['', ''],
		['on', ''],
		['', 'on'],
		['on', 'on'],
		['on', 'invalid'],
		['invalid', 'on'],
		['invalid', 'invalid'],
		['invalid', ''],
		['', 'invalid']
	],
	cnt = 0,
	len = program.length,
	time = 1000,
	flg = {
		p: true,
		loop: false,
		key: false
	},
	fnc = function(){
		if(!flg.loop){
			flg.loop = true;
			$sec.eq(0)[0].className = program[cnt%len][0];
			$sec.eq(1)[0].className = program[cnt%len][1];
			cnt++;
			setTimeout(function(){
				flg.loop = false;
				flg.key = false;
				if(flg.p) fnc();
			}, time);
		}
	};
fnc();


/* ----------------------------------------
	Mode
---------------------------------------- */

document.onkeydown = function(e){
console.log(e);
	if(e.key=='Enter') $('#idling').toggleClass('on');
	else if(e.key=='m') $('body').toggleClass('responsive');
	else if(e.key=='i') $('#info').fadeToggle(300);
	else if(e.key=='p'){
		if(!flg.key){
			flg.key = true;
			flg.p = !flg.p;
			fnc();
		}
	}
};

//EOS
});