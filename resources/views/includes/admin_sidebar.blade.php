<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('/x_wedaelc/index') }}" class="site_title"><i class="fa fa-cogs"></i> <span>CONSOLE</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="{{ Gravatar::src(Auth::user()->email) }}" alt="Avatar of {{ Auth::user()->name }}" class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>ようこそ,</span>
                <h2>{{ Auth::user()->name }}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>管理</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="{{ url('/x_wedaelc/entryList') }}">
                            <i class="fa fa-list"></i>応募者一覧
                        </a>
                    </li>
                        <li>
                            <a href="{{ url('/x_wedaelc/entryListTw') }}">
                                <i class="fa fa-list"></i>Twitter応募者一覧
                            </a>
                        </li>
                    <li>
                        <a href="{{ url('/x_wedaelc/download') }}">
                            <i class="fa fa-download"></i>EXCEL ダウンロード
                        </a>
                    </li>
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->
    </div>
</div>
