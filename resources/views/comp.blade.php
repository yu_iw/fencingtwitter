@extends('layouts.entry')
@section('main_container')
    <div class="contents_inner">
        <section id ="comp_area">
            <div class="inner">
                <img src="img/product.png" alt="cleardew製品画像" class="product">
                <p class="thanks pink">ご応募ありがとうございました。</p>
            </div>
        </section>
        <div class="btn_area">
            <a href="/cleadew/index" class="back">
                <img src="img/top_btn.png" alt="トップページに戻る" class="pc">
                <img src="img/top_btn_sp.png" alt="トップページに戻る" class="sp">
            </a>
        </div>
    </div>
@endsection