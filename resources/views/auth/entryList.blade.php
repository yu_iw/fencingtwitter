@extends('layouts.admin_blank')
@push('scripts')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/dataTables.material.min.js"></script>
    <script type="text/javascript">
        $(function() {
            if(getParam('prize') != null){
              var paramPrize = getParam('prize');
            }else{
              var paramPrize = 1;
            }
            $('.btn-prize').eq(paramPrize-1).addClass('btn-primary');
            function getParam(name, url) {
                if (!url) url = window.location.href;
                name = name.replace(/[\[\]]/g, "\\$&");
                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));
            }
            $('#dataTable').DataTable({
                "order": [ 4, 'asc' ],
                "pageLength": 100
            });
            $('#check100').on('click', function(){
                var notChecked = $('#dataTable input:not(:checked)').slice(0, 100);
                notChecked.prop("checked",true);
            });

        });
    </script>
@endpush
@section('main_container')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
                <h3>LIST</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-list"></i> 応募者一覧</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="display: block;">
                    <form action="{{url('/x_wedaelc/updateEntry')}}" method="post">
                        メールテンプレート：<br>
                        {{Form::select('mail_template',$mailArray)}}
                        <br><br>
                        <p class="text-muted font-13 m-b-30">
                            チェックボタンを切り替えて更新ボタンを押下すると送信されます。<br>
                            <span id="check100" class="btn btn-default">表示されている100件にチェックを入れる</span>
                        </p>
                        <p class="text-muted font-13 m-b-30">
                            賞品絞り込み<br>
                            <a href="?prize=1" class="btn btn-prize btn-default">A ツープラスワン</a>
                            <a href="?prize=2" class="btn btn-prize btn-default">B ヘアカラートリートメント</a>
                            <a href="?prize=3" class="btn btn-prize btn-default">C リタッチコンシーラー</a>
                            <a href="?prize=4" class="btn btn-prize btn-default">D ハワイアンダンス</a>
                            <a href="?prize=5" class="btn btn-prize btn-default">E 社交ダンス</a>
                        </p>
                        {{ csrf_field() }}
                        <table id="dataTable" class="table mdl-data-table dataTable no-footer">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>名前</th>
                                    <th>賞品</th>
                                    <th>メールアドレス</th>
                                    <th>当選月</th>
                                    <th>DB挿入日時</th>
                                    <th>最終送信日時</th>
                                    <th>送信</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($entrys as $entry)
                                <tr class="@if($entry->duplicated) duplicated @endif">
                                    <td>{{$entry->id}}</td>
                                    <td>{{$entry->name}}</td>
                                    <td>
                                          @if($entry->prize == 1) A ツープラスワン<br> @endif
                                          @if($entry->prize == 2) B ヘアカラートリートメント<br> @endif
                                          @if($entry->prize == 3) C リタッチコンシーラー<br> @endif
                                    </td>
                                    <td>{{$entry->email}}
                                        <a href="mailto:{{$entry->email}}">&nbsp;<i class="fa fa-envelope-o"></i></a></td>
                                    <td>{{$entry->month}}月</td>
                                    <td>{{$entry->created_at}}</td>
                                    <td>{{$entry->updated_at}}</td>
                                    <td>
                                      <input type="checkbox" name="checked[]" value="{{$entry->id}}">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                          <input type="submit" class="btn btn-success" style="width: 30%;">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /page content -->
@endsection
