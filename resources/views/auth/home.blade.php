@extends('layouts.admin_blank')
@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
                <h3>{{ Auth::user()->name }}さん</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row top_tiles">
            <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="count">{{$info['countSns']}}<small>人</small></div>
                    <h3 class="green">Twitter応募件数</h3>
                    <p>投稿＆フォロー済みのTwitter応募件数です</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="count">{{$info['countTag1']}}<small>人</small></div>
                    <p>#ツープラスワンヘアカラークリームタイプ</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="count">{{$info['countTag2']}}<small>人</small></div>
                    <p>#ヘアカラートリートメント</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="count">{{$info['countTag3']}}<small>人</small></div>
                    <p>#リタッチコンシーラー</p>
                </div>
            </div>
      	</div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-flag"></i> お知らせ</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="display: block;">
                        <ul class="list-unstyled timeline widget">
                            <li>
                                <div class="block">
                                    <div class="block_content">
                                        <h2 class="title">
                                            <a>アカウントを作成しました。</a>
                                        </h2>
                                        <div class="byline">
                                            <span>2018.10.05</span></a>
                                        </div>
                                        <p class="excerpt">
                                            追加する場合は<a href="mailto:kazuyuki@grpht.co.jp">kazuyuki@grpht.co.jp</a>までご連絡ください。
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /page content -->
@endsection
