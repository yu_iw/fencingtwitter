@extends('layouts.admin_blank')
@push('scripts')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/dataTables.material.min.js"></script>
    <script src="{{ asset('js/clipboard.js') }}"></script>

@endpush
@section('main_container')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
                <h3>LIST</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <!--<div class="row top_tiles">
            <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="count">{{$info['countSns']}}<small>人</small></div>
                    <h3 class="green">Twitter応募件数</h3>
                    <p>投稿＆フォロー済みのTwitter応募件数です</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="count">{{$info['countTag1']}}<small>人</small></div>
                    <p>#ツープラスワンヘアカラークリームタイプ</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="count">{{$info['countTag2']}}<small>人</small></div>
                    <p>#ヘアカラートリートメント</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="count">{{$info['countTag3']}}<small>人</small></div>
                    <p>#リタッチコンシーラー</p>
                </div>
            </div>
      	</div>--.

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-list"></i> 応募者一覧</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="display: block;">
                        <!--<p class="text-muted font-13 m-b-30">
                            賞品絞り込み<br>
                            <a href="?prize=1" class="btn btn-prize btn-default">A ツープラスワン</a>
                            <a href="?prize=2" class="btn btn-prize btn-default">B ヘアカラートリートメント</a>
                            <a href="?prize=3" class="btn btn-prize btn-default">C リタッチコンシーラー</a>
                        </p>-->
                        <form action="{{url('/x_wedaelc/updateEntry')}}" method="post">
                        {{ csrf_field() }}
                        <table id="dataTable" class="table mdl-data-table dataTable no-footer">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Twitter</th>
                                    <th>本文</th>
                                    <th>ハッシュタグ</th>
                                    <th>応募日時</th>
                                    <th>DirectMessage</th>
                                    <th>送信済</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($entrys as $entry)
                                <tr class="@if($entry->duplicated) duplicated @endif">
                                    <td>{{$entry->id}}</td>
                                    <!--<td>{{$entry->last_nm}}&nbsp;{{$entry->first_nm}}
                                        <a href="mailto:{{$entry->email}}">&nbsp;<i class="fa fa-envelope-o"></i></a>
                                    </td>-->
                                    <td>  <a href="https://twitter.com/intent/user?user_id={{$entry->user_id}}" target="_blank">{{'@'}}{{$entry->screen_name}}</a>
                                    </td>
                                    <td>
                                         {{$entry->text}}
                                    </td>
                                    <td><a target="_blank" href="https://twitter.com/{{$entry->screen_name}}/status/{{$entry->post_id}}">{{date("Y-m-d H:i:s",$entry->posted_at)}}</a></td>
                                    <td>
                                    <?php $week = array( "日", "月", "火", "水", "木", "金", "土" ); ?>
                                      <button type="button" class="btn btn-primary sendDm" data-prize data-dm="" data-screen-name="{{$entry->screen_name}}"  data-id="{{$entry->id}}">DMを送る</button>
                                    </td>
                                    <td style="font-size: 24px">
                                         @if($entry->checked) ✓ @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            <!--<input type="submit" class="btn btn-success" style="width: 30%;">-->
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /page content -->
@endsection
