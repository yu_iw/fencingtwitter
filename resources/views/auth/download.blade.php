@extends('layouts.admin_blank')
@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
                <h3>DOWNLOAD</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-download"></i> EXCEL ダウンロード</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="display: block;">
                        <p class="text-muted font-13 m-b-30">
                            Twitter応募者をダウンロードします。
                        </p>
                        <div class="text-center">
                            <a class="btn btn-app" href="{{url('/x_wedaelc/excel_twitter')}}">
                                <i class="fa fa-download"></i> Twitter応募者 ダウンロード
                            </a>
                        </div>
                    </div>
                    <div class="x_content" style="display: block;">
                        <p class="text-muted font-13 m-b-30">
                            当選者一覧をダウンロードします。
                        </p>
                        <div class="text-center">
                            <a class="btn btn-app" href="{{url('/x_wedaelc/excel_winners')}}">
                                <i class="fa fa-download"></i> 当選者一覧 ダウンロード
                            </a>
                        </div>
                    </div>
                    <div class="x_content" style="display: block;">
                        <p class="text-muted font-13 m-b-30">
                            未入力者一覧をダウンロードします。
                        </p>
                        <div class="text-center">
                            <a class="btn btn-app" href="{{url('/x_wedaelc/excel_notinput')}}">
                                <i class="fa fa-download"></i> 未入力者一覧 ダウンロード
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /page content -->
@endsection
