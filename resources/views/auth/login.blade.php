<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>ADMIN CONSOLE</title>
    
    <!-- Bootstrap -->
    <link href="{{ asset("admin/css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("admin/css/font-awesome.min.css") }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("admin/css/gentelella.min.css") }}" rel="stylesheet">

	<link href="{{ asset("admin/css/common.css") }}" rel="stylesheet">
</head>

<body class="login">
<div>
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
				{!! BootForm::open(['url' => url('/x_wedaelc/login'), 'method' => 'post']) !!}
                    
				<h1>LOG IN</h1>
			
				{!! BootForm::email('email', 'メールアドレス', old('email'), ['placeholder' => 'メールアドレス', 'afterInput' => '<span>test</span>'] ) !!}
			
				{!! BootForm::password('password', 'パスワード', ['placeholder' => 'パスワード']) !!}
				
				<div>
					{!! BootForm::submit('Log in', ['class' => 'btn btn-success btn-default submit btn-block', 'style' => 'margin: 0']) !!}
					<!-- <a class="reset_pass" href="{{  url('/password/reset') }}">パスワードを忘れた方はこちら</a> -->
				</div>
                    
				<div class="clearfix"></div>
                    
				<div class="separator">
					<!-- <p class="change_link">
						<a href="{{ url('/register') }}" class="to_register"> アカウント登録 </a>
					</p> -->
                        
					<div class="clearfix"></div>
					<br />
                        
					<div>
						<h1><i class="fa fa-cogs"></i> ADMIN CONSOLE</h1>
						<p>©2018 POWERED BY GRAPHITE INC.</p>
					</div>
				</div>
				{!! BootForm::close() !!}
            </section>
        </div>
    </div>
</div>
</body>
</html>