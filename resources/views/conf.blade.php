@extends('layouts.entry')
@section('main_container')
    <div class="contents_inner">
        <form method="post" action="{{url('store')}}">
            {{ csrf_field() }}
            <section id ="conf_area">
                <div class="inner">
                    <h3><img src="img/ttl_form.png" alt="キャンペーン応募フォーム"></h3>
                    <p class="center">以下の内容でよろしければ「応募する」ボタンを、修正する場合は「戻る」ボタンをクリックしてください。</p>

                    <dl class="clearfix">
                        <dt>応募商品<sup>＊</sup></dt>
                        <dd class="space course">{{array_search($e->prize,Config::get('cleadew.items'))}}</dd>
                        @for ($i = 1; $i <= 5; $i++)
                            @if(isset($e['image'.$i]))
                                <dt>レシート画像<!-- {{$i}} --><sup>＊</sup></dt>
                                <dd class="space"><img src="/cleadew/data/temp/{{$e['image'.$i]}}"></dd>
                            @endif
                        @endfor
                        <dt>氏名<sup>＊</sup></dt>
                        <dd>{{$e->last_nm}} {{$e->first_nm}}</dd>
                        <dt>氏名（フリガナ）<sup>＊</sup></dt>
                        <dd class="space">{{$e->last_kana}} {{$e->first_kana}}</dd>
                        <dt>メールアドレス<sup>＊</sup></dt>
                        <dd class="space">{{$e->email}}</dd>
                        <dt>郵便番号<sup>＊</sup></dt>
                        <dd>{{$e->postal1}}-{{$e->postal2}}</dd>
                        <dt>都道府県<sup>＊</sup></dt>
                        <dd>{{array_search($e->prefecture_id,Config::get('cleadew.prefectures'))}}</dd>
                        <dt>市町村・番地<sup>＊</sup></dt>
                        <dd>{{$e->address1}}</dd>
                        <dt>建物名・部屋番号</dt>
                        <dd class="space">{{$e->address2}}</dd>
                        <dt>アンケート<sup>＊</sup></dt>
                        <dd>{{array_search($e->answer,Config::get('cleadew.question'))}} {{$e->answer_input}}</dd>
                    </dl>
                </div>
            </section>
            <div class="conf_btn_area">
                <input type="submit" value="" alt="" class="apply_btn"/>
                <a href="javascript:history.back()" class="back_btn">
                    <img src="img/back_btn.png" alt="戻る">
                </a>
            </div>
        </form>
    </div>
@endsection