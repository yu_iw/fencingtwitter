@extends('layouts.entry')
@section('main_container')
    <div class="contents_inner">
        <form method="post" action="{{url('input_store')}}">
            <section id="form_area">
          			<div id="conf" class="inner">
            				<h2>入力内容確認</h2>

                    <dl class="clearfix">
                        <dt>名前</dt>
                        <dd>{{$w->name}}</dd>
                        <dt>フリガナ</dt>
                        <dd>{{$w->kana}}</dd>
                        <dt>郵便番号</dt>
                        <dd>{{$w->postal1}}-{{$w->postal2}}</dd>
                        <dt>都道府県</dt>
                        <dd>{{array_search($w->prefecture_id,Config::get('wella.prefectures'))}}</dd>
                        <dt>市町村</dt>
                        <dd>{{$w->address1}}</dd>
                        <dt>電話番号</dt>
                        <dd>{{$w->tel1}}-{{$w->tel2}}-{{$w->tel3}}</dd>
                    </dl>
                </div>

            		<div class="btn_area">
                    {{ csrf_field() }}
                    <a href="javascript:history.back()" class="btn">戻る</a>
              			<input type="submit" id="submit" name="submit" value="送信する" alt="送信する" class="btn btn_comp">
            		</div>
            </section>
        </form>
    </div>
@endsection
