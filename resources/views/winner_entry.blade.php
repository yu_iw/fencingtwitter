@extends('layouts.entry')

@section('main_script')
<script src="//ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
<script type="text/javascript">
  var zen2han = function(str) {
    str = str.replace(/[０-９]/g, function (s) {
      return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
    })
    return str;
  }
  $(function () {
    $(".number").on("change", function () {
      var str = $(this).val();
      $(this).val(zen2han(str));
    });
  });
</script>
@endsection

@section('main_container')
    <script src="//ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    <div class="contents_inner">
        <form method="post" action="{{url('input_conf')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="c" value="{{$_GET['c']}}">
            @if($e)
            <input type="hidden" name="eid" value="{{$e->id}}">
            @else
            <input type="hidden" name="hid" value="{{$h->id}}">
            @endif
            <section id="form_area">
          			<div id="entry" class="inner">
              			<h2>発送先入力フォーム</h2>

                    <dl class="clearfix">
                        <dt>賞品</dt>
                        <dd>
                            @if($e)
                            {{array_search($e->prize,Config::get('wella.prizes'))}}
                            <input type="hidden" name="prize" value="{{$e->prize}}" />
                            @else
                            {{array_search($h->prize,Config::get('wella.prizes'))}}
                            <input type="hidden" name="prize" value="{{$h->prize}}" />
                            @endif
                        </dd>
                        <dt>名前<span class="required">必須</span></dt>
                        <dd>
                            <input type="text" class="size_l" name="name" class="name" value="{{old('name')}}" placeholder="">
                            @if($errors->has('name'))
                                <p class="error_text">{{ $errors->first('name') }}</p>
                            @endif
                        </dd>
                        <dt>フリガナ<span class="required">必須</span></dt>
                        <dd>
                            <input type="text" class="size_l" name="kana" class="kana" value="{{old('kana')}}" placeholder="">
                            @if($errors->has('kana'))
                                <p class="error_text">{{ $errors->first('kana') }}</p>
                            @endif
                        </dd>
                        <dt>郵便番号 <span class="required">必須</span></dt>
                        <dd>
                            <input type="text" class="size_s number" name="postal1" class="short" maxlength="3" value="{{old('postal1')}}"> - <input type="text" class="size_s number" name="postal2" class="short" maxlength="4" value="{{old('postal2')}}"> <a class="address_auto" onclick="AjaxZip3.zip2addr('postal1','postal2','prefecture_id','address1');">住所自動入力</a>
                            @if($errors->has('postal1') || $errors->has('postal2'))
                                <p class="error_text">郵便番号は必須です。</p>
                            @endif
                        </dd>
                        <dt>都道府県 <span class="required">必須</span></dt>
                        <dd>
                            <div class="selectbox">
                                <label><select name="prefecture_id">
                                    <option value="">選択してください</option>
                                    @foreach (Config::get('wella.prefectures') as $key => $value)
                                    <option value="{{$value}}" @if(old('prefecture_id')==$value)selected="selected"@endif>{{$key}}</option>
                                    @endforeach
                                    </select>
                                </label>
                            </div>
                            @if($errors->has('prefecture_id'))
                                <p class="error_text">{{ $errors->first('prefecture_id') }}</p>
                            @endif
                        </dd>
                        <dt>住所<span class="required">必須</span></dt>
                        <dd>
                            <input type="text" class="size_l" name="address1" class="address" value="{{old('address1')}}">
                            @if($errors->has('address1'))
                                <p class="error_text">{{ $errors->first('address1') }}</p>
                            @endif
                        </dd>
                        <dt>電話番号 <span class="required">必須</span></dt>
                        <dd>
                            <input type="text" class="size_s number" name="tel1" class="short" maxlength="4" value="{{old('tel1')}}"> - <input type="text" class="size_s number" name="tel2" class="short" maxlength="4" value="{{old('tel2')}}"> - <input type="text" class="size_s number" name="tel3" class="short" maxlength="4" value="{{old('tel3')}}">
                            @if($errors->has('tel1') || $errors->has('tel2') || $errors->has('tel3'))
                                <p class="error_text">電話番号は必須です。</p>
                            @endif
                        </dd>
                    </dl>
                </div>
            		<div class="btn_area">
              			<input type="submit" name="submit" value="確認画面へ" alt="確認画面へ" class="btn btn_conf">
            		</div>
        		</section>
    		</form>
    </div>
@endsection
