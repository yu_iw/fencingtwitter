@extends('layouts.entry')
@section('main_container')
    <div class="contents_inner">
        <section id="form_area">
      			<div id="comp" class="inner">
        				<h2>発送申込受付完了</h2>

                <p id="thanks">
                    お申込みいただき、有難うございました。
                </p>

            </div>
            <div class="btn_area">
        		</div>
        </section>
    </div>
@endsection
