@extends('layouts.entry')
@section('main_container')
    <script src="//ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    <div class="contents_inner">
        <form method="post" action="{{url('conf')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <section id ="form_area">
                <div class="inner">
                    <h3><img src="img/ttl_form.png" alt="キャンペーン応募フォーム"></h3>
                    <p class="center">以下のフォームに必要事項をご入力いただき、「確認する」ボタンをクリックしてください。</p>
                    <p class="pink center">（＊印のある項目は必ず入力してください。）</p>
                    <dl>
                        <dt>応募商品<sup>＊</sup></dt>
                        <dd class="space course">
                            @foreach (Config::get('cleadew.items') as $key => $value)
                                <input type="radio" value="{{$value}}" name="prize" class="radio_btn" id="radio{{$value}}" @if(old('prize')==$value)checked="checked"@endif>
                                <label for="radio{{$value}}">{{$key}}</label><br>
                            @endforeach
                            @if($errors->has('prize'))
                                <p class="error_text">{{ $errors->first('prize') }}</p>
                            @endif
                        </dd>
                        <dt>レシート画像<sup>＊</sup></dt>
                        <dd class="space">
                            @for ($i = 1; $i <= 1; $i++)
                            <div class="upload">
                                <input type="file" name="image{{$i}}" id="files" value="{{old('image'.$i)}}" accept="image/*">
                                <input type="text" id="filename" placeholder="選択されていません" readonly />
                                <div id="upload_btn">参照</div>
                            </div>
                            @endfor
                            <small>※5MB以下のjpeg、png画像をご使用ください</small>
                            @if($errors->has('image1'))
                                <p class="error_text">{{ $errors->first('image1') }}</p>
                            @endif
                        </dd>
                        <dt>氏名<sup>＊</sup></dt>
                        <dd><input type="text" name="last_nm" placeholder="姓" value="{{old('last_nm')}}" ><input type="text" name="first_nm" placeholder="名" value="{{old('first_nm')}}">
                            @if($errors->has('last_nm'))
                                <p class="error_text">{{ $errors->first('last_nm') }}</p>
                            @endif
                            @if($errors->has('first_nm'))
                                <p class="error_text">{{ $errors->first('first_nm') }}</p>
                            @endif
                        </dd>
                        <dt>氏名（フリガナ）<sup>＊</sup></dt>
                        <dd class="space"><input type="text" name="last_kana" placeholder="セイ" value="{{old('last_kana')}}"><input type="text" name="first_kana" placeholder="メイ" value="{{old('first_kana')}}">
                            @if($errors->has('last_kana'))
                                <p class="error_text">{{ $errors->first('last_kana') }}</p>
                            @endif
                            @if($errors->has('first_kana'))
                                <p class="error_text">{{ $errors->first('first_kana') }}</p>
                            @endif
                        </dd>
                        <dt>メールアドレス<sup>＊</sup></dt>
                        <dd class="space"><input type="email" name="email" value="{{old('email')}}" placeholder="メールアドレス">
                            @if($errors->has('email'))
                                <p class="error_text">{{ $errors->first('email') }}</p>
                            @endif
                        </dd>
                        <dt>郵便番号<sup>＊</sup></dt>
                        <dd><input type="text" name="postal1" maxlength="3" value="{{old('postal1')}}"> <span class="pink">-</span> <input type="text" name="postal2" maxlength="4" value="{{old('postal2')}}"><a class="address_auto" onclick="AjaxZip3.zip2addr('postal1','postal2','prefecture_id','address1');">住所自動入力</a>
                            @if($errors->has('postal1') || $errors->has('postal2'))
                                <p class="error_text">郵便番号は必須です。</p>
                            @endif
                        </dd>
                        <dt>都道府県<sup>＊</sup></dt>
                        <dd>
                            <div class="selectbox">
                                <label><select name="prefecture_id">
                                    <option value="">選択してください</option>
                                    @foreach (Config::get('cleadew.prefectures') as $key => $value)
                                    <option value="{{$value}}" @if(old('prefecture_id')==$value)selected="selected"@endif>{{$key}}</option>
                                    @endforeach
                                    </select></label>
                            </div>
                            @if($errors->has('prefecture_id'))
                                <p class="error_text">{{ $errors->first('prefecture_id') }}</p>
                            @endif
                        </dd>
                        <dt>市区町村・番地<sup>＊</sup></dt>
                        <dd><input type="text" name="address1" class="address" value="{{old('address1')}}" placeholder="市区町村・番地">
                            @if($errors->has('address1'))
                                <p class="error_text">{{ $errors->first('address1') }}</p>
                            @endif
                        </dd>
                        <dt>建物名・部屋番号</dt>
                        <dd class="space"><input type="text" name="address2" value="{{old('address2')}}" placeholder="建物名・部屋番号" class="address"></dd>
                        <dt>アンケート<sup>＊</sup></dt>
                        <dd class="enquete">
                            <p class="question">キャンペーンを知ったきっかけは何ですか。</p>
                            @foreach (Config::get('cleadew.question') as $key => $value)
                                <input type="radio" name="answer" id="radio2_{{$value}}" class="radio_btn" value="{{$value}}" @if(old('answer')==$value)checked="checked"@endif>
                                <label for="radio2_{{$value}}">{{$key}}</label>
                                @if (!$loop->last)
                                    @if ($loop->index == 2)
                                        <br>
                                    @else
                                        <br class="sp">
                                    @endif
                                @endif
                            @endforeach
                                <input type="text" name="answer_input" placeholder="その他" class="other" value="{{old('answer_input')}}"><br>
                            @if($errors->has('answer'))
                                <p class="error_text">{{ $errors->first('answer') }}</p>
                            @endif
                            @if($errors->has('answer_input'))
                                <p class="error_text">{{ $errors->first('answer_input') }}</p>
                            @endif
                        </dd>
                    </dl>
                </div>
            </section>
            <div class="btn_area">
                <input type="submit" alt="" value="" class="conf_btn"/>
            </div>
        </form>
    </div>
@endsection