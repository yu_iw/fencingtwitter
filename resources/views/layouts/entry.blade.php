<!DOCTYPE html>
<html lang="ja">
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126956618-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-126956618-1');
  </script>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <title>オトナ髪デビューキャンペーン 発送先入力フォーム</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="copyright" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="format-detection" content="telephone=no">

    <link rel="contents" href="/">
    <link rel="index" href="/index.html">

    <link rel="stylesheet" href="css/setting.css" media="all">
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/style_sp.css" media="screen">

    <script src="js/jquery-1.12.3.min.js"></script>
    <script src="js/common.js"></script>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/selectivizr-min.js"></script>
    <![endif]-->
</head>
<body id="sub">
  <div class="header clearfix" style="top: 0px;">
      <h2 class="enq_title_txt">オトナ髪デビューキャンペーン</h2>
      <b></b>
  </div>
  <div id="container">
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WKXHQP2" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
      <article id="contents">
          @yield('main_container')
      </article>
    	<footer>
    	</footer>
  <!--container--></div>
  </body>
  </html>
