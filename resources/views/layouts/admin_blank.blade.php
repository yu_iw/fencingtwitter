<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ADMIN CONSOLE | HOME</title>

        <!-- Material & DataTables -->
        <link href="//cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css" rel="stylesheet">
        <link href="//cdn.datatables.net/1.10.16/css/dataTables.material.min.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="{{ asset("admin/css/bootstrap.min.css") }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset("admin/css/font-awesome.min.css") }}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{{ asset("admin/css/gentelella.min.css") }}" rel="stylesheet">
        <!-- Switchery -->
        <link href="{{ asset("admin/css/switchery.min.css") }}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{{ asset("admin/css/common.css") }}" rel="stylesheet">

        @stack('stylesheets')

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                @include('includes.admin_sidebar')

                @include('includes/topbar')

                @yield('main_container')

                @include('includes/footer')

            </div>
        </div>

        <!-- jQuery -->
        <script src="{{ asset("admin/js/jquery.min.js") }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset("admin/js/bootstrap.min.js") }}"></script>
        <!-- Custom Theme Scripts -->
        <script src="{{ asset("admin/js/gentelella.min.js") }}"></script>
        <!-- Switchery -->
        <script src="{{ asset("admin/js/switchery.min.js") }}"></script>

        @stack('scripts')

    </body>
</html>
