<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115629769-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-115629769-1');
    </script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <title>wella</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="copyright" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="format-detection" content="telephone=no">

    <link rel="contents" href="/">
    <link rel="index" href="/sitemap.html">

    <link rel="stylesheet" href="css/setting.css" media="all">
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/style_sp.css" media="screen">

    <script src="js/jquery-1.12.3.min.js"></script>
    <script src="js/common.js"></script>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/selectivizr-min.js"></script>
    <![endif]-->
</head>

<body id="index">
<div id="container">
    <header>
        <div class="inner"><h1><a href="http://www.ophtecs.co.jp/" target="_blank"><img src="img/logo.png" alt="Ophtecs"></a></h1></div>
    </header>
    <article id="main">
        <div class="inner">
            <h2>
                <img src="img/title.png" alt="cleadew 春から始める新習慣！フレッシュアップキャンペーン実施中！" class="pc">
                <img src="img/title_sp.png" alt="cleadew 春から始める新習慣！フレッシュアップキャンペーン実施中！" class="sp">
            </h2>
            <img src="img/pop.png" alt="人気のアイテムが当たる！" class="pop pc">
            <img src="img/pop_sp.png" alt="人気のアイテムが当たる！" class="pop sp">
            <ul class="clearfix">
                <li><img src="img/present01.png" alt="ビューティーアップコース"></li>
                <li><img src="img/present02.png" alt="ライフアップコース"></li>
                <li><img src="img/present03.png" alt="QUOカード3000円分"></li>
            </ul>
        </div>
    </article>
    <article id="contents">
        <div class="contents_inner">
            <section id ="overview">
                <div class="inner">
                    <h3><img src="img/ttl_overview.png" alt="キャンペーン概要"></h3>
                    <p>キャンペーン期間中に対象商品をご購入いただき、店頭POPやウェブサイトから当サイトにアクセスし、応募された方の中で豪華商品をプレゼント。</p>
                    <div>
                        <dl class="clearfix">
                            <dt>対象商品</dt>
                            <dd>
                                cleadewファーストケア（28日分）<br>
                                cleadewリペア&amp;モイスト（360mL×2本）<br>
                                cleadew O2セプト（30日分）
                            </dd>
                            <dt>キャンペーン<br class="pc">期間</dt>
                            <dd>
                                レシート有効期間：<br class="sp">2018年3月20日（火）〜4月30日（月）<br>
                                応募受付期間：<br class="sp">2018年３月20日（火）〜4月30日（月）</dd>
                            <dt>応募方法</dt>
                            <dd>
                                <span class="indent">①スマートフォン等で上記応募対象商品の購入がわかるレシートの写真を撮影。<br></span>
                                ②応募ページよりレシートをアップロード。<br>
                                ③応募情報フォームより必要事項を入力。<br>
                                ④応募完了！<br>
                                <div class="caution">
                                    <small class="mrgn">※パソコンで応募の場合はメール等でパソコンに写真をお送りください。</small><br class="pc">
                                    <small>※締め切り時間付近はアクセスが集中することがございますので、時間に余裕を持ってご応募いただけますようお願いします。</small><br class="pc">
                                    <small>※応募対象商品が確認できないレシートは無効とさせていただきます。</small>
                                </div>
                            </dd>
                            <dt>お問い合わせ</dt>
                            <dd>
                                cleadew春から始める新習慣！<br class="sp">フレッシュアップキャンペーン事務局<br>
                                E-mail <a href="mailto:info@cleadew-cp.jp">info@cleadew-cp.jp</a><br>
                                <div class="caution">
                                    <small class="mrgn">※お問い合わせ内容により、オフテクスからご連絡させていただく場合がございます。</small><br class="pc">
                                    <small>※キャンペーン以外のお問い合わせについてはオフテクスお客様相談室をご案内させていただきます。</small>
                                </div>
                            </dd>
                        </dl>
                    </div>
                </div>
            </section>
            <section id="agreement">
                <div class="inner">
                    <h3><img src="img/ttl_agreement.png" alt="応募規約"></h3>
                    <div class="scroll">
                        <div>
                            <ul>
                                <li>・商品によっては、一部お取扱いのない店舗もございます。</li>
                                <li>・レシートのコピーは無効となります。</li>
                                <li>・複数名によるレシートの流用は無効となります。</li>
                                <li>・クレジットカードでお支払いの場合は、レシートの下部に印字されているクレジットカード情報部分は除き、お買い上げ情報が記載された上部のみを撮影ください。</li>
                                <li>・お一人様につき、一口の応募とさせて頂きます。</li>
                                <li>・長いレシートは対象商品部分が見えるように撮影してください。</li>
                                <li>・本キャンペーンのご応募の際にご記入いただきました個人情報（郵便番号・住所・氏名・電話番号等）は、
                                    賞品の発送、応募者・当選者への諸連絡、重複当選の有無の確認に利用させていただきます。また、マーケティング等の目的のために個人を特定しない統計的情報の形で利用させていただきます。<br>個人情報の管理責任者は株式会社 オフテクスです。詳細につきましては「<a href="http://www.ophtecs.co.jp/privacy/" target="_blank">個人情報保護について</a>」をご参照ください。</li>
                                <li>・当選者の長期不在や、賞品お届け先ご住所や転居先が不明等の理由により、
                                    賞品のお届けができない場合は、ご当選を無効とさせていただく場合がありますので、予めご了承ください。</li>
                                <li>・記入欄は漏れなくご記入ください。記入漏れがある場合は、ご当選を無効とさせていただく場合がありますので、予めご了承ください。</li>
                                <li>・本キャンペーンへのご参加は、日本在住の方に限らせていただきます。</li>
                                <li>・賞品の発送先は日本国内に限らせていただきます。</li>
                                <li>・ご当選者の当選の権利を第三者へ譲渡することはできません。</li>
                                <li>・その他、ご応募に関して不正な行為があった場合、当選を取り消させていただく場合がございます。</li>
                                <li>・賞品の仕様は予告なく変わる可能性がございます。</li>
                                <li>・ご当選された賞品は返品・交換できません。</li>
                                <li>・主催者側（株式会社 オフテクス）は、キャンペーン詳細確認に伴う使用機器・通信における障害、損傷及び応募時の不具合等についての責任は一切負いかねます。</li>
                                <li>・株式会社 オフテクス社員ならびに関係者は本キャンペーンに応募できません。</li>
                                <li>※応募期間後もキャンペーンのご案内が記載されている商品が販売されていることがありますがご容赦ください。</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <div class="btn_area">
                本キャンペーンは終了しました。たくさんのご応募ありがとうございました。
            </div>
        </div>
    </article>

    <footer>
        <div class="inner">
            <img src="img/info.png" alt="" class="info">
            <a href="http://www.ophtecs.co.jp/" class="to_top" target="_blank"><img src="img/info_url.png" alt="オフティクスHPへ"></a>
        </div>
    </footer>
    <!--container--></div>
</body>
</html>
