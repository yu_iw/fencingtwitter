{{$e->name}}様<br><br>
ウエラトーン ヘアカラートリートメント サンプリングキャンペーン事務局でございます。<br>
先週、「ご使用後アンケート」のメールをお送りいたしましたが、アンケートへのご回答はお済みでしょうか？<br>
<br>
↓下記ご使用後アンケートにお答えいただいた方の中から、<br>
髪と地肌にうるおいを与えると話題の「Panasonic ナノケア ヘアードライヤー」、もしくは「JCB プレモカード　20,000円分」が<br>
合計10名様に抽選で当たります。<br>
<img src="https://cp-jm.com/img/panasonic.jpg" /><img src="https://cp-jm.com/img/jcb.jpg" /><br>
<a href="https://questant.jp/q/wellaton_sampling-post">https://questant.jp/q/wellaton_sampling-post</a><br>
(回答期限： 2019年3月4日(月) 23:59まで)<br>
※期間内にご入力いただけないと参加権利を失いますので、ご注意ください。<br>
※当選発表は当選メールをもって代えさせていただきます。<br>
<br>
今後ともウエラ製品をご愛顧いただけますよう、何卒お願い申し上げます。 <br>
<br>
※このご確認メールは、「ウエラトーン　ヘアカラトリートメント　サンプリングキャンペーン」にご応募いただいた方全員にお送りしています。<br>
すでに「ご使用後アンケート」にご回答いただいていた場合は何卒ご容赦くださいますようお願い申し上げます。<br>
<br>
【キャンペーン関するお問い合わせ先】<br>
「ウエラトーン ヘアカラートリートメント サンプリングキャンペーン」事務局<br>
E-mail： wella2018@cp-jm.com <br>
営業期間：土日祝日を除く、10:00～18:00<br>
　　　　　　　 2019年1月15日(火)～2019年3月22日(金)18:00まで<br>
<br>
※製品やブランドに関するお問い合わせは、ウエラお客様相談室までご連絡ください。<br>
　 ウエラお客様相談室：0120-308-168　　受付時間：土日祝日を除く、9:15～17:00<br>
<br>
ブランドウェブサイト：https://www.wella.com/retail/jp-JP/<br>
ウエラ公式Twitter：https://twitter.com/wella_jp<br>
ウエラ公式Instagram：https://www.instagram.com/wella_jp/<br>
ウエラ公式Facebook：https://www.facebook.com/wellajp.official/ <br>
