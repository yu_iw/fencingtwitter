{{$e->name}}様<br><br><br>
先日は「ウエラトーン へアカラートリートメント　サンプリングキャンペーン」のアンケートにご回答いただきありがとうございました。<br>
その後、ウエラのサンプル製品はお使いいただけましたでしょうか？<br>
<br>
↓下記ご使用後アンケートにお答えいただくと、さらに豪華賞品が当たる抽選会に参加できます！<br>
<a href="https://questant.jp/q/wellaton_sampling-post">https://questant.jp/q/wellaton_sampling-post</a><br>
(回答期限：<?php $week = array( "日", "月", "火", "水", "木", "金", "土" ); ?>{{date('m月d日（', strtotime('1 week')).$week[date('w')].date('）H時i分', strtotime('1 week'))}})<br>
<br>
※当選発表は当選メールをもって代えさせていただきます。<br>
※当選した方はメッセージ内に賞品発送のために必要な情報を登録するフォームへのURLを記載いたします。<br>
指定の期日以内にご登録ください。<br>
<br>
今後ともウエラ製品をよろしくお願いいたします。<br>
<br>
※このメールは、「ウエラトーン　ヘアカラトリートメント　サンプリングキャンペーン」アンケートにご回答いただいた方にお送りしています。<br>
<br>
【キャンペーン関するお問い合わせ先】<br>
「ウエラトーン ヘアカラートリートメント サンプリングキャンペーン」事務局<br>
E-mail： wella2018@cp-jm.com<br>
営業期間：土日祝日を除く、10:00～18:00<br>
　　　　　　　　2019年1月15日(火)～2019年3月22日(金)18:00まで<br>
<br>
※製品やブランドに関するお問い合わせは、ウエラお客様相談室までご連絡ください。<br>
<br>
ウエラお客様相談室：0120-308-168　　受付時間：土日祝日を除く、9:15～17:00<br>
ブランドウェブサイト：https://www.wella.com/retail/jp-JP/<br>
ウエラ公式Twitter：https://twitter.com/wella_jp<br>
ウエラ公式Instagram：https://www.instagram.com/wella_jp/<br>
ウエラ公式Facebook：https://www.facebook.com/wellajp.official/ <br>
-------------------------------------------------------------
