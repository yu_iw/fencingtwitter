<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\SnsController;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            var_dump("start");
            $sc = new SnsController();
            $sc->checkFollower();
            var_dump("end");
        })->everyMinute();
        $schedule->call(function () {
            var_dump("start");
            $sc = new SnsController();
            $sc->searchMonth();
            var_dump("end");
        })->twiceDaily(0, 6);
        $schedule->call(function () {
            var_dump("start");
            $sc = new SnsController();
            $sc->searchMonth();
            var_dump("end");
        })->twiceDaily(12, 18);
    }
}
