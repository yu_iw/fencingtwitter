<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class RestrictIp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $config = \Config::get('restrict_ip');
        if ($config['enable'] !== true) {
            return $next($request);
        }
        if ($config['isProxy'] === true) {
            $request->setTrustedProxies([$request->ip()]);
        }
        if ($this->isAllow($request->ip(), $config['allowIps']) === false) {
            Log::debug('cannot access from : '. $request->ip());
            abort(403);
        }
        return $next($request);
    }

    private function isAllow($remoteIp, array $accepts)
    {
        foreach ($accepts as $accept) {
            if ($this->isIn($remoteIp, $accept)) {
                return true;
            }
        }
        return false;
    }
    private function isIn($remoteIp, $accept)
    {
        if (strpos($accept, '/') === false) {
            return ($remoteIp === $accept);
        }
        list($acceptIp, $mask) = explode('/', $accept);
        $acceptLong            = ip2long($acceptIp) >> (32 - $mask);
        $remoteLong            = ip2long($remoteIp) >> (32 - $mask);

        return ($acceptLong == $remoteLong);
    }
}
