<?php

namespace App\Http\Controllers;

use App\Models\Sns;
use Illuminate\Http\Request;
use Thujohn\Twitter\Facades\Twitter;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;

class SnsController extends Controller
{
    public function searchHashtag()
    {
        ini_set('max_execution_time', 180);
        // ツイート検索パラメータの設定、「q」は検索文字列、「count」は取得件数（最大100件）
        $params = array(
        'q'					=> '#フェンシング -RT',
        'result_type'		=>	'mixed', //なるべく多く取得
        'count' 			=>  100,
        'include_entities'	=>	true //trueにすると添付URLについての情報を追加で取得できる
    );
        $list = array();
        $requestNum = 1;//APIリクエスト100件毎
        try {
            for ($i = 0; $i < $requestNum; $i++) {
                $response = Twitter::getSearch($params);
                foreach ($response->statuses as $key => $value) {
                    //var_dump($value);
                }
                //dd($response);
                foreach ($response->statuses as $data) {
                    //dd($data->entities->hashtags);
                    array_push($list, array(
                        'post_id' => $data->id_str,
                        'user_id' => $data->user->id_str,
                        'screen_name' => $data->user->screen_name,
                        'name' => $data->user->name,
                        'text' => $data->text,
                        //'hashtag' => $data->entities->hashtags,
                        'posted_at' => strtotime($data->created_at),
                        'code' => substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, 15)
                    ));
                }
                //dd($list);
                // 先頭の「?」を除去
                if (!empty($response->search_metadata->next_results)) {
                    $nextResults = preg_replace('/^\?/', '', $response->search_metadata->next_results);
                    parse_str($nextResults, $params);
                } else {
                    break;
                }
            }
            //dd($list);
            $tw = new Sns();
            foreach ($list as $datam) {
                // 新規作成処理
                $flag = $tw->firstOrCreate(['post_id' => $datam['post_id']], $datam);
                var_export($flag);
            }
        } catch (Exception $e) {
            //dd(Twitter::error());
            dd(Twitter::logs());
        }


        $this->checkDup();
        //var_dump($tw);
        //dd($result);
        //dd($list);
        return view('searchHashtag');
    }

    public function checkFollower()
    {
        $params = array(
      'screen_name'					=> 'wella_jp', //本番用
      //'screen_name'					=> '###', //本番用
      'stringify_ids'					=> 'true', //文字列
      'count'					=> 5000 //リクエスト数
    );
        $list = array();
        $requestNum = 1;//APIリクエスト5000件毎
        try {
            $tw = new Sns();
            $data = $tw->where('follow_judged', false)->get();
            if ($data->first()) {
                //dd($data);
                foreach ($data as $datam) {
                    $userIds[] = $datam['user_id'];
                }
                //print_r($request_number);
                for ($i = 0; $i < $requestNum; $i++) {
                    $response = Twitter::getFollowersIds($params);
                    //dd($response);
                    $list = array_merge($list, $response->ids);
                    $tw->whereIn('user_id', $userIds)->update(['follow_judged'=>true,'updated_at' => Carbon::now()]);
                    // 先頭の「?」を除去
                    if (!empty($response->next_cursor)) {
                        $nextCursor = preg_replace('/^\?/', '', $response->next_cursor);
                        parse_str($nextCursor, $params);
                    } else {
                        break;
                    }
                }
                $tw->whereIn('user_id', $list)->update(['followed'=>true]);
            } else {
                echo "すべてフォロー判定済み";
            }

            // var_dump($tw);
        } catch (Exception $e) {
            //dd(Twitter::error());
            dd(Twitter::logs());
        }

        //dd($result);
        //dd($list);
        return view('checkFollower');
    }


    public function tweetList()
    {
        try {
            $response = Twitter::getUserTimeline(['screen_name' => 'gra_test','count' => 20, 'format' => 'array']);
        } catch (Exception $e) {
            // dd(Twitter::error());
            dd(Twitter::logs());
        }

        //dd($result);
        dd($list);
        return view('tweetList');
    }


    public function searchMonth()
    {
        ini_set('max_execution_time', 1800);
        // ツイート検索パラメータの設定、「q」は検索文字列、「count」は取得件数（最大100件）
        $params = array(
            'query'					=> '#ツープラスワンヘアカラークリームタイプ OR #ヘアカラートリートメント OR #リタッチコンシーラー -RT',
            //'fromDate'		=>	'201810280000', //なるべく多く取得
            //'toDate'		=>	'201811010000', //なるべく多く取得
            'maxResults' 			=>  100
        );
        $list = array();
        $requestNum = 1;//APIリクエスト100件毎
        try {
            for ($i = 0; $i < $requestNum; $i++) {
                //$response = Twitter::getSearchMonth($params);
                $response = Twitter::get('tweets/search/30day/wella2018.json', $params);
                //dd($response);
                echo "searchMonth:" + $i;
                var_dump($response);
                foreach ($response->results as $data) {
                    //dd($data);
                    array_push($list, array(
                      'post_id' => $data->id_str,
                      'user_id' => $data->user->id_str,
                      'screen_name' => $data->user->screen_name,
                      'name' => $data->user->name,
                      'text' => $data->text,
                      'posted_at' => strtotime($data->created_at),
                      'code' => substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, 15)
                    ));
                }
                //dd($list);
                // 先頭の「?」を除去
                if (!empty($response->next)) {
                    $nextResults = preg_replace('/^\?/', '', $response->next);
                    //dd($nextResults);
                    $params['next'] = $nextResults;
                //parse_str("next=".$nextResults, $params);
            //dd($params);
                } else {
                    break;
                }
            }
            //dd($list);
            $tw = new Sns();
            foreach ($list as $datam) {
                // 新規作成処理
                $flag = $tw->firstOrCreate(['post_id' => $datam['post_id']], $datam);
                //var_export($flag);
            }
        } catch (Exception $e) {
            dd(Twitter::error());
            dd(Twitter::logs());
        }

        $this->checkDup();
    }

    private function checkDup()
    {
        //重複
        $sns = Sns::all();
        $entry = array();
        //重複チェック
        foreach ($sns as $data) {
            $count = 0;
            $data->duplicated = false;
            foreach ($sns as $d) {
                if ($d->user_id === $data->user_id) {
                    $count++;
                } else {
                }
            }
            if ($count > 1) {
                $data->duplicated = true;
                $dup = Sns::find($data->id);
                $dup->duplicated = true;
                $dup->save();
            }
            $entry[] = $data;
        }
    }

    private function getApi()
    {
      try {
              //$response = Twitter::getSearchMonth($params);
              $response = Twitter::get('tweets/search/30day/wella2018.json', $params);
              dd($respinse);

      } catch (Exception $e) {
          dd(Twitter::error());
          dd(Twitter::logs());
      }
    }
}
