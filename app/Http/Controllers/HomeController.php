<?php

namespace App\Http\Controllers;

use App\Models\Entry;
use App\Models\Sns;
use App\Models\WinnerEntry;
use App\Http\Requests\EntryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Log;
use File;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('error');
    }
    public function error()
    {
        return view('error');
    }
    public function error_term()
    {
        return view('error_term');
    }
    public function entry()
    {
        return view('error');
    }
    public function conf(EntryRequest $request)
    {
        $entry =  new Entry();
        $entry->fill($request->except(['_token']));
        session(['entry' => $entry ]);

        for ($i = 1; $i<5; $i++) {
            $imgtmp = $request->file('image'.$i);
            if (!empty($imgtmp)) {
                $passtmp = str_random(10) . '.' .$imgtmp->getClientOriginalExtension();
                $image = Image::make($imgtmp);
                if ($image->filesize() > 2000000) {
                    $image->resize(600, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $image->save(public_path(). '/data/temp/'. $passtmp);
                $entry['image'.$i] = $passtmp;
            }
        }

        return view('conf')
            ->with('e', $entry);
    }
    public function store(Request $request)
    {
        $entry = new Entry();
        $entry = session('entry');
        if (!empty($entry)) {
            if (!empty($entry['postal1']) && !empty($entry['postal2'])) {
                $entry['postal'] = $entry['postal1'] . '-' . $entry['postal2'];
                unset($entry['postal1']);
                unset($entry['postal2']);
            }
            $nextId = Entry::max('id') + 1;
            for ($i = 1; $i<5; $i++) {
                if (!empty($entry['image'.$i])) {
                    File::makeDirectory(public_path().'/data/comp/'. $nextId .'/', 0775, true, true);
                    File::move(public_path(). '/data/temp/'. $entry['image'.$i], public_path().'/data/comp/'. $nextId .'/'.$entry['image'.$i]);
                    $entry['image'.$i] = '/cleadew/data/comp/'. $nextId .'/'.$entry['image'.$i];
                }
            }
            $entry->save();

            Mail::send(['text' => 'mail.entry'], ['e' => $entry], function ($message) use ($entry) {
                $message->from(Config::get('cleadew.admin_email'), Config::get('cleadew.admin_email_info'));
                $message->to($entry->email, $entry->email)->subject('【応募完了】cleadew春から始める新習慣！フレッシュアップキャンペーンのご応募ありがとうございます。');
            });
        } else {
            return redirect('index');
        }
        $request->session()->flush();
        $request->session()->regenerateToken();

        return redirect('comp');
    }
    public function comp(Request $request)
    {
        return view('comp');
    }

    //当選者：景品発送受付ページ
    public function winnerEntry(Request $request)
    {
        //URLにユーザコードのパラメータ(c)がない場合はtopに飛ばす
        if (empty($request['c'])) {
            return redirect('error');
        }

        //ユーザコードが存在し、メール送信済み(checked=true)の場合は当選者情報を取得
        $entrys = Entry::query()
            ->where('code', '=', $request['c'])
            ->where('checked', '=', 'true')
            ->first();

        $hashtags = Sns::where('code', '=', $request['c'])
                    ->first();

        //当選者情報が取得できない場合はtopに飛ばす
        if (($entrys == null && empty($entrys))
            && ($hashtags == null && empty($hashtags))) {
            return redirect('error');
        }

        //1週間過ぎていたらアウト
        if (($entrys != null || !empty($entrys))) {
            $now = strtotime(date("Y/m/d H:i:s", strtotime('-7 day')));
            if ($now > strtotime($entrys->updated_at)) {
                return redirect('error_term');
            }
        }

        //当選者でwinnersフォームが未入力の場合
        //dd($entrys->id);
        if (!empty($entrys)) {
            $winners = WinnerEntry::query()
                ->where('entry_id', '=', $entrys->id)
                ->first();
        } else {
            $winners = WinnerEntry::query()
                ->where('hashtag_id', '=', $hashtags->id)
                ->first();
        }

        //当選者でwinners入力済みの場合はtopに飛ばす
        if ($winners != null && !empty($winners)) {
            return redirect('error');
        }

        if (empty($entrys)) {
            if (!strpos($hashtags->text, "#ツープラスワンヘアカラークリームタイプ") === false) {
                $hashtags->prize = 1;
            }
            if (!strpos($hashtags->text, "#ヘアカラートリートメント") === false) {
                $hashtags->prize = 2;
            }
            if (!strpos($hashtags->text, "#リタッチコンシーラー") === false) {
                $hashtags->prize = 3;
            }
        }

        return view('winner_entry')
            ->with('e', $entrys)
            ->with('h', $hashtags);
    }
    public function winnerConf(Request $request)
    {
        //いきなりアクセスした場合、入力がない場合はentryに飛ばす
        if (empty($request)) {
            return redirect('input_entry');
        }
        if (!empty($request['c'])) {
            $winnerentry =  new WinnerEntry();
            $winnerentry->fill($request->except(['_token']));

            //dd($request);
            //ユーザコードeidをセット
            if (!empty($request['eid'])) {
                $winnerentry->entry_id = $request['eid'];
            } else {
                $winnerentry->hashtag_id = $request['hid'];
            }

            $this->validate($request, [
              'name'  => 'required',
              'kana' => 'required',
              'postal1' => 'required||numeric',
              'postal2' => 'required||numeric',
              'prefecture_id' => 'required||integer',
              'address1' => 'required',
              'tel1' => 'required||numeric',
              'tel2' => 'required||numeric',
              'tel3' => 'required||numeric',
          ], [], [
              'name'  => '名前',
              'kana' => 'フリガナ',
              'postal1' => '郵便番号',
              'postal2' => '郵便番号',
              'prefecture_id' => '都道府県',
              'address1' => '住所',
              'tel1' => '電話番号',
              'tel2' => '電話番号',
              'tel3' => '電話番号',
          ]);
        } else {
            return redirect('error');
        }

        session(['winner_entry' => $winnerentry ]);

        return view('winner_conf')
            ->with('w', $winnerentry);
    }
    public function winnerStore(Request $request)
    {
        $winnerentry = new WinnerEntry();
        $winnerentry = session('winner_entry');
        //dd($winnerentry);
        if (!empty($winnerentry)) {
            if (!empty($winnerentry['postal1']) && !empty($winnerentry['postal2'])) {
                $winnerentry['postal'] = $winnerentry['postal1'] . '-' . $winnerentry['postal2'];
                unset($winnerentry['postal1']);
                unset($winnerentry['postal2']);
            }
            if (!empty($winnerentry['tel1']) && !empty($winnerentry['tel2']) && !empty($winnerentry['tel3'])) {
                $winnerentry['tel'] = $winnerentry['tel1'] . '-' . $winnerentry['tel2'] . '-' . $winnerentry['tel3'];
                unset($winnerentry['tel1']);
                unset($winnerentry['tel2']);
                unset($winnerentry['tel3']);
            }
            $winnerentry->save();
        } else {
            return redirect('error');
        }
        $request->session()->regenerateToken();

        return redirect('input_comp');
    }
    public function winnerComp(Request $request)
    {
        // セッションがない場合はtopに飛ばす
        // if (empty($request->session()->has('winner_entry'))) {
        //     return redirect('error');
        // }

        //セッションを削除
        $request->session()->flush();
        return view('winner_comp');
    }
}
