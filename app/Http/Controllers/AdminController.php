<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Sns;
use App\Models\Entry;
use App\Models\WinnerEntry;
//use App\Http\Controllers\SnsController;
use Thujohn\Twitter\Facades\Twitter;
use App\Http\Requests;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Log;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = $this->count();
        //dd($info);
        return view('auth.home')
            ->with('info', $info);
    }
    public function entryList()
    {
        //重複
        $all = Entry::all();
        $mailArray = array(
          "" => "-選択して下さい-",
          "a" => "A ２＋１ 10月応募者分",
          "b" => "B ２＋１ 11月応募者分",
          "c" => "C ヘアカラートリートメント 11月応募者分",
          "d" => "D リタッチコンシーラー 10月応募・落選者から繰り上げ当選分",
          "e" => "E リタッチコンシーラー 11月応募者分",
          "f" => "F ２＋１ リマインダー案内",
          "g" => "G ヘアカラートリートメント・リタッチコンシーラー リマインダー案内",
          "h" => "H ヘアカラ―トリートメント 10月応募・落選者から繰り上げ当選分",
          "i" => "I ヘアカラ―トリートメント 12月当選分",
          "j" => "J リタッチコンシーラー 12月当選分",
          "k" => "K ２＋１ 12月当選分",
          "l" => "L ハワイアンダンス アンケートメール",
          "m" => "M 社交ダンス アンケートメール",
          "n" => "N ハワイアンダンス リマインドメール",
          "o" => "O 社交ダンス リマインドメール",
        );
        $prize = 1;
        $prize = Input::get('prize');
        if (!$prize) {
            $prize=1;
        }
        $entrys = $all->where('prize', $prize);
        //var_export($entrys);
        // foreach ($all as $data) {
        //     $count = 0;
        //     $data->duplicated = false;
        //     foreach ($all as $d) {
        //         if ($d->email === $data->email) {
        //             $count++;
        //         } else {
        //         }
        //     }
        //     if ($count > 1) {
        //         $data->duplicated = true;
        //     }
        //     $entry[] = $data;
        // }
        //dd($entry);
        return view('auth.entryList')
            ->with('mailArray', $mailArray)
            ->with('entrys', $entrys);
    }
    public function entryListTw()
    {
        //重複
        //$sns = Sns::where('id',)->get();
        $prize = 1;
        $prize = Input::get('prize');

        $mailArray = array(
          "" => "-選択して下さい-",
          "a" => "A ２＋１ 10月応募者分",
          "b" => "B ２＋１ 11月応募者分",
          "c" => "C ヘアカラートリートメント 11月応募者分",
          "d" => "D リタッチコンシーラー 10月応募・落選者から繰り上げ当選分",
          "e" => "E リタッチコンシーラー 11月応募者分",
          "f" => "F ２＋１ リマインダー案内",
          "g" => "G ヘアカラートリートメント・リタッチコンシーラー リマインダー案内",
          "h" => "H ヘアカラ―トリートメント 10月応募・落選者から繰り上げ当選分",
          "i" => "I ヘアカラ―トリートメント 12月当選分",
          "j" => "J リタッチコンシーラー 12月当選分",
          "k" => "K ２＋１ 12月当選分",
          "l" => "L ハワイアンダンス アンケートメール",
          "m" => "M 社交ダンス アンケートメール",
          "n" => "N ハワイアンダンス リマインドメール",
          "o" => "O 社交ダンス リマインドメール",
        );
        switch ($prize) {
          case 1:
            $like = '#ツープラスワンヘアカラークリームタイプ';
            break;
          case 2:
            $like = '#ヘアカラートリートメント';
            break;
          case 3:
            $like = '#リタッチコンシーラー';
            break;
          default:
            $like = '#ツープラスワンヘアカラークリームタイプ';
        }
        //$entrys = $sns->where('text', 'like', '%カラー\n%');
        //var_export($entrys);
        //$entrys = Sns::where('text', 'like', '%'.$like.'%')->get();
        $entrys = Sns::get();
        //var_export($sns);
        // foreach ($sns as $key => $value) {
        //   var_dump($value->hashtag);
        // }
        // dd($sns);
        $info = $this->count();

        foreach ($entrys as $key => $value) {
            if (!strpos($value->text, "#ツープラスワンヘアカラークリームタイプ") === false) {
                $entrys[$key]->hashtag = 1;
            }
            if (!strpos($value->text, "#ヘアカラートリートメント") === false) {
                $entrys[$key]->hashtag = 2;
            }
            if (!strpos($value->text, "#リタッチコンシーラー") === false) {
                $entrys[$key]->hashtag = 3;
            }
        }
        //dd($info);
        return view('auth.entryListTw')
          ->with('entrys', $entrys)
            ->with('mailArray', $mailArray)
          ->with('info', $info);
    }

    public function updateEntry(Request $request)
{
        foreach ($request->checked as $checked) {
            $entry = Entry::find($checked);
            $entry->checked = true;
            $entry->save();
        }
        return view('auth.updateEntry')
            ->with('num', $count);
    }

    public function copyDm(Request $request)
    {
        $mailId = Input::get('mail');
        $id = Input::get('id');
        $entry = Sns::find($id);
        return view('mail.'.$mailId)
          ->with('e', $entry);
    }

    public function checkDm(Request $request)
    {
        //dd($request->id);
        $entry = Sns::find($request->id);
        //dd($entry);
        $entry->checked = true;
        try {
            //dd($entry);
            $response = $entry->save();
            //  dd($response);
        } catch (Exception $e) {
            dd(Twitter::logs());
        }
        return response()->json(
          [
              'data' => $response
          ],
          200,
            [],
          JSON_UNESCAPED_UNICODE
      );
    }
    public function download()
    {
        return view('auth.download');
    }
    public function excel()
    {
        $entrys = Entry::where('checked', 'true')
                    ->orderBy('id')
                    ->get();

        Excel::create('entrys_'.date('Ymd'), function ($excel) use ($entrys) {
            $excel->sheet('Sheet 1', function ($sheet) use ($entrys) {
                $sheet->setAutoSize(true);
                $sheet->appendRow(array(
                    'ID','希望する賞品','氏名','カナ','メールアドレス','郵便番号','住所','アンケート','アンケート（その他）','応募日時'
                ));
                foreach ($entrys as $entry) {
                    $sheet->appendRow(array(
                        $entry->id,
                        array_search($entry->prize, Config::get('cleadew.items')),
                        $entry->last_nm.' '.$entry->first_nm,
                        $entry->last_kana.' '.$entry->first_kana,
                        $entry->email,
                        $entry->postal,
                        array_search($entry->prefecture_id, Config::get('cleadew.prefectures')).$entry->address1.$entry->address2,
                        array_search($entry->answer, Config::get('cleadew.question')),
                        $entry->answer_input,
                        $entry->created_at,
                    ));
                }
            });
        })->export('xls');
    }
    public function excelTwitter()
    {
        $sns = Sns::all();
        $sns1 = array();
        $sns2 = array();
        $sns3 = array();
        foreach ($sns as $key => $value) {
            if (!strpos($value->text, "#ツープラスワンヘアカラークリームタイプ") === false) {
                $sns[$key]->hashtag = 1;
                $sns1[] = $sns[$key];
            }
            if (!strpos($value->text, "#ヘアカラートリートメント") === false) {
                $sns[$key]->hashtag = 2;
                $sns2[] = $sns[$key];
            }
            if (!strpos($value->text, "#リタッチコンシーラー") === false) {
                $sns[$key]->hashtag = 3;
                $sns3[] = $sns[$key];
            }
        }

        Excel::create('twitters_'.date('Ymd'), function ($excel) use ($sns1,$sns2,$sns3) {
            $excel->sheet('ツープラスワンヘアカラークリームタイプ', function ($sheet) use ($sns1) {
                $sheet->setAutoSize(true);
                $sheet->appendRow(array(
                    'ID','Twitter','重複投稿','応募日時'
                ));
                foreach ($sns1 as $s) {
                    $duplicated = '';
                    if ($s->duplicated) {
                        $duplicated = '◯';
                    }
                    $sheet->appendRow(array(
                        $s->id,
                        "@".$s->screen_name,
                        $duplicated,
                        date("Y-m-d H:i:s", $s->posted_at),
                    ));
                }
            });
            $excel->sheet('ヘアカラートリートメント', function ($sheet) use ($sns2) {
                $sheet->setAutoSize(true);
                $sheet->appendRow(array(
                    'ID','Twitter','重複投稿','応募日時'
                ));
                foreach ($sns2 as $s) {
                    $duplicated = '';
                    if ($s->duplicated) {
                        $duplicated = '◯';
                    }
                    $sheet->appendRow(array(
                        $s->id,
                        "@".$s->screen_name,
                        $duplicated,
                        date("Y-m-d H:i:s", $s->posted_at),
                    ));
                }
            });
            $excel->sheet('リタッチコンシーラー', function ($sheet) use ($sns3) {
                $sheet->setAutoSize(true);
                $sheet->appendRow(array(
                    'ID','Twitter','重複投稿','応募日時'
                ));
                foreach ($sns3 as $s) {
                    $duplicated = '';
                    if ($s->duplicated) {
                        $duplicated = '◯';
                    }
                    $sheet->appendRow(array(
                        $s->id,
                        "@".$s->screen_name,
                        $duplicated,
                        date("Y-m-d H:i:s", $s->posted_at),
                    ));
                }
            });
        })->export('xls');
    }
    public function excelWinners()
    {
        $winner = WinnerEntry::all();

        Excel::create('winners_'.date('Ymd'), function ($excel) use ($winner) {
            $excel->sheet('test', function ($sheet) use ($winner) {
                $sheet->setAutoSize(true);
                $sheet->appendRow(array(
                    'ID','応募種別','メールアドレスorTwitterアカウント','名前','フリガナ','郵便番号','住所','電話番号','賞品種類','当選月','入力日時'));
                foreach ($winner as $w) {
                    $type = '';
                    if ($w->entry_id != '') {
                        $type = '通常応募';
                        $e = Entry::find($w->entry_id);
                        $month = $e->month . "月";
                        $account = $e->email;
                    } elseif ($w->hashtag_id != '') {
                        $type = 'Twitter';
                        $s = Sns::find($w->hashtag_id);
                        $datetime = date_create($s->created_at);
                        // $datetime->modify('-1 month');
                        $month = $datetime->format('m')."月";
                        $account = '@'.$s->screen_name;
                    } else {
                        $type = '不明';
                    }
                    $prize = '';
                    switch ($w->prize) {
                    case 1:
                      $prize = 'ツープラスワンヘアカラークリームタイプ';
                      break;
                    case 2:
                      $prize = 'ヘアカラートリートメント';
                      break;
                    case 3:
                      $prize = 'リタッチコンシーラー';
                      break;
                  }
                    $sheet->appendRow(array(
                      $w->id,
                      $type,
                      $account,
                      $w->name,
                      $w->kana,
                      $w->postal,
                      array_search($w->prefecture_id, Config::get('wella.prefectures')).$w->address1,
                      $w->tel,
                      $prize,
                      $month,
                      $w->created_at
                  ));
                }
            });
        })->export('xls');
    }
    public function excelNotInput()
    {
        $sns = Sns::where('checked', '=', true)
                    ->whereNotIn('id', function ($q) {
                        $q->select('hashtag_id')->from('winners')->whereNotNull('hashtag_id');
                    })->orderBy('id')->get();

        $entry = Entry::where('checked', '=', 'true')
                        ->whereNotIn('id', function ($q) {
                            $q->select('entry_id')->from('winners')->whereNotNull('entry_id');
                        })->orderBy('id')->get();

        Excel::create('notinput_'.date('Ymd'), function ($excel) use ($sns,$entry) {
            $excel->sheet('Twitter未入力者', function ($sheet) use ($sns) {
                $sheet->setAutoSize(true);
                $sheet->appendRow(array(
                    'ID','Twitter','重複投稿','応募日時','URL'
                ));
                foreach ($sns as $s) {
                    $duplicated = '';
                    if ($s->duplicated) {
                        $duplicated = '◯';
                    }
                    $sheet->appendRow(array(
                        $s->id,
                        "@".$s->screen_name,
                        $duplicated,
                        date("Y-m-d H:i:s", $s->posted_at),
                        "https://cp-jm.com/input_entry?c=".$s->code
                    ));
                }
            });
            $excel->sheet('応募フォーム未入力者', function ($sheet) use ($entry) {
                $sheet->setAutoSize(true);
                $sheet->appendRow(array(
                    'ID','希望する賞品','氏名','カナ','メールアドレス','URL'
                ));
                foreach ($entry as $e) {
                    $sheet->appendRow(array(
                        $e->id,
                        array_search($e->prize, Config::get('wella.prizes')),
                        $e->name,
                        $e->kana,
                        $e->email,
                        "https://cp-jm.com/input_entry?c=".$e->code
                    ));
                }
            });
        })->export('xls');
    }
    private function count()
    {
        //重複
        $sns = Sns::all();
        $entry = array();
        //dd($entry);
        $countSns = Sns::where('followed', true)->count();
        $countTag1 = Sns::where('text', 'LIKE', "%ツープラスワンヘアカラークリームタイプ%")->where('followed', true)->count();
        $countTag2 = Sns::where('text', 'LIKE', "%ヘアカラートリートメント%")->where('followed', true)->count();
        $countTag3 = Sns::where('text', 'LIKE', "%リタッチコンシーラー%")->where('followed', true)->count();
        $countTag12 = Sns::where('text', 'LIKE', "%#ツープラスワンヘアカラークリームタイプ%")->where('text', 'LIKE', "%#ヘアカラートリートメント%")->where('followed', true)->count();
        $countTag13 = Sns::where('text', 'LIKE', "%#ツープラスワンヘアカラークリームタイプ%")->where('text', 'LIKE', "%#ヘアカラートリートメント%")->where('followed', true)->count();
        $countTag23 = Sns::where('text', 'LIKE', "%#ヘアカラートリートメント%")->where('text', 'LIKE', "%#リタッチコンシーラー%")->where('followed', true)->count();
        $countTag123 = Sns::where('text', 'LIKE', "%#ツープラスワンヘアカラークリームタイプ%")->where('text', 'LIKE', "%#ヘアカラートリートメント%")->where('text', 'LIKE', "%#リタッチコンシーラー%")->where('followed', true)->count();
        $info = array(
        'countSns' => $countSns,
        'countTag1' => $countTag1,
        'countTag2' => $countTag2,
        'countTag3' => $countTag3,
        'countTag12' => $countTag12,
        'countTag13' => $countTag13,
        'countTag23' => $countTag23,
        'countTag123' => $countTag123,
      );
        return $info;
    }

    private function postDm($parameters = [])
    {
        if ((!array_key_exists('user_id', $parameters) && !array_key_exists('screen_name', $parameters)) || !array_key_exists('text', $parameters)) {
            throw new BadMethodCallException('Parameter required missing : user_id, screen_name or text');
        }

        //return $this->post('direct_messages/new', $parameters);

        /* 参考 [PHP] ライブラリに頼らないTwitterAPI入門
         * https://qiita.com/mpyw/items/b59d3ce03f08be126000
         */
        $url = 'https://api.twitter.com/1.1/direct_messages/events/new.json';
        $userId = $parameters['user_id'];
        $consumerKey = function_exists('env') ? env('TWITTER_CONSUMER_KEY', '') : '';
        $consumerSecret = function_exists('env') ? env('TWITTER_CONSUMER_SECRET', '') : '';
        $accessToken = function_exists('env') ? env('TWITTER_ACCESS_TOKEN', '') : '';
        $accessTokenSecret  = function_exists('env') ? env('TWITTER_ACCESS_TOKEN_SECRET', '') : '';

        $json = [
            'event'=>[
                'type'=>'message_create',
                'message_create'=>[
                    'target'=>[
                        'recipient_id'=>$userId
                    ],
                    'message_data'=>[
                        'text'=> $parameters['text'],
                        /*
                        'attachment'=>[
                            'type'=>'media',
                            'id'=>[
                                '...'
                            ]
                        ]
                        */
                    ]

                ]
            ]
        ];

        $oauth_params = [
            'oauth_consumer_key'     => $consumerKey,
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp'        => time(),
            'oauth_version'          => '1.0a',
            'oauth_nonce'            => bin2hex(openssl_random_pseudo_bytes(16)),
            'oauth_token'            => $accessToken,
        ];
        //dd($oauth_params);
        $base = $oauth_params;

        // キー
        $key = [$consumerSecret, $accessTokenSecret];
        uksort($base, 'strnatcmp');

        $oauth_params['oauth_signature'] = base64_encode(hash_hmac(
            'sha1',
            implode('&', array_map('rawurlencode', array(
                'POST',
                $url,
                str_replace(
                    array('+', '%7E'),
                    array('%20', '~'),
                    http_build_query($base, '', '&')
                )
            ))),
            implode('&', array_map('rawurlencode', $key)),
            true
        ));
        foreach ($oauth_params as $name => $value) {
            $items[] = sprintf('%s="%s"', urlencode($name), urlencode($value));
        }
        $signature = 'Authorization: OAuth ' . implode(', ', $items);


        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL            => $url,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => json_encode($json),
            CURLOPT_HTTPHEADER     => [
                'Content-Type: application/json',
                $signature
            ],
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => 'gzip',
            CURLINFO_HEADER_OUT       => true,
        ]);
        $response =  curl_exec($ch);//実行
        //var_dump($response);
        //var_dump(curl_getinfo($ch));
        curl_close($ch);
        return $response;
    }
}
