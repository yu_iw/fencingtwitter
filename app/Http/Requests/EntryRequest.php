<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prize'=> 'required | numeric',
            'last_nm'=> 'required',
            'first_nm'=> 'required',
            'last_kana'=> 'required | katakana',
            'first_kana'=> 'required | katakana',
            'email'=> 'required | email',
            'postal1'=> 'required',
            'postal2'=> 'required',
            'prefecture_id'=> 'required | numeric',
            'address1'=> 'required',
            'address2'=> '',
            'image1'=> 'required',
            'image2'=> '',
            'image3'=> '',
            'image4'=> '',
            'image5'=> '',
            'answer'=> 'required',
            'answer_input'=> 'required_if:answer,4',
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }

    public function attributes()
    {
        return [
            'prize'=> '応募商品',
            'last_nm'=> '姓',
            'first_nm'=> '名',
            'last_kana'=> 'セイ',
            'first_kana'=> 'メイ',
            'email'=> 'メールアドレス',
            'postal'=> '郵便番号',
            'prefecture_id'=> '都道府県',
            'address1'=> '市区町村・番地',
            'address2'=> '建物名・部屋番号',
            'image1'=> 'レシート画像',
            'answer'=> 'アンケート',
            'answer_input'=> 'アンケート（その他）',
        ];
    }
}
