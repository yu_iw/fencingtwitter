<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entry extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'prize','last_nm','first_nm','last_kana','first_kana','email','postal1','postal2','prefecture_id','address1','address2','image1','image2','image3','image4','image5','answer','answer_input'
    ];

    protected $hidden = [

    ];

    protected $dates = ['deleted_at'];
    protected $table = 'entry';
}
