<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entry extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'prize','last_nm','first_nm','last_kana','first_kana','email','postal1','postal2','prefecture_id','address1','address2','image1','image2','image3','image4','image5','answer','answer_input','month'
    ];

    protected $hidden = [

    ];

    protected $casts = [
        'created_at' => 'string',
        'updated_at' => 'string'
    ];
    protected $dates = ['deleted_at'];
    protected $table = 'entrys';
    protected $dateFormat = 'Y-m-d H:i:s.u';
}
