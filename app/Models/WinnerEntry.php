<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WinnerEntry extends Model
{
    protected $fillable = [
        'name','kana','postal1','postal2','prefecture_id','address1','tel1','tel2','tel3', 'prize'
    ];

    protected $hidden = [
    ];

    protected $dates = ['deleted_at'];
    protected $table = 'winners';
}
