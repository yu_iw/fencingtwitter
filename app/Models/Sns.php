<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sns extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'hashtags';

    protected $fillable = [
        'post_id', 'user_id', 'screen_name', 'name', 'text', 'posted_at','followed','follow_judged','code','win_flg','dm_sent','updatet_at'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
