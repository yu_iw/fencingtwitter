<?php

return [
    'enable'   => env('IP_LIMIT_ENABLE', false),
    'isProxy'  => env('IP_LIMIT_PROXY',  false),
    'allowIps' => [
        env('LOCALHOST_IP', '127.0.0.1'),
        '183.77.249.55',
        //pvenue
        '14.3.54.165',
        '106.161.247.194',
        '106.161.250.11',
        '14.3.148.194',
        '39.110.201.194'
    ],
];
